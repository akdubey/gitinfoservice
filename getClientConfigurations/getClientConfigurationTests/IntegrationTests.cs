﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Collections.Generic;
using System.Net;
using getClientConfigurations.Models;

namespace getClientConfigurationTests
{
    [TestClass]
    public class IntegrationTests
    {
        private String targetURL = "http://localhost:233/API/getClientConfiguration/Fastaff_test";

        [TestMethod]
        public void ensurePackageDataIsReturnedProperly()
        {
            String testResourcePath = "Resources/FastaffResponse.json";

            // verifies resource file can be obtained
            Assert.IsTrue(File.Exists(testResourcePath));
            String payloadFinal = File.ReadAllText(testResourcePath);
            //Console.WriteLine("Original payload = [{0}]", payloadFinal);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(targetURL);

            //set settings
            request.Method = "GET";
            
            // send it out
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            String responseText = "";
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                responseText = reader.ReadToEnd();
            }
            Assert.AreEqual(payloadFinal, responseText);
        }
    }
}
