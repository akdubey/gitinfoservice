﻿using log4net.Config;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace MultiPhraseCreateOrder.Tests
{
	[TestClass()]
	class UnitTestsConfig
	{
		[AssemblyInitialize()]
		public static void AssemblyInit(TestContext context)
		{
			// This configure a log4net to log to Console.
			// Hence all unit test will log4net messages to its "output"
			BasicConfigurator.Configure();
		}
	}
}
