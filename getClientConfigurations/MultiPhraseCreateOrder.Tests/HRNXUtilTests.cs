﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using getClientConfigurations.Models;
using System.IO;
using System.Net;
using System.Collections.Generic;
using System.Resources;
using getClientConfigurations.ATSHandlers;
using System.Data.Odbc;
using System.Text;

namespace MultiPhraseCreateOrder.Tests
{
    [TestClass]
    public class HRNXUtilTests
    {
        private ResourceManager rmUnitTest = new ResourceManager("MultiPhraseCreateOrder.Tests.Properties.Resources", typeof(HRNXUtilTests).Assembly);
        private ResourceManager rm = new ResourceManager("getClientConfigurations.Properties.HRNX", typeof(HRNXDBAccessor).Assembly);

        [TestMethod]
        public void EnsureTxnIDCanbeGenerated()
        {
            HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));

            Order request = new Order();

            String txn_id = dbAccessor.generateTxnID(request, "EnsureTxnIDCanbeGenerated unitTest");
            Console.WriteLine(string.Format("[{0}]", txn_id));

            Assert.IsFalse(String.IsNullOrEmpty(txn_id));
        }

        [TestMethod]
        public void EnsurePushToErrorStackActuallyGeneratesANewRecordInErrorStack()
        {
            HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));


            Order fakeOrder = new Order();
            fakeOrder.client_group_id = "client_test";
            fakeOrder.additionalData.Add(OrderAdditionalDataKeys.serviceProviderID,"sver");
            fakeOrder.txn_id = "asbasd";
            fakeOrder.serviceProviderWorkflowStarts();

            // Pull last error from errorStack currently
            String lastErrorID = dbAccessor.getLatestErrorID(fakeOrder, "EnsurePushToErrorStackActuallyGeneratesANewRecordInErrorStack unit test");
            // Console.WriteLine("Last errorID is [{0}]",lastErrorID);

            HRNXv1Error expectedError = new HRNXv1Error();
            expectedError.error_severity = 7;
            expectedError.error_loc = "someSCript";
            expectedError.create_alert = true;
            expectedError.error_short = "inert";
            expectedError.error_long = null;
            expectedError.error_blob = null;
            expectedError.error_blob_2 = "";

            Boolean writeSuccess = dbAccessor.pushToErrorStack(expectedError.error_severity, expectedError.error_loc, expectedError.create_alert, fakeOrder, expectedError.error_short, expectedError.error_long, expectedError.error_blob, expectedError.error_blob_2);

            // Check #1, make sure the DB write is right
            Assert.IsTrue(writeSuccess);

            List<HRNXv1Error> returnedErrors = dbAccessor.getErrorFromErrorStackSince(lastErrorID, fakeOrder, "EnsurePushToErrorStackActuallyGeneratesANewRecordInErrorStack unit test");

            // Check #2, make sure only 1 new error is generated
            Assert.AreEqual(1,returnedErrors.Count);

            // Check #3, make sure Each data entry matches with what we expect
            Assert.AreEqual(expectedError.error_severity, returnedErrors[0].error_severity);
            Assert.AreEqual(expectedError.error_loc, returnedErrors[0].error_loc);
            Assert.AreEqual(expectedError.create_alert, returnedErrors[0].create_alert);
            Assert.AreEqual(fakeOrder.client_group_id, returnedErrors[0].client_group_id);
            Assert.AreEqual(fakeOrder.getCurrentServiceProviderID(), returnedErrors[0].service_tp_id);
            Assert.AreEqual(fakeOrder.txn_id, returnedErrors[0].txn_id);
            Assert.AreEqual(expectedError.error_short, returnedErrors[0].error_short);
            Assert.AreEqual(expectedError.error_long, returnedErrors[0].error_long);
            Assert.AreEqual(expectedError.error_blob, returnedErrors[0].error_blob);
            Assert.AreEqual(null, returnedErrors[0].error_blob_2);           
        }

        [TestMethod]
        public void EnsureATSCredentialsCanBeReturned()
        {
            HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));

            Order request = new Order();
            request.client_group_id = "2569";

            String value = dbAccessor.getATSCredentials(request, "EnsureATSCredentialsCanBeReturned unit test", "icims6","username");
            Assert.AreEqual("CredentialCheckAPIUser", value);
        }

        [TestMethod]
        public void EnsureNonExistingATSCredentialsWillReturnBlank()
        {
            HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
            Order request = new Order();
            String value = dbAccessor.getATSCredentials(request, "EnsureNonExistingATSCredentialsWillReturnBlank unit test", "icims6", "username");
            Assert.IsTrue(String.IsNullOrEmpty(value));
        }

        [TestMethod]
        public void EnsureLoadWidgetXSLTCanBeReturned()
        {
            String testResourcePath = "Resources/ChemicalBankLoadWIdgetXSLT.xslt";

            // verifies resource file can be obtained
            Assert.IsTrue(File.Exists(testResourcePath));
            String payloadFinal = File.ReadAllText(testResourcePath);

            HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
            Order request = new Order();
            request.client_group_id = "2569";
            String origin = "EnsureLoadWidgetXSLTCanBeReturned unit test";

            String loadWidgetXSLT = dbAccessor.getLoadWidgetXSLT(request,origin, "icims6");
            Assert.AreEqual(TestUtil.normalizeSpace(payloadFinal), TestUtil.normalizeSpace(loadWidgetXSLT));
        }

        [TestMethod]
        public void EnsureWriteToQProcessingCreatesANewRecordAndNoError()
        {
            HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));

            Order requestToLog = new Order();
            String origin = "EnsureWriteToQProcessingCreatesANewRecordAndNoError unit test";

            requestToLog.txn_id = dbAccessor.generateTxnID(requestToLog,origin);
            requestToLog.status = OrderStatus.submitted_passthru;
            requestToLog.client_group_id = "CLientNo";
            requestToLog.client_user_id = "CLientUser";
            requestToLog.candidate_id = "1234User";
            requestToLog.createOrderHandlerName = "Some handler createOrder";
            requestToLog.orderingIP = "1.1.1.1";
            requestToLog.payload_hrnx = "<xml></xml?";

            String oldTxnID = dbAccessor.getLatestOrderTxnID(requestToLog, origin);

            dbAccessor.writeToQProcessing(requestToLog, "unit test");

            Order newEntry = dbAccessor.getLatestOrder(requestToLog, origin);

            // Validate newEntry is not old last entry
            Assert.AreNotEqual(oldTxnID, newEntry.txn_id);

            // Validate newEntry contains the proper information
            Assert.AreEqual(requestToLog.txn_id, newEntry.txn_id);
            Assert.AreEqual(requestToLog.status, newEntry.status);
            Assert.AreEqual(requestToLog.client_group_id, newEntry.client_group_id);
            Assert.AreEqual(requestToLog.client_user_id, newEntry.client_user_id);
            Assert.AreEqual(requestToLog.candidate_id, newEntry.candidate_id);
            Assert.AreEqual(requestToLog.createOrderHandlerName, newEntry.createOrderHandlerName);
            Assert.AreEqual(requestToLog.orderingIP, newEntry.orderingIP);
            Assert.AreEqual(requestToLog.payload_hrnx, newEntry.payload_hrnx);
        }

        [TestMethod]
        public void EnsureNonExistingClientGroupIDWillReturnAnEmptyListOfServiceProviders()
        {
            HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));

            Order request = new Order();
            request.client_group_id = "SomeNonExistingClient";
            String origin = "EnsureNonExistingClientGroupIDWillBeCatchOnErrorStack unit test";

            // save last error
            String lastErrorID = dbAccessor.getLatestErrorID(request,origin);

            String[] result = dbAccessor.isPassThruClient(request,origin);

            // Check #1, an empty list should be returned since there was an error
            Assert.AreEqual(0,result.Length);
        }

        [TestMethod]
        public void EnsureGetFieldEndpointCanBeReturned()
        {
            HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));

            String origin = "EnsureGetFieldEndpointCanBeReturned unit test";
            Order request = new Order();
            request.client_group_id = "2569";

            // save last error
            String lastErrorID = dbAccessor.getLatestErrorID(request,origin);

            List<Tuple<String, String>> addressReplacement = new List<Tuple<string, string>>();
            String expectedDoamin = "someTestValue";
            addressReplacement.Add(new Tuple<string, string>("^^PERSON_URL^^", expectedDoamin));

            getFieldEndpoint endpointData = dbAccessor.getTemplateEndpoint("icims6", "icims6_ep3", request, origin, addressReplacement);

            Assert.IsFalse(String.IsNullOrEmpty(endpointData.url));
            Assert.IsFalse(String.IsNullOrEmpty(endpointData.xslt));

            Console.WriteLine(string.Format("URL returned is [{0}]", endpointData.url));
            Console.WriteLine(string.Format("xslt returned is [{0}]", endpointData.xslt));
        }
        [TestMethod]
        public void EnsureGetFieldEndpointWillNotErrorIfNoReplacementAndInvalidClientGroup()
        {
            HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));

            String origin = "EnsureGetFieldEndpointCanBeReturned unit test";
            Order request = new Order();
            request.client_group_id = "NonExistanceClient";

            // save last error
            String lastErrorID = dbAccessor.getLatestErrorID(request,origin);

            getFieldEndpoint endpointData = dbAccessor.getTemplateEndpoint("nonService", "_ep3", request, origin, null);

            List<HRNXv1Error> newErrors = dbAccessor.getErrorFromErrorStackSince(lastErrorID, request, origin);

            Assert.AreEqual(0, newErrors.Count);

            Assert.IsTrue(String.IsNullOrEmpty(endpointData.url));
            Assert.IsTrue(String.IsNullOrEmpty(endpointData.xslt));
            Console.WriteLine(string.Format("URL returned is [{0}]", endpointData.url));
            Console.WriteLine(string.Format("xslt returned is [{0}]", endpointData.xslt));
        }

        [TestMethod]
        public void EnsureGetPhraseWorks()
        {
            HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));

            String origin = "EnsureGetPhraseWorks unit test";
            Order request = new Order();
            request.client_group_id = "2569";
            String service_tp_id = "clearstar2~301";
            request.additionalData.Add(OrderAdditionalDataKeys.serviceProviderID,service_tp_id);

            // save last error
            String lastErrorID = dbAccessor.getLatestErrorID(request,origin);

            HRNXPhase result = dbAccessor.getPhraseData(request, origin, "77066", service_tp_id);
            Assert.AreEqual(icimsPhrase.CREDIT_CHECK, (icimsPhrase)Enum.Parse(typeof(icimsPhrase),result.enumValue));
            Assert.AreEqual("CRCK_00193", result.data);
            List<HRNXv1Error> newErrors = dbAccessor.getErrorFromErrorStackSince(lastErrorID, request, origin);
            Assert.AreEqual(0, newErrors.Count);

            HRNXPhase result2 = dbAccessor.getPhraseData(request, origin, "somethingEls", service_tp_id);
            Assert.IsNull(result2);
            List<HRNXv1Error> newErrors2 = dbAccessor.getErrorFromErrorStackSince(lastErrorID, request, origin);
            Assert.AreEqual(0, newErrors2.Count);
        }
        [TestMethod]
        public void EnsureXSLTTransformWorks()
        {
            HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
            String origin = System.Reflection.MethodBase.GetCurrentMethod().Name;

            // Load the files
            String input = TestUtil.getResource("xsltTestInput.xml");

            String output = TestUtil.getResource("xsltTestOutput.xml");
            String outputNomralize = TestUtil.normalizeSpace(output);

            String xslt = TestUtil.getResource("xsltTestXslt.xslt");

            String returnOutput = HRNXUtil.applyXSLT(input, xslt, dbAccessor,null,origin);
            //Console.WriteLine(returnOutput);
            String returnOutputNomralize = TestUtil.normalizeSpace(returnOutput);

            Assert.AreEqual(outputNomralize, returnOutputNomralize, true);
        }
        [TestMethod]
        public void EnsureXSLTExceptionsAreThrownToErrorStack()
        {
            HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
            String origin = System.Reflection.MethodBase.GetCurrentMethod().Name;

            // save current txn_id
            String lastErrorID = dbAccessor.getLatestErrorID(null, origin);

            // Load the files
            String input = TestUtil.getResource("xsltTestInput.xml");
            String output = TestUtil.getResource("xsltTestOutput.xml");
            String xslt = TestUtil.getResource("xsltTestInvalidXslt.xslt");

            String returnOutput = HRNXUtil.applyXSLT(input, xslt, dbAccessor, null, origin);

            // Check #1, return Empty String
            Assert.IsTrue(String.IsNullOrEmpty(returnOutput));

            // CHeck #2, an entry is added to errorStack
            List<HRNXv1Error> returnedErrors = dbAccessor.getErrorFromErrorStackSince(lastErrorID, null, origin);
            Assert.AreEqual(1, returnedErrors.Count);

            // Check #3, check data in ErrorStack
            Assert.AreEqual(rm.GetString("XSLT_TRANSFORM_ERROR"),returnedErrors[0].error_short);
            Assert.AreEqual(input, returnedErrors[0].error_blob);
            Assert.AreEqual(xslt, returnedErrors[0].error_blob_2);
        }

        [TestMethod]
        public void EnsureIfXSLTIsInvalidXMLErrorsAreThrownToErrorStack()
        {
            HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
            String origin = System.Reflection.MethodBase.GetCurrentMethod().Name;

            // save current txn_id
            String lastErrorID = dbAccessor.getLatestErrorID(null, origin);

            // Load the files
            String input = TestUtil.getResource("xsltTestInput.xml");
            String output = TestUtil.getResource("xsltTestOutput.xml");
            String xslt = TestUtil.getResource("xsltTestInvalidXML.xslt");

            String returnOutput = HRNXUtil.applyXSLT(input, xslt, dbAccessor, null, origin);

            // Check #1, return Empty String
            Assert.IsTrue(String.IsNullOrEmpty(returnOutput));

            // CHeck #2, an entry is added to errorStack
            List<HRNXv1Error> returnedErrors = dbAccessor.getErrorFromErrorStackSince(lastErrorID, null, origin);
            Assert.AreEqual(1, returnedErrors.Count);

            // Check #3, check data in ErrorStack
            Assert.AreEqual(rm.GetString("XSLT_TRANSFORM_ERROR"), returnedErrors[0].error_short);
            Assert.AreEqual(input, returnedErrors[0].error_blob);
            Assert.AreEqual(xslt, returnedErrors[0].error_blob_2);
        }
		[TestMethod]
		public void EnsureRemoveCharacterWorks()
		{
			String reportXML = TestUtil.getResource("inputWithUnicode.txt");
			String expectedResult = TestUtil.getResource("outputWithUnicode.txt");
			String result = HRNXUtil.removeNonPrintableCharacterBesideFormatting(reportXML);

			Assert.AreEqual(expectedResult,result);
		}
		/// <summary>
		/// This is really just a helper to test XML serializing, there are no actual validation done
		/// </summary>
		[TestMethod]
		public void EnsureReportsCanBeSearilizeProperly()
		{
			String txn_id = "1234";
			String client_user_id = "SomeUser";
			String client_group_id = "2569";
			String candidateID = "123_ad";
			OrderStatus currentStatus = OrderStatus.processing;
			String atsID = "icims6";
			String service_tp_id = "clearstar2~301";
			String vendorOrderID = "2011052262218263";
			String serviceName = "Some Service";

			String expectedOutput = TestUtil.getResource("VendorXML.xml");

			HRNXResult target = new HRNXResult();
			target.txn_id = txn_id;
			target.client_user_id = client_user_id;
			target.client_group_id = client_group_id;
			target.tp_uqid = candidateID;
			target.status = currentStatus;
			target.tp_id = atsID;

			HRNXResultDetail detailReport = new HRNXResultDetail();
			detailReport.tp = service_tp_id;
			detailReport.VendorOrderId = vendorOrderID;
			detailReport.name = serviceName;

			HRNXResultDetailCollection deatilList = new HRNXResultDetailCollection();
			deatilList.service = detailReport;
			target.services = deatilList;

			String result = HRNXUtil.serializeObjToXML(target);

			Assert.AreEqual(TestUtil.normalizeSpace(expectedOutput), TestUtil.normalizeSpace(result));
		}
		[TestMethod]
		public void EnsureClientFlagsCanBeExtractedProperly()
		{
			String[] clientOptions = { "clearstarEduMax=1", "clearstarEduMax=2" };

			// Check #1: Make sure we only extract the 1st occurance
			String value1 = HRNXUtil.extractClientFlagValue(clientOptions, "clearstarEduMax");
			Assert.AreEqual("1",value1);

			// Check #2: Make sure we will get an empty string if the flag doesn't exist
			String value2 = HRNXUtil.extractClientFlagValue(clientOptions, "SomeFakeValue");
			Assert.IsTrue(String.IsNullOrEmpty(value2));
		}
		[TestMethod]
		public void EnsureODBCCommandGetConvertedToSQLStatementProperly()
		{
			String expectedResult = "INSERT INTO `hrnx`.`hrnx_error_stack` (`error_timedatestamp`, `error_severity`, `error_loc`, `create_alert`, `client_group_id`, `error_blob`, `txn_id`, `error_short`) VALUES ('2015-08-11 17:49:27', '5', '[DEV]esb/ws/talentplus1/request/post.event', '0', 'SomeClient', '<xsl:value-of select=\"/order/subject/field[@name=\\'email\\']\"/>', NULL, 'some\t\r\nvalue')";
			String payload = "<xsl:value-of select=\"/order/subject/field[@name='email']\"/>";

			string query = "INSERT INTO `hrnx`.`hrnx_error_stack` (`error_timedatestamp`, `error_severity`, `error_loc`, `create_alert`, `client_group_id`, `error_blob`, `txn_id`, `error_short`) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
			OdbcCommand command = new OdbcCommand(query);
			command.Parameters.AddWithValue("@param1", new DateTime(2015,8,11,17,49,27));
			command.Parameters.AddWithValue("@param2", 5);
			command.Parameters.AddWithValue("@param3", "[DEV]esb/ws/talentplus1/request/post.event");
			command.Parameters.AddWithValue("@param4", "0");
			command.Parameters.AddWithValue("@param5", "SomeClient");
			command.Parameters.AddWithValue("@param6", payload);
			command.Parameters.AddWithValue("@param7", HRNXUtil.sendNullIfEmpty(""));

			StringBuilder eight = new StringBuilder();
			eight.Append("some");
			eight.Append("\t");
			eight.Append(Environment.NewLine);
			eight.Append("value");
			command.Parameters.AddWithValue("@param8", eight.ToString());

			String result = HRNXUtil.convertODBCToSQL(command);
			Console.WriteLine(result);

			Assert.AreEqual(expectedResult, result);
		}
	}
}
