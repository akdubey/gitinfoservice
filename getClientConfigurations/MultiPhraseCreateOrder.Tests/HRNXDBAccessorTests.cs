﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using getClientConfigurations.Models;
using System.IO;
using System.Net;
using System.Collections.Generic;
using System.Resources;
using System.Data.Odbc;
using getClientConfigurations.ServiceProviderHandlers;

namespace getClientConfigurations.Models
{
    [TestClass]
    public class HRNXDBAccessorTests
    {
        private ResourceManager rmUnitTest = new ResourceManager("MultiPhraseCreateOrder.Tests.Properties.Resources", typeof(HRNXDBAccessorTests).Assembly);
        private ResourceManager rm = new ResourceManager("getClientConfigurations.Properties.HRNX", typeof(HRNXDBAccessor).Assembly);

        [TestMethod]
        public void EnsureReadAndWriteToATSDuplicateTable()
        {
            HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));

            String newDuplicateKey = System.Reflection.MethodBase.GetCurrentMethod().Name + "_" + Guid.NewGuid().ToString();
            String hrnxPayload = "Some unit test fake value";
            String location = System.Reflection.MethodBase.GetCurrentMethod().Name;
            Order request = new Order();
            String currentTxnID = Guid.NewGuid().ToString();
            request.txn_id = currentTxnID;
            request.additionalData.Add(OrderAdditionalDataKeys.serviceProviderID,"unit test");

            Console.WriteLine(string.Format("duplicateKey is [{0}]",newDuplicateKey));

            Boolean dbWriteSuccess = dbAccessor.writeToATSDuplicate(request, location, newDuplicateKey, hrnxPayload);
            Assert.IsTrue(dbWriteSuccess);

            // now, we pull data back and compare them
            HRNXATSDuplicateEntry original= dbAccessor.getATSDuplicateEntryData(request, newDuplicateKey, "EnsureReadAndWriteToATSDuplicateTable unit test");

            Assert.IsNotNull(original);
            Assert.AreEqual(currentTxnID, original.original_txn_id);
            Assert.AreEqual(hrnxPayload, original.hrnx_processed_payload);
        }
        [TestMethod]
        public void EnsureClearStarCredentialsCanBeRead()
        {
            HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));

            String newDuplicateKey = System.Reflection.MethodBase.GetCurrentMethod().Name + "_" + Guid.NewGuid().ToString();
            String location = System.Reflection.MethodBase.GetCurrentMethod().Name;
            Order request = new Order();
            request.client_group_id = "2569";
            request.additionalData.Add(OrderAdditionalDataKeys.serviceProviderID, "clearstar2~301");

            clearStarCredential credentials = dbAccessor.getClearStarCredential(request, location);

            Assert.AreEqual("CRCK_13644", credentials.customerID);
            Assert.AreEqual("chemicalbank_testuser", credentials.username);
            Assert.AreEqual("Chemical@2018", credentials.password);
            Assert.AreEqual("301", credentials.businessUnitID);
        }
        [TestMethod]
        public void EnsureQIncomingReadWriteWorks()
        {
            HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
            String origin = System.Reflection.MethodBase.GetCurrentMethod().Name;

            String txn_id = Guid.NewGuid().ToString();
            String payload_original = "SOme service Provider content";
            String payload_hrnx = "<report></report>";
            String service_tp_id = "someServiceProviderID";

            Order request = new Order();
            request.txn_id = txn_id;
            request.additionalData.Add(OrderAdditionalDataKeys.serviceProviderID, service_tp_id);
            request.serviceProviderWorkflowStarts();
            request.status = OrderStatus.processing;
            request.orderingIP = "localhost";

            Boolean dbwriteSuccess = dbAccessor.writeToQIncoming(request, origin, payload_hrnx, payload_original);

            // Check #1, DB write works
            Assert.IsTrue(dbwriteSuccess);

            List<HRNXQIncomingEntry> qIncomingEntries = dbAccessor.getQIncomingEntriesFromDB(request, origin);

            // Check #2, only 1 qIncoming entry created
            Assert.AreEqual(1, qIncomingEntries.Count);

            // Check #3, ensure data is identical
            Assert.AreEqual(txn_id, qIncomingEntries[0].txn_id);
            Assert.AreEqual(service_tp_id, qIncomingEntries[0].service_tp_id);
            Assert.AreEqual(request.status, qIncomingEntries[0].status);
            Assert.AreEqual("2", qIncomingEntries[0].version);
            Assert.AreEqual(request.orderingIP, qIncomingEntries[0].ip_address);
            Assert.AreEqual(payload_hrnx, qIncomingEntries[0].payload_hrnx);
            Assert.AreEqual(payload_original, qIncomingEntries[0].payload_original);
        }
        [TestMethod]
        public void EnsureQProcessingStatusCanBeUpdated()
        {
            HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
            String origin = System.Reflection.MethodBase.GetCurrentMethod().Name;

            String txn_id = Guid.NewGuid().ToString();
            String service_tp_id = "someServiceProviderID";
            OrderStatus oldStatus = OrderStatus.passthru;
            OrderStatus newStatus = OrderStatus.submitted_passthru;

            Order request = new Order();
            request.txn_id = txn_id;
            request.client_group_id = "Some Client";
            request.client_user_id = "groupAuth";
            request.additionalData.Add(OrderAdditionalDataKeys.serviceProviderID, service_tp_id);
            request.serviceProviderWorkflowStarts();
            request.status = oldStatus;
            request.orderingIP = "localhost";

            Boolean dbwriteSuccess = dbAccessor.writeToQProcessing(request, origin);
            Assert.IsTrue(dbwriteSuccess);

            dbwriteSuccess = dbAccessor.setQProcessStatus(request, origin, newStatus);

            // Check #1, DB write works
            Assert.IsTrue(dbwriteSuccess);

            // Check #2, ensure the status has been updated
            Order returnVal = dbAccessor.getOrderFromDB(txn_id, request, origin);
            Assert.AreEqual(newStatus, returnVal.status);
        }
		[TestMethod]
		public void EnsureVendorOrderIDLookupReadWriteWorks()
		{
			HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;

			String txn_id = Guid.NewGuid().ToString();
			String service_tp_id = "someServiceProviderID";
			String vendorOrderID = location + "_" + txn_id;

			Order request = new Order();
			request.txn_id = txn_id;
			request.additionalData.Add(OrderAdditionalDataKeys.serviceProviderID, service_tp_id);
			request.serviceProviderWorkflowStarts();

			Boolean isWriteSuccess = dbAccessor.writeToVendorOrderID(request, location, vendorOrderID);
			Assert.IsTrue(isWriteSuccess);

			List<VendorOrderIDLookupEntry> result = dbAccessor.getVendorOrderIDLookupEntriesFromDB(request, location, vendorOrderID);

			// Check #1: Ensure one new entry has been added
			Assert.AreEqual(1, result.Count);

			// Check #2: Verify the data matches with what we wanted
			Assert.AreEqual(txn_id, result[0].txn_id);
			Assert.AreEqual(service_tp_id, result[0].service_tp_id);
		}
		[TestMethod]
		public void EnsureVendorOrderIDCanBeReset()
		{
			HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;

			String txn_id = Guid.NewGuid().ToString();
			String service_tp_id = "someServiceProviderID";
			String vendorOrderID = location + "_" + txn_id;

			Order request = new Order();
			request.txn_id = txn_id;
			request.additionalData.Add(OrderAdditionalDataKeys.serviceProviderID, service_tp_id);
			request.serviceProviderWorkflowStarts();

			Boolean isWriteSuccess = dbAccessor.writeToVendorOrderID(request, location, vendorOrderID);
			// Check #1: Make sure the write to DB is successful
			Assert.IsTrue(isWriteSuccess);

			// mark it as complete
			Boolean markAsCompleteSuccess = dbAccessor.markVendorOrderIDasCompleted(request, location);
			// Check #2: Make sure the update is successful
			Assert.IsTrue(markAsCompleteSuccess);

			// pull it from DB and confirm it has been marked as completed
			List<VendorOrderIDLookupEntry> result = dbAccessor.getVendorOrderIDLookupEntriesFromDBRegardlessOfStatus(request, location, vendorOrderID);

			// Check #3: Ensure one new entry has been added
			Assert.AreEqual(1, result.Count);

			// Check #4: Verify the data matches with what we wanted
			Assert.AreEqual(txn_id, result[0].txn_id);
			Assert.AreEqual(service_tp_id, result[0].service_tp_id);
			Assert.AreEqual("sent", result[0].status);

			// Now try to reset it
			Boolean resetSuccess = dbAccessor.resetVendorOrderIDLookupEntries(request, location);
			// Check #5: Make sure the update is successful
			Assert.IsTrue(resetSuccess);

			// pull it from DB and confirm it has been marked as completed
			result = dbAccessor.getVendorOrderIDLookupEntriesFromDBRegardlessOfStatus(request, location, vendorOrderID);
			
			// Check #6: Verify the data matches with what we wanted
			Assert.AreEqual("processing", result[0].status);
		}
		[TestMethod]
		public void EnsureHRNXServiceTypeMappingCanBePull()
		{
			HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
			String origin = System.Reflection.MethodBase.GetCurrentMethod().Name;

			String service_tp_id = "clearstar2~301";
			String client_group_id = "2569";
			String service_id = "CRCK_00012";

			Order request = new Order();
			request.client_group_id = client_group_id;
			request.additionalData.Add(OrderAdditionalDataKeys.serviceProviderID, service_tp_id);
			request.serviceProviderWorkflowStarts();

			String hrnxService = dbAccessor.convertToHRNXService(request, origin,service_id);

			// Check #1, verify value matches expected
			Assert.AreEqual("education", hrnxService);
		}
		[TestMethod]
		public void EnsurePackageCanBeTranslatedToServiceID()
		{
			HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
			String origin = System.Reflection.MethodBase.GetCurrentMethod().Name;

			String client_group_id = "2569";
			String package_id = "CRCK_01148";

			Order request = new Order();
			request.client_group_id = client_group_id;
			request.serviceProviderWorkflowStarts();

			List<String> result = dbAccessor.getServiceIDAssociateWithPackage(request, origin, package_id);

			// Check #1, verify count matches expected
			Assert.AreEqual(6, result.Count);

			// Check #2, check specific values
			Assert.IsTrue(result.Contains("CRCK_00012"));
			Assert.IsTrue(result.Contains("CRCK_00017"));
			Assert.IsTrue(result.Contains("CRCK_00022"));
			Assert.IsTrue(result.Contains("CRCK_00042"));
			Assert.IsTrue(result.Contains("CRCK_00052"));
			Assert.IsTrue(result.Contains("CRCK_00074"));
		}
		[TestMethod]
		public void EnsureEmptyPackageCanBeTranslatedToEmptyList()
		{
			HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
			String origin = System.Reflection.MethodBase.GetCurrentMethod().Name;

			String client_group_id = "2569";
			String package_id = "CRCK_01148_vasfasf";

			Order request = new Order();
			request.client_group_id = client_group_id;
			request.serviceProviderWorkflowStarts();

			List<String> result = dbAccessor.getServiceIDAssociateWithPackage(request, origin, package_id);

			// Check #1, verify count matches expected
			Assert.AreEqual(0, result.Count);
		}
		[TestMethod]
		public void EnsurePendingActionEntryReadWriteWorks()
		{
			HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;

			String txn_id = Guid.NewGuid().ToString();
			String service_tp_id = "someServiceProviderID";
			String client_group_id = "SomeClient";
			PendingActionAction currentAction = PendingActionAction.uploadDocument;
			String referenceID = location + "_" + txn_id;
			String referenceData = "Some saved data";

			Order request = new Order();
			request.txn_id = txn_id;
			request.client_group_id = client_group_id;
			request.additionalData.Add(OrderAdditionalDataKeys.serviceProviderID, service_tp_id);
			request.serviceProviderWorkflowStarts();

			Boolean isWriteSuccess = dbAccessor.addEntryToPendingAction(request, location, currentAction, referenceID, referenceData);
			Assert.IsTrue(isWriteSuccess);

			PendingActionEntry result = dbAccessor.getPendingActionEntryFromDBUsingTxnID(request, location);

			// Check #1: Verify the data matches with what we wanted
			Assert.AreEqual(txn_id, result.txn_id);
			Assert.AreEqual(service_tp_id, result.service_tp_id);
			Assert.AreEqual(currentAction, result.pending_action);
			Assert.AreEqual(referenceID, result.reference_id);
			Assert.AreEqual(referenceData, result.reference_data);
		}
		[TestMethod]
		public void EnsureClientOptionsCanBeRead()
		{
			HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;

			String client_group_id = "2569";

			Order request = new Order();
			request.client_group_id = client_group_id;

			String[] options = dbAccessor.getClientOptions(request, location);

			// Check #1: Make sure the value returned is not null
			Assert.IsNotNull(options);

			// Check #2: Check the values are proper
			Assert.AreEqual(2, options.Length);
			Assert.AreEqual("clearstarEduMax=1",options[0]);
			Assert.AreEqual("clearstarEmpMax=2", options[1]);
		}
	}
}
