﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using getClientConfigurations.Models;
using System.IO;
using System.Net;
using System.Collections.Generic;
using System.Resources;

namespace MultiPhraseCreateOrder.Tests
{
    [TestClass]
    public class IntegationTests
    {
        private ResourceManager rm = new ResourceManager("getClientConfigurations.Properties.HRNX", typeof(HRNXDBAccessor).Assembly);
        private ResourceManager rmUnitTest = new ResourceManager("MultiPhraseCreateOrder.Tests.Properties.Resources", typeof(MainRunnerUnitTests).Assembly);

		private HRNXWebResponse placeOrderEnd2End(String payloadFinal, String atsID, String service_tp_id, String location, HRNXWebAccessor webAccessor, HRNXDBAccessor dbaccessor)
		{
			int timeout = 5 * 60 * 1000;  //5minute as timeout
			String MULTIPHRASE_CREATEORDER_URL = rmUnitTest.GetString("LocalDomain") + "/API/CreateOrder/MultiplePhase/" + atsID + "/" + service_tp_id;
			//MULTIPHRASE_CREATEORDER_URL = "https://api-staging.hrintegrations.com/API/CreateOrder/MultiplePhase/" + atsID + "/" + service_tp_id;

			return webAccessor.httpSend(MULTIPHRASE_CREATEORDER_URL, "POST", "application/json", payloadFinal, timeout);
		}

		[TestMethod]
        public void IntegrationTestEnsureEndToEndWorks()
        {
            String testResourcePath = "Resources/orderRequestCredit.json";
            String location = System.Reflection.MethodBase.GetCurrentMethod().Name;
			String atsID = "icims6";
			String service_tp_id = "clearstar2~301";

			// verifies resource file can be obtained
			Assert.IsTrue(File.Exists(testResourcePath));
            String payloadFinal = File.ReadAllText(testResourcePath);
            //Console.WriteLine("Original payload = [{0}]", payloadFinal);

            HRNXWebAccessor webAccessor = new HRNXWebAccessor();

            // save current txn_id
            HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
            String oldTxnID = dbaccessor.getLatestOrderTxnID(null, location);
            String lastErrorID = dbaccessor.getLatestErrorID(null, location);

			HRNXWebResponse hrnxResponse = placeOrderEnd2End(payloadFinal, atsID, service_tp_id, location, webAccessor, dbaccessor);
			Console.WriteLine("return response texte = [{0}]",hrnxResponse.responseText);
            Console.WriteLine("return status = [{0}]", hrnxResponse.status);
            Assert.AreEqual(HttpStatusCode.OK, hrnxResponse.status);

            // Check #1: Ensure a new txn is generated
            Order target = dbaccessor.getLatestOrder(null, location);
            String newTxnID = target.txn_id;
            Assert.AreNotEqual(oldTxnID, newTxnID);

            // Check #2, make sure no errors are generated
            String newErrorID = dbaccessor.getLatestErrorID(null, location);
            Assert.AreEqual(lastErrorID, newErrorID);

            // Check #3, make sure txn is in the proper status
            Assert.AreEqual(OrderStatus.submitted_passthru, target.status);

            // Check #4, make sure qIncoming extry exist, and all its data is correct
            List<HRNXQIncomingEntry> qIncoming = dbaccessor.getQIncomingEntriesFromDB(target, location);
            Assert.AreEqual(2, qIncoming.Count);
            Assert.AreEqual(OrderStatus.processing, qIncoming[0].status);
            Assert.AreEqual(service_tp_id, qIncoming[0].service_tp_id);
            StringAssert.StartsWith(qIncoming[0].payload_hrnx,"<report>");
        }

        [TestMethod]
        public void errorShortTooLongWillGetLoggedToErrorStack()
        {
            Order request = new Order();
            String origin = System.Reflection.MethodBase.GetCurrentMethod().Name;

            // Pull last error from errorStack currently
            HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
            String lastErrorID = dbAccessor.getLatestErrorID(request,origin);

            int timeout = 5 * 60 * 1000;  //5minute as timeout
            String testResourcePath = "Resources/orderRequest.json";
            String badURL = rmUnitTest.GetString("LocalDomain") + "/API/CreateOrder/MultiplePhase/icimasd7asfffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffs6/clearstar2~301";

            // verifies resource file can be obtained
            Assert.IsTrue(File.Exists(testResourcePath));
            String payloadFinal = File.ReadAllText(testResourcePath);
            //Console.WriteLine("Original payload = [{0}]", payloadFinal);

            HRNXWebAccessor webAccessor = new HRNXWebAccessor();

            // Check #1, verify we got a Bad Request back (since our ATSID is too long)
            HRNXWebResponse hrnxResponse = webAccessor.httpSend(badURL, "POST", "application/json", payloadFinal, timeout);
            Console.WriteLine("return response texte = [{0}]", hrnxResponse.responseText);
            Console.WriteLine("return status = [{0}]", hrnxResponse.status);
            Assert.AreEqual(HttpStatusCode.BadRequest, hrnxResponse.status);

            // Check #2, verify we got a new Error on ErrorStack
            List<HRNXv1Error> returnedErrors = dbAccessor.getErrorFromErrorStackSince(lastErrorID, request, origin);
            Assert.AreEqual(1, returnedErrors.Count);

            // Check #3, verify we got a SQL_Update failure
            Assert.AreEqual(rm.GetString("SQL_FAILED_ERROR_SHORT"), returnedErrors[0].error_short);
        }
		[TestMethod]
		public void EnsureSearchTxnWorks()
		{
			///=====================
			/// Order credit first
			///=====================
			String testResourcePath = "Resources/orderRequestCredit.json";
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;
			String atsID = "icims6";
			String service_tp_id = "clearstar2~301";
			String CandidateID = Guid.NewGuid().ToString();

			// verifies resource file can be obtained
			Assert.IsTrue(File.Exists(testResourcePath));
			String payloadFinal = File.ReadAllText(testResourcePath);
			payloadFinal = payloadFinal.Replace("45392", CandidateID);

			HRNXWebAccessor webAccessor = new HRNXWebAccessor();
			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));

			HRNXWebResponse hrnxResponse = placeOrderEnd2End(payloadFinal, atsID, service_tp_id, location, webAccessor, dbaccessor);
			//Console.WriteLine("return response texte = [{0}]", hrnxResponse.responseText);
			//Console.WriteLine("return status = [{0}]", hrnxResponse.status);
			Assert.AreEqual(HttpStatusCode.OK, hrnxResponse.status);

			///=====================
			/// Now, order background
			///=====================
			String bgTestResourcePath = "Resources/orderRequest.json";
			Assert.IsTrue(File.Exists(bgTestResourcePath));
			String bgPayloadFinal = File.ReadAllText(bgTestResourcePath);
			bgPayloadFinal = bgPayloadFinal.Replace("45392", CandidateID);

			// save current txn_id
			String oldTxnID = dbaccessor.getLatestOrderTxnID(null, location);
			String lastErrorID = dbaccessor.getLatestErrorID(null, location);

			HRNXWebResponse bgHrnxResponse = placeOrderEnd2End(bgPayloadFinal, atsID, service_tp_id, location, webAccessor, dbaccessor);
			Console.WriteLine("return response texte = [{0}]", hrnxResponse.responseText);
			Console.WriteLine("return status = [{0}]", hrnxResponse.status);
			Assert.AreEqual(HttpStatusCode.OK, hrnxResponse.status);

			// Check #1: Ensure NO new txn is generated
			Order target = dbaccessor.getLatestOrder(null, location);
			String newTxnID = target.txn_id;
			Assert.AreEqual(oldTxnID, newTxnID);

			// Check #2, make sure no errors are generated
			String newErrorID = dbaccessor.getLatestErrorID(null, location);
			Assert.AreEqual(lastErrorID, newErrorID);

			// Check #3, make sure txn is in the proper status
			Assert.AreEqual(OrderStatus.submitted_passthru, target.status);

			// Check #4, make sure qIncoming extry exist, and all its data is correct
			List<HRNXQIncomingEntry> qIncoming = dbaccessor.getQIncomingEntriesFromDB(target, location);
			Assert.AreEqual(3, qIncoming.Count);

			// CHeck #4.1, credit's ordering
			Assert.AreEqual(OrderStatus.processing, qIncoming[0].status);
			Assert.AreEqual(service_tp_id, qIncoming[0].service_tp_id);
			StringAssert.Contains(qIncoming[0].payload_original, "<GetISOCountryList ");
			StringAssert.Contains(qIncoming[0].payload_original, "<prof:CreateProfileForCountry ");
			StringAssert.Contains(qIncoming[0].payload_original, "<prof:AddServiceToProfile ");
			StringAssert.Contains(qIncoming[0].payload_original, "<prof:TransmitProfile ");
			StringAssert.Contains(qIncoming[0].payload_hrnx, "<VendorOrderId>2011052262218263</VendorOrderId>");
			StringAssert.Contains(qIncoming[0].payload_original, "<prof:sServiceNo>CRCK_00193</prof:sServiceNo>");

			// CHeck #4.1, credit's ordering
			Assert.AreEqual(OrderStatus.processing, qIncoming[2].status);
			Assert.AreEqual(service_tp_id, qIncoming[2].service_tp_id);
			StringAssert.Contains(qIncoming[2].payload_original, "<prof:ReOpenProfile ");
			StringAssert.Contains(qIncoming[2].payload_original, "<prof:sServiceNo>CRCK_00041</prof:sServiceNo>");
		}

		/// <summary>
		/// This test requires NCI to be installed locally
		/// </summary>
		[TestMethod]
		public void Integration_EnsureSuccessWhenAuthFormIDIsNull()
		{
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;
			String atsID = "icims6";
			String service_tp_id = "clearstar2~301";

			String payloadFinal = TestUtil.getResource("icims6/orderRequestTemplate.json");

			// Update the test to fit the client's configuration
			payloadFinal = payloadFinal.Replace("^^^orderingStatus^^^", "8770");
			payloadFinal = payloadFinal.Replace("^^^customerID^^^", "9078");
			payloadFinal = payloadFinal.Replace("^^^candidateID^^^", "125");

			HRNXWebAccessor webAccessor = new HRNXWebAccessor();

			// save current txn_id
			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
			String oldTxnID = dbaccessor.getLatestOrderTxnID(null, location);
			String lastErrorID = dbaccessor.getLatestErrorID(null, location);

			HRNXWebResponse hrnxResponse = placeOrderEnd2End(payloadFinal, atsID, service_tp_id, location, webAccessor, dbaccessor);
			Console.WriteLine("return response texte = [{0}]", hrnxResponse.responseText);
			Console.WriteLine("return status = [{0}]", hrnxResponse.status);
			Assert.AreEqual(HttpStatusCode.OK, hrnxResponse.status);

			// Check #1: Ensure a new txn is generated
			Order target = dbaccessor.getLatestOrder(null, location);
			String newTxnID = target.txn_id;
			Assert.AreNotEqual(oldTxnID, newTxnID);

			// Check #2, make sure no errors are generated
			String newErrorID = dbaccessor.getLatestErrorID(null, location);
			Assert.AreEqual(lastErrorID, newErrorID);

			// Check #3, make sure txn is in the proper status
			Assert.AreEqual(OrderStatus.submitted_passthru, target.status);

			// Check #4, make sure qIncoming extry exist, and all its data is correct
			List<HRNXQIncomingEntry> qIncoming = dbaccessor.getQIncomingEntriesFromDB(target, location);
			Assert.AreEqual(OrderStatus.processing, qIncoming[0].status);
			Assert.AreEqual(service_tp_id, qIncoming[0].service_tp_id);
			StringAssert.StartsWith(qIncoming[0].payload_hrnx, "<report>");

			// NIC doesn't have authform right now, so we expect only 1 qIncoming record
			Assert.AreEqual(1, qIncoming.Count);
		}
	}
}
