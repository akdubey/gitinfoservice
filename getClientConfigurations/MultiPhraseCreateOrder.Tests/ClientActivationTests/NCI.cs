﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using getClientConfigurations.Models;
using System.IO;
using System.Net;
using System.Collections.Generic;
using System.Resources;

namespace MultiPhraseCreateOrder.Tests
{
	[TestClass]
	public class NCI
	{
		private ResourceManager rm = new ResourceManager("getClientConfigurations.Properties.HRNX", typeof(HRNXDBAccessor).Assembly);
		private ResourceManager rmUnitTest = new ResourceManager("MultiPhraseCreateOrder.Tests.Properties.Resources", typeof(MainRunnerUnitTests).Assembly);

		private HRNXWebResponse placeOrderEnd2End(String payloadFinal, String atsID, String service_tp_id, String location, HRNXWebAccessor webAccessor, HRNXDBAccessor dbaccessor)
		{
			int timeout = 5 * 60 * 1000;  //5minute as timeout
			String MULTIPHRASE_CREATEORDER_URL = rmUnitTest.GetString("LocalDomain") + "/API/CreateOrder/MultiplePhase/" + atsID + "/" + service_tp_id;
			//MULTIPHRASE_CREATEORDER_URL = "https://api-staging.hrintegrations.com/API/CreateOrder/MultiplePhase/" + atsID + "/" + service_tp_id;

			return webAccessor.httpSend(MULTIPHRASE_CREATEORDER_URL, "POST", "application/json", payloadFinal, timeout);
		}

		[TestMethod]
		public void NCI_E2E()
		{
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;
			String atsID = "icims6";
			String service_tp_id = "clearstar2~301";

			String payloadFinal = TestUtil.getResource("icims6/orderRequestTemplate.json");

			// Update the test to fit the client's configuration
			// NIC will only ever order background check, so always 8770
			payloadFinal = payloadFinal.Replace("^^^orderingStatus^^^", "8770");
			payloadFinal = payloadFinal.Replace("^^^customerID^^^", "9078");
			payloadFinal = payloadFinal.Replace("^^^candidateID^^^", "125");

			HRNXWebAccessor webAccessor = new HRNXWebAccessor();

			// save current txn_id
			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
			String oldTxnID = dbaccessor.getLatestOrderTxnID(null, location);
			String lastErrorID = dbaccessor.getLatestErrorID(null, location);

			HRNXWebResponse hrnxResponse = placeOrderEnd2End(payloadFinal, atsID, service_tp_id, location, webAccessor, dbaccessor);
			Console.WriteLine("return response texte = [{0}]", hrnxResponse.responseText);
			Console.WriteLine("return status = [{0}]", hrnxResponse.status);
			Assert.AreEqual(HttpStatusCode.OK, hrnxResponse.status);

			// Check #1: Ensure a new txn is generated
			Order target = dbaccessor.getLatestOrder(null, location);
			String newTxnID = target.txn_id;
			Assert.AreNotEqual(oldTxnID, newTxnID);

			// Check #2, make sure no errors are generated
			String newErrorID = dbaccessor.getLatestErrorID(null, location);
			Assert.AreEqual(lastErrorID, newErrorID);

			// Check #3, make sure txn is in the proper status
			Assert.AreEqual(OrderStatus.submitted_passthru, target.status);

			// Check #4, make sure qIncoming extry exist, and all its data is correct
			List<HRNXQIncomingEntry> qIncoming = dbaccessor.getQIncomingEntriesFromDB(target, location);
			Assert.AreEqual(OrderStatus.processing, qIncoming[0].status);
			Assert.AreEqual(service_tp_id, qIncoming[0].service_tp_id);
			StringAssert.StartsWith(qIncoming[0].payload_hrnx, "<report>");

			// NIC doesn't have authform right now, so we expect only 1 qIncoming record
			Assert.AreEqual(1, qIncoming.Count);
		}
	}
}
