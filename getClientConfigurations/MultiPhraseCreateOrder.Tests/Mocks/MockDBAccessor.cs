﻿using getClientConfigurations.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Odbc;

namespace MultiPhraseCreateOrder.Tests.Mocks
{
    class MockDBAccessor: HRNXDBAccessor
    {
        public HRNXATSDuplicateEntry duplicateEntry;
        public HRNXPhase currentPhrase;

        public MockDBAccessor() : base("") {}

        public override HRNXPhase getPhraseData(Order request, String origin, String phrase, String service_tp_id)
        {
            return currentPhrase;
        }

        public override HRNXATSDuplicateEntry getATSDuplicateEntryData(Order request, String duplicateKey, String origin)
        {
            return duplicateEntry;
        }
    }
}
