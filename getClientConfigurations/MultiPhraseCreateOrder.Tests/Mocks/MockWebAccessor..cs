﻿using getClientConfigurations.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace MultiPhraseCreateOrder.Tests.Mocks
{
    class MockWebAccessor: HRNXWebAccessor
    {
        public String payload { get; set; }
        public String url { get; set; }
        public String ip { get; set; }

        public override String extractRequestPayloadFromHttpRequest(HttpContext currentContext)
        {
            return payload;
        }

        public override String extractRequestURLFromHttpRequest(HttpContext currentContext)
        {
            return url;
        }
        public override String extractRequesterIPFromHttpRequest(HttpContext curentContext)
        {
            return ip;
        }

    }
}
