﻿using getClientConfigurations.Models;

namespace getClientConfigurations.ServiceProviderHandlers
{
    public class MockServiceProviderHandler : ServiceProviderHandlerBase
    {
        public Order orderToReturn {get; set;}

        public override Order providerSpecificProcessing(Order hrnxOrder, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor)
        {
            return orderToReturn;
        }
    }
}