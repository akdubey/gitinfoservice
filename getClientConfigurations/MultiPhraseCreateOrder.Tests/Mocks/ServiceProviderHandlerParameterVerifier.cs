﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using getClientConfigurations.Models;
using getClientConfigurations.ServiceProviderHandlers;

namespace getClientConfigurations.ATSHandlers
{
    public class ServiceProviderHandlerParameterVerifier : IServiceProviderHandler
    {
        public Order savedOrder { get; set; }

        Order IServiceProviderHandler.processRequest(Order hrnxOrder, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor)
        {
            savedOrder = hrnxOrder;
            return hrnxOrder;
        }
    }
}