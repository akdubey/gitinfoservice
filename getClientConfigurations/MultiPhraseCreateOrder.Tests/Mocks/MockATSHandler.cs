﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using getClientConfigurations.Models;

namespace getClientConfigurations.ATSHandlers
{
    public class mockATSHandler : ATSHandlerBase
    {
        public Order orderToBeReturned { get; set; }
        public string rawPayload { get; set; }
        public CreateOrderResponse orderResponse { get; set; }

		public override string getID()
		{
			return "mockATS";
		}

		public override Order analyzeRequest(Order request, OrderRequest rawRequest, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor)
        {
            this.rawPayload = rawPayload;
            return orderToBeReturned;
        }
        public override Order createANewOrder(Order result, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor)
        {
            throw new NotImplementedException();
        }

        public override Order findExistingOrder(Order result, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor)
        {
            return result;
        }

        public override void getServiceProviderID(Order result, HRNXWebAccessor webAccessor, HRNXDBAccessor dbAccessor)
        {
			this.associatedServiceProviderID = "Something";
        }

        public override bool needToCreateNewOrder(Order result, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor)
        {
            return false;
        }

        public override CreateOrderResponse sendAcknowledgement(Order result, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor)
        {
            return orderResponse;
        }
    }
}