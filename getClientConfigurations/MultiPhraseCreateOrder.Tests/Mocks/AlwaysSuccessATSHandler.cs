﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using getClientConfigurations.Models;
using System.Net;

namespace getClientConfigurations.ATSHandlers
{
    public class AlwaysSuccessATSHandler : mockATSHandler
    {
        public override Order analyzeRequest(Order request, OrderRequest rawRequest, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor)
        {
            Order result = new Order();

            result.status = OrderStatus.submitted_passthru;

            return result;
        }

        public override CreateOrderResponse sendAcknowledgement(Order result, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor)
        {
            CreateOrderResponse resultVal = new CreateOrderResponse();
            resultVal.status = HttpStatusCode.OK;

            return resultVal;
        }
    }
}