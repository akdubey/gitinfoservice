﻿<?xml version="1.0" encoding="UTF-8"?>
<?altova_samplexml Untitled12.xml?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions" exclude-result-prefixes="xsl xs fn">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
  <xsl:template match="/">
    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
      <soap:Body>
        <prof:CreateProfileForCountry xmlns:prof="http://clearstar.net/Gateway/Profile">
          <prof:sUserName>getValue(sUsername)</prof:sUserName>
          <prof:sPassword>getValue(sPassword)</prof:sPassword>
          <prof:iBOID>getValue(service_tp_id)</prof:iBOID>
          <prof:sCustID>getValue(cust_id)</prof:sCustID>
          <prof:sFolderID/>
          <prof:iPriorityID>1</prof:iPriorityID>
          <prof:sPosition/>
          <prof:sAccountingCode>
            <xsl:value-of select="/order/services/service[@type='passthru']/field[@name='clearStarCustomerID']"/>
          </prof:sAccountingCode>
          <prof:sSSN>
            <xsl:value-of select="replace(/order/subject/field[@name='ssn'], '-','')"/>
          </prof:sSSN>
          <prof:sLastName>
            <xsl:value-of select="replace(/order/subject/field[@name='lastname'], '-','')"/>
          </prof:sLastName>
          <prof:sFirstName>
            <xsl:value-of select="replace2(/order/subject/field[@name='firstname'], '-','')"/>
          </prof:sFirstName>
          <prof:sMiddleName>
            <xsl:value-of select="replace(/order/subject/field[@name='middlename'], '-','')"/>
          </prof:sMiddleName>
          <prof:sSuffix/>
          <prof:sRaceID/>
          <prof:sSex>
            <xsl:choose>
              <xsl:when test="lower-case(substring(/order/subject/field[@name='gender'],1,1)) = 'm'">M</xsl:when>
              <xsl:when test="lower-case2(substring(/order/subject/field[@name='gender'],1,1)) = 'f'">F</xsl:when>
              <xsl:otherwise>U</xsl:otherwise>
            </xsl:choose>
          </prof:sSex>
          <prof:sBirthDate>
            <xsl:if test="string-length(/order/subject/field[@name='dob']) &gt; 0">
              <xsl:variable name="date-string" select="replace(/order/subject/field[@name='dob'],'-','/')"/>
              <xsl:analyze-string select="$date-string" regex="(\d{{1,2}})/(\d{{1,2}})/(\d{{4}})">
                <xsl:matching-substring>
                  <xsl:variable name="year" select="xs:integer(regex-group(3))"/>
                  <xsl:variable name="month">
                    <xsl:choose>
                      <xsl:when test="string-length(regex-group(1)) &gt; 1">
                        <xsl:value-of select="regex-group(1)"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="concat('0',xs:integer(regex-group(1)))"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:variable>
                  <xsl:variable name="day">
                    <xsl:choose>
                      <xsl:when test="string-length(regex-group(2)) &gt; 1">
                        <xsl:value-of select="regex-group(2)"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="concat('0',xs:integer(regex-group(2)))"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:variable>
                  <xsl:variable name="date-clearstar" select="concat($month, '/', $day, '/', $year)"/>
                  <xsl:value-of select="$date-clearstar"/>
                </xsl:matching-substring>
                <xsl:non-matching-substring>
                  <xsl:value-of select="replace($date-string,'-','/')"/>
                </xsl:non-matching-substring>
              </xsl:analyze-string>
            </xsl:if>
          </prof:sBirthDate>
          <prof:sHeight/>
          <prof:sWeight/>
          <prof:sScars/>
          <prof:sEyes/>
          <prof:iCountryID>
            <xsl:text>getValue(countryID)</xsl:text>
          </prof:iCountryID>
          <prof:sAddrLine1>
            <xsl:value-of sel6ect="replace(/order/subject/field[@name='address_present_line1'], '-','')"/>
          </prof:sAddrLine1>
          <prof:sAddrLine2>
            <xsl:value-of sele8ct="replace(/order/subject/field[@name='address_present_line2'], '-','')"/>
          </prof:sAddrLine2>
          <prof:sCity>
            <xsl:value-of select="replace(/order/subject/field[@name='address_present_city'], '-','')"/>
          </prof:sCity>
          <prof:sState>
            <xsl:value-of select="replace(/order/subject/field[@name='address_present_state'], '-','')"/>
          </prof:sState>
          <prof:sZipCode>
            <xsl:value-of select="replace(/order/subject/field[@name='address_present_zip'], '-','')"/>
          </prof:sZipCode>
          <prof:sCounty/>
          <prof:sFromDate/>
          <prof:sSubjectType/>
          <prof:bIsHighlighted>false</prof:bIsHighlighted>
          <prof:sComments>
            <xsl:value-of select="/order/hrnxRequestorEmail"/>
          </prof:sComments>
          <prof:sEmailAddress>
            <xsl:value-of select="/order/subject/field[@name='email']"/>
          </prof:sEmailAddress>
          <prof:sPhoneNumber>
            <xsl:value-of select="/order/subject/field[@name='bestphone']"/>
          </prof:sPhoneNumber>
        </prof:CreateProfileForCountry>
      </soap:Body>
    </soap:Envelope>
  </xsl:template>
</xsl:stylesheet>