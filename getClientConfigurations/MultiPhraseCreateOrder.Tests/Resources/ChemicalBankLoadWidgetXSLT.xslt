<?xml version="1.0" encoding="UTF-8"?>
<order>
  <txn_id>^^txn_id^^</txn_id>
  <txn_requestdate timezone="utc">^^txn_requestdate^^</txn_requestdate>
  <friendly_id/>
  <hrnxXmlVersion>1.0</hrnxXmlVersion>
  <client_user_id/>
  <client_group_id/>
  <tp_id>^^tp_id^^</tp_id>
  <tp_uqid/>
  <package/>
  <postbackURL/>
  <workflowDetailsURL/>
  <quoteback id="1"/>
  <quoteback id="2"/>
  <requisitionId/>
  <requisitionJobTitle/>
  <custom_questions/>
  <checkedOffItems/>
  <hrnxRequestor>^^requestor_name^^</hrnxRequestor>
  <hrnxRequestorEmail>^^requestor_email^^</hrnxRequestorEmail>
  <eForm type="Authorization">^^auth_form^^</eForm>
  <subject/>
  <services>
    <service/>
  </services>
</order>