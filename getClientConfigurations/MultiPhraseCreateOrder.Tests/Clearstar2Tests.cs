﻿using getClientConfigurations.Models;
using getClientConfigurations.ServiceProviderHandlers;
using log4net.Config;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Resources;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace MultiPhraseCreateOrder.Tests.Resources
{
    [TestClass]
    public class ClearStar2Tests
    {
        private ResourceManager rmUnitTest = new ResourceManager("MultiPhraseCreateOrder.Tests.Properties.Resources", typeof(HRNXUtilTests).Assembly);
        private ResourceManager rm = new ResourceManager("getClientConfigurations.Properties.HRNX", typeof(HRNXDBAccessor).Assembly);

		private static readonly log4net.ILog log = log4net.LogManager.GetLogger("MultiPhaseCreateOrderMainLogger");

		private Order orderNewCredit(String location, HRNXWebAccessor webAccessor, HRNXDBAccessor dbaccessor)
		{
			String txn_id = Guid.NewGuid().ToString();
			//txn_id = "cee0a983-baae-11e7-a0e7-12b5fc3c4575";
			String service_tp_id = "clearstar2~301";
			Console.WriteLine(string.Format("txnID is {0}", txn_id));
			String client_group_id = "2569";

			Order requestOrder = new Order();
			requestOrder.txn_id = txn_id;
			requestOrder.client_group_id = client_group_id;
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.serviceProviderID, service_tp_id);
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.isNewOrder, "true");
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.package, "CRCK_00193");
			String payloadFinal = TestUtil.getResource("clearstar2/end2endHRNXPayload.xml");
			requestOrder.payload_hrnx = payloadFinal;

			clearstar target = new clearstar();
			Order result = target.processRequest(requestOrder, dbaccessor, webAccessor);

			return result;
		}

		[TestMethod]
        public void ensureSuccessWhenSuccessNewOrdering()
        {
            String location = System.Reflection.MethodBase.GetCurrentMethod().Name;
            HRNXWebAccessor webAccessor = new HRNXWebAccessor();
            HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));

            // save current last error
            String lastErrorID = dbaccessor.getLatestErrorID(null, location);

			Order result = orderNewCredit(location, webAccessor, dbaccessor);
			String txn_id = result.txn_id;
			String service_tp_id = result.getCurrentServiceProviderID();
			String client_group_id = result.client_group_id;

			// Check #1, no new errors
			String latestErrorID = dbaccessor.getLatestErrorID(null, location);
            Assert.AreEqual(lastErrorID, latestErrorID);

            // Check #2, make sure the order returned has proper status
            Assert.AreEqual(OrderStatus.processing,result.status);

            List<HRNXQIncomingEntry> qIncomingEntry =  dbaccessor.getQIncomingEntriesFromDB(result, location);
			int index = qIncomingEntry.Count - 2;  //this is because you want to look at the laest one as I maybe changing txn_id in loocal test. However, due to AuthForm you actually want to look at the 2nd last record

			// Check #3.1, make sure we attempt to lookup country
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<GetISOCountryList ");

            // Check #3.2, make sure we are extracting country properly
            StringAssert.Contains(qIncomingEntry[index].payload_original, "ExtractedCountryCode = [233]");

			// Check #3.3, make sure we attempt to createOrder
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:CreateProfileForCountry ");

			// Check #3.4, make sure we are extracting profileID properly
			StringAssert.Contains(qIncomingEntry[index].payload_original, "extractedProfileID = [2011052262218263]");

			// Check #3.5, make sure we attempt to addServiceToOrder
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:AddServiceToProfile ");

			// Check #3.6, make sure we attempt to transmitProfile
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:TransmitProfile ");

			// CHeck #4, make sure no string replacement was missed
			Assert.IsFalse(qIncomingEntry[index].payload_original.Contains("getValue(sUsername)"));
			Assert.IsFalse(qIncomingEntry[index].payload_original.Contains("getValue(sPassword)"));
			Assert.IsFalse(qIncomingEntry[index].payload_original.Contains("getValue(service_tp_id)"));
			Assert.IsFalse(qIncomingEntry[index].payload_original.Contains("getValue(cust_id)"));
			Assert.IsFalse(qIncomingEntry[index].payload_original.Contains("getValue(countryID)"));
			Assert.IsFalse(qIncomingEntry[index].payload_original.Contains("getValue(profile_no)"));
			Assert.IsFalse(qIncomingEntry[index].payload_original.Contains("getValue(service_no)"));

			// Check 5, make sure reportXML looks fine
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(qIncomingEntry[index].payload_hrnx);

			Assert.AreEqual(txn_id, xmlDoc.SelectSingleNode("report/txn_id").InnerText);
			Assert.AreEqual(client_group_id, xmlDoc.SelectSingleNode("report/client_group_id").InnerText);
			Assert.AreEqual(OrderStatus.processing.ToString(), xmlDoc.SelectSingleNode("report/status").InnerText);
			Assert.AreEqual("2011052262218263", xmlDoc.SelectSingleNode("report/services/service[@tp='"+service_tp_id+ "']/VendorOrderId").InnerText);

			// Check 6, make sure vendor_order_id_lookup entry has been created
			List<VendorOrderIDLookupEntry> vendorOrderIDEntry = dbaccessor.getVendorOrderIDLookupEntriesFromDB(result, location);
			// Check 6.1: make sure there is only one entry
			Assert.AreEqual(1, vendorOrderIDEntry.Count);
			// Check 6.2: make sure the key matches what we want it to be
			Assert.AreEqual("2011052262218263_CRCK_13644", vendorOrderIDEntry[0].vendor_order_id);
			// Check 6.3: make sure the serviceProviderID is used
			Assert.AreEqual(service_tp_id, vendorOrderIDEntry[0].service_tp_id);
			// Check 6.3: make sure the status is appropriate
			Assert.AreEqual("processing", vendorOrderIDEntry[0].status);

			// Check 7, make sure pendingAction entry has been created
			//PendingActionEntry pendingAction = dbaccessor.getPendingActionEntryFromDBUsingTxnID(result, location);

			// Check 7.1, make sure data are correct
			//Assert.AreEqual(txn_id,pendingAction.txn_id);
			//Assert.AreEqual(client_group_id, pendingAction.client_group_id);
			//Assert.AreEqual(service_tp_id, pendingAction.service_tp_id);
			//Assert.AreEqual("2011052262218263", pendingAction.reference_id);
			//Assert.AreEqual("JVBERi0xLjUNCjQgMC3RhcnR4cmVmDQoyMDEzMDUNCiUlRU9GDQo=", pendingAction.reference_data);
			//Assert.AreEqual(PendingActionAction.uploadDocument, pendingAction.pending_action);
			//Assert.AreEqual(PendingActionStatus.pending, pendingAction.status);

			//Check 8, Rather than pending_action table, my recent change want to verify data has been writen to qIncoming
			Assert.IsTrue(qIncomingEntry[index+1].payload_original.Contains("JVBERi0xLjUNCjQgMC3RhcnR4cmVmDQoyMDEzMDUNCiUlRU9GDQo="));
		}
		[TestMethod]
		public void ensureSuccessWhenLookupOrdering()
		{
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;
			String txn_id = Guid.NewGuid().ToString();
			//txn_id = "9e76e6cd-bea6-11e7-947d-12b5fc3c4575";
			String service_tp_id = "clearstar2~301";
			Console.WriteLine(string.Format("txnID is {0}", txn_id));
			String client_group_id = "2569";
			String profileID = "2011052262218263";

			Order requestOrder = new Order();
			requestOrder.txn_id = txn_id;
			requestOrder.client_group_id = client_group_id;
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.serviceProviderID, service_tp_id);
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.isNewOrder, "true");
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.package, "CRCK_00193");
			String payloadFinal = TestUtil.getResource("clearstar2/end2endHRNXPayload.xml");
			requestOrder.payload_hrnx = payloadFinal;

			HRNXWebAccessor webAccessor = new HRNXWebAccessor();
			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));

			// save current last error
			String lastErrorID = dbaccessor.getLatestErrorID(null, location);

			clearstar target = new clearstar();
			Order result;
			result = target.processRequest(requestOrder, dbaccessor, webAccessor);

			// Now, modify the input to simulate 2nd order comes in
			requestOrder.additionalData[OrderAdditionalDataKeys.isNewOrder] = "false";
			// it also won't have the packageName defined in the 2nd round
			requestOrder.additionalData.Remove(OrderAdditionalDataKeys.packageName);

			// Reprocess
			result = target.processRequest(requestOrder, dbaccessor, webAccessor);

			// Check #1, no new errors
			String latestErrorID = dbaccessor.getLatestErrorID(null, location);
			Assert.AreEqual(lastErrorID, latestErrorID);

			// Check #2, make sure the order returned has proper status
			Assert.AreEqual(OrderStatus.processing, result.status);

			List<HRNXQIncomingEntry> qIncomingEntry = dbaccessor.getQIncomingEntriesFromDB(requestOrder, location);
			int index = qIncomingEntry.Count - 1;  //this is because you want to look at the laest one as I maybe changing txn_id in loocal test

			// Check #3.2, make sure we DO NOT attempt to createOrder
			Assert.IsFalse(qIncomingEntry[index].payload_original.Contains("<prof:CreateProfileForCountry "));

			// Check #3.3, make sure we attempt to ReOpenProfile
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:ReOpenProfile ");

			// Check #3.4, make sure we attempt to addServiceToOrder
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:AddServiceToProfile ");

			// Check #3.5, make sure we DO NOT attempt to transmitProfile
			Assert.IsFalse(qIncomingEntry[index].payload_original.Contains("<prof:TransmitProfile "));

			// CHeck #4, make sure no string replacement was missed
			Assert.IsFalse(qIncomingEntry[index].payload_original.Contains("getValue(sUsername)"));
			Assert.IsFalse(qIncomingEntry[index].payload_original.Contains("getValue(sPassword)"));
			Assert.IsFalse(qIncomingEntry[index].payload_original.Contains("getValue(service_tp_id)"));
			Assert.IsFalse(qIncomingEntry[index].payload_original.Contains("getValue(cust_id)"));
			Assert.IsFalse(qIncomingEntry[index].payload_original.Contains("getValue(countryID)"));
			Assert.IsFalse(qIncomingEntry[index].payload_original.Contains("getValue(profile_no)"));
			Assert.IsFalse(qIncomingEntry[index].payload_original.Contains("getValue(service_no)"));

			// Check 5, make sure reportXML looks fine
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(qIncomingEntry[index].payload_hrnx);

			Assert.AreEqual(txn_id, xmlDoc.SelectSingleNode("report/txn_id").InnerText);
			Assert.AreEqual(client_group_id, xmlDoc.SelectSingleNode("report/client_group_id").InnerText);
			Assert.AreEqual(OrderStatus.processing.ToString(), xmlDoc.SelectSingleNode("report/status").InnerText);
			Assert.AreEqual(profileID, xmlDoc.SelectSingleNode("report/services/service[@tp='" + service_tp_id + "']/VendorOrderId").InnerText);

			// Check 6, make sure vendor_order_id_lookup entry remains 1 because we SHOULD NOT be creating new vendorOrderLookupEntry in Lookup
			List<VendorOrderIDLookupEntry> vendorOrderIDEntry = dbaccessor.getVendorOrderIDLookupEntriesFromDB(result, location);
			Assert.AreEqual(1, vendorOrderIDEntry.Count);

			// make sure at the end, the vendorOrderEntry is processing, so results can come in
			Assert.AreEqual("processing", vendorOrderIDEntry[0].status);
		}

		[TestMethod]
		public void ensureEmptyCountryCodeWillErrorOut()
		{
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;
			String txn_id = Guid.NewGuid().ToString();
			//String txn_id = "cee0a983-baae-11e7-a0e7-12b5fc3c4575";
			String service_tp_id = "clearstar2~301";
			Console.WriteLine(string.Format("txnID is {0}", txn_id));

			Order requestOrder = new Order();
			requestOrder.txn_id = txn_id;
			requestOrder.client_group_id = "2569";
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.serviceProviderID, service_tp_id);
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.isNewOrder, "true");
			String payloadFinal = TestUtil.getResource("clearstar2/end2endHRNXPayloadEmptyCountry.xml");
			requestOrder.payload_hrnx = payloadFinal;

			HRNXWebAccessor webAccessor = new HRNXWebAccessor();
			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));

			// save current last error
			String lastErrorID = dbaccessor.getLatestErrorID(null, location);

			clearstar target = new clearstar();

			Order result = target.processRequest(requestOrder, dbaccessor, webAccessor);

			// Check #1, ensure 1 new error
			List<HRNXv1Error> errorList = dbaccessor.getErrorFromErrorStackSince(lastErrorID,null, location);
			Assert.AreEqual(1, errorList.Count);

			// Check #2, make sure the error is the one we expect
			Assert.AreEqual(rm.GetString("EMPTY_CURRENT_COUNTRY_CODE"), errorList[0].error_short);
			Assert.AreEqual(payloadFinal, errorList[0].error_blob);
			Assert.AreEqual("order/subject/field[@name='address_present_country']", errorList[0].error_blob_2);

			// Check #2, make sure the order returned has proper status
			Assert.AreEqual(OrderStatus.rejected_w_errors, result.status);

			// Check #3, ensure no new qIncoming entry
			List<HRNXQIncomingEntry> qIncomingEntry = dbaccessor.getQIncomingEntriesFromDB(requestOrder, location);
			Assert.AreEqual(qIncomingEntry.Count, 0);
		}
		[TestMethod]
		public void ensureNon200ClearStarWebResponseWillTriggerAnError()
		{
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;
			String txn_id = Guid.NewGuid().ToString();
			//String txn_id = "cee0a983-baae-11e7-a0e7-12b5fc3c4575";
			String service_tp_id = "clearstar2~301";
			String targetURL = "http://localhost/hrnx/git_only/stub/clearstar/request/mockCr1234eateProfile.event";
			String caller = "unit test";

			Order requestOrder = new Order();
			requestOrder.txn_id = txn_id;
			requestOrder.client_group_id = "2569";
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.serviceProviderID, service_tp_id);
			String payloadFinal = "Unit test sample payload";

			HRNXWebAccessor webAccessor = new HRNXWebAccessor();
			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));

			StringBuilder sb = new StringBuilder();

			// save current last error
			String lastErrorID = dbaccessor.getLatestErrorID(null, location);

			clearstar target = new clearstar();

			clearStarResponse parsedResponse = target.sendToClearStar(targetURL, payloadFinal, "unit test soapAction", caller, requestOrder, dbaccessor, webAccessor, sb);

			// Check #1: Make sure the object returned is NULL
			Assert.IsNull(parsedResponse);

			// Check #2: Make sure an error is logged to errorStack
			List<HRNXv1Error> errorList = dbaccessor.getErrorFromErrorStackSince(lastErrorID, null, location);
			Assert.AreEqual(1, errorList.Count);

			// Check #3: Verfiy the proper error is thrown
			Assert.AreEqual(string.Format(rm.GetString("CLEARSTAR_RETURNED_NON_200"), caller), errorList[0].error_short);
		}
		[TestMethod]
		public void ensureClearStarXMLErrorWebResponseWillTriggerAnError()
		{
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;
			String txn_id = Guid.NewGuid().ToString();
			//String txn_id = "cee0a983-baae-11e7-a0e7-12b5fc3c4575";
			String service_tp_id = "clearstar2~301";
			String targetURL = "http://localhost/hrnx/git_only/stub/clearstar/request/mockCreateProfileXMLError.event";
			String caller = "unit test";

			Order requestOrder = new Order();
			requestOrder.txn_id = txn_id;
			requestOrder.client_group_id = "2569";
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.serviceProviderID, service_tp_id);
			String payloadFinal = "Unit test sample payload";

			HRNXWebAccessor webAccessor = new HRNXWebAccessor();
			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));

			StringBuilder sb = new StringBuilder();

			// save current last error
			String lastErrorID = dbaccessor.getLatestErrorID(null, location);

			clearstar target = new clearstar();

			clearStarResponse parsedResponse = target.sendToClearStar(targetURL, payloadFinal, "unit test soapAction", caller, requestOrder, dbaccessor, webAccessor, sb);

			// Check #1: Make sure the object returned is NULL
			Assert.IsNull(parsedResponse);

			// Check #2: Make sure an error is logged to errorStack
			List<HRNXv1Error> errorList = dbaccessor.getErrorFromErrorStackSince(lastErrorID, null, location);
			Assert.AreEqual(1, errorList.Count);

			// Check #3: Verfiy the proper error is thrown
			Assert.AreEqual(string.Format(rm.GetString("FAILED_TO_PARSE_CLEARSTAR_RESPONSE"), caller), errorList[0].error_short);
		}
		[TestMethod]
		public void ensureClearStarErrorWebResponseWillTriggerAnError()
		{
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;
			String txn_id = Guid.NewGuid().ToString();
			//String txn_id = "cee0a983-baae-11e7-a0e7-12b5fc3c4575";
			String service_tp_id = "clearstar2~301";
			String targetURL = "http://localhost/hrnx/git_only/stub/clearstar/request/mockCreateProfileError.event";
			String caller = "unit test";

			Order requestOrder = new Order();
			requestOrder.txn_id = txn_id;
			requestOrder.client_group_id = "2569";
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.serviceProviderID, service_tp_id);
			String payloadFinal = "Unit test sample payload";

			HRNXWebAccessor webAccessor = new HRNXWebAccessor();
			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));

			StringBuilder sb = new StringBuilder();

			// save current last error
			String lastErrorID = dbaccessor.getLatestErrorID(null, location);

			clearstar target = new clearstar();

			clearStarResponse parsedResponse = target.sendToClearStar(targetURL, payloadFinal, "unit test soapAction", caller, requestOrder, dbaccessor, webAccessor, sb);

			// Check #1: Make sure the object returned is NULL
			Assert.IsNull(parsedResponse);

			// Check #2: Make sure an error is logged to errorStack
			List<HRNXv1Error> errorList = dbaccessor.getErrorFromErrorStackSince(lastErrorID, null, location);
			Assert.AreEqual(1, errorList.Count);

			// Check #3: Verfiy the proper error is thrown
			Assert.AreEqual(string.Format(rm.GetString("CLEARSTAR_REPORTED_AN_ERROR"), caller), errorList[0].error_short);
		}
		[TestMethod]
		public void ensureWarningClearStarWebResponseWillTriggerAnErrorButContinues()
		{
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;
			String txn_id = Guid.NewGuid().ToString();
			//String txn_id = "cee0a983-baae-11e7-a0e7-12b5fc3c4575";
			String service_tp_id = "clearstar2~301";
			String targetURL = "http://localhost/hrnx/git_only/stub/clearstar/request/mockCreateProfileWarning.event";
			String caller = "unit test";

			Order requestOrder = new Order();
			requestOrder.txn_id = txn_id;
			requestOrder.client_group_id = "2569";
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.serviceProviderID, service_tp_id);
			String payloadFinal = "Unit test sample payload";

			HRNXWebAccessor webAccessor = new HRNXWebAccessor();
			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));

			StringBuilder sb = new StringBuilder();

			// save current last error
			String lastErrorID = dbaccessor.getLatestErrorID(null, location);

			clearstar target = new clearstar();

			clearStarResponse parsedResponse = target.sendToClearStar(targetURL, payloadFinal, "unit test soapAction", caller, requestOrder, dbaccessor, webAccessor, sb);

			// Check #1: Make sure the object returned is NULL
			Assert.IsNotNull(parsedResponse);

			// Check #2: Make sure an error is logged to errorStack
			List<HRNXv1Error> errorList = dbaccessor.getErrorFromErrorStackSince(lastErrorID, null, location);
			Assert.AreEqual(1, errorList.Count);

			// Check #3: Verfiy the proper error is thrown
			Assert.AreEqual(string.Format(rm.GetString("CLEARSTAR_REPORTED_A_WARNING"), caller), errorList[0].error_short);
		}
		[TestMethod]
		[Ignore]
		public void ensureStatusIsRejectedWErrorsIfAnErrorOccursWithOptionalWebServiceCall()
		{
			//TODO: Have a way to programmatically change ep4 to trigger this. For now, doing this manually, and ignore it in auto-run

			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;
			String txn_id = Guid.NewGuid().ToString();
			txn_id = "cee0a983-baae-11e7-a0e7-12b5fc3c4575";
			String service_tp_id = "clearstar2~301";
			Console.WriteLine(string.Format("txnID is {0}", txn_id));
			String client_group_id = "2569";

			Order requestOrder = new Order();
			requestOrder.txn_id = txn_id;
			requestOrder.client_group_id = client_group_id;
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.serviceProviderID, service_tp_id);
			String payloadFinal = TestUtil.getResource("clearstar2/end2endHRNXPayload.xml");
			requestOrder.payload_hrnx = payloadFinal;

			HRNXWebAccessor webAccessor = new HRNXWebAccessor();
			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));

			// save current last error
			String lastErrorID = dbaccessor.getLatestErrorID(null, location);

			// save current amount of qIncoming entry
			int currentQIncomingSize = dbaccessor.getQIncomingEntriesFromDB(requestOrder, location).Count;

			clearstar target = new clearstar();

			Order result = target.processRequest(requestOrder, dbaccessor, webAccessor);

			// Check #1: Make sure an error is logged to errorStack
			List<HRNXv1Error> errorList = dbaccessor.getErrorFromErrorStackSince(lastErrorID, null, location);
			Assert.AreEqual(1, errorList.Count);

			// Check #2, make sure the order returned has proper status
			Assert.AreEqual(OrderStatus.rejected_w_errors, result.status);

			// Check #3, make sure a new entry has been generated in qIncoming
			List<HRNXQIncomingEntry> qIncomingEntry = dbaccessor.getQIncomingEntriesFromDB(requestOrder, location);
			int newQIncomingSize = dbaccessor.getQIncomingEntriesFromDB(requestOrder, location).Count;
			Assert.AreEqual(currentQIncomingSize+1, newQIncomingSize);

			// Check #3.1, make sure a new entry has proper status
			Assert.AreEqual(OrderStatus.rejected_w_errors, qIncomingEntry[newQIncomingSize-1].status);
		}
		[TestMethod]
		public void ensureProperErrorIfIsNewOrderHasNonBooleanValue()
		{
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;
			String txn_id = Guid.NewGuid().ToString();
			//txn_id = "cee0a983-baae-11e7-a0e7-12b5fc3c4575";
			String service_tp_id = "clearstar2~301";
			Console.WriteLine(string.Format("txnID is {0}", txn_id));
			String client_group_id = "2569";

			Order requestOrder = new Order();
			requestOrder.txn_id = txn_id;
			requestOrder.client_group_id = client_group_id;
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.serviceProviderID, service_tp_id);
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.isNewOrder, "abc");
			String payloadFinal = TestUtil.getResource("clearstar2/end2endHRNXPayload.xml");
			requestOrder.payload_hrnx = payloadFinal;

			HRNXWebAccessor webAccessor = new HRNXWebAccessor();
			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));

			// save current last error
			String lastErrorID = dbaccessor.getLatestErrorID(null, location);

			clearstar target = new clearstar();

			Order result = target.processRequest(requestOrder, dbaccessor, webAccessor);

			// Check #1: Make sure an error is logged to errorStack
			List<HRNXv1Error> errorList = dbaccessor.getErrorFromErrorStackSince(lastErrorID, null, location);
			Assert.AreEqual(1, errorList.Count);

			// Check #2, make sure the error is the appropriate one
			Assert.AreEqual(rm.GetString("CANNOT_DETERMINE_NEW_ORDER"), errorList[0].error_short);

			// Check #2, make sure the order returned has proper status
			Assert.AreEqual(OrderStatus.rejected_w_errors, result.status);
		}
		[TestMethod]
		public void ensureProperErrorIfIsNewOrderIsNotSpecified()
		{
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;
			String txn_id = Guid.NewGuid().ToString();
			//txn_id = "cee0a983-baae-11e7-a0e7-12b5fc3c4575";
			String service_tp_id = "clearstar2~301";
			Console.WriteLine(string.Format("txnID is {0}", txn_id));
			String client_group_id = "2569";

			Order requestOrder = new Order();
			requestOrder.txn_id = txn_id;
			requestOrder.client_group_id = client_group_id;
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.serviceProviderID, service_tp_id);
			String payloadFinal = TestUtil.getResource("clearstar2/end2endHRNXPayload.xml");
			requestOrder.payload_hrnx = payloadFinal;

			HRNXWebAccessor webAccessor = new HRNXWebAccessor();
			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));

			// save current last error
			String lastErrorID = dbaccessor.getLatestErrorID(null, location);

			clearstar target = new clearstar();

			Order result = target.processRequest(requestOrder, dbaccessor, webAccessor);

			// Check #1: Make sure an error is logged to errorStack
			List<HRNXv1Error> errorList = dbaccessor.getErrorFromErrorStackSince(lastErrorID, null, location);
			Assert.AreEqual(1, errorList.Count);

			// Check #2, make sure the error is the appropriate one
			Assert.AreEqual(rm.GetString("NEW_ORDER_KEY_NOT_FOUND"), errorList[0].error_short);

			// Check #2, make sure the order returned has proper status
			Assert.AreEqual(OrderStatus.rejected_w_errors, result.status);
		}
		[TestMethod]
		public void ensureProfileExtractionWorks()
		{
			// Create an order first
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;
			StringBuilder resultBuffer = new StringBuilder();
			HRNXWebAccessor webAccessor = new HRNXWebAccessor();
			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));

			Order result = orderNewCredit(location, webAccessor, dbaccessor);
			String txn_id = result.txn_id;
			String service_tp_id = result.getCurrentServiceProviderID();
			String client_group_id = result.client_group_id;

			// mark the vendorOrderID as complete to simulate when results came in
			Boolean markAsCompleteIsSuccessful = dbaccessor.markVendorOrderIDasCompleted(result, location);
			Assert.IsTrue(markAsCompleteIsSuccessful);

			// save current last error
			String lastErrorID = dbaccessor.getLatestErrorID(null, location);

			// try to get the ProfileID
			String profileID = clearstar.findProfileIDFromTxn(result, dbaccessor, resultBuffer);
			Assert.AreEqual("2011052262218263", profileID);
		}
		[TestMethod]
		public void ensureProperErrorIfProfileExtractionFails()
		{
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;
			StringBuilder resultBuffer = new StringBuilder();
			HRNXWebAccessor webAccessor = new HRNXWebAccessor();
			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));

			// save current last error
			String lastErrorID = dbaccessor.getLatestErrorID(null, location);

			String txn_id = Guid.NewGuid().ToString();
			//txn_id = "cee0a983-baae-11e7-a0e7-12b5fc3c4575";
			String service_tp_id = "clearstar2~301";
			Console.WriteLine(string.Format("txnID is {0}", txn_id));
			String client_group_id = "2569";

			Order requestOrder = new Order();
			requestOrder.txn_id = txn_id;
			requestOrder.client_group_id = client_group_id;
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.serviceProviderID, service_tp_id);
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.isNewOrder, "false");
			String payloadFinal = TestUtil.getResource("clearstar2/end2endHRNXPayload.xml");
			requestOrder.payload_hrnx = payloadFinal;

			clearstar target = new clearstar();
			Order result = target.processRequest(requestOrder, dbaccessor, webAccessor);

			// Check #1: Make sure an error is logged to errorStack
			List<HRNXv1Error> errorList = dbaccessor.getErrorFromErrorStackSince(lastErrorID, null, location);
			Assert.AreEqual(1, errorList.Count);

			// Check #2, make sure the error is the appropriate one
			Assert.AreEqual(rm.GetString("UNABLE_TO_FIND_CLEARSTAR_PROFILE_ID"), errorList[0].error_short);

			// Check #2, make sure the order returned has proper status
			Assert.AreEqual(OrderStatus.rejected_w_errors, result.status);
		}
		[TestMethod]
		public void ensureSuccessWhenOrderingCountyCriminal()
		{
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;
			String txn_id = Guid.NewGuid().ToString();
			String service_tp_id = "clearstar2~301";
			Console.WriteLine(string.Format("txnID is {0}", txn_id));
			String client_group_id = "2569";

			Order requestOrder = new Order();
			requestOrder.txn_id = txn_id;
			requestOrder.client_group_id = client_group_id;
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.serviceProviderID, service_tp_id);
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.isNewOrder, "true");
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.package, "CRCK_00052");
			String payloadFinal = TestUtil.getResource("clearstar2/end2endHRNXPayloadMaxData.xml");
			requestOrder.payload_hrnx = payloadFinal;

			HRNXWebAccessor webAccessor = new HRNXWebAccessor();
			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));

			// save current last error
			String lastErrorID = dbaccessor.getLatestErrorID(null, location);

			clearstar target = new clearstar();

			Order result = target.processRequest(requestOrder, dbaccessor, webAccessor);

			// Check #1, no new errors
			String latestErrorID = dbaccessor.getLatestErrorID(null, location);
			Assert.AreEqual(lastErrorID, latestErrorID);

			// Check #2, make sure the order returned has proper status
			Assert.AreEqual(OrderStatus.processing, result.status);

			List<HRNXQIncomingEntry> qIncomingEntry = dbaccessor.getQIncomingEntriesFromDB(requestOrder, location);
			int index = qIncomingEntry.Count - 2;  //this is because you want to look at the laest one as I maybe changing txn_id in loocal test. However, due to AuthForm you actually want to look at the 2nd last record

			// Check #3, make sure we attempt to AddOrderToProfile
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:AddOrderToProfile ");

			// Check #4, make sure the data is mapped properly
			//StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"30753\" Value=\"USA\"");
			//StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"30754\" Value=\"WA\"");
			//StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"30755\" Value=\"King\"");
			//StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"30756\" Value=\"Seattle\"");
			//StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"30757\" Value=\"98101\"");

			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sCountry>USA</prof:sCountry>");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sState>WA</prof:sState>");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sCounty>King</prof:sCounty>");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sCity>Seattle</prof:sCity>");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sZip>98101</prof:sZip>");
		}
		[TestMethod]
		public void ensureSuccessWhenOrderingFederalCriminal()
		{
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;
			String txn_id = Guid.NewGuid().ToString();
			String service_tp_id = "clearstar2~301";
			Console.WriteLine(string.Format("txnID is {0}", txn_id));
			String client_group_id = "2569";

			Order requestOrder = new Order();
			requestOrder.txn_id = txn_id;
			requestOrder.client_group_id = client_group_id;
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.serviceProviderID, service_tp_id);
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.isNewOrder, "true");
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.package, "CRCK_00022");
			String payloadFinal = TestUtil.getResource("clearstar2/end2endHRNXPayloadMaxData.xml");
			requestOrder.payload_hrnx = payloadFinal;

			HRNXWebAccessor webAccessor = new HRNXWebAccessor();
			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));

			// save current last error
			String lastErrorID = dbaccessor.getLatestErrorID(null, location);

			clearstar target = new clearstar();

			Order result = target.processRequest(requestOrder, dbaccessor, webAccessor);

			// Check #1, no new errors
			String latestErrorID = dbaccessor.getLatestErrorID(null, location);
			Assert.AreEqual(lastErrorID, latestErrorID);

			// Check #2, make sure the order returned has proper status
			Assert.AreEqual(OrderStatus.processing, result.status);

			List<HRNXQIncomingEntry> qIncomingEntry = dbaccessor.getQIncomingEntriesFromDB(requestOrder, location);
			int index = qIncomingEntry.Count - 2;  //this is because you want to look at the laest one as I maybe changing txn_id in loocal test. However, due to AuthForm you actually want to look at the 2nd last record

			// Check #3, make sure we attempt to AddOrderToProfile
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:AddOrderToProfile ");

			// Check #4, make sure the data is mapped properly
			//StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"30664\" Value=\"USA\"");
			//StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"30665\" Value=\"WA\"");
			//StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"30666\" Value=\"King\"");
			//StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"30667\" Value=\"Seattle\"");
			//StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"30668\" Value=\"98101\"");

			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sCountry>USA</prof:sCountry>");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sState>WA</prof:sState>");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sCounty>King</prof:sCounty>");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sCity>Seattle</prof:sCity>");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sZip>98101</prof:sZip>");
		}
		[TestMethod]
		public void ensureSuccessWhenOrderingAdverseAction()
		{
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;
			String txn_id = Guid.NewGuid().ToString();
			String service_tp_id = "clearstar2~301";
			Console.WriteLine(string.Format("txnID is {0}", txn_id));
			String client_group_id = "2569";

			Order requestOrder = new Order();
			requestOrder.txn_id = txn_id;
			requestOrder.client_group_id = client_group_id;
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.serviceProviderID, service_tp_id);
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.isNewOrder, "true");
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.package, "CRCK_00665");
			String payloadFinal = TestUtil.getResource("clearstar2/end2endHRNXPayloadMaxData.xml");
			requestOrder.payload_hrnx = payloadFinal;

			HRNXWebAccessor webAccessor = new HRNXWebAccessor();
			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));

			// save current last error
			String lastErrorID = dbaccessor.getLatestErrorID(null, location);

			clearstar target = new clearstar();

			Order result = target.processRequest(requestOrder, dbaccessor, webAccessor);

			// Check #1, no new errors
			String latestErrorID = dbaccessor.getLatestErrorID(null, location);
			Assert.AreEqual(lastErrorID, latestErrorID);

			// Check #2, make sure the order returned has proper status
			Assert.AreEqual(OrderStatus.processing, result.status);

			List<HRNXQIncomingEntry> qIncomingEntry = dbaccessor.getQIncomingEntriesFromDB(requestOrder, location);
			int index = qIncomingEntry.Count - 2;  //this is because you want to look at the laest one as I maybe changing txn_id in loocal test. However, due to AuthForm you actually want to look at the 2nd last record

			// Check #3, make sure we attempt to AddOrderToProfile
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:AddOrderToProfile ");

			// Check #4, make sure the data is mapped properly
			//StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"211896\" Value=\"spapptesting@gmail.com\"");
			//StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"211897\" Value=\"37Admn@dvcr.com\"");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sCountry>spapptesting@gmail.com</prof:sCountry>");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sState>37Admn@dvcr.com</prof:sState>");
		}
		[TestMethod]
		public void ensureSuccessWhenOrderingMVR()
		{
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;
			String txn_id = Guid.NewGuid().ToString();
			String service_tp_id = "clearstar2~301";
			Console.WriteLine(string.Format("txnID is {0}", txn_id));
			String client_group_id = "2569";

			Order requestOrder = new Order();
			requestOrder.txn_id = txn_id;
			requestOrder.client_group_id = client_group_id;
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.serviceProviderID, service_tp_id);
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.isNewOrder, "true");
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.package, "CRCK_00024");
			String payloadFinal = TestUtil.getResource("clearstar2/end2endHRNXPayloadMaxData.xml");
			requestOrder.payload_hrnx = payloadFinal;

			HRNXWebAccessor webAccessor = new HRNXWebAccessor();
			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));

			// save current last error
			String lastErrorID = dbaccessor.getLatestErrorID(null, location);

			clearstar target = new clearstar();

			Order result = target.processRequest(requestOrder, dbaccessor, webAccessor);

			// Check #1, no new errors
			String latestErrorID = dbaccessor.getLatestErrorID(null, location);
			Assert.AreEqual(lastErrorID, latestErrorID);

			// Check #2, make sure the order returned has proper status
			Assert.AreEqual(OrderStatus.processing, result.status);

			List<HRNXQIncomingEntry> qIncomingEntry = dbaccessor.getQIncomingEntriesFromDB(requestOrder, location);
			int index = qIncomingEntry.Count - 2;  //this is because you want to look at the laest one as I maybe changing txn_id in loocal test. However, due to AuthForm you actually want to look at the 2nd last record

			// Check #3, make sure we attempt to AddOrderToProfile
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:AddOrderToProfile ");

			// Check #4, make sure the data is mapped properly
			//StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"30676\" Value=\"NJ\"");
			//StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"30677\" Value=\"A1111-11111-11111\"");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"30679\" Value=\"male\"");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sState>NJ</prof:sState>");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sCity>A1111-11111-11111</prof:sCity>");
		}
		[TestMethod]
		public void ensureSuccessWhenOrderingEducation()
		{
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;
			String txn_id = Guid.NewGuid().ToString();
			String service_tp_id = "clearstar2~301";
			Console.WriteLine(string.Format("txnID is {0}", txn_id));
			String client_group_id = "2569";

			Order requestOrder = new Order();
			requestOrder.txn_id = txn_id;
			requestOrder.client_group_id = client_group_id;
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.serviceProviderID, service_tp_id);
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.isNewOrder, "true");
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.package, "CRCK_00012");
			String payloadFinal = TestUtil.getResource("clearstar2/end2endHRNXPayloadMaxData.xml");
			requestOrder.payload_hrnx = payloadFinal;

			HRNXWebAccessor webAccessor = new HRNXWebAccessor();
			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));

			// save current last error
			String lastErrorID = dbaccessor.getLatestErrorID(null, location);

			clearstar target = new clearstar();

			Order result = target.processRequest(requestOrder, dbaccessor, webAccessor);

			// Check #1, no new errors
			String latestErrorID = dbaccessor.getLatestErrorID(null, location);
			Assert.AreEqual(lastErrorID, latestErrorID);

			// Check #2, make sure the order returned has proper status
			Assert.AreEqual(OrderStatus.processing, result.status);

			List<HRNXQIncomingEntry> qIncomingEntry = dbaccessor.getQIncomingEntriesFromDB(requestOrder, location);
			int index = qIncomingEntry.Count - 2;  //this is because you want to look at the laest one as I maybe changing txn_id in loocal test. However, due to AuthForm you actually want to look at the 2nd last record

			// Check #3, make sure we attempt to AddOrderToProfile
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:AddOrderToProfile ");

			// Check #4, make sure the data is mapped properly
			//StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"30576\" Value=\"Brookdale Community College\"");
			//StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"30577\" Value=\"Lincroft\"");
			//StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"30578\" Value=\"NJ\"");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"30582\" Value=\"01/02/1990 to 11/02/2010\"");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"30583\" Value=\"A.A.S\"");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"30584\" Value=\"Business Management\"");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"30585\" Value=\"(456)555-4837\"");

			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sCountry>Brookdale Community College</prof:sCountry>");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sState>Lincroft</prof:sState>");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sCounty>NJ</prof:sCounty>");

			// Check #5, make sure there are 2 AddOrderToProfile because there are 1 education, dispite having 2 education input because we have serviceMax defined
			int count = new Regex(Regex.Escape("<prof:AddOrderToProfile ")).Matches(qIncomingEntry[index].payload_original).Count;
			Assert.AreEqual(1, count);
		}
		[TestMethod]
		public void ensureSuccessWhenOrderingEmployment()
		{
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;
			String txn_id = Guid.NewGuid().ToString();
			String service_tp_id = "clearstar2~301";
			Console.WriteLine(string.Format("txnID is {0}", txn_id));
			String client_group_id = "2569";

			Order requestOrder = new Order();
			requestOrder.txn_id = txn_id;
			requestOrder.client_group_id = client_group_id;
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.serviceProviderID, service_tp_id);
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.isNewOrder, "true");
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.package, "CRCK_00017");
			String payloadFinal = TestUtil.getResource("clearstar2/end2endHRNXPayloadMaxData.xml");
			requestOrder.payload_hrnx = payloadFinal;

			HRNXWebAccessor webAccessor = new HRNXWebAccessor();
			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));

			// save current last error
			String lastErrorID = dbaccessor.getLatestErrorID(null, location);

			clearstar target = new clearstar();

			Order result = target.processRequest(requestOrder, dbaccessor, webAccessor);

			// Check #1, no new errors
			String latestErrorID = dbaccessor.getLatestErrorID(null, location);
			Assert.AreEqual(lastErrorID, latestErrorID);

			// Check #2, make sure the order returned has proper status
			Assert.AreEqual(OrderStatus.processing, result.status);

			List<HRNXQIncomingEntry> qIncomingEntry = dbaccessor.getQIncomingEntriesFromDB(requestOrder, location);
			int index = qIncomingEntry.Count - 2;  //this is because you want to look at the laest one as I maybe changing txn_id in loocal test. However, due to AuthForm you actually want to look at the 2nd last record

			// Check #3, make sure we attempt to AddOrderToProfile
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:AddOrderToProfile ");

			// Check #4, make sure the data is mapped properly
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sCountry>DBMI</prof:sCountry>");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sState>Henry Test</prof:sState>");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sCounty>Edison</prof:sCounty>");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sCity>NJ</prof:sCity>");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sZip>604-878-1234</prof:sZip>");

			//StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"30631\" Value=\"DBMI\"");
			//StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"30632\" Value=\"Henry Test\"");
			//StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"30633\" Value=\"Edison\"");
			//StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"30634\" Value=\"NJ\"");
			//StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"30635\" Value=\"604-878-1234\"");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"30636\" Value=\"01/01/2007\"");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"30637\" Value=\"01/05/2009\"");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"30638\" Value=\"Marketing Manager\"");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"30639\" Value=\"90.0\"");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"30641\" Value=\"(456)555-4837\"");

			// Check #5, make sure there are 2 AddOrderToProfile because there are 2 employment MAX, but 3 employment input
			int count = new Regex(Regex.Escape("<prof:AddOrderToProfile ")).Matches(qIncomingEntry[index].payload_original).Count;
			Assert.AreEqual(2, count);
		}
		[TestMethod]
		public void ensureSuccessWhenOrderingPackage()
		{
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;
			String txn_id = Guid.NewGuid().ToString();
			String service_tp_id = "clearstar2~301";
			Console.WriteLine(string.Format("txnID is {0}", txn_id));
			String client_group_id = "2569";

			Order requestOrder = new Order();
			requestOrder.txn_id = txn_id;
			requestOrder.client_group_id = client_group_id;
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.serviceProviderID, service_tp_id);
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.isNewOrder, "true");
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.package, "CRCK_01148");
			String payloadFinal = TestUtil.getResource("clearstar2/end2endHRNXPayloadMaxData.xml");
			requestOrder.payload_hrnx = payloadFinal;

			HRNXWebAccessor webAccessor = new HRNXWebAccessor();
			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));

			// save current last error
			String lastErrorID = dbaccessor.getLatestErrorID(null, location);

			clearstar target = new clearstar();

			Order result = target.processRequest(requestOrder, dbaccessor, webAccessor);

			// Check #1, no new errors
			String latestErrorID = dbaccessor.getLatestErrorID(null, location);
			Assert.AreEqual(lastErrorID, latestErrorID);

			// Check #2, make sure the order returned has proper status
			Assert.AreEqual(OrderStatus.processing, result.status);

			List<HRNXQIncomingEntry> qIncomingEntry = dbaccessor.getQIncomingEntriesFromDB(requestOrder, location);
			int index = qIncomingEntry.Count - 2;  //this is because you want to look at the laest one as I maybe changing txn_id in loocal test. However, due to AuthForm you actually want to look at the 2nd last record

			// Check #3, make sure we attempt to add data for the services with additional data required
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sServiceNo>CRCK_00012</prof:sServiceNo>");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sServiceNo>CRCK_00017</prof:sServiceNo>");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sServiceNo>CRCK_00022</prof:sServiceNo>");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sServiceNo>CRCK_00052</prof:sServiceNo>");
		}
		[TestMethod]
		public void clearStar2EnsureSuccessWhenOrderingCountyCriminalWithoutCountyFromATS()
		{
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;
			String txn_id = Guid.NewGuid().ToString();
			String service_tp_id = "clearstar2~301";
			Console.WriteLine(string.Format("txnID is {0}", txn_id));
			String client_group_id = "2569";

			Order requestOrder = new Order();
			requestOrder.txn_id = txn_id;
			requestOrder.client_group_id = client_group_id;
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.serviceProviderID, service_tp_id);
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.isNewOrder, "true");
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.package, "CRCK_00052");
			String payloadFinal = TestUtil.getResource("clearstar2/end2endHRNXPayloadForCriminalWithoutCounty.xml");
			requestOrder.payload_hrnx = payloadFinal;

			HRNXWebAccessor webAccessor = new HRNXWebAccessor();
			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));

			// save current last error
			String lastErrorID = dbaccessor.getLatestErrorID(null, location);

			clearstar target = new clearstar();

			Order result = target.processRequest(requestOrder, dbaccessor, webAccessor);

			// Check #1, no new errors
			String latestErrorID = dbaccessor.getLatestErrorID(null, location);
			Assert.AreEqual(lastErrorID, latestErrorID);

			// Check #2, make sure the order returned has proper status
			Assert.AreEqual(OrderStatus.processing, result.status);

			List<HRNXQIncomingEntry> qIncomingEntry = dbaccessor.getQIncomingEntriesFromDB(requestOrder, location);
			int index = qIncomingEntry.Count - 1;  //this is because you want to look at the laest one as I maybe changing txn_id in loocal test

			// Check #3, make sure we attempt to AddOrderToProfile
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:AddOrderToProfile ");

			// Check #4, make sure the data is mapped properly
			//StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"30753\" Value=\"USA\"");
			//StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"30754\" Value=\"WA\"");
			//StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"30755\" Value=\"King\"");
			//StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"30756\" Value=\"Seattle\"");
			//StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"30757\" Value=\"98007\"");

			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sCountry>USA</prof:sCountry>");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sState>WA</prof:sState>");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sCounty>King</prof:sCounty>");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sCity>Seattle</prof:sCity>");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sZip>98007</prof:sZip>");
		}
		[TestMethod]
		public void ensureSuccessWhenOrderingPackageWithNoEducationNoEmployment()
		{
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;
			String txn_id = Guid.NewGuid().ToString();
			String service_tp_id = "clearstar2~301";
			Console.WriteLine(string.Format("txnID is {0}", txn_id));
			String client_group_id = "2569";

			Order requestOrder = new Order();
			requestOrder.txn_id = txn_id;
			requestOrder.client_group_id = client_group_id;
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.serviceProviderID, service_tp_id);
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.isNewOrder, "true");
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.package, "CRCK_01148");
			String payloadFinal = TestUtil.getResource("clearstar2/end2endHRNXPayloadNoEduNoEmp.xml");
			requestOrder.payload_hrnx = payloadFinal;

			HRNXWebAccessor webAccessor = new HRNXWebAccessor();
			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));

			// save current last error
			String lastErrorID = dbaccessor.getLatestErrorID(null, location);

			clearstar target = new clearstar();

			Order result = target.processRequest(requestOrder, dbaccessor, webAccessor);

			// Check #1, no new errors
			String latestErrorID = dbaccessor.getLatestErrorID(null, location);
			Assert.AreEqual(lastErrorID, latestErrorID);

			// Check #2, make sure the order returned has proper status
			Assert.AreEqual(OrderStatus.processing, result.status);

			List<HRNXQIncomingEntry> qIncomingEntry = dbaccessor.getQIncomingEntriesFromDB(requestOrder, location);
			int index = qIncomingEntry.Count - 2;  //this is because you want to look at the laest one as I maybe changing txn_id in loocal test. However, due to AuthForm you actually want to look at the 2nd last record

			// Check #3, make sure we attempt to add data for the services with additional data required
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sServiceNo>CRCK_00022</prof:sServiceNo>");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sServiceNo>CRCK_00052</prof:sServiceNo>");

			// Check #4, make sure we won't send edu or emp if there are less than the serviceMax amount to send
			Assert.IsFalse(qIncomingEntry[index].payload_original.Contains("<prof:sServiceNo>CRCK_00012</prof:sServiceNo>"));
			Assert.IsFalse(qIncomingEntry[index].payload_original.Contains("<prof:sServiceNo>CRCK_00017</prof:sServiceNo>"));
		}
		[TestMethod]
		public void ensureSuccessWhenOrderingLicense()
		{
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;
			String txn_id = Guid.NewGuid().ToString();
			String service_tp_id = "clearstar2~301";
			Console.WriteLine(string.Format("txnID is {0}", txn_id));
			String client_group_id = "9078";

			Order requestOrder = new Order();
			requestOrder.txn_id = txn_id;
			requestOrder.client_group_id = client_group_id;
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.serviceProviderID, service_tp_id);
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.isNewOrder, "true");
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.package, "CRCK_00032");
			String payloadFinal = TestUtil.getResource("clearstar2/end2endHRNXPayloadMaxData.xml");
			requestOrder.payload_hrnx = payloadFinal;

			HRNXWebAccessor webAccessor = new HRNXWebAccessor();
			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));

			// save current last error
			String lastErrorID = dbaccessor.getLatestErrorID(null, location);

			clearstar target = new clearstar();

			Order result = target.processRequest(requestOrder, dbaccessor, webAccessor);

			// Check #1, no new errors
			String latestErrorID = dbaccessor.getLatestErrorID(null, location);
			Assert.AreEqual(lastErrorID, latestErrorID);

			// Check #2, make sure the order returned has proper status
			Assert.AreEqual(OrderStatus.processing, result.status);

			List<HRNXQIncomingEntry> qIncomingEntry = dbaccessor.getQIncomingEntriesFromDB(requestOrder, location);
			int index = qIncomingEntry.Count - 2;  //this is because you want to look at the laest one as I maybe changing txn_id in loocal test. However, due to AuthForm you actually want to look at the 2nd last record

			// Check #3, make sure we attempt to AddOrderToProfile
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:AddOrderToProfile ");

			// Check #4, make sure the data is mapped properly
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sCountry>NJ</prof:sCountry>");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sState>Test License</prof:sState>");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sCity>123</prof:sCity>");

			// Check #4, make sure MULTIPLE data are getting mapped as well
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sCountry>WA</prof:sCountry>");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sState>Nursing Cert</prof:sState>");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sCity>ABC123</prof:sCity>");
		}
		/// <summary>
		/// This requires NCI which uses the new service
		/// </summary>
		[TestMethod]
		public void ensureSuccessWhenOrderingEducationLv3()
		{
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;
			String txn_id = Guid.NewGuid().ToString();
			String service_tp_id = "clearstar2~301";
			Console.WriteLine(string.Format("txnID is {0}", txn_id));
			String client_group_id = "9078";

			Order requestOrder = new Order();
			requestOrder.txn_id = txn_id;
			requestOrder.client_group_id = client_group_id;
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.serviceProviderID, service_tp_id);
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.isNewOrder, "true");
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.package, "CRCK_00622");
			String payloadFinal = TestUtil.getResource("clearstar2/end2endHRNXPayloadMaxData.xml");
			requestOrder.payload_hrnx = payloadFinal;

			HRNXWebAccessor webAccessor = new HRNXWebAccessor();
			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));

			// save current last error
			String lastErrorID = dbaccessor.getLatestErrorID(null, location);

			clearstar target = new clearstar();

			Order result = target.processRequest(requestOrder, dbaccessor, webAccessor);

			// Check #1, no new errors
			String latestErrorID = dbaccessor.getLatestErrorID(null, location);
			Assert.AreEqual(lastErrorID, latestErrorID);

			// Check #2, make sure the order returned has proper status
			Assert.AreEqual(OrderStatus.processing, result.status);

			List<HRNXQIncomingEntry> qIncomingEntry = dbaccessor.getQIncomingEntriesFromDB(requestOrder, location);
			int index = qIncomingEntry.Count - 2;  //this is because you want to look at the laest one as I maybe changing txn_id in loocal test. However, due to authForm data, we actually need to go back 1 more record than just changing from length to index

			// Check #3, make sure we attempt to AddOrderToProfile
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:AddOrderToProfile ");

			// Check #4, make sure the data is mapped properly
			StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"195047\" Value=\"01/02/1990 to 11/02/2010\"");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"195048\" Value=\"A.A.S\"");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"195049\" Value=\"Business Management\"");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"195050\" Value=\"(456)555-4837\"");

			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sCountry>Brookdale Community College</prof:sCountry>");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sState>Lincroft</prof:sState>");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sCounty>NJ</prof:sCounty>");

			// Check #5, make sure there are 2 AddOrderToProfile because there are NCI does NOT have a serviceMax for education
			int count = new Regex(Regex.Escape("<prof:AddOrderToProfile ")).Matches(qIncomingEntry[index].payload_original).Count;
			Assert.AreEqual(2, count);
		}
		/// <summary>
		/// This requires NCI which uses the new service
		/// </summary>
		[TestMethod]
		public void ensureSuccessWhenOrderingEmploymentLv3()
		{
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;
			String txn_id = Guid.NewGuid().ToString();
			String service_tp_id = "clearstar2~301";
			Console.WriteLine(string.Format("txnID is {0}", txn_id));
			String client_group_id = "9078";

			Order requestOrder = new Order();
			requestOrder.txn_id = txn_id;
			requestOrder.client_group_id = client_group_id;
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.serviceProviderID, service_tp_id);
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.isNewOrder, "true");
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.package, "CRCK_00624");
			String payloadFinal = TestUtil.getResource("clearstar2/end2endHRNXPayloadMaxData.xml");
			requestOrder.payload_hrnx = payloadFinal;

			HRNXWebAccessor webAccessor = new HRNXWebAccessor();
			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));

			// save current last error
			String lastErrorID = dbaccessor.getLatestErrorID(null, location);

			clearstar target = new clearstar();

			Order result = target.processRequest(requestOrder, dbaccessor, webAccessor);

			// Check #1, no new errors
			String latestErrorID = dbaccessor.getLatestErrorID(null, location);
			Assert.AreEqual(lastErrorID, latestErrorID);

			// Check #2, make sure the order returned has proper status
			Assert.AreEqual(OrderStatus.processing, result.status);

			List<HRNXQIncomingEntry> qIncomingEntry = dbaccessor.getQIncomingEntriesFromDB(requestOrder, location);
			int index = qIncomingEntry.Count - 2;  //this is because you want to look at the laest one as I maybe changing txn_id in loocal test. However, due to authForm data, we actually need to go back 1 more record than just changing from length to index

			// Check #3, make sure we attempt to AddOrderToProfile
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:AddOrderToProfile ");

			// Check #4, make sure the data is mapped properly
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sCountry>DBMI</prof:sCountry>");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sState>Henry Test</prof:sState>");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sCounty>Edison</prof:sCounty>");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sCity>NJ</prof:sCity>");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sZip>604-878-1234</prof:sZip>");

			StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"195083\" Value=\"01/01/2007\"");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"195084\" Value=\"01/05/2009\"");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"195085\" Value=\"Marketing Manager\"");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"195086\" Value=\"90.0\"");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"195088\" Value=\"(456)555-4837\"");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "FieldID=\"195089\" Value=\"Laid Off\"");

			// Check #5, make sure there are 2 AddOrderToProfile because there are NCI does NOT have a serviceMax for education
			int count = new Regex(Regex.Escape("<prof:AddOrderToProfile ")).Matches(qIncomingEntry[index].payload_original).Count;
			Assert.AreEqual(3, count);
		}
		/// <summary>
		/// This requires NCI which uses the new service
		/// </summary>
		[TestMethod]
		public void ensureSuccessWhenOrderingReferenceLv3()
		{
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;
			String txn_id = Guid.NewGuid().ToString();
			String service_tp_id = "clearstar2~301";
			Console.WriteLine(string.Format("txnID is {0}", txn_id));
			String client_group_id = "9078";

			Order requestOrder = new Order();
			requestOrder.txn_id = txn_id;
			requestOrder.client_group_id = client_group_id;
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.serviceProviderID, service_tp_id);
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.isNewOrder, "true");
			requestOrder.additionalData.Add(OrderAdditionalDataKeys.package, "CRCK_00623");
			String payloadFinal = TestUtil.getResource("clearstar2/end2endHRNXPayloadMaxData.xml");
			requestOrder.payload_hrnx = payloadFinal;

			HRNXWebAccessor webAccessor = new HRNXWebAccessor();
			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));

			// save current last error
			String lastErrorID = dbaccessor.getLatestErrorID(null, location);

			clearstar target = new clearstar();

			Order result = target.processRequest(requestOrder, dbaccessor, webAccessor);

			// Check #1, no new errors
			String latestErrorID = dbaccessor.getLatestErrorID(null, location);
			Assert.AreEqual(lastErrorID, latestErrorID);

			// Check #2, make sure the order returned has proper status
			Assert.AreEqual(OrderStatus.processing, result.status);

			List<HRNXQIncomingEntry> qIncomingEntry = dbaccessor.getQIncomingEntriesFromDB(requestOrder, location);
			int index = qIncomingEntry.Count - 2;  //this is because you want to look at the laest one as I maybe changing txn_id in loocal test. However, due to authForm data, we actually need to go back 1 more record than just changing from length to index

			// Check #3, make sure we attempt to AddOrderToProfile
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:AddOrderToProfile ");

			// Check #4, make sure the data is mapped properly
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sCountry>Mike TestCandidate</prof:sCountry>");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sState>732-555-5555</prof:sState>");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sCounty>NJ</prof:sCounty>");
			StringAssert.Contains(qIncomingEntry[index].payload_original, "<prof:sCity>(456)555-4837</prof:sCity>");

			// Check #5, make sure there are 1 AddOrderToProfile because there is only one reference in the test data
			int count = new Regex(Regex.Escape("<prof:AddOrderToProfile ")).Matches(qIncomingEntry[index].payload_original).Count;
			Assert.AreEqual(1, count);
		}
	}
}
