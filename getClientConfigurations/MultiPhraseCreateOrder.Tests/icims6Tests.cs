﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using getClientConfigurations.Models;
using System.IO;
using System.Net;
using System.Collections.Generic;
using System.Resources;
using System.Data.Odbc;
using getClientConfigurations.ATSHandlers;
using MultiPhraseCreateOrder.Tests.Mocks;
using System.Text.RegularExpressions;
using System.Xml;
using System.Text;

namespace MultiPhraseCreateOrder.Tests
{
    [TestClass]
    public class icims6Tests
    {
        private ResourceManager rmUnitTest = new ResourceManager("MultiPhraseCreateOrder.Tests.Properties.Resources", typeof(HRNXUtilTests).Assembly);
        private ResourceManager rm = new ResourceManager("getClientConfigurations.Properties.HRNX", typeof(HRNXDBAccessor).Assembly);

        [TestMethod]
        public void Icims6EnsureDataFieldsAreExtractedProperly()
        {
			String candidateID = Guid.NewGuid().ToString();

            String testResourcePath = "Resources/orderRequestCredit.json";
            Assert.IsTrue(File.Exists(testResourcePath));
            String payload = File.ReadAllText(testResourcePath);

			String expectedAuthFormData =  TestUtil.getResource("icims6/icimsAuthFormExpectedBase64String.txt");

			// Replace the candidateID, so will always have a unique candidate per run
			payload = payload.Replace("45392", candidateID);

            Order blankOrder = new Order();

            String ip = "127.0.0.1 (127.0.0.1)";
            String package = "CRCK_00193";

            HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
            MockWebAccessor webaccessor = new MockWebAccessor();
            webaccessor.ip = ip;


            // save current txn_id
            String oldTxnID = dbaccessor.getLatestOrderTxnID(blankOrder, "ensureDataFieldsAreExtractedProperly");
            String lastErrorID = dbaccessor.getLatestErrorID(blankOrder, "ensureDataFieldsAreExtractedProperly");

            OrderRequest request = new OrderRequest();
            request.rawPayload = payload;

            IATSHandler testClient = new icims6();
            Order result = testClient.processRequest(request, dbaccessor, webaccessor);

			// Check #1, make sure a new record is created in qProcessing
			String newTxnID = dbaccessor.getLatestOrderTxnID(result, "ensureDataFieldsAreExtractedProperly");
			Assert.AreNotEqual(oldTxnID, newTxnID);

			// Check #2, make sure no errors are generated
			String newErrorID = dbaccessor.getLatestErrorID(result, "ensureDataFieldsAreExtractedProperly");
			Assert.AreEqual(lastErrorID, newErrorID);

			// Check #3, make sure data returned are proper
			Assert.AreEqual("2569",result.client_group_id);
            Assert.AreEqual("groupAuth",result.client_user_id);
            Assert.AreEqual(OrderStatus.passthru, result.status);
            Assert.AreEqual(candidateID,result.candidate_id);
            Assert.AreEqual(icims6.ICIMS6_LOCATION_NAME,result.createOrderHandlerName);
            Assert.AreEqual(result.orderingIP, ip);
            Assert.AreEqual(result.additionalData[OrderAdditionalDataKeys.package], package);

            // Check # 4, check the XML content
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(result.payload_hrnx);

            Assert.AreEqual(newTxnID, xmlDoc.SelectSingleNode("order/txn_id").InnerText);
            Assert.AreEqual(result.client_group_id, xmlDoc.SelectSingleNode("order/client_group_id").InnerText);
            Assert.AreEqual("icims6", xmlDoc.SelectSingleNode("order/tp_id").InnerText);
            Assert.AreEqual(result.candidate_id, xmlDoc.SelectSingleNode("order/tp_uqid").InnerText);
            Assert.AreEqual("58278", xmlDoc.SelectSingleNode("order/quoteback[@id='2']").InnerText);
            Assert.AreEqual("http://localhost/hrnx/git_only/stub/icims1/mockICIMSUpdate.event", xmlDoc.SelectSingleNode("order/workflowDetailsURL").InnerText);
            Assert.AreEqual("5155", xmlDoc.SelectSingleNode("order/requisitionId").InnerText);
			Assert.AreEqual("37Admn@dvcr.com", xmlDoc.SelectSingleNode("order/hrnxRequestorEmail").InnerText);
			Assert.AreEqual("Brandon", xmlDoc.SelectSingleNode("order/subject/field[@name='firstname']").InnerText);
            Assert.AreEqual("TestCandidate", xmlDoc.SelectSingleNode("order/subject/field[@name='lastname']").InnerText);
            Assert.AreEqual("spapptesting@gmail.com", xmlDoc.SelectSingleNode("order/subject/field[@name='email']").InnerText);
			Assert.AreEqual("123-45-6789", xmlDoc.SelectSingleNode("order/subject/field[@name='ssn']").InnerText);
			Assert.AreEqual("male", xmlDoc.SelectSingleNode("order/subject/field[@name='gender2']").InnerText);
			Assert.AreEqual("01/02/1990", xmlDoc.SelectSingleNode("order/subject/field[@name='dob']").InnerText);
			Assert.AreEqual("32 Supersonic Way", xmlDoc.SelectSingleNode("order/subject/field[@name='address_present_line1']").InnerText);
			Assert.AreEqual("Seattle", xmlDoc.SelectSingleNode("order/subject/field[@name='address_present_city']").InnerText);
			Assert.AreEqual("WA", xmlDoc.SelectSingleNode("order/subject/field[@name='address_present_state']").InnerText);
			Assert.AreEqual("98101", xmlDoc.SelectSingleNode("order/subject/field[@name='address_present_zip']").InnerText);
			Assert.AreEqual("US", xmlDoc.SelectSingleNode("order/subject/field[@name='address_present_country']").InnerText);
			Assert.AreEqual("King", xmlDoc.SelectSingleNode("order/subject/field[@name='address_present_county']").InnerText);
			Assert.AreEqual("(456)555-4837", xmlDoc.SelectSingleNode("order/subject/field[@name='bestphone']").InnerText);
			Assert.AreEqual(expectedAuthFormData, xmlDoc.SelectSingleNode("order/eForm[@type='Authorization']").InnerText);

			Assert.AreEqual("clearstar2~301", xmlDoc.SelectSingleNode("order/services/service[@type='passthru']/@tp").InnerText);
            Assert.AreEqual(package, xmlDoc.SelectSingleNode("order/services/service[@type='passthru'][@tp='clearstar2~301']/field[@name='package'][@type='value']").InnerText);

			Assert.AreEqual("A1111-11111-11111", xmlDoc.SelectSingleNode("order/services/service[@name='mvr'][1]/field[@name='dlnumber']").InnerText);
			Assert.AreEqual("NJ", xmlDoc.SelectSingleNode("order/services/service[@name='mvr'][1]/field[@name='dlstate']").InnerText);

			Assert.AreEqual("Brookdale Community College", xmlDoc.SelectSingleNode("order/services/service[@name='education'][1]/field[@name='schoolName']").InnerText);
			Assert.AreEqual("Lincroft", xmlDoc.SelectSingleNode("order/services/service[@name='education'][1]/field[@name='schoolCity']").InnerText);
			Assert.AreEqual("US", xmlDoc.SelectSingleNode("order/services/service[@name='education'][1]/field[@name='schoolCountry']").InnerText);
			Assert.AreEqual("NJ", xmlDoc.SelectSingleNode("order/services/service[@name='education'][1]/field[@name='schoolState']").InnerText);
			Assert.AreEqual("A.A.S", xmlDoc.SelectSingleNode("order/services/service[@name='education'][1]/field[@name='degreeName']").InnerText);
			Assert.AreEqual("Business Management", xmlDoc.SelectSingleNode("order/services/service[@name='education'][1]/field[@name='major']").InnerText);
			Assert.AreEqual("01/02/1990", xmlDoc.SelectSingleNode("order/services/service[@name='education'][1]/field[@name='schoolStart_mmddyyyy']").InnerText);
			Assert.AreEqual("11/02/2010", xmlDoc.SelectSingleNode("order/services/service[@name='education'][1]/field[@name='graduationDate_mmddyyyy']").InnerText);

			Assert.AreEqual("DBMI", xmlDoc.SelectSingleNode("order/services/service[@name='employment'][1]/field[@name='companyName']").InnerText);
			Assert.AreEqual("Edison", xmlDoc.SelectSingleNode("order/services/service[@name='employment'][1]/field[@name='company_city']").InnerText);
			Assert.AreEqual("NJ", xmlDoc.SelectSingleNode("order/services/service[@name='employment'][1]/field[@name='company_state']").InnerText);
			Assert.AreEqual("01/01/2007", xmlDoc.SelectSingleNode("order/services/service[@name='employment'][1]/field[@name='startDate']").InnerText);
			Assert.AreEqual("01/05/2009", xmlDoc.SelectSingleNode("order/services/service[@name='employment'][1]/field[@name='endDate']").InnerText);
			Assert.AreEqual("Marketing Manager", xmlDoc.SelectSingleNode("order/services/service[@name='employment'][1]/field[@name='position']").InnerText);
			Assert.AreEqual("false", xmlDoc.SelectSingleNode("order/services/service[@name='employment'][1]/field[@name='permissionToContact']").InnerText);
			Assert.AreEqual("90.0", xmlDoc.SelectSingleNode("order/services/service[@name='employment'][1]/field[@name='payEnd']").InnerText);
			Assert.AreEqual("Henry Test", xmlDoc.SelectSingleNode("order/services/service[@name='employment'][1]/field[@name='supervisorName']").InnerText);
			Assert.AreEqual("604-878-1234", xmlDoc.SelectSingleNode("order/services/service[@name='employment'][1]/field[@name='companyPhone']").InnerText);
			Assert.AreEqual("Test", xmlDoc.SelectSingleNode("order/services/service[@name='employment'][1]/field[@name='reasonForLeaving']").InnerText);

			Assert.AreEqual("123", xmlDoc.SelectSingleNode("order/services/service[@name='license'][1]/field[@name='licenseNumber']").InnerText);
			Assert.AreEqual("Test License", xmlDoc.SelectSingleNode("order/services/service[@name='license'][1]/field[@name='licenseName']").InnerText);
			Assert.AreEqual("NJ", xmlDoc.SelectSingleNode("order/services/service[@name='license'][1]/field[@name='licenseState']").InnerText);

			Assert.AreEqual("New Brunswick", xmlDoc.SelectSingleNode("order/services/service[@name='reference'][1]/field[@name='reference_city']").InnerText);
			Assert.AreEqual("NJ", xmlDoc.SelectSingleNode("order/services/service[@name='reference'][1]/field[@name='reference_state']").InnerText);
			Assert.AreEqual("Mike", xmlDoc.SelectSingleNode("order/services/service[@name='reference'][1]/field[@name='reference_fname']").InnerText);
			Assert.AreEqual("08901", xmlDoc.SelectSingleNode("order/services/service[@name='reference'][1]/field[@name='reference_zip']").InnerText);
			Assert.AreEqual("TestCandidate", xmlDoc.SelectSingleNode("order/services/service[@name='reference'][1]/field[@name='reference_lname']").InnerText);
			Assert.AreEqual("Middlesx", xmlDoc.SelectSingleNode("order/services/service[@name='reference'][1]/field[@name='reference_county']").InnerText);
			Assert.AreEqual("Test Job", xmlDoc.SelectSingleNode("order/services/service[@name='reference'][1]/field[@name='reference_company']").InnerText);
			Assert.AreEqual("160 Louis Street", xmlDoc.SelectSingleNode("order/services/service[@name='reference'][1]/field[@name='reference_addressLine1']").InnerText);
			Assert.AreEqual("160 Louis street Line 2", xmlDoc.SelectSingleNode("order/services/service[@name='reference'][1]/field[@name='reference_addressLine2']").InnerText);
			Assert.AreEqual("MikeTest@mailinator.com", xmlDoc.SelectSingleNode("order/services/service[@name='reference'][1]/field[@name='reference_email']").InnerText);
			Assert.AreEqual("US", xmlDoc.SelectSingleNode("order/services/service[@name='reference'][1]/field[@name='reference_country']").InnerText);
			Assert.AreEqual("732-555-5555", xmlDoc.SelectSingleNode("order/services/service[@name='reference'][1]/field[@name='reference_phone']").InnerText);
		}

		[TestMethod]
        public void ensureDataFieldsAreExtractedProperlyWhenWeAreLoadingPackageFromDb()
        {
            String testResourcePath = "Resources/orderRequestCredit.json";
            Assert.IsTrue(File.Exists(testResourcePath));
            String payload = File.ReadAllText(testResourcePath);

            String ip = "127.0.0.1 (127.0.0.1)";
            String package = "CRCK_00193";

            HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
            MockWebAccessor webaccessor = new MockWebAccessor();
            webaccessor.ip = ip;
            IATSHandler testClient = new icims6();
            Order blankOrder = new Order();

            // save current txn_id
            String oldTxnID = dbaccessor.getLatestOrderTxnID(blankOrder, "ensureDataFieldsAreExtractedProperlyWhenWeAreLoadingPackageFromDb");
            String lastErrorID = dbaccessor.getLatestErrorID(blankOrder, "ensureDataFieldsAreExtractedProperlyWhenWeAreLoadingPackageFromDb");

            OrderRequest request = new OrderRequest();
            request.rawPayload = payload;
            Order result = testClient.processRequest(request, dbaccessor, webaccessor);

            // Check #1, make sure data returned are proper
            Assert.AreEqual(result.additionalData[OrderAdditionalDataKeys.package], package);


            // Check #2, make sure a new record is created in qProcessing
            String newTxnID = dbaccessor.getLatestOrderTxnID(result, "ensureDataFieldsAreExtractedProperlyWhenWeAreLoadingPackageFromDb");
            Assert.AreNotEqual(oldTxnID, newTxnID);

            // Check #3, make sure no errors are generated
            String newErrorID = dbaccessor.getLatestErrorID(result, "ensureDataFieldsAreExtractedProperlyWhenWeAreLoadingPackageFromDb");
            Assert.AreEqual(lastErrorID, newErrorID);

            // Check # 4, check the XML content
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(result.payload_hrnx);

            Assert.AreEqual("clearstar2~301", xmlDoc.SelectSingleNode("order/services/service[@type='passthru']/@tp").InnerText);
            Assert.AreEqual(package, xmlDoc.SelectSingleNode("order/services/service[@type='passthru'][@tp='clearstar2~301']/field[@name='package'][@type='value']").InnerText);
        }

		[TestMethod]
		public void ensureEmptyInputAreHandled()
		{
			String payload = "";

			HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
			MockWebAccessor webAccessor = new MockWebAccessor();

			// save last error
			String lastErrorID = dbAccessor.getLatestErrorID(new Order(), "ensureInvalidJSONAreHandled");

			IATSHandler testClient = new icims6();
			OrderRequest request = new OrderRequest();
			request.rawPayload = payload;
			Order result = testClient.processRequest(request, dbAccessor, webAccessor);

			// Check #1: Validate the order returned reports the expected error
			Assert.AreEqual(OrderStatus.rejected_w_errors, result.status);
			Assert.AreEqual(rm.GetString("INVALID_REQUEST_EMPTY"), result.errorReason);

			// Check #2, verify we got a new Error on ErrorStack
			List<HRNXv1Error> returnedErrors = dbAccessor.getErrorFromErrorStackSince(lastErrorID, result, "ensureInvalidJSONAreHandled");
			Assert.AreEqual(1, returnedErrors.Count);

			// Check #3, verify we got the appropriate error
			Assert.AreEqual(rm.GetString("INVALID_REQUEST_EMPTY"), returnedErrors[0].error_short);
		}

		[TestMethod]
        public void ensureInvalidJSONAreHandled()
        {
            String testResourcePath = "Resources/invalidJsonRequest.json";
            Assert.IsTrue(File.Exists(testResourcePath));
            String payload = File.ReadAllText(testResourcePath);

            HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
            MockWebAccessor webAccessor = new MockWebAccessor();

            // save last error
            String lastErrorID = dbAccessor.getLatestErrorID(new Order(), "ensureInvalidJSONAreHandled");

            IATSHandler testClient = new icims6();
            OrderRequest request = new OrderRequest();
            request.rawPayload = payload;
            Order result = testClient.processRequest(request, dbAccessor, webAccessor);

            // Check #1: Validate the order returned reports the expected error
            Assert.AreEqual(result.status, OrderStatus.rejected_w_errors);
            Assert.AreEqual(result.errorReason, rm.GetString("INVALID_REQUEST_JSON"));

            // Check #2, verify we got a new Error on ErrorStack
            List<HRNXv1Error> returnedErrors = dbAccessor.getErrorFromErrorStackSince(lastErrorID,result, "ensureInvalidJSONAreHandled");
            Assert.AreEqual(1, returnedErrors.Count);

            // Check #3, verify we got the appropriate error
            Assert.AreEqual(rm.GetString("INVALID_REQUEST_JSON"), returnedErrors[0].error_short);
            Assert.AreEqual(payload, returnedErrors[0].error_blob);
        }

        [TestMethod]
        public void ensureInvalidClientGroupIDIsHandled()
        {
            String testResourcePath = "Resources/orderRequestInvalidClientGroupID.json";
            Assert.IsTrue(File.Exists(testResourcePath));
            String payload = File.ReadAllText(testResourcePath);

            HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
            MockWebAccessor webAccessor = new MockWebAccessor();

            // save last error
            String lastErrorID = dbAccessor.getLatestErrorID(new Order(), "ensureEmptyClientGroupIDIsHandled");

            IATSHandler testClient = new icims6();
            OrderRequest request = new OrderRequest();
            request.rawPayload = payload;
            Order result = testClient.processRequest(request, dbAccessor, webAccessor);

            // Check #1: Validate the order returned reports the expected error
            Assert.AreEqual(result.status, OrderStatus.rejected_w_errors);
            Assert.AreEqual(result.errorReason, rm.GetString("CLIENT_GROUP_ID_INVALID"));

            // Check #2, verify we got a new Error on ErrorStack
            List<HRNXv1Error> returnedErrors = dbAccessor.getErrorFromErrorStackSince(lastErrorID, result, "ensureEmptyClientGroupIDIsHandled");
            Assert.AreEqual(1, returnedErrors.Count);

            // Check #3, verify we got the appropriate error
            Assert.AreEqual(rm.GetString("CLIENT_GROUP_ID_INVALID"), returnedErrors[0].error_short);
            Assert.AreEqual(payload, returnedErrors[0].error_blob);
        }

        [TestMethod]
        public void ensureMissingSystemIDIsDetected()
        {
            String testResourcePath = "Resources/orderRequestNoSystemID.json";
            Assert.IsTrue(File.Exists(testResourcePath));
            String payload = File.ReadAllText(testResourcePath);

            HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
            MockWebAccessor webAccessor = new MockWebAccessor();

            // save last error
            String lastErrorID = dbAccessor.getLatestErrorID(new Order(), "ensureMissingSystemIDIsDetected unit test");

            IATSHandler testClient = new icims6();
            OrderRequest request = new OrderRequest();
            request.rawPayload = payload;
            Order result = testClient.processRequest(request, dbAccessor, webAccessor);

            // Check #1: Validate the order returned reports the expected error
            Assert.AreEqual(OrderStatus.rejected_w_errors, result.status);
            Assert.AreEqual(rm.GetString("REQUIRED_DATA_MISSING"),result.errorReason);

            // Check #2, verify we got a new Error on ErrorStack
            List<HRNXv1Error> returnedErrors = dbAccessor.getErrorFromErrorStackSince(lastErrorID,result, "ensureMissingSystemIDIsDetected unit test");
            Assert.AreEqual(1, returnedErrors.Count);

            // Check #3, verify we got the appropriate error
            Assert.AreEqual(rm.GetString("REQUIRED_DATA_MISSING"), returnedErrors[0].error_short);
            Assert.AreEqual(rm.GetString("SYSTEM_ID"), returnedErrors[0].error_long);
            Assert.AreEqual(payload, returnedErrors[0].error_blob);
        }

        [TestMethod]
        public void ensureMissingPersonIDIsDetected()
        {
            String testResourcePath = "Resources/orderRequestNoCandidateID.json";
            Assert.IsTrue(File.Exists(testResourcePath));
            String payload = File.ReadAllText(testResourcePath);

            HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
            MockWebAccessor webAccessor = new MockWebAccessor();

            // save last error
            String lastErrorID = dbAccessor.getLatestErrorID(new Order(), "ensureMissingPersonIDIsDetected unit test");

            IATSHandler testClient = new icims6();
            OrderRequest request = new OrderRequest();
            request.rawPayload = payload;
            Order result = testClient.processRequest(request, dbAccessor, webAccessor);

            // Check #1: Validate the order returned reports the expected error
            Assert.AreEqual(OrderStatus.rejected_w_errors, result.status);
            Assert.AreEqual(rm.GetString("REQUIRED_DATA_MISSING"), result.errorReason);

            // Check #2, verify we got a new Error on ErrorStack
            List<HRNXv1Error> returnedErrors = dbAccessor.getErrorFromErrorStackSince(lastErrorID,result, "ensureMissingPersonIDIsDetected unit test");
            Assert.AreEqual(1, returnedErrors.Count);

            // Check #3, verify we got the appropriate error
            Assert.AreEqual(rm.GetString("REQUIRED_DATA_MISSING"), returnedErrors[0].error_short);
            Assert.AreEqual(rm.GetString("CANDIDATE_ID"), returnedErrors[0].error_long);
            Assert.AreEqual(payload, returnedErrors[0].error_blob);
        }
        [TestMethod]
        public void Icims6EnsureErrorsAreThrownSubjectProfileInvalid()
        {
            String testResourcePath = "Resources/orderRequestInvalidSubjectProfileURL.json";
            Assert.IsTrue(File.Exists(testResourcePath));
            String payload = File.ReadAllText(testResourcePath);
            String origin = System.Reflection.MethodBase.GetCurrentMethod().Name;

            String ip = "127.0.0.1 (127.0.0.1)";

            HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
            MockWebAccessor webaccessor = new MockWebAccessor();
            webaccessor.ip = ip;

            // save current txn_id
            String oldTxnID = dbaccessor.getLatestOrderTxnID(new Order(),origin);
            String lastErrorID = dbaccessor.getLatestErrorID(new Order(), origin);

            IATSHandler testClient = new icims6();
            OrderRequest request = new OrderRequest();
            request.rawPayload = payload;
            Order result = testClient.processRequest(request, dbaccessor, webaccessor);

            // Check #1, make sure an error was thrown
            Assert.AreEqual(result.status, OrderStatus.rejected_w_errors);

            // Check #2, make sure NO new record is created in qProcessing
            String newTxnID = dbaccessor.getLatestOrderTxnID(result,origin);
            Assert.AreEqual(oldTxnID, newTxnID);

            // Check #3, make sure a new errors is generated
            List<HRNXv1Error> returnedErrors = dbaccessor.getErrorFromErrorStackSince(lastErrorID, result, origin);
            Assert.AreEqual(1, returnedErrors.Count);

            // Check #3, verify we got the appropriate error
            Assert.AreEqual(rm.GetString("INVALID_ICIMS_SUBJECT_PROFILE_JSON"), returnedErrors[0].error_short);

        }
        [TestMethod]
        public void ensureErrorsAreThrownIfNoPersonURL()
        {
            String testResourcePath = "Resources/orderRequestNoPersonURL.json";
            Assert.IsTrue(File.Exists(testResourcePath));
            String payload = File.ReadAllText(testResourcePath);
            String origin = System.Reflection.MethodBase.GetCurrentMethod().Name;

            String ip = "127.0.0.1 (127.0.0.1)";

            HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
            MockWebAccessor webaccessor = new MockWebAccessor();
            webaccessor.ip = ip;

            // save current txn_id
            String oldTxnID = dbaccessor.getLatestOrderTxnID(new Order(), origin);
            String lastErrorID = dbaccessor.getLatestErrorID(new Order(), origin);

            IATSHandler testClient = new icims6();
            OrderRequest request = new OrderRequest();
            request.rawPayload = payload;
            Order result = testClient.processRequest(request, dbaccessor, webaccessor);

            // Check #1, make sure an error was thrown
            Assert.AreEqual(result.status, OrderStatus.rejected_w_errors);

            // Check #2, make sure NO new record is created in qProcessing
            String newTxnID = dbaccessor.getLatestOrderTxnID(result, origin);
            Assert.AreEqual(oldTxnID, newTxnID);

            // Check #3, make sure a new errors is generated
            List<HRNXv1Error> returnedErrors = dbaccessor.getErrorFromErrorStackSince(lastErrorID, result, origin);
            Assert.AreEqual(1, returnedErrors.Count);

            // Check #3, verify we got the appropriate error
            Assert.AreEqual(rm.GetString("REQUIRED_DATA_MISSING"), returnedErrors[0].error_short);
            Assert.AreEqual(rm.GetString("PERSON_URL"), returnedErrors[0].error_long);
        }

        [TestMethod]
        public void ensureErrorsAreThrownIfNoWorkflowURL()
        {
            String testResourcePath = "Resources/orderRequestNoWorkflowURL.json";
            Assert.IsTrue(File.Exists(testResourcePath));
            String payload = File.ReadAllText(testResourcePath);
            String origin = System.Reflection.MethodBase.GetCurrentMethod().Name;

            String ip = "127.0.0.1 (127.0.0.1)";

            HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
            MockWebAccessor webaccessor = new MockWebAccessor();
            webaccessor.ip = ip;

            // save current txn_id
            String oldTxnID = dbaccessor.getLatestOrderTxnID(new Order(), origin);
            String lastErrorID = dbaccessor.getLatestErrorID(new Order(), origin);

            IATSHandler testClient = new icims6();
            OrderRequest request = new OrderRequest();
            request.rawPayload = payload;
            Order result = testClient.processRequest(request, dbaccessor, webaccessor);

            // Check #1, make sure an error was thrown
            Assert.AreEqual(result.status, OrderStatus.rejected_w_errors);

            // Check #2, make sure NO new record is created in qProcessing
            String newTxnID = dbaccessor.getLatestOrderTxnID(result, origin);
            Assert.AreEqual(oldTxnID, newTxnID);

            // Check #3, make sure a new errors is generated
            List<HRNXv1Error> returnedErrors = dbaccessor.getErrorFromErrorStackSince(lastErrorID, result, origin);
            Assert.AreEqual(1, returnedErrors.Count);

            // Check #3, verify we got the appropriate error
            Assert.AreEqual(rm.GetString("REQUIRED_DATA_MISSING"), returnedErrors[0].error_short);
            Assert.AreEqual(rm.GetString("APPLICANT_WORKFLOW_URL"), returnedErrors[0].error_long);
        }

        [TestMethod]
        public void ensureErrorsAreThrownIfNoJobURL()
        {
            String testResourcePath = "Resources/orderRequestNoJobURL.json";
            Assert.IsTrue(File.Exists(testResourcePath));
            String payload = File.ReadAllText(testResourcePath);

            String ip = "127.0.0.1 (127.0.0.1)";

            HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
            MockWebAccessor webaccessor = new MockWebAccessor();
            webaccessor.ip = ip;
            String origin = System.Reflection.MethodBase.GetCurrentMethod().Name;

            // save current txn_id
            String oldTxnID = dbaccessor.getLatestOrderTxnID(new Order(), origin);
            String lastErrorID = dbaccessor.getLatestErrorID(new Order(), origin);

            IATSHandler testClient = new icims6();
            OrderRequest request = new OrderRequest();
            request.rawPayload = payload;
            Order result = testClient.processRequest(request, dbaccessor, webaccessor);

            // Check #1, make sure an error was thrown
            Assert.AreEqual(result.status, OrderStatus.rejected_w_errors);

            // Check #2, make sure NO new record is created in qProcessing
            String newTxnID = dbaccessor.getLatestOrderTxnID(result, origin);
            Assert.AreEqual(oldTxnID, newTxnID);

            // Check #3, make sure a new errors is generated
            List<HRNXv1Error> returnedErrors = dbaccessor.getErrorFromErrorStackSince(lastErrorID, result, origin);
            Assert.AreEqual(1, returnedErrors.Count);

            // Check #3, verify we got the appropriate error
            Assert.AreEqual(rm.GetString("REQUIRED_DATA_MISSING"), returnedErrors[0].error_short);
            Assert.AreEqual(rm.GetString("JOB_URL"), returnedErrors[0].error_long);
        }
        [TestMethod]
        public void ensureErrorsAreThrownIfFailToGetIcimsJobProfile()
        {
            String testResourcePath = "Resources/orderRequestInvalidJobProfileURL.json";
            Assert.IsTrue(File.Exists(testResourcePath));
            String payload = File.ReadAllText(testResourcePath);
			String candidateID = Guid.NewGuid().ToString();
			payload = payload.Replace("^^^CANDIDATE_ID^^^", candidateID);
            String origin = System.Reflection.MethodBase.GetCurrentMethod().Name;

            String ip = "127.0.0.1 (127.0.0.1)";

            HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
            MockWebAccessor webaccessor = new MockWebAccessor();
            webaccessor.ip = ip;

            // save current txn_id
            String oldTxnID = dbaccessor.getLatestOrderTxnID(new Order(), origin);
            String lastErrorID = dbaccessor.getLatestErrorID(new Order(),origin);

            IATSHandler testClient = new icims6();
            OrderRequest request = new OrderRequest();
            request.rawPayload = payload;
            Order result = testClient.processRequest(request, dbaccessor, webaccessor);

			// Check #1, make sure NO new record is created in qProcessing
			String newTxnID = dbaccessor.getLatestOrderTxnID(result, origin);
			Assert.AreEqual(oldTxnID, newTxnID);

			// Check #2, make sure an error was thrown
			Assert.AreEqual(OrderStatus.rejected_w_errors, result.status);

            // Check #3, make sure a new errors is generated
            List<HRNXv1Error> returnedErrors = dbaccessor.getErrorFromErrorStackSince(lastErrorID, result, origin);
            Assert.AreEqual(1, returnedErrors.Count);

            // Check #3, verify we got the appropriate error
            Assert.AreEqual(string.Format(rm.GetString("ICIMS_RETURNED_NON_OK_RESPONSE"),"getPackage"), returnedErrors[0].error_short);
        }
        [TestMethod]
        public void ensureNeedToCreateNewOrderWorks()
        {
            icimsRequest mockRequest = new icimsRequest();
            icims6 target = new icims6();
            target.parsedRequest = mockRequest;

            Order request = new Order();
            request.client_group_id = System.Reflection.MethodBase.GetCurrentMethod().ToString();

            MockDBAccessor mockDB = new MockDBAccessor();
            HRNXPhase currentPhrase = new HRNXPhase();
            currentPhrase.enumValue = ((int)icimsPhrase.BACKGROUND).ToString();
            mockDB.currentPhrase = currentPhrase;

            HRNXWebAccessor webAccessor = new HRNXWebAccessor();

            // in the case, we are saying DBAccessor is returning no atsDuplicate entry, so we need to return True
            Boolean returnVal = target.needToCreateNewOrder(request, mockDB, webAccessor);
            Assert.IsTrue(returnVal);

            // in the case, we are saying DBAccessor is returning an atsDuplicate entry, so we need to use its txn_id
            HRNXATSDuplicateEntry entry = new HRNXATSDuplicateEntry();
            String txn_id = Guid.NewGuid().ToString();
            entry.original_txn_id = txn_id;
            mockDB.duplicateEntry = entry;

            Boolean returnVal2 = target.needToCreateNewOrder(request, mockDB, webAccessor);
            Assert.IsFalse(returnVal2);
            Assert.AreEqual(txn_id, target.original_txn_id);
        }
        [TestMethod]
        public void ensureWeRetryIcimsHttpRequestTheAmountOfTimeWeSpecified()
        {
            int expectedTries = icims6.ICIMS_RETRY_MAX;

            icims6 target = new icims6();
            String targetURL = "http://localhost/hrnx/git_only/stub/genericTimeout.event";
            StringBuilder logContent = new StringBuilder();
            HRNXWebAccessor webAccessor = new HRNXWebAccessor();
			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;

			HRNXWebResponse response = target.httpSendToIcims(targetURL, "GET", "text/xml", "", null, 100, logContent, webAccessor, dbaccessor,new Order(), location);

            String logContentStr = logContent.ToString();
            int count = new Regex(Regex.Escape(targetURL)).Matches(logContentStr).Count;

            Console.WriteLine(logContentStr);
            Assert.AreEqual(expectedTries, count);
        }
		[TestMethod]
		public void ensureOKResponseIfOrderIsProcessing()
		{
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;
			String client_group_id = "2569";
			String candidateID = new Random().Next().ToString();
			String jobID = location;
			icimsPhrase currentPhrase = icimsPhrase.CREDIT_CHECK;

			icims6 target = new icims6();
			target.workflowURL = "http://localhost/hrnx/git_only/stub/icims1/mockICIMSUpdate.event";
			target.client_group_id = client_group_id;
			target.candidateID = candidateID;
			target.jobID = jobID;
			target.currentPhrase = currentPhrase;

			HRNXWebAccessor webAccessor = new HRNXWebAccessor();
			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
			String txn_id = Guid.NewGuid().ToString();
			//txn_id = "cee0a983-baae-11e7-a0e7-12b5fc3c4575";

			Order currentResult = new Order();
			currentResult.status = OrderStatus.processing;
			currentResult.txn_id = txn_id;
			currentResult.additionalData.Add(OrderAdditionalDataKeys.serviceProviderID, "unit test");
			currentResult.additionalData.Add(OrderAdditionalDataKeys.isNewOrder, "true");

			CreateOrderResponse targetResponse = target.sendAcknowledgement(currentResult, dbaccessor, webAccessor);

			Assert.AreEqual(HttpStatusCode.OK, targetResponse.status);

			// Check #2: Ensure ats_duplicate entry is created
			String duplicateKey = client_group_id + "_" + candidateID + "_" + jobID + "_" + currentPhrase;
			Console.WriteLine("DuplicateKey is [" + duplicateKey + "]");
			HRNXATSDuplicateEntry dupEntry = dbaccessor.getATSDuplicateEntryData(currentResult, duplicateKey, location);
			Assert.IsNotNull(dupEntry);
			Assert.AreEqual(txn_id, dupEntry.original_txn_id);
		}
		[TestMethod]
		public void ensureErrorResponseIfOrderIsRejectedWErrors()
		{
			icims6 target = new icims6();
			target.postbackLocation = "http://localhost/hrnx/git_only/stub/icims1/mockICIMSUpdate.event";
			HRNXWebAccessor webAccessor = new HRNXWebAccessor();
			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
			String txn_id = Guid.NewGuid().ToString();
			//txn_id = "cee0a983-baae-11e7-a0e7-12b5fc3c4575";
			String errorReason = "This is a unit test";
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;
			String lastErrorID = dbaccessor.getLatestErrorID(null, location);

			Order currentResult = new Order();
			currentResult.status = OrderStatus.rejected_w_errors;
			currentResult.txn_id = txn_id;
			currentResult.errorReason = errorReason;
				
			CreateOrderResponse targetResponse = target.sendAcknowledgement(currentResult, dbaccessor, webAccessor);

			// Check #1: Verify the response back to icims will be 406
			Assert.AreEqual(HttpStatusCode.BadRequest, targetResponse.status);

			// Check #2: Ensure no new error is logged, since it would have been logged in the first place
			List<HRNXv1Error> newErrors = dbaccessor.getErrorFromErrorStackSince(lastErrorID, null, location);
			Assert.AreEqual(0, newErrors.Count);
		}
		[TestMethod]
		public void ensureErrorResponseIfIcimsUpdateFailed()
		{
			icims6 target = new icims6();
			target.postbackLocation = "http://localhost/hrnx/git_only/stub/icims1/mockICIMSUpdateError.event";
			HRNXWebAccessor webAccessor = new HRNXWebAccessor();
			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
			String txn_id = Guid.NewGuid().ToString();
			//txn_id = "cee0a983-baae-11e7-a0e7-12b5fc3c4575";
			String errorReason = "This is a unit test";
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;
			String lastErrorID = dbaccessor.getLatestErrorID(null, location);

			Order currentResult = new Order();
			currentResult.status = OrderStatus.passthru;
			currentResult.txn_id = txn_id;
			currentResult.errorReason = errorReason;

			CreateOrderResponse targetResponse = target.sendAcknowledgement(currentResult, dbaccessor, webAccessor);

			// Check #1: Verify the response back to icims will be 406
			Assert.AreEqual(HttpStatusCode.BadRequest, targetResponse.status);

			// Check #2: Ensure an error is logged and it is the correct one
			List<HRNXv1Error> newErrors = dbaccessor.getErrorFromErrorStackSince(lastErrorID, null, location);
			Assert.AreEqual(2, newErrors.Count);

			Assert.AreEqual(rm.GetString("UNRECOGNIZE_HRNX_STATUS"), newErrors[0].error_short);
			Assert.AreEqual(errorReason, newErrors[0].error_blob_2);
			Assert.AreEqual(rm.GetString("ICIMS_UPDATE_FAIL"),newErrors[1].error_short);
		}
		/// <summary>
		/// NOTE: This doesn't mean Chemical Bank will work with just minData because clearStar actually needs more than just minData
		/// </summary>
		[TestMethod]
		public void ensureOrderExtractWithMinData()
		{
			String candidateID = Guid.NewGuid().ToString();

			String testResourcePath = "Resources/orderRequestCreditMinData.json";
			Assert.IsTrue(File.Exists(testResourcePath));
			String payload = File.ReadAllText(testResourcePath);
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;

			// Replace the candidateID, so will always have a unique candidate per run
			payload = payload.Replace("45392", candidateID);

			String ip = "127.0.0.1 (127.0.0.1)";
			String package = "CRCK_00193";

			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
			MockWebAccessor webaccessor = new MockWebAccessor();
			webaccessor.ip = ip;


			// save current txn_id
			String oldTxnID = dbaccessor.getLatestOrderTxnID(null, location);
			String lastErrorID = dbaccessor.getLatestErrorID(null, location);

			OrderRequest request = new OrderRequest();
			request.rawPayload = payload;

			IATSHandler testClient = new icims6();
			Order result = testClient.processRequest(request, dbaccessor, webaccessor);

			// Check #1, make sure a new record is created in qProcessing
			String newTxnID = dbaccessor.getLatestOrderTxnID(result, location);
			Assert.AreNotEqual(oldTxnID, newTxnID);

			// Check #2, make sure no errors are generated
			String newErrorID = dbaccessor.getLatestErrorID(result, location);
			Assert.AreEqual(lastErrorID, newErrorID);
		}
		[TestMethod]
		public void ensureErrorsAreThrownIfUnknownStage()
		{
			String testResourcePath = "Resources/orderRequestUnknownStage.json";
			Assert.IsTrue(File.Exists(testResourcePath));
			String payload = File.ReadAllText(testResourcePath);
			String origin = System.Reflection.MethodBase.GetCurrentMethod().Name;

			String ip = "127.0.0.1 (127.0.0.1)";

			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
			MockWebAccessor webaccessor = new MockWebAccessor();
			webaccessor.ip = ip;

			// save current txn_id
			String oldTxnID = dbaccessor.getLatestOrderTxnID(new Order(), origin);
			String lastErrorID = dbaccessor.getLatestErrorID(new Order(), origin);

			IATSHandler testClient = new icims6();
			OrderRequest request = new OrderRequest();
			request.rawPayload = payload;
			Order result = testClient.processRequest(request, dbaccessor, webaccessor);

			// Check #1, make sure an error was thrown
			Assert.AreEqual(result.status, OrderStatus.rejected_w_errors);

			// Check #2, make sure NO new record is created in qProcessing
			String newTxnID = dbaccessor.getLatestOrderTxnID(result, origin);
			Assert.AreEqual(oldTxnID, newTxnID);

			// Check #3, make sure a new errors is generated
			List<HRNXv1Error> returnedErrors = dbaccessor.getErrorFromErrorStackSince(lastErrorID, result, origin);
			Assert.AreEqual(2, returnedErrors.Count);

			// Check #3, verify we got the appropriate error
			Assert.AreEqual(rm.GetString("UNKNOWN_STAGE"), returnedErrors[0].error_short);
			Assert.AreEqual("1234", returnedErrors[0].error_long);

			Assert.AreEqual(rm.GetString("HRNX_EXCEPTION_THROWN"), returnedErrors[1].error_short);
		}
		[Ignore]
		[TestMethod]
		public void ensureNoServiceProviderAssociatedWithOrderIsHandled()
		{
			//TODO:  Setup DB
			// Change endpoint_category from passthru to pass2thru

			String payload = TestUtil.getResource("orderRequestCredit.json");
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;

			HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
			MockWebAccessor webAccessor = new MockWebAccessor();

			// save last error
			String lastErrorID = dbAccessor.getLatestErrorID(new Order(), location);

			IATSHandler testClient = new icims6();
			OrderRequest request = new OrderRequest();
			request.rawPayload = payload;
			Order result = testClient.processRequest(request, dbAccessor, webAccessor);

			// Check #1: Validate the order returned reports the expected error
			Assert.AreEqual(result.status, OrderStatus.rejected_w_errors);
			Assert.AreEqual(result.errorReason, rm.GetString("ORDER_SERVICE_PROVIDER_MISSING"));

			// Check #2, verify we got a new Error on ErrorStack
			List<HRNXv1Error> returnedErrors = dbAccessor.getErrorFromErrorStackSince(lastErrorID, result, location);
			Assert.AreEqual(1, returnedErrors.Count);

			// Check #3, verify we got the appropriate error
			Assert.AreEqual(rm.GetString("ORDER_SERVICE_PROVIDER_MISSING"), returnedErrors[0].error_short);
		}
		[TestMethod]
		public void ensureErrorsAreThrownIfIcimsWebServiceFailed()
		{
			String testResourcePath = "Resources/orderRequestTLSError.json";
			Assert.IsTrue(File.Exists(testResourcePath));
			String payload = File.ReadAllText(testResourcePath);

			String ip = "127.0.0.1 (127.0.0.1)";

			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
			MockWebAccessor webaccessor = new MockWebAccessor();
			webaccessor.ip = ip;
			String origin = System.Reflection.MethodBase.GetCurrentMethod().Name;

			// save current txn_id
			String oldTxnID = dbaccessor.getLatestOrderTxnID(new Order(), origin);
			String lastErrorID = dbaccessor.getLatestErrorID(new Order(), origin);

			IATSHandler testClient = new icims6();
			OrderRequest request = new OrderRequest();
			request.rawPayload = payload;
			Order result = testClient.processRequest(request, dbaccessor, webaccessor);

			// Check #1, make sure an error was thrown
			Assert.AreEqual(result.status, OrderStatus.rejected_w_errors);

			// Check #2, make sure NO new record is created in qProcessing
			String newTxnID = dbaccessor.getLatestOrderTxnID(result, origin);
			Assert.AreEqual(oldTxnID, newTxnID);

			// Check #3, make sure a new errors is generated
			List<HRNXv1Error> returnedErrors = dbaccessor.getErrorFromErrorStackSince(lastErrorID, result, origin);
			Assert.AreEqual(1, returnedErrors.Count);

			// Check #3, verify we got the appropriate error
			Assert.AreEqual(string.Format(rm.GetString("ICIMS_RETURNED_NON_OK_RESPONSE"), "subjectProfile"), returnedErrors[0].error_short);
		}
		[TestMethod]
		public void Icims6EnsureErrorsAreThrownSubjectProfileEmpty()
		{
			String testResourcePath = "Resources/orderRequestSubjectProfileEmpty.json";
			Assert.IsTrue(File.Exists(testResourcePath));
			String payload = File.ReadAllText(testResourcePath);

			String ip = "127.0.0.1 (127.0.0.1)";

			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
			MockWebAccessor webaccessor = new MockWebAccessor();
			webaccessor.ip = ip;
			String origin = System.Reflection.MethodBase.GetCurrentMethod().Name;

			// save current txn_id
			String oldTxnID = dbaccessor.getLatestOrderTxnID(new Order(), origin);
			String lastErrorID = dbaccessor.getLatestErrorID(new Order(), origin);

			IATSHandler testClient = new icims6();
			OrderRequest request = new OrderRequest();
			request.rawPayload = payload;
			Order result = testClient.processRequest(request, dbaccessor, webaccessor);

			// Check #1, make sure an error was thrown
			Assert.AreEqual(result.status, OrderStatus.rejected_w_errors);

			// Check #2, make sure NO new record is created in qProcessing
			String newTxnID = dbaccessor.getLatestOrderTxnID(result, origin);
			Assert.AreEqual(oldTxnID, newTxnID);

			// Check #3, make sure a new errors is generated
			List<HRNXv1Error> returnedErrors = dbaccessor.getErrorFromErrorStackSince(lastErrorID, result, origin);
			Assert.AreEqual(1, returnedErrors.Count);

			// Check #3, verify we got the appropriate error
			Assert.AreEqual(string.Format(rm.GetString("ICIMS_RETURNED_EMPTY_RESPONSE"), "subjectProfile"), returnedErrors[0].error_short);
		}
		[TestMethod]
		public void Icims6EnsureErrorsAreThrownSubjectProfileMissingFirstname()
		{
			String testResourcePath = "Resources/orderRequestSubjectProfileMissingFirstname.json";
			Assert.IsTrue(File.Exists(testResourcePath));
			String payload = File.ReadAllText(testResourcePath);

			String ip = "127.0.0.1 (127.0.0.1)";

			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
			MockWebAccessor webaccessor = new MockWebAccessor();
			webaccessor.ip = ip;
			String origin = System.Reflection.MethodBase.GetCurrentMethod().Name;

			// save current txn_id
			String oldTxnID = dbaccessor.getLatestOrderTxnID(new Order(), origin);
			String lastErrorID = dbaccessor.getLatestErrorID(new Order(), origin);

			IATSHandler testClient = new icims6();
			OrderRequest request = new OrderRequest();
			request.rawPayload = payload;
			Order result = testClient.processRequest(request, dbaccessor, webaccessor);

			// Check #1, make sure an error was thrown
			Assert.AreEqual(result.status, OrderStatus.rejected_w_errors);

			// Check #2, make sure NO new record is created in qProcessing
			String newTxnID = dbaccessor.getLatestOrderTxnID(result, origin);
			Assert.AreEqual(oldTxnID, newTxnID);

			// Check #3, make sure a new errors is generated
			List<HRNXv1Error> returnedErrors = dbaccessor.getErrorFromErrorStackSince(lastErrorID, result, origin);
			Assert.AreEqual(1, returnedErrors.Count);

			// Check #3, verify we got the appropriate error
			Assert.AreEqual(rm.GetString("MISSING_FIRSTNAME"), returnedErrors[0].error_short);
		}
		[TestMethod]
		public void Icims6EnsureErrorsAreThrownSubjectProfileMissingLastname()
		{
			String testResourcePath = "Resources/orderRequestSubjectProfileMissingLastname.json";
			Assert.IsTrue(File.Exists(testResourcePath));
			String payload = File.ReadAllText(testResourcePath);

			String ip = "127.0.0.1 (127.0.0.1)";

			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
			MockWebAccessor webaccessor = new MockWebAccessor();
			webaccessor.ip = ip;
			String origin = System.Reflection.MethodBase.GetCurrentMethod().Name;

			// save current txn_id
			String oldTxnID = dbaccessor.getLatestOrderTxnID(new Order(), origin);
			String lastErrorID = dbaccessor.getLatestErrorID(new Order(), origin);

			IATSHandler testClient = new icims6();
			OrderRequest request = new OrderRequest();
			request.rawPayload = payload;
			Order result = testClient.processRequest(request, dbaccessor, webaccessor);

			// Check #1, make sure an error was thrown
			Assert.AreEqual(result.status, OrderStatus.rejected_w_errors);

			// Check #2, make sure NO new record is created in qProcessing
			String newTxnID = dbaccessor.getLatestOrderTxnID(result, origin);
			Assert.AreEqual(oldTxnID, newTxnID);

			// Check #3, make sure a new errors is generated
			List<HRNXv1Error> returnedErrors = dbaccessor.getErrorFromErrorStackSince(lastErrorID, result, origin);
			Assert.AreEqual(1, returnedErrors.Count);

			// Check #3, verify we got the appropriate error
			Assert.AreEqual(rm.GetString("MISSING_LASTNAME"), returnedErrors[0].error_short);
		}
		[TestMethod]
		public void Icims6EnsureErrorsAreThrownSubjectProfileMissingEmail()
		{
			String testResourcePath = "Resources/orderRequestSubjectProfileMissingEmail.json";
			Assert.IsTrue(File.Exists(testResourcePath));
			String payload = File.ReadAllText(testResourcePath);

			String ip = "127.0.0.1 (127.0.0.1)";

			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
			MockWebAccessor webaccessor = new MockWebAccessor();
			webaccessor.ip = ip;
			String origin = System.Reflection.MethodBase.GetCurrentMethod().Name;

			// save current txn_id
			String oldTxnID = dbaccessor.getLatestOrderTxnID(new Order(), origin);
			String lastErrorID = dbaccessor.getLatestErrorID(new Order(), origin);

			IATSHandler testClient = new icims6();
			OrderRequest request = new OrderRequest();
			request.rawPayload = payload;
			Order result = testClient.processRequest(request, dbaccessor, webaccessor);

			// Check #1, make sure an error was thrown
			Assert.AreEqual(result.status, OrderStatus.rejected_w_errors);

			// Check #2, make sure NO new record is created in qProcessing
			String newTxnID = dbaccessor.getLatestOrderTxnID(result, origin);
			Assert.AreEqual(oldTxnID, newTxnID);

			// Check #3, make sure a new errors is generated
			List<HRNXv1Error> returnedErrors = dbaccessor.getErrorFromErrorStackSince(lastErrorID, result, origin);
			Assert.AreEqual(1, returnedErrors.Count);

			// Check #3, verify we got the appropriate error
			Assert.AreEqual(rm.GetString("MISSING_EMAIL"), returnedErrors[0].error_short);
		}
		[TestMethod]
		public void ensureEmptyJSONRequestAreHandled()
		{
			String payload = "{}";

			HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
			MockWebAccessor webAccessor = new MockWebAccessor();
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;

			// save last error
			String lastErrorID = dbAccessor.getLatestErrorID(new Order(), location);

			IATSHandler testClient = new icims6();
			OrderRequest request = new OrderRequest();
			request.rawPayload = payload;
			Order result = testClient.processRequest(request, dbAccessor, webAccessor);

			// Check #1: Validate the order returned reports the expected error
			Assert.AreEqual(result.status, OrderStatus.rejected_w_errors);
			Assert.AreEqual(result.errorReason, rm.GetString("CLIENT_GROUP_ID_EMPTY"));

			// Check #2, verify we got a new Error on ErrorStack
			List<HRNXv1Error> returnedErrors = dbAccessor.getErrorFromErrorStackSince(lastErrorID, result, location);
			Assert.AreEqual(1, returnedErrors.Count);

			// Check #3, verify we got the appropriate error
			Assert.AreEqual(rm.GetString("CLIENT_GROUP_ID_EMPTY"), returnedErrors[0].error_short);
			Assert.AreEqual(payload, returnedErrors[0].error_blob);
		}
		[TestMethod]
		public void ensureErrorsAreThrownIfEmptyJobProfile()
		{
			String testResourcePath = "Resources/orderRequestEmptyJobProfile.json";
			Assert.IsTrue(File.Exists(testResourcePath));
			String payload = File.ReadAllText(testResourcePath);
			String candidateID = Guid.NewGuid().ToString();
			payload = payload.Replace("^^^CANDIDATE_ID^^^", candidateID);
			String origin = System.Reflection.MethodBase.GetCurrentMethod().Name;

			String ip = "127.0.0.1 (127.0.0.1)";

			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
			MockWebAccessor webaccessor = new MockWebAccessor();
			webaccessor.ip = ip;

			// save current txn_id
			String oldTxnID = dbaccessor.getLatestOrderTxnID(new Order(), origin);
			String lastErrorID = dbaccessor.getLatestErrorID(new Order(), origin);

			IATSHandler testClient = new icims6();
			OrderRequest request = new OrderRequest();
			request.rawPayload = payload;
			Order result = testClient.processRequest(request, dbaccessor, webaccessor);

			// Check #1, make sure NO new record is created in qProcessing
			String newTxnID = dbaccessor.getLatestOrderTxnID(result, origin);
			Assert.AreEqual(oldTxnID, newTxnID);

			// Check #2, make sure an error was thrown
			Assert.AreEqual(OrderStatus.rejected_w_errors, result.status);

			// Check #3, make sure a new errors is generated
			List<HRNXv1Error> returnedErrors = dbaccessor.getErrorFromErrorStackSince(lastErrorID, result, origin);
			Assert.AreEqual(1, returnedErrors.Count);

			// Check #3, verify we got the appropriate error
			Assert.AreEqual(string.Format(rm.GetString("ICIMS_RETURNED_EMPTY_RESPONSE"), "jobProfile"), returnedErrors[0].error_short);
		}
		[TestMethod]
		public void ensureErrorsAreThrownIfInvalidJobProfile()
		{
			String testResourcePath = "Resources/orderRequestInvalidJobProfile.json";
			Assert.IsTrue(File.Exists(testResourcePath));
			String payload = File.ReadAllText(testResourcePath);
			String candidateID = Guid.NewGuid().ToString();
			payload = payload.Replace("^^^CANDIDATE_ID^^^", candidateID);
			String origin = System.Reflection.MethodBase.GetCurrentMethod().Name;

			String ip = "127.0.0.1 (127.0.0.1)";

			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
			MockWebAccessor webaccessor = new MockWebAccessor();
			webaccessor.ip = ip;

			// save current txn_id
			String oldTxnID = dbaccessor.getLatestOrderTxnID(new Order(), origin);
			String lastErrorID = dbaccessor.getLatestErrorID(new Order(), origin);

			IATSHandler testClient = new icims6();
			OrderRequest request = new OrderRequest();
			request.rawPayload = payload;
			Order result = testClient.processRequest(request, dbaccessor, webaccessor);

			// Check #1, make sure NO new record is created in qProcessing
			String newTxnID = dbaccessor.getLatestOrderTxnID(result, origin);
			Assert.AreEqual(oldTxnID, newTxnID);

			// Check #2, make sure an error was thrown
			Assert.AreEqual(OrderStatus.rejected_w_errors, result.status);

			// Check #3, make sure a new errors is generated
			List<HRNXv1Error> returnedErrors = dbaccessor.getErrorFromErrorStackSince(lastErrorID, result, origin);
			Assert.AreEqual(1, returnedErrors.Count);

			// Check #3, verify we got the appropriate error
			Assert.AreEqual(rm.GetString("INVALID_ICIMS_JOB_PROFILE_JSON"), returnedErrors[0].error_short);
		}
		[TestMethod]
		public void ensureErrorsAreThrownIfMissingPackage()
		{
			String testResourcePath = "Resources/orderRequestJobProfileMissingPackage.json";
			Assert.IsTrue(File.Exists(testResourcePath));
			String payload = File.ReadAllText(testResourcePath);
			String candidateID = Guid.NewGuid().ToString();
			payload = payload.Replace("^^^CANDIDATE_ID^^^", candidateID);
			String origin = System.Reflection.MethodBase.GetCurrentMethod().Name;

			String ip = "127.0.0.1 (127.0.0.1)";

			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
			MockWebAccessor webaccessor = new MockWebAccessor();
			webaccessor.ip = ip;

			// save current txn_id
			String oldTxnID = dbaccessor.getLatestOrderTxnID(new Order(), origin);
			String lastErrorID = dbaccessor.getLatestErrorID(new Order(), origin);

			IATSHandler testClient = new icims6();
			OrderRequest request = new OrderRequest();
			request.rawPayload = payload;
			Order result = testClient.processRequest(request, dbaccessor, webaccessor);

			// Check #1, make sure NO new record is created in qProcessing
			String newTxnID = dbaccessor.getLatestOrderTxnID(result, origin);
			Assert.AreEqual(oldTxnID, newTxnID);

			// Check #2, make sure an error was thrown
			Assert.AreEqual(OrderStatus.rejected_w_errors, result.status);

			// Check #3, make sure a new errors is generated
			List<HRNXv1Error> returnedErrors = dbaccessor.getErrorFromErrorStackSince(lastErrorID, result, origin);
			Assert.AreEqual(1, returnedErrors.Count);

			// Check #3, verify we got the appropriate error
			Assert.AreEqual(rm.GetString("NO_SERVICE_TO_ORDER"), returnedErrors[0].error_short);
		}
		[Ignore]
		[TestMethod]
		public void icims6EnsureLoadRecruiterStillOrdersWithEmptyResponse()
		{
			//TODO: Setup ewp6 to point to Empty response


			String candidateID = Guid.NewGuid().ToString();

			String testResourcePath = "Resources/orderRequestCreditMinData.json";
			Assert.IsTrue(File.Exists(testResourcePath));
			String payload = File.ReadAllText(testResourcePath);
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;

			// Replace the candidateID, so will always have a unique candidate per run
			payload = payload.Replace("45392", candidateID);

			String ip = "127.0.0.1 (127.0.0.1)";

			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
			MockWebAccessor webaccessor = new MockWebAccessor();
			webaccessor.ip = ip;


			// save current txn_id
			String oldTxnID = dbaccessor.getLatestOrderTxnID(null, location);
			String lastErrorID = dbaccessor.getLatestErrorID(null, location);

			OrderRequest request = new OrderRequest();
			request.rawPayload = payload;

			IATSHandler testClient = new icims6();
			Order result = testClient.processRequest(request, dbaccessor, webaccessor);

			// Check #1, make sure a new record is created in qProcessing
			String newTxnID = dbaccessor.getLatestOrderTxnID(result, location);
			Assert.AreNotEqual(oldTxnID, newTxnID);

			// Check #2, make sure no errors are generated
			String newErrorID = dbaccessor.getLatestErrorID(result, location);
			Assert.AreEqual(lastErrorID, newErrorID);
		}
		[Ignore]
		[TestMethod]
		public void icims6EnsureLoadRecruiterStillOrdersWithEmptyJSONResponse()
		{
			//TODO: Setup ewp6 to point to Empty JSON response


			String candidateID = Guid.NewGuid().ToString();

			String testResourcePath = "Resources/orderRequestCreditMinData.json";
			Assert.IsTrue(File.Exists(testResourcePath));
			String payload = File.ReadAllText(testResourcePath);
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;

			// Replace the candidateID, so will always have a unique candidate per run
			payload = payload.Replace("45392", candidateID);

			String ip = "127.0.0.1 (127.0.0.1)";
			
			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
			MockWebAccessor webaccessor = new MockWebAccessor();
			webaccessor.ip = ip;


			// save current txn_id
			String oldTxnID = dbaccessor.getLatestOrderTxnID(null, location);
			String lastErrorID = dbaccessor.getLatestErrorID(null, location);

			OrderRequest request = new OrderRequest();
			request.rawPayload = payload;

			IATSHandler testClient = new icims6();
			Order result = testClient.processRequest(request, dbaccessor, webaccessor);

			// Check #1, make sure a new record is created in qProcessing
			String newTxnID = dbaccessor.getLatestOrderTxnID(result, location);
			Assert.AreNotEqual(oldTxnID, newTxnID);

			// Check #2, make sure no errors are generated
			String newErrorID = dbaccessor.getLatestErrorID(result, location);
			Assert.AreEqual(lastErrorID, newErrorID);
		}
		[Ignore]
		[TestMethod]
		public void icims6EnsureLoadRecruiterErrorOutIfInvalidEP6URL()
		{
			//TODO: Setup ewp6 to point to invalid URL




			String testResourcePath = "Resources/orderRequestCreditMinData.json";
			Assert.IsTrue(File.Exists(testResourcePath));
			String payload = File.ReadAllText(testResourcePath);

			String ip = "127.0.0.1 (127.0.0.1)";

			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
			MockWebAccessor webaccessor = new MockWebAccessor();
			webaccessor.ip = ip;
			String origin = System.Reflection.MethodBase.GetCurrentMethod().Name;

			// save current txn_id
			String oldTxnID = dbaccessor.getLatestOrderTxnID(new Order(), origin);
			String lastErrorID = dbaccessor.getLatestErrorID(new Order(), origin);

			IATSHandler testClient = new icims6();
			OrderRequest request = new OrderRequest();
			request.rawPayload = payload;
			Order result = testClient.processRequest(request, dbaccessor, webaccessor);

			// Check #1, because this is an optional field, we will let it continue workflow
			Assert.AreEqual(result.status, OrderStatus.passthru);

			// Check #2, make sure NO new record is created in qProcessing
			String newTxnID = dbaccessor.getLatestOrderTxnID(result, origin);
			Assert.AreNotEqual(oldTxnID, newTxnID);

			// Check #3, make sure a new errors is generated
			List<HRNXv1Error> returnedErrors = dbaccessor.getErrorFromErrorStackSince(lastErrorID, result, origin);
			Assert.AreEqual(1, returnedErrors.Count);

			// Check #3, verify we got the appropriate error
			Assert.AreEqual(string.Format(rm.GetString("ICIMS_RETURNED_NON_OK_RESPONSE"), "user"), returnedErrors[0].error_short);
		}

		[TestMethod]
		public void Icims6EnsureMultipleLicenseReferenceDataCanBeSent()
		{
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;
			String candidateID = Guid.NewGuid().ToString();
			String ip = "127.0.0.1 (127.0.0.1)";
			String payloadFinal = TestUtil.getResource("icims6/orderRequestTemplateMultiple.json");

			// Update the test to fit the client's configuration
			// NIC will only ever order background check, so always 8770
			payloadFinal = payloadFinal.Replace("^^^orderingStatus^^^", "8770");
			payloadFinal = payloadFinal.Replace("^^^customerID^^^", "9078");
			payloadFinal = payloadFinal.Replace("^^^candidateID^^^", candidateID);

			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
			MockWebAccessor webaccessor = new MockWebAccessor();
			webaccessor.ip = ip;

			// save current txn_id
			String oldTxnID = dbaccessor.getLatestOrderTxnID(null, location);
			String lastErrorID = dbaccessor.getLatestErrorID(null, location);

			OrderRequest request = new OrderRequest();
			request.rawPayload = payloadFinal;

			IATSHandler testClient = new icims6();
			Order result = testClient.processRequest(request, dbaccessor, webaccessor);

			// Check #1, make sure a new record is created in qProcessing
			String newTxnID = dbaccessor.getLatestOrderTxnID(result, location);
			Assert.AreNotEqual(oldTxnID, newTxnID);

			// Check #2, make sure no errors are generated
			String newErrorID = dbaccessor.getLatestErrorID(result, location);
			Assert.AreEqual(lastErrorID, newErrorID);

			// Check #3, make sure data returned are proper
			Assert.AreEqual(OrderStatus.passthru, result.status);
			Assert.AreEqual(candidateID, result.candidate_id);

			// Check # 4, check the XML content
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(result.payload_hrnx);

			Assert.AreEqual("123", xmlDoc.SelectSingleNode("order/services/service[@name='license'][1]/field[@name='licenseNumber']").InnerText);
			Assert.AreEqual("Test License", xmlDoc.SelectSingleNode("order/services/service[@name='license'][1]/field[@name='licenseName']").InnerText);
			Assert.AreEqual("NJ", xmlDoc.SelectSingleNode("order/services/service[@name='license'][1]/field[@name='licenseState']").InnerText);

			Assert.AreEqual("ABC123", xmlDoc.SelectSingleNode("order/services/service[@name='license'][2]/field[@name='licenseNumber']").InnerText);
			Assert.AreEqual("Nursing Cert", xmlDoc.SelectSingleNode("order/services/service[@name='license'][2]/field[@name='licenseName']").InnerText);
			Assert.AreEqual("WA", xmlDoc.SelectSingleNode("order/services/service[@name='license'][2]/field[@name='licenseState']").InnerText);

			Assert.AreEqual("New Brunswick", xmlDoc.SelectSingleNode("order/services/service[@name='reference'][1]/field[@name='reference_city']").InnerText);
			Assert.AreEqual("NJ", xmlDoc.SelectSingleNode("order/services/service[@name='reference'][1]/field[@name='reference_state']").InnerText);
			Assert.AreEqual("Mike", xmlDoc.SelectSingleNode("order/services/service[@name='reference'][1]/field[@name='reference_fname']").InnerText);
			Assert.AreEqual("08901", xmlDoc.SelectSingleNode("order/services/service[@name='reference'][1]/field[@name='reference_zip']").InnerText);
			Assert.AreEqual("TestCandidate", xmlDoc.SelectSingleNode("order/services/service[@name='reference'][1]/field[@name='reference_lname']").InnerText);
			Assert.AreEqual("Middlesx", xmlDoc.SelectSingleNode("order/services/service[@name='reference'][1]/field[@name='reference_county']").InnerText);
			Assert.AreEqual("Test Job", xmlDoc.SelectSingleNode("order/services/service[@name='reference'][1]/field[@name='reference_company']").InnerText);
			Assert.AreEqual("160 Louis Street", xmlDoc.SelectSingleNode("order/services/service[@name='reference'][1]/field[@name='reference_addressLine1']").InnerText);
			Assert.AreEqual("160 Louis street Line 2", xmlDoc.SelectSingleNode("order/services/service[@name='reference'][1]/field[@name='reference_addressLine2']").InnerText);
			Assert.AreEqual("MikeTest@mailinator.com", xmlDoc.SelectSingleNode("order/services/service[@name='reference'][1]/field[@name='reference_email']").InnerText);
			Assert.AreEqual("US", xmlDoc.SelectSingleNode("order/services/service[@name='reference'][1]/field[@name='reference_country']").InnerText);
			Assert.AreEqual("732-555-5555", xmlDoc.SelectSingleNode("order/services/service[@name='reference'][1]/field[@name='reference_phone']").InnerText);

			Assert.AreEqual("New West", xmlDoc.SelectSingleNode("order/services/service[@name='reference'][2]/field[@name='reference_city']").InnerText);
			Assert.AreEqual("WA", xmlDoc.SelectSingleNode("order/services/service[@name='reference'][2]/field[@name='reference_state']").InnerText);
			Assert.AreEqual("MikeTwo", xmlDoc.SelectSingleNode("order/services/service[@name='reference'][2]/field[@name='reference_fname']").InnerText);
			Assert.AreEqual("98007", xmlDoc.SelectSingleNode("order/services/service[@name='reference'][2]/field[@name='reference_zip']").InnerText);
			Assert.AreEqual("Test", xmlDoc.SelectSingleNode("order/services/service[@name='reference'][2]/field[@name='reference_lname']").InnerText);
			Assert.AreEqual("Whoknows", xmlDoc.SelectSingleNode("order/services/service[@name='reference'][2]/field[@name='reference_county']").InnerText);
			Assert.AreEqual("Test Company", xmlDoc.SelectSingleNode("order/services/service[@name='reference'][2]/field[@name='reference_company']").InnerText);
			Assert.AreEqual("123 Louis Street", xmlDoc.SelectSingleNode("order/services/service[@name='reference'][2]/field[@name='reference_addressLine1']").InnerText);
			Assert.AreEqual("456 Louis street", xmlDoc.SelectSingleNode("order/services/service[@name='reference'][2]/field[@name='reference_addressLine2']").InnerText);
			Assert.AreEqual("TestCan@mailinator.com", xmlDoc.SelectSingleNode("order/services/service[@name='reference'][2]/field[@name='reference_email']").InnerText);
			Assert.AreEqual("CA", xmlDoc.SelectSingleNode("order/services/service[@name='reference'][2]/field[@name='reference_country']").InnerText);
			Assert.AreEqual("123-456-7890", xmlDoc.SelectSingleNode("order/services/service[@name='reference'][2]/field[@name='reference_phone']").InnerText);
		}

		[TestMethod]
		public void Icims6EnsureNullStateWillBeHandled()
		{
			String location = System.Reflection.MethodBase.GetCurrentMethod().Name;
			String candidateID = Guid.NewGuid().ToString();
			String ip = "127.0.0.1 (127.0.0.1)";
			String payloadFinal = TestUtil.getResource("icims6/orderRequestTemplateNullState.json");

			// Update the test to fit the client's configuration
			// NIC will only ever order background check, so always 8770
			payloadFinal = payloadFinal.Replace("^^^orderingStatus^^^", "8770");
			payloadFinal = payloadFinal.Replace("^^^customerID^^^", "9078");
			payloadFinal = payloadFinal.Replace("^^^candidateID^^^", candidateID);

			HRNXDBAccessor dbaccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
			MockWebAccessor webaccessor = new MockWebAccessor();
			webaccessor.ip = ip;

			// save current txn_id
			String oldTxnID = dbaccessor.getLatestOrderTxnID(null, location);
			String lastErrorID = dbaccessor.getLatestErrorID(null, location);

			OrderRequest request = new OrderRequest();
			request.rawPayload = payloadFinal;

			IATSHandler testClient = new icims6();
			Order result = testClient.processRequest(request, dbaccessor, webaccessor);

			// Check #1, make sure a new record is created in qProcessing
			String newTxnID = dbaccessor.getLatestOrderTxnID(result, location);
			Assert.AreNotEqual(oldTxnID, newTxnID);

			// Check #2, make sure no errors are generated
			String newErrorID = dbaccessor.getLatestErrorID(result, location);
			Assert.AreEqual(lastErrorID, newErrorID);

			// Check #3, make sure data returned are proper
			Assert.AreEqual(OrderStatus.passthru, result.status);
			Assert.AreEqual(candidateID, result.candidate_id);

			// Check # 4, check the XML content
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(result.payload_hrnx);

			Assert.IsNull(xmlDoc.SelectSingleNode("order/subject/field[@name='address_present_state']"));
			Assert.IsNull(xmlDoc.SelectSingleNode("order/subject/field[@name='address_present_country']"));

			Assert.IsNull(xmlDoc.SelectSingleNode("order/services/service[@name='education'][1]/field[@name='schoolCountry']"));
			Assert.IsNull(xmlDoc.SelectSingleNode("order/services/service[@name='education'][1]/field[@name='major']"));
			Assert.IsNull(xmlDoc.SelectSingleNode("order/services/service[@name='education'][1]/field[@name='degreeName']"));
			Assert.IsNull(xmlDoc.SelectSingleNode("order/services/service[@name='education'][1]/field[@name='schoolState']"));

			Assert.IsNull(xmlDoc.SelectSingleNode("order/services/service[@name='license'][1]/field[@name='licenseState']"));

			Assert.IsNull(xmlDoc.SelectSingleNode("order/services/service[@name='mvr'][1]/field[@name='dlstate']"));

			Assert.IsNull(xmlDoc.SelectSingleNode("order/services/service[@name='employment'][1]/field[@name='company_country']"));
			Assert.IsNull(xmlDoc.SelectSingleNode("order/services/service[@name='employment'][1]/field[@name='payEnd']"));
			Assert.IsNull(xmlDoc.SelectSingleNode("order/services/service[@name='employment'][1]/field[@name='company_state']"));
			Assert.IsNull(xmlDoc.SelectSingleNode("order/services/service[@name='employment'][1]/field[@name='permissionToContact']"));

			Assert.IsNull(xmlDoc.SelectSingleNode("order/services/service[@name='reference'][1]/field[@name='reference_state']"));
			Assert.IsNull(xmlDoc.SelectSingleNode("order/services/service[@name='reference'][1]/field[@name='reference_country']"));
		}
	}
}
