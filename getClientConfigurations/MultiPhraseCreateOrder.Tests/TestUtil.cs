﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MultiPhraseCreateOrder.Tests
{
    class TestUtil
    {
        public static String normalizeSpace(String input)
        {
            Regex normalizeSpace = new Regex(@"\s+", RegexOptions.Compiled);
            return normalizeSpace.Replace(input, " ");
        }

        public static String getResource(String resourceName)
        {
            String xsltPath = "Resources/"+ resourceName;
            String result = File.ReadAllText(xsltPath);

            return result;
        }
    }
}
