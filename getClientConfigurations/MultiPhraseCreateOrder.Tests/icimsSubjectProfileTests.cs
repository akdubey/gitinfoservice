﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using getClientConfigurations.Models;
using System.IO;
using System.Net;
using System.Collections.Generic;
using System.Resources;
using System.Data.Odbc;
using getClientConfigurations.ATSHandlers;
using MultiPhraseCreateOrder.Tests.Mocks;
using System.Text.RegularExpressions;
using System.Xml;
using System.Text;

namespace MultiPhraseCreateOrder.Tests
{
    [TestClass]
    public class icimsSubjectProfileTests
	{
        private ResourceManager rmUnitTest = new ResourceManager("MultiPhraseCreateOrder.Tests.Properties.Resources", typeof(HRNXUtilTests).Assembly);
        private ResourceManager rm = new ResourceManager("getClientConfigurations.Properties.HRNX", typeof(HRNXDBAccessor).Assembly);

		[TestMethod]
		public void ensureMaleGenderIsMappedProperly()
		{
			String gender1 = "Male";
			icimsSubjectProfile target = new icimsSubjectProfile();
			target.gender = gender1;

			String converted = target.convertToHRNXSubjectProifle();
			Console.WriteLine(string.Format("Gender1 converted result is [{0}]",converted));
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(converted);
			Assert.AreEqual("male", xmlDoc.SelectSingleNode("subject/field[@name='gender2']").InnerText);
		}
		[TestMethod]
		public void ensureFemaleGenderIsMappedProperly()
		{
			String gender1 = "Female";
			icimsSubjectProfile target = new icimsSubjectProfile();
			target.gender = gender1;

			String converted = target.convertToHRNXSubjectProifle();
			Console.WriteLine(string.Format("Gender1 converted result is [{0}]", converted));
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(converted);
			Assert.AreEqual("female", xmlDoc.SelectSingleNode("subject/field[@name='gender2']").InnerText);
		}
		[TestMethod]
		public void ensureNoGenderIsMappedProperly()
		{
			icimsSubjectProfile target = new icimsSubjectProfile();

			String converted = target.convertToHRNXSubjectProifle();
			Console.WriteLine(string.Format("Gender1 converted result is [{0}]", converted));
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(converted);
			Assert.AreEqual("unspecified", xmlDoc.SelectSingleNode("subject/field[@name='gender2']").InnerText);
		}
		[TestMethod]
		public void ensureCurrentAddressIsMappedProperly()
		{
			icimsSubjectProfile target = new icimsSubjectProfile();
			List<icimsAddress> addressList = new List<icimsAddress>();
			icimsAddress targetAddress = new icimsAddress();
			targetAddress.addressstreet1 = "123 Test Ave";
			targetAddress.addressstreet2 = "Line 2";
			targetAddress.addresscounty = "Kings";
			targetAddress.addresscity = "Seattle";
			targetAddress.addresszip = "98007";
			icimsState state = new icimsState();
			state.id = "D41001033";
			targetAddress.addressstate = state;
			icimsCountry country = new icimsCountry();
			country.abbrev = "US";
			targetAddress.addresscountry = country;
			addressList.Add(targetAddress);
			target.addresses = addressList;
			icimsAddressType type = new icimsAddressType();
			type.id = "D84002";
			targetAddress.addresstype = type;

			String converted = target.convertToHRNXSubjectProifle();
			Console.WriteLine(string.Format("Gender1 converted result is [{0}]", converted));
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(converted);
			Assert.AreEqual(targetAddress.addressstreet1, xmlDoc.SelectSingleNode("subject/field[@name='address_present_line1']").InnerText);
			Assert.AreEqual(targetAddress.addressstreet2, xmlDoc.SelectSingleNode("subject/field[@name='address_present_line2']").InnerText);
			Assert.AreEqual(targetAddress.addresscounty, xmlDoc.SelectSingleNode("subject/field[@name='address_present_county']").InnerText);
			Assert.AreEqual(targetAddress.addresscity, xmlDoc.SelectSingleNode("subject/field[@name='address_present_city']").InnerText);
			Assert.AreEqual("NJ", xmlDoc.SelectSingleNode("subject/field[@name='address_present_state']").InnerText);
			Assert.AreEqual(targetAddress.addresszip, xmlDoc.SelectSingleNode("subject/field[@name='address_present_zip']").InnerText);
			Assert.AreEqual("US", xmlDoc.SelectSingleNode("subject/field[@name='address_present_country']").InnerText);
		}
		[TestMethod]
		public void ensureBirthdayIsMappedProperly()
		{
			String dob = "1990-01-02";
			icimsSubjectProfile target = new icimsSubjectProfile();
			target.birthdate = dob;

			String converted = target.convertToHRNXSubjectProifle();
			Console.WriteLine(string.Format("dob converted result is [{0}]", converted));
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(converted);
			Assert.AreEqual("01/02/1990", xmlDoc.SelectSingleNode("subject/field[@name='dob']").InnerText);
		}
		[TestMethod]
		public void ensureBirthdayInWrongFormatIsMappedDirectly()
		{
			String dob = "Jan 22nd, 1990";
			icimsSubjectProfile target = new icimsSubjectProfile();
			target.birthdate = dob;

			String converted = target.convertToHRNXSubjectProifle();
			Console.WriteLine(string.Format("dob converted result is [{0}]", converted));
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(converted);
			Assert.AreEqual(dob, xmlDoc.SelectSingleNode("subject/field[@name='dob']").InnerText);
		}
		[TestMethod]
		public void ensureBirthdayTooLongWillIgnoreExtraCharactersIsMappedProperly()
		{
			String dob = "1990-01-0212312414asasf";
			icimsSubjectProfile target = new icimsSubjectProfile();
			target.birthdate = dob;

			String converted = target.convertToHRNXSubjectProifle();
			Console.WriteLine(string.Format("dob converted result is [{0}]", converted));
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(converted);
			Assert.AreEqual("01/02/1990", xmlDoc.SelectSingleNode("subject/field[@name='dob']").InnerText);
		}
		[TestMethod]
		public void ensurePhoneIsMappedProperly()
		{
			String phoneNum1 = "1234567890";
			String phoneNum2 = "11234567890";

			icimsSubjectProfile target = new icimsSubjectProfile();

			icimsPhone current1 = new icimsPhone();
			current1.phonenumber = phoneNum1;
			icimsPhoneType type1 = new icimsPhoneType();
			type1.id = "D83002";
			current1.phonetype = type1;

			icimsPhone current2 = new icimsPhone();
			current2.phonenumber = phoneNum2;
			icimsPhoneType type2 = new icimsPhoneType();
			type2.id = "D83004";
			current2.phonetype = type2;

			List<icimsPhone> phoneLists = new List<icimsPhone>();
			phoneLists.Add(current1);
			phoneLists.Add(current2);
			target.phones = phoneLists;

			String converted = target.convertToHRNXSubjectProifle();
			Console.WriteLine(string.Format("dob converted result is [{0}]", converted));
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(converted);
			Assert.AreEqual("123-456-7890", xmlDoc.SelectSingleNode("subject/field[@name='bestphone']").InnerText);
			Assert.AreEqual("123-456-7890", xmlDoc.SelectSingleNode("subject/field[@name='bestphone2']").InnerText);
		}
		[TestMethod]
		public void ensurePhoneOutOfOurFormatIsMappedProperly()
		{
			String phoneNum1 = "123";
			String phoneNum2 = "abcdefg1234567890";

			icimsSubjectProfile target = new icimsSubjectProfile();

			icimsPhone current1 = new icimsPhone();
			current1.phonenumber = phoneNum1;
			icimsPhoneType type1 = new icimsPhoneType();
			type1.id = "D83002";
			current1.phonetype = type1;

			icimsPhone current2 = new icimsPhone();
			current2.phonenumber = phoneNum2;
			icimsPhoneType type2 = new icimsPhoneType();
			type2.id = "D83004";
			current2.phonetype = type2;

			List<icimsPhone> phoneLists = new List<icimsPhone>();
			phoneLists.Add(current1);
			phoneLists.Add(current2);
			target.phones = phoneLists;

			String converted = target.convertToHRNXSubjectProifle();
			Console.WriteLine(string.Format("dob converted result is [{0}]", converted));
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(converted);
			Assert.AreEqual(phoneNum1, xmlDoc.SelectSingleNode("subject/field[@name='bestphone']").InnerText);
			Assert.AreEqual(phoneNum2, xmlDoc.SelectSingleNode("subject/field[@name='bestphone2']").InnerText);
		}
		[TestMethod]
		public void ensurePermissionToContactIsMappedProperly()
		{
			icimsSubjectProfile target = new icimsSubjectProfile();

			Workmaywecontact permissionToContact = new Workmaywecontact();

			// Negative case
			permissionToContact.value = "No";
			string result  = target.convertToHRNXBoolean(permissionToContact);
			Assert.AreEqual("false", result);

			// Positive case
			permissionToContact.value = "Yes";
			result = target.convertToHRNXBoolean(permissionToContact);
			Assert.AreEqual("true", result);

			// NULL case
			result = target.convertToHRNXBoolean(null);
			Assert.AreEqual("false", result);
		}
	}
}
