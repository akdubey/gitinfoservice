﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using getClientConfigurations.Models;
using System.IO;
using System.Net;
using System.Resources;
using getClientConfigurations.Controllers;
using System.Web.Http;
using System.Web.Http.Results;
using System.Net.Http;
using System.Collections.Generic;
using getClientConfigurations.ATSHandlers;
using MultiPhraseCreateOrder.Tests.Mocks;
using getClientConfigurations.ServiceProviderHandlers;
using getClientConfigurations.Exceptions;

namespace MultiPhraseCreateOrder.Tests
{
    [TestClass]
    public class MainRunnerUnitTests
    {
        private ResourceManager rm = new ResourceManager("getClientConfigurations.Properties.HRNX", typeof(MultiPhaseCreateOrderController).Assembly);
        private ResourceManager rmUnitTest = new ResourceManager("MultiPhraseCreateOrder.Tests.Properties.Resources", typeof(MainRunnerUnitTests).Assembly);

        [TestMethod]
        public void MainRunnerEnsureErrorWhenInvalidATSSpecified()
        {
            string atsID = "nonexistATSID";
            String expectedURL = "http://www.hrnx.com";
            String expectedPayload = "Some payload";
            String origin = System.Reflection.MethodBase.GetCurrentMethod().Name;

            HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
            MockWebAccessor webAccessor = new MockWebAccessor();
            webAccessor.url = expectedURL;
            webAccessor.payload = expectedPayload;

            // Save the latest errorID
            String lastErrorID = dbAccessor.getLatestErrorID(null, origin);

            MultiPhaseCreateOrderController webClient = new MultiPhaseCreateOrderController(dbAccessor, webAccessor);

           IHttpActionResult rawREsponse = webClient.acceptOrder(atsID, "other");

            // # Check it was a Ok result
            Assert.IsInstanceOfType(rawREsponse, typeof(BadRequestErrorMessageResult));

            BadRequestErrorMessageResult convertedResult = rawREsponse as BadRequestErrorMessageResult;

            // #2 Check the response is what we expected
            string expectedMsg = string.Format(rm.GetString("INVALID_ATS_ID"), atsID);
            StringAssert.Contains(convertedResult.Message, expectedMsg);

            // #3 Check errorStack has a new entry
            List<HRNXv1Error> returnedErrors = dbAccessor.getErrorFromErrorStackSince(lastErrorID, null, origin);

            // Check #3.1, make sure only 1 new error is generated
            Assert.AreEqual(1, returnedErrors.Count);

            // Check #3.2, make sure Each data entry matches with what we expect
            Assert.AreEqual(expectedMsg, returnedErrors[0].error_short);
            Assert.AreEqual(expectedURL, returnedErrors[0].error_blob);
            Assert.AreEqual(expectedPayload, returnedErrors[0].error_blob_2);
        }
        [TestMethod]
        public void MainRunnerEnsureATSHandlerCanReceievPayloadFromCreateOrder()
        {
            string atsID = "atsVerifier1";
            string spID = "mockSPHandler";
            AlwaysSuccessATSHandler tester = new AlwaysSuccessATSHandler();

            MockServiceProviderHandler spTester = new MockServiceProviderHandler();
			spTester.orderToReturn = new Order();

			Dictionary<string, IServiceProviderHandler> sphandlersMapping = new Dictionary<string, IServiceProviderHandler>();
            sphandlersMapping.Add(spID, spTester);

            Dictionary<string, IATSHandler> handlersMapping = new Dictionary<string, IATSHandler>();
            handlersMapping.Add(atsID,tester);

            HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
            MockWebAccessor webAccessor = new MockWebAccessor();

            // verifies resource file can be obtained
            String testResourcePath = "Resources/orderRequest.json";
            Assert.IsTrue(File.Exists(testResourcePath));
            String payloadFinal = File.ReadAllText(testResourcePath);
            webAccessor.payload = payloadFinal;

            MultiPhaseCreateOrderController webClient = new MultiPhaseCreateOrderController(dbAccessor, webAccessor, handlersMapping, sphandlersMapping);

            IHttpActionResult rawREsponse = webClient.acceptOrder(atsID, spID);

            // Check #1: it was a Ok result
            Assert.IsInstanceOfType(rawREsponse, typeof(OkNegotiatedContentResult<String>));

            // Check #2: check the handler DID recieved the payload we sent in
            tester.rawPayload = payloadFinal;

            // Write out the output
            OkNegotiatedContentResult<String> convertedResult = rawREsponse as OkNegotiatedContentResult<String>;
            Console.WriteLine(convertedResult.Content);

        }

        [TestMethod]
        public void MainRunnerEndToEndWorks()
        {
            // TODO: Replace this with a mock test ATS handler
            string atsID = "alwaysSuccessATSHandler";
            string spID = "mockSPHandler";
            AlwaysSuccessATSHandler tester = new AlwaysSuccessATSHandler();
            MockServiceProviderHandler spTester = new MockServiceProviderHandler();
			spTester.orderToReturn = new Order();

			Dictionary<string, IATSHandler> handlersMapping = new Dictionary<string, IATSHandler>();
            handlersMapping.Add(atsID, tester);

            Dictionary<string, IServiceProviderHandler> sphandlersMapping = new Dictionary<string, IServiceProviderHandler>();
            sphandlersMapping.Add(spID, spTester);

            HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
            MockWebAccessor webAccessor = new MockWebAccessor();
            webAccessor.url = "SomeURL";
            webAccessor.payload = default(string);
            MultiPhaseCreateOrderController webClient = new MultiPhaseCreateOrderController(dbAccessor, webAccessor,handlersMapping, sphandlersMapping);

            IHttpActionResult rawREsponse = webClient.acceptOrder(atsID, spID);

            // # Check it was a Ok result
            Assert.IsInstanceOfType(rawREsponse, typeof(OkNegotiatedContentResult<String>));

            // Write out the output
            OkNegotiatedContentResult<String> convertedResult = rawREsponse as OkNegotiatedContentResult<String>;
            Console.WriteLine(convertedResult.Content);

        }
        [TestMethod]
        public void MainRunnerEnsureServiceProviderHandlerWillReceiveOrderFromATSHandlerIfSuccessful()
        {
            Order target = new Order();
            target.status = OrderStatus.submitted_passthru;
            target.client_group_id = "SomeClient";
            target.candidate_id = "12345";

            CreateOrderResponse ra = new CreateOrderResponse();
            ra.status = HttpStatusCode.OK;

            string atsID = "atsVerifier1";
            mockATSHandler mock = new mockATSHandler();
            mock.orderToBeReturned = target;
            mock.orderResponse = ra;

            string serviceProviderID = "other";
            ServiceProviderHandlerParameterVerifier tester = new ServiceProviderHandlerParameterVerifier();

            Dictionary<string, IATSHandler> handlersMapping = new Dictionary<string, IATSHandler>();
            handlersMapping.Add(atsID, mock);

            Dictionary<string, IServiceProviderHandler> sphandlersMapping = new Dictionary<string, IServiceProviderHandler>();
            sphandlersMapping.Add(serviceProviderID, tester);

            HRNXDBAccessor dbAccessor = new HRNXDBAccessor("");
            MockWebAccessor webAccessor = new MockWebAccessor();

            MultiPhaseCreateOrderController webClient = new MultiPhaseCreateOrderController(dbAccessor, webAccessor, handlersMapping, sphandlersMapping);

            IHttpActionResult rawREsponse = webClient.acceptOrder(atsID, serviceProviderID);

            // Check #1: it was a Ok result
            Assert.IsInstanceOfType(rawREsponse, typeof(OkNegotiatedContentResult<String>));

            // Check #2: check the handler DID recieved the payload we sent in
            Assert.AreSame(target,tester.savedOrder);
        }

        [TestMethod]
        public void MainRunnerEnsureNonRecognizeServiceProviderWillResultInError()
        {
            Order target = new Order();
            target.status = OrderStatus.submitted_passthru;
            target.client_group_id = "SomeClient";
            target.candidate_id = "12345";

            CreateOrderResponse ra = new CreateOrderResponse();
            ra.status = HttpStatusCode.OK;

            string atsID = "atsVerifier1";
            mockATSHandler mock = new mockATSHandler();
            mock.orderToBeReturned = target;
            mock.orderResponse = ra;

            Dictionary<string, IATSHandler> handlersMapping = new Dictionary<string, IATSHandler>();
            handlersMapping.Add(atsID, mock);

            String origin = System.Reflection.MethodBase.GetCurrentMethod().Name;
            String serviceProviderID = "non-existance SP";
            String expectedURL = "http://www.hrnx.com";
            String expectedPayload = "Some payload";

            HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
            MockWebAccessor webAccessor = new MockWebAccessor();
            webAccessor.url = expectedURL;
            webAccessor.payload = expectedPayload;

            // Save the latest errorID
            String lastErrorID = dbAccessor.getLatestErrorID(null, origin);

            MultiPhaseCreateOrderController webClient = new MultiPhaseCreateOrderController(dbAccessor, webAccessor, handlersMapping);

            IHttpActionResult rawREsponse = webClient.acceptOrder(atsID, serviceProviderID);

            // Check #1: it was a BadRequest result
            Assert.IsInstanceOfType(rawREsponse, typeof(BadRequestErrorMessageResult));

            BadRequestErrorMessageResult convertedResult = rawREsponse as BadRequestErrorMessageResult;

            // #2 Check the response is what we expected
            string expectedMsg = string.Format(rm.GetString("INVALID_SERVICE_PROVIDER_ID"), serviceProviderID);
            StringAssert.Contains(convertedResult.Message, expectedMsg);

            // #3 Check errorStack has a new entry
            List<HRNXv1Error> returnedErrors = dbAccessor.getErrorFromErrorStackSince(lastErrorID, null, origin);

            // Check #3.1, make sure only 1 new error is generated
            Assert.AreEqual(1, returnedErrors.Count);

            // Check #3.2, make sure Each data entry matches with what we expect
            Assert.AreEqual(expectedMsg, returnedErrors[0].error_short);
            Assert.AreEqual(expectedURL, returnedErrors[0].error_blob);
            Assert.AreEqual(expectedPayload, returnedErrors[0].error_blob_2);
        }

        [TestMethod]
        public void MainRunnerEnsureNonRecognizedAcknowledgementTriggerAnUnknownAcknoledgementException()
        {
            Order target = new Order();
            target.status = OrderStatus.submitted_passthru;
            target.client_group_id = "SomeClient";
            target.candidate_id = "12345";

            string atsID = "atsVerifier1";
            mockATSHandler mock = new mockATSHandler();
            mock.orderToBeReturned = target;
            // purposely, don't set anything for acknowledgement

            Dictionary<string, IATSHandler> handlersMapping = new Dictionary<string, IATSHandler>();
            handlersMapping.Add(atsID, mock);

            String origin = System.Reflection.MethodBase.GetCurrentMethod().Name;
            String serviceProviderID = "other";
            String expectedURL = "http://www.hrnx.com";
            String expectedPayload = "Some payload";

            ServiceProviderHandlerParameterVerifier tester = new ServiceProviderHandlerParameterVerifier();
            Dictionary<string, IServiceProviderHandler> sphandlersMapping = new Dictionary<string, IServiceProviderHandler>();
            sphandlersMapping.Add(serviceProviderID, tester);

            HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
            MockWebAccessor webAccessor = new MockWebAccessor();
            webAccessor.url = expectedURL;
            webAccessor.payload = expectedPayload;

            // Save the latest errorID
            String lastErrorID = dbAccessor.getLatestErrorID(null, origin);

            MultiPhaseCreateOrderController webClient = new MultiPhaseCreateOrderController(dbAccessor, webAccessor, handlersMapping, sphandlersMapping);

            IHttpActionResult rawREsponse = webClient.acceptOrder(atsID, serviceProviderID);

            // Check #1: it was a BadRequest result
            Assert.IsInstanceOfType(rawREsponse, typeof(ExceptionResult));

            ExceptionResult convertedResult = rawREsponse as ExceptionResult;

            // #2 Check the response is what we expected
            Assert.IsInstanceOfType(convertedResult.Exception, typeof(UnknownAcknowledgementException));

            // #3 Check errorStack has a new entry
            List<HRNXv1Error> returnedErrors = dbAccessor.getErrorFromErrorStackSince(lastErrorID, null, origin);

            // Check #3.1, make sure only 1 new error is generated
            Assert.AreEqual(1, returnedErrors.Count);

            // Check #3.2, make sure Each data entry matches with what we expect
            Assert.AreEqual(UnknownAcknowledgementException.errorShort, returnedErrors[0].error_short);
            Assert.AreEqual(UnknownAcknowledgementException.errorDetail, returnedErrors[0].error_long);
            Assert.AreEqual(expectedURL, returnedErrors[0].error_blob);
            Assert.AreEqual(expectedPayload, returnedErrors[0].error_blob_2);
        }
		[TestMethod]
		public void MainRunnerEnsureErrorWhenATSIsMissing()
		{
			String expectedURL = "http://www.hrnx.com";
			String expectedPayload = "Some payload";
			String origin = System.Reflection.MethodBase.GetCurrentMethod().Name;

			HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
			MockWebAccessor webAccessor = new MockWebAccessor();
			webAccessor.url = expectedURL;
			webAccessor.payload = expectedPayload;

			// Save the latest errorID
			String lastErrorID = dbAccessor.getLatestErrorID(null, origin);

			MultiPhaseCreateOrderController webClient = new MultiPhaseCreateOrderController(dbAccessor, webAccessor);

			IHttpActionResult rawREsponse = webClient.acceptOrder(null, "other");

			// # Check it was a Ok result
			Assert.IsInstanceOfType(rawREsponse, typeof(BadRequestErrorMessageResult));

			BadRequestErrorMessageResult convertedResult = rawREsponse as BadRequestErrorMessageResult;

			// #2 Check the response is what we expected
			string expectedMsg = string.Format(rm.GetString("MISSING_ATS_ID"));
			StringAssert.Contains(convertedResult.Message, expectedMsg);

			// #3 Check errorStack has a new entry
			List<HRNXv1Error> returnedErrors = dbAccessor.getErrorFromErrorStackSince(lastErrorID, null, origin);

			// Check #3.1, make sure only 1 new error is generated
			Assert.AreEqual(1, returnedErrors.Count);

			// Check #3.2, make sure Each data entry matches with what we expect
			Assert.AreEqual(expectedMsg, returnedErrors[0].error_short);
			Assert.AreEqual(expectedURL, returnedErrors[0].error_blob);
			Assert.AreEqual(expectedPayload, returnedErrors[0].error_blob_2);
		}
		[TestMethod]
		public void MainRunnerEnsureErrorWhenServiceProviderIsMissing()
		{
			Order target = new Order();
			target.status = OrderStatus.submitted_passthru;
			target.client_group_id = "SomeClient";
			target.candidate_id = "12345";

			CreateOrderResponse ra = new CreateOrderResponse();
			ra.status = HttpStatusCode.OK;

			string atsID = "atsVerifier1";
			mockATSHandler mock = new mockATSHandler();
			mock.orderToBeReturned = target;
			mock.orderResponse = ra;

			Dictionary<string, IATSHandler> handlersMapping = new Dictionary<string, IATSHandler>();
			handlersMapping.Add(atsID, mock);

			String origin = System.Reflection.MethodBase.GetCurrentMethod().Name;
			String expectedURL = "http://www.hrnx.com";
			String expectedPayload = "Some payload";

			HRNXDBAccessor dbAccessor = new HRNXDBAccessor(rmUnitTest.GetString("LocalDBConnectStr"));
			MockWebAccessor webAccessor = new MockWebAccessor();
			webAccessor.url = expectedURL;
			webAccessor.payload = expectedPayload;

			// Save the latest errorID
			String lastErrorID = dbAccessor.getLatestErrorID(null, origin);

			MultiPhaseCreateOrderController webClient = new MultiPhaseCreateOrderController(dbAccessor, webAccessor, handlersMapping);

			IHttpActionResult rawREsponse = webClient.acceptOrder(atsID, null);

			// Check #1: it was a BadRequest result
			Assert.IsInstanceOfType(rawREsponse, typeof(BadRequestErrorMessageResult));

			BadRequestErrorMessageResult convertedResult = rawREsponse as BadRequestErrorMessageResult;

			// #2 Check the response is what we expected
			string expectedMsg = rm.GetString("MISSING_SERVICE_PROVIDER_ID");
			StringAssert.Contains(convertedResult.Message, expectedMsg);

			// #3 Check errorStack has a new entry
			List<HRNXv1Error> returnedErrors = dbAccessor.getErrorFromErrorStackSince(lastErrorID, null, origin);

			// Check #3.1, make sure only 1 new error is generated
			Assert.AreEqual(1, returnedErrors.Count);

			// Check #3.2, make sure Each data entry matches with what we expect
			Assert.AreEqual(expectedMsg, returnedErrors[0].error_short);
			Assert.AreEqual(expectedURL, returnedErrors[0].error_blob);
			Assert.AreEqual(expectedPayload, returnedErrors[0].error_blob_2);
		}
	}
}
