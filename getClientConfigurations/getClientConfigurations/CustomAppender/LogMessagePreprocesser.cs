﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using log4net.Core;
using log4net.Layout.Pattern;

namespace getClientConfigurations.CustomAppender
{
    public class LogMessagePreprocesser : PatternLayoutConverter
    {
        protected override void Convert(TextWriter writer, LoggingEvent loggingEvent)
        {
            string originalString = loggingEvent.RenderedMessage;
            string finalString = originalString;

            // #1: Replace all quotes with another set of quote, so we are escaping
            finalString = finalString.Replace("\"","\"\"");

            // #2: Surround the entire string with quote, so excel will treat as a single string
            finalString = "\"" + finalString + "\"";

            writer.Write(finalString);
        }
    }
}