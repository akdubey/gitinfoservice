﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using getClientConfigurations.Models;
using System.Web.Http;

namespace getClientConfigurations.ATSHandlers
{
    public interface IATSHandler
    {
        /// <summary>
        /// Main of ATS Handler
        /// </summary>
        /// <param name="rawRequest">raw request</param>
        /// <param name="dbAccessor">DB accessor to use</param>
        /// <param name="webAccessor">Web accessor to use</param>
        /// <returns></returns>
        Order processRequest(OrderRequest rawRequest, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor);

        /// <summary>
        /// Ask the ATS handler to analyze the raw Request and save things up
        /// </summary>
        /// <param name="request"></param>
        /// <param name="rawRequest"></param>
        /// <param name="dbAccessor"></param>
        /// <param name="webAccessor"></param>
        /// <returns></returns>
        Order analyzeRequest(Order request, OrderRequest rawRequest, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor);

        void getServiceProviderID(Order result, HRNXWebAccessor webAccessor, HRNXDBAccessor dbAccessor);
        Boolean needToCreateNewOrder(Order result, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor);
        Order createANewOrder(Order result, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor);
        Order findExistingOrder(Order result, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor);

		// Acknowledgement side
		CreateOrderResponse processResult(Order result, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor);
		CreateOrderResponse sendAcknowledgement(Order result, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor);
    }
}
