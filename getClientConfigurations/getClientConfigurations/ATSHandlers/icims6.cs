﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using getClientConfigurations.Models;
using Newtonsoft.Json;
using System.Resources;
using System.Text;
using System.Net;
using System.Web.Http;
using Newtonsoft.Json.Converters;
using getClientConfigurations.Exceptions;

namespace getClientConfigurations.ATSHandlers
{
    public class icims6 : ATSHandlerBase
    {
        private ResourceManager rm = new ResourceManager("getClientConfigurations.Properties.HRNX", typeof(HRNXDBAccessor).Assembly);
		private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		/// <summary>
		/// Constants for icims
		/// </summary>
		public static readonly String ICIMS6_LOCATION_NAME = "ATSHandler_icims6";
		public static readonly String ATS_ID = "icims6";
        private static readonly String clientUserID = "groupAuth";
        public static readonly int timeout = 10000; // Requested by Siegy. Because we need to call 2 calls to icims, we have to limit our timeout of each call to 10 seconds
        public static readonly int ICIMS_RETRY_MAX = 3;

        public icimsRequest parsedRequest;
        public icimsJobProfile parsedJobProfile;
        public icimsSubjectProfile parsedSubjectProfile;
        public icimsUser parsedUser;
        public icimsAuthForm icimsAuthForms;
        private String initialRequest;
        public String client_group_id;
        public String candidateID;
        private String system_id;
        public String jobID;
        private String profileURL;
        private String jobURL;
        public String workflowURL;
		public String postbackLocation;
		private String recruiter;
        private String recruiterEmail;
        private String AuthFormData;
        public icimsPhrase currentPhrase;
        private String currentPackage;
        public String original_txn_id;
        private List<Tuple<String, String>> icimsHeader;

		public icims6()
        {
            location = ICIMS6_LOCATION_NAME;
        }
        public override string getID()
        {
            return ATS_ID;
        }

        private List<Tuple<String, String>> constructIcimsHeader(Order request, HRNXDBAccessor dbAccessor)
        {
            String username = dbAccessor.getATSCredentials(request, location, ATS_ID, "username");
            String password = dbAccessor.getATSCredentials(request, location, ATS_ID, "password");

            List<Tuple<String, String>> headers = new List<Tuple<string, string>>();
            headers.Add(HRNXUtil.generateBasicAuthHeader(username, password));
            return headers;
        }

        /// <summary>
        /// TODO: Copy over logic from HRNX that extracts recruiter info from icims.
        /// 
        /// Set recruiter info based on data in icims
        /// </summary>
        /// <param name="result">The Order object</param>
        /// <param name="icimsHeader">headers to make icims web service call</param>
        /// <param name="timeout">timeout to be used</param>
        /// <param name="webAccessor">web accessor to make http calls</param>
        /// <param name="dbAccessor">db accessor to make DB calls</param>
        /// <param name="logContent">tracks all http calls made, to be stored in AWACS</param>
        private void loadRecruiterInfo(Order result, List<Tuple<String, String>> icimsHeader, int timeout, HRNXWebAccessor webAccessor, HRNXDBAccessor dbAccessor, StringBuilder logContent)
        {
            if (!String.IsNullOrEmpty(parsedRequest.userId))
            {
                List<Tuple<string, string>> replacements = new List<Tuple<string, string>>();
                replacements.Add(new Tuple<string, string>("^^^CLIENT_GROUP_ID^^^", result.client_group_id));
                replacements.Add(new Tuple<string, string>("^^^USER_ID^^^", parsedRequest.userId));

                getFieldEndpoint endpointData = dbAccessor.getTemplateEndpoint(ATS_ID, ATS_ID + "_ep6", result, location, replacements);
                String recruiterURL = endpointData.url;

                recruiterURL = recruiterURL + "?fields=email";

                HRNXWebResponse rawResponse = httpSendToIcims(recruiterURL, "GET", "application/json", "", icimsHeader, timeout, logContent, webAccessor, dbAccessor, result, "user");

				if (rawResponse != null)
				{
					// Create icimsUser from the data
					try
					{
						parsedUser = JsonConvert.DeserializeObject<icimsUser>(rawResponse.responseText);
						if (parsedUser != null)
							recruiterEmail = parsedUser.email;
						else
							log.Debug("parsedUser appears to be empty");
					}
					catch (Newtonsoft.Json.JsonReaderException e)
					{
						// This field is optional, so not going to error out here, but we will log it
						log.Fatal(e);
					}
				}
				else
					log.Debug("send to icims reported an error");
			}

        }
        /// <summary>
        /// TODO: Copy over logic from HRNX that extracts recruiter info from icims.
        /// 
        /// Load in AuthForm data from icims into HRNX
        /// </summary>
        /// <param name="result">The Order object</param>
        /// <param name="icimsHeader">headers to make icims web service call</param>
        /// <param name="timeout">timeout to be used</param>
        /// <param name="webAccessor">web accessor to make http calls</param>
        /// <param name="dbAccessor">db accessor to make DB calls</param>
        /// <param name="logContent">tracks all http calls made, to be stored in AWACS</param>
        private void loadAuthForm(Order result, List<Tuple<String, String>> icimsHeader, int timeout, HRNXWebAccessor webAccessor, HRNXDBAccessor dbAccessor, StringBuilder logContent)
        {
			log.Debug("Enter loadAuthForm");
			string AuthFormId = dbAccessor.getATSCredentials(result, location, ATS_ID, "AuthFormID");
            if (!String.IsNullOrEmpty(AuthFormId))
            {
                List<Tuple<string, string>> replacements = new List<Tuple<string, string>>();
                replacements.Add(new Tuple<string, string>("^^^CLIENT_GROUP_ID^^^", result.client_group_id));

                getFieldEndpoint endpointData = dbAccessor.getTemplateEndpoint(ATS_ID, ATS_ID + "_ep7", result, location, replacements);
                String searchURL = endpointData.url;
                String payloadJson = endpointData.xslt;
				log.Debug(string.Format("searchURL = [{0}]", searchURL));
				log.Debug(string.Format("payloadJson = [{0}]", payloadJson));

				payloadJson = payloadJson.Replace("^^^AUTH_FORM_ID^^^", AuthFormId);
                payloadJson = payloadJson.Replace("^^^CANDIDATE_ID^^^", parsedRequest.personId);
                HRNXWebResponse rawResponse = httpSendToIcims(searchURL, "POST", "application/json", payloadJson, icimsHeader, timeout, logContent, webAccessor, dbAccessor, result, "searchURL");

				if(rawResponse == null)
				{
					log.Debug("rawResponse is null. Exiting loadAuthForm()");
					// Then, we cannot process anymore 
					return;
				}
                try
                {
                    icimsAuthForms = JsonConvert.DeserializeObject<icimsAuthForm>(rawResponse.responseText);
					if (icimsAuthForms == null)
					{
						log.Debug("icimsAuthForms is null");
						AuthFormData = "";
					}
					else
					{
						if ((icimsAuthForms.searchResults.Count > -1))
						{
							// Our search should only returned 1 entry.
							// Regardless, we will ALWAYS pick the 1st entry
							string formId = icimsAuthForms.searchResults[0].id;
							if (!String.IsNullOrEmpty(formId))
							{
								String username = dbAccessor.getATSCredentials(result, location, ATS_ID, "username");
								String password = dbAccessor.getATSCredentials(result, location, ATS_ID, "password");
								log.Debug(string.Format("username = [{0}]", username));
								log.Debug(string.Format("password = [{0}]", password));

								replacements.Add(new Tuple<string, string>("^^^FORM_ID^^^", formId));
								endpointData = dbAccessor.getTemplateEndpoint(ATS_ID, ATS_ID + "_ep8", result, location, replacements);
								String icimsPDFURL = endpointData.url;
								String hrnxConverterURLTemplate = endpointData.xslt;
								log.Debug(string.Format("icimsPDFURL = [{0}]", icimsPDFURL));
								log.Debug(string.Format("hrnxConverterURLTemplate = [{0}]", hrnxConverterURLTemplate));

								hrnxConverterURLTemplate = hrnxConverterURLTemplate.Replace("^^^TARGET_URL^^^", icimsPDFURL);
								hrnxConverterURLTemplate = hrnxConverterURLTemplate.Replace("^^^USERNAME^^^", username);
								hrnxConverterURLTemplate = hrnxConverterURLTemplate.Replace("^^^PASSWORD^^^", password);

								HRNXWebResponse responseForAuthForm = httpSendToIcims(hrnxConverterURLTemplate, "GET", "application/json", "", icimsHeader, timeout, logContent, webAccessor, dbAccessor, result, "AuthForm");
								if (responseForAuthForm == null)
								{
									log.Debug("responseForAuthForm is null. Exiting loadAuthForm()");
									// Then, we cannot process anymore 
									return;
								}
								String AuthFormBinaryData = responseForAuthForm.responseText;
								log.Debug(string.Format("AuthFormBinaryData for txn_id [{0}] = [{1}]", result.txn_id, AuthFormBinaryData));

								// The response from our web service is double-quoted, so we want to remove the quotes here
								AuthFormData = AuthFormBinaryData.Substring(1, AuthFormBinaryData.Length - 2);
							}
							else
								log.Debug("formId is empty");
						}
						else
							log.Debug("searchResults has a count < -1");
					}
				}
				catch (Newtonsoft.Json.JsonReaderException e)
                {
					log.Fatal(e);
                    HRNXUtil.logException(result, 1, location, rm.GetString("INVALID_ICIMS_AUTH_FORM_JSON"), logContent.ToString(), e, dbAccessor);
                    AuthFormData = "";
                }
			}
			else
			{
				log.Debug("AuthFormId is blank");
			}
			log.Debug("Exit loadAuthForm");
		}
		public HRNXWebResponse httpSendToIcims(String targetURL, String httpMethod, String contentType, String payload, List<Tuple<String, String>> icimsHeader, int timeout, StringBuilder logContent, HRNXWebAccessor webAccessor, HRNXDBAccessor dbAccessor, Order result, String caller)
        {
            HRNXWebResponse rawResponse;
            int currentTry = 0;

            do
            {
                rawResponse = webAccessor.httpSend(targetURL, httpMethod, contentType, payload, icimsHeader, timeout, logContent);
                currentTry++;
            }
            while (HttpStatusCode.RequestTimeout.Equals(rawResponse.status) && currentTry < ICIMS_RETRY_MAX);

			// Validate the response, so we can throw errors out
			if (HttpStatusCode.OK.Equals(rawResponse.status) || HttpStatusCode.Created.Equals(rawResponse.status) || HttpStatusCode.NoContent.Equals(rawResponse.status))
				return rawResponse;
			else
			{
				HRNXUtil.logError(result, 3, location, string.Format(rm.GetString("ICIMS_RETURNED_NON_OK_RESPONSE"), caller), "", logContent.ToString(), "", dbAccessor);
				return null;
			}
		}
		private String getSubjectProfileData(Order result, String profileURL, List<Tuple<String, String>> icimsHeader, int timeout, HRNXWebAccessor webAccessor, HRNXDBAccessor dbAccessor, StringBuilder logContent)
        {
            profileURL = profileURL + "?fields=firstname,middlename,lastname,email,gender,birthdate,addresses,phones,usssn,workexperience,education,references,addresshistory,drivinglicensenumber,drivinglicensestate,licensecertification";

            HRNXWebResponse rawResponse = httpSendToIcims(profileURL, "GET", "application/json", "", icimsHeader, timeout, logContent, webAccessor, dbAccessor, result, "subjectProfile");
			if(rawResponse == null)
			{
				// Then something went wrong, so we need to just return
				return default(string);
			}

			// Create icimsSubjectProfile xml from the data
			try
            {
                parsedSubjectProfile = JsonConvert.DeserializeObject<icimsSubjectProfile>(rawResponse.responseText);
                if (parsedSubjectProfile == null)
                {
                    HRNXUtil.logError(result, 3, location, string.Format(rm.GetString("ICIMS_RETURNED_EMPTY_RESPONSE"), "subjectProfile"), "", logContent.ToString(), "", dbAccessor);
                    return default(String);
                }
				if (String.IsNullOrEmpty(parsedSubjectProfile.firstname))
				{
					HRNXUtil.logError(result, 3, location, rm.GetString("MISSING_FIRSTNAME"), "", logContent.ToString(), "", dbAccessor);
					return default(String);
				}
				if (String.IsNullOrEmpty(parsedSubjectProfile.lastname))
				{
					HRNXUtil.logError(result, 3, location, rm.GetString("MISSING_LASTNAME"), "", logContent.ToString(), "", dbAccessor);
					return default(String);
				}
				if (String.IsNullOrEmpty(parsedSubjectProfile.email))
				{
					HRNXUtil.logError(result, 3, location, rm.GetString("MISSING_EMAIL"), "", logContent.ToString(), "", dbAccessor);
					return default(String);
				}
				return parsedSubjectProfile.convertToHRNXSubjectProifle();
            }
            catch (Newtonsoft.Json.JsonReaderException e)
            {
                HRNXUtil.logException(result, 3, location, rm.GetString("INVALID_ICIMS_SUBJECT_PROFILE_JSON"), logContent.ToString(), e, dbAccessor);
                return default(String);
            }
        }

        private String getServiceData(Order result, String jobURL, List<Tuple<String, String>> icimsHeader, int timeout, HRNXWebAccessor webAccessor, HRNXDBAccessor dbAccessor, StringBuilder logContent)
        {

            currentPackage = getPackage(result, jobURL, icimsHeader, timeout, webAccessor, dbAccessor, logContent);

            if (String.IsNullOrEmpty(currentPackage))
                // If we fail to extract package, then we should bubble the error up above
                return default(string);

            // Update additionalData of Order
            result.additionalData.Add(OrderAdditionalDataKeys.package, currentPackage);

            StringBuilder serviceXML = new StringBuilder();
            serviceXML.Append("\t\t<service type=\"passthru\" tp=\"");
            serviceXML.Append(associatedServiceProviderID);
            serviceXML.Append("\">");
            serviceXML.Append(Environment.NewLine);

            serviceXML.Append("\t\t\t<field name=\"package\" type=\"value\">");
            serviceXML.Append("<![CDATA[");

            serviceXML.Append(currentPackage);

            serviceXML.Append("]]>");
            serviceXML.Append("</field>");
            serviceXML.Append(Environment.NewLine);

            serviceXML.Append("\t\t</service>");
            serviceXML.Append(Environment.NewLine);

            extractEducationData(serviceXML);
            extractEmploymentData(serviceXML);
            extractReferenceData(serviceXML);
            extractMVRData(serviceXML);
			extractLicenseData(serviceXML);

			return serviceXML.ToString();
        }
		/// <summary>
		/// Extract License data from icims over to HRNX
		/// </summary>
		/// <param name="serviceXML"></param>
		private void extractLicenseData(StringBuilder serviceXML)
		{
			parsedSubjectProfile.licenseProcessing(serviceXML);
		}

		/// <summary>
		/// Extract education data from icims over to HRNX
		/// </summary>
		/// <param name="serviceXML"></param>
		private void extractEducationData(StringBuilder serviceXML)
        {
            parsedSubjectProfile.EducationProcessing(serviceXML);
        }

        /// <summary>
        /// Extract employment data from icims over to HRNX
        /// </summary>
        /// <param name="serviceXML"></param>
        private void extractEmploymentData(StringBuilder serviceXML)
        {
            parsedSubjectProfile.EmploymentProcessing(serviceXML);
        }
        /// <summary>
        /// Extract reference data from icims over to HRNX
        /// </summary>
        /// <param name="serviceXML"></param>
        private void extractReferenceData(StringBuilder serviceXML)
        {
			parsedSubjectProfile.referenceProcessing(serviceXML);
		}
		/// <summary>
		/// Extract MVR data from icims over to HRNX
		/// </summary>
		/// <param name="serviceXML"></param>
		private void extractMVRData(StringBuilder serviceXML)
        {
            parsedSubjectProfile.MVRProcessing(serviceXML);
        }
        private String extractLocationFromIcims(Order result, String workflowURL, String payloadJson, List<Tuple<String, String>> icimsHeader, int timeout, HRNXWebAccessor webAccessor, HRNXDBAccessor dbAccessor, StringBuilder logContent)
        {
            HRNXWebResponse response = httpSendToIcims(workflowURL, "POST", "application/json", payloadJson, icimsHeader, timeout, logContent, webAccessor, dbAccessor, result, "extractLocationFromIcims");

			if(response == null)
			{
				log.Debug("Cannot extract location from icims because we got a non-ok response from icims");
				return default(String);
			}
			try
            {
                return response.headers["Location"];
            }
            catch (System.Collections.Generic.KeyNotFoundException e)
            {
                HRNXUtil.logException(result, 3, location, rm.GetString("UNABLE_TO_GET_POSTBACK_LOCATION"), logContent.ToString(), e, dbAccessor);
                return default(String);
            }
        }

        public override Order analyzeRequest(Order result, OrderRequest rawRequest, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor)
        {
            // Convert to icimsRequest
            initialRequest = rawRequest.rawPayload;

            try
            {
                parsedRequest = JsonConvert.DeserializeObject<icimsRequest>(initialRequest);
            }
            catch (Newtonsoft.Json.JsonReaderException e)
            {
                HRNXUtil.logException(result, 3, location, rm.GetString("INVALID_REQUEST_JSON"), initialRequest, e, dbAccessor);
                return result;
            }
            if (parsedRequest == null)
            {
                HRNXUtil.logError(result, 3, location, rm.GetString("INVALID_REQUEST_EMPTY"), "", initialRequest, "", dbAccessor);
                return result;
            }

            client_group_id = parsedRequest.customerId;
            if (String.IsNullOrEmpty(client_group_id))
            {
                HRNXUtil.logError(result, 3, location, rm.GetString("CLIENT_GROUP_ID_EMPTY"), "", initialRequest, "", dbAccessor);
                return result;
            }

            result.client_group_id = client_group_id;
            result.client_user_id = clientUserID;

            if (!dbAccessor.isClientGroupIDValid(result, location))
            {
                HRNXUtil.logError(result, 3, location, rm.GetString("CLIENT_GROUP_ID_INVALID"), "", initialRequest, "", dbAccessor);
                return result;
            }

            system_id = parsedRequest.systemId;
            if (String.IsNullOrEmpty(system_id))
            {
                HRNXUtil.logError(result, 3, location, rm.GetString("REQUIRED_DATA_MISSING"), rm.GetString("SYSTEM_ID"), initialRequest, "", dbAccessor);
                return result;
            }
            jobID = parsedRequest.jobId;

            candidateID = parsedRequest.personId;
            if (String.IsNullOrEmpty(candidateID))
            {
                HRNXUtil.logError(result, 3, location, rm.GetString("REQUIRED_DATA_MISSING"), rm.GetString("CANDIDATE_ID"), initialRequest, "", dbAccessor);
                return result;
            }
            result.candidate_id = candidateID;

            profileURL = parsedRequest.getProfileURL();
            if (String.IsNullOrEmpty(profileURL))
            {
                HRNXUtil.logError(result, 3, location, rm.GetString("REQUIRED_DATA_MISSING"), rm.GetString("PERSON_URL"), initialRequest, "", dbAccessor);
                return result;
            }

            jobURL = parsedRequest.getJobURL();
            if (String.IsNullOrEmpty(jobURL))
            {
                HRNXUtil.logError(result, 3, location, rm.GetString("REQUIRED_DATA_MISSING"), rm.GetString("JOB_URL"), initialRequest, "", dbAccessor);
                return result;
            }

            workflowURL = parsedRequest.getWorkflowURL();
            if (String.IsNullOrEmpty(workflowURL))
            {
                HRNXUtil.logError(result, 3, location, rm.GetString("REQUIRED_DATA_MISSING"), rm.GetString("APPLICANT_WORKFLOW_URL"), initialRequest, "", dbAccessor);
                return result;
            }

			icimsHeader = constructIcimsHeader(result, dbAccessor);

			// Make it use TLS 1.2 because icims needs it
			ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

			result.status = OrderStatus.passthru;
            return result;
        }

        public override bool needToCreateNewOrder(Order request, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor)
        {
            String phrase = parsedRequest.newStatus;

            // Pull DB to figure out what type this status is
            HRNXPhase phraseData = dbAccessor.getPhraseData(request, location, phrase, associatedServiceProviderID);
			if (phraseData == null)
			{
				HRNXUtil.logError(request, 3, location, rm.GetString("UNKNOWN_STAGE"), phrase, initialRequest, "", dbAccessor);
				throw new CannotDetermineOrderStatusException();
			}

			icimsPhrase currentPhrase = (icimsPhrase)Enum.Parse(typeof(icimsPhrase), phraseData.enumValue);

			if (icimsPhrase.BACKGROUND.Equals(currentPhrase))
            {
                // If background, then we need to check duplicate table to see if we have any credit already ordered
                String creditCheckDuplicateKey = client_group_id + "_" + candidateID + "_" + jobID + "_" + icimsPhrase.CREDIT_CHECK;
                HRNXATSDuplicateEntry entry = dbAccessor.getATSDuplicateEntryData(request, creditCheckDuplicateKey, location);

                if (!(entry == null))
                {
                    // save off the original txn_id, so we don't need to repull
                    original_txn_id = entry.original_txn_id;
                    return false;
                }
            }

            // so if it gets to here because it didn't hit the other paths, then we will create new txn_id
            return true;
        }

        public override Order createANewOrder(Order result, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor)
        {
            StringBuilder logContent = new StringBuilder();

            // Extract data from icims request and build out qProcessing payload
            String subjecrProfileXML = getSubjectProfileData(result, profileURL, icimsHeader, timeout, webAccessor, dbAccessor, logContent);
            if (String.IsNullOrEmpty(subjecrProfileXML))
            {
                // then an error has occurred, and we had tracked the error previous, so just return
                return result;
            }

            // Load recruiter info from icims
            loadRecruiterInfo(result, icimsHeader, timeout, webAccessor, dbAccessor, logContent);

            // GENERATING SERVICE NODES
            String serviceXML = getServiceData(result, jobURL, icimsHeader, timeout, webAccessor, dbAccessor, logContent);
            if (String.IsNullOrEmpty(serviceXML))
            {
                // then an error has occurred, and we had tracked the error previous, so just return
                return result;
            }

            String createOrderXML = dbAccessor.getLoadWidgetXSLT(result, location, ATS_ID);

            String txn_id = dbAccessor.generateTxnID(result, location);

            //GENERATE PAYLOAD TO GET FINAL WORKFLOW URL(A URL ON WHICH WE WILL UPDATE THE RESULT USING PATCH)
            List<Tuple<String, String>> addressReplacement = new List<Tuple<string, string>>();
            addressReplacement.Add(new Tuple<string, string>("^^PERSON_URL^^", profileURL));
            getFieldEndpoint endpoint = dbAccessor.getTemplateEndpoint(ATS_ID, ATS_ID + "_ep3", result, location, addressReplacement);
            String fullWorkflowUrl = endpoint.url;
            String payloadJson = endpoint.xslt;

            postbackLocation = extractLocationFromIcims(result, fullWorkflowUrl, payloadJson, icimsHeader, timeout, webAccessor, dbAccessor, logContent);
            if (String.IsNullOrEmpty(postbackLocation))
            {
                // then an error has occurred, and we had tracked the error previous, so just return
                return result;
            }

			// Load AuthForm info from icims
			//
			// Need a AuthForm specific logContent because it will be too large to be logged in erroStack
			// Therefore, the best we can do is logged to log4net
			StringBuilder authFormLogContent = new StringBuilder();
			loadAuthForm(result, icimsHeader, timeout, webAccessor, dbAccessor, authFormLogContent);

			createOrderXML = createOrderXML.Replace("^^txn_id^^", txn_id);
            createOrderXML = createOrderXML.Replace("^^txn_requestdate^^", HRNXUtil.getCurrentTimeInUTCInStringForm());
            createOrderXML = createOrderXML.Replace("<client_group_id/>", "<client_group_id>" + client_group_id + "</client_group_id>");
            createOrderXML = createOrderXML.Replace("^^tp_id^^", ATS_ID);
            createOrderXML = createOrderXML.Replace("<tp_uqid/>", "<tp_uqid>" + candidateID + "</tp_uqid>");
            createOrderXML = createOrderXML.Replace("<workflowDetailsURL/>", "<workflowDetailsURL>" + postbackLocation + "</workflowDetailsURL>");
            createOrderXML = createOrderXML.Replace("<quoteback id=\"2\"/>", "<quoteback id=\"2\">" + system_id + "</quoteback>");
            createOrderXML = createOrderXML.Replace("<requisitionId/>", "<requisitionId>" + jobID + "</requisitionId>");
            createOrderXML = createOrderXML.Replace("^^requestor_email^^", recruiterEmail);
            createOrderXML = createOrderXML.Replace("^^requestor_name^^", recruiter);
            createOrderXML = createOrderXML.Replace("^^auth_form^^", AuthFormData);
            createOrderXML = createOrderXML.Replace("<subject/>", subjecrProfileXML);
            createOrderXML = createOrderXML.Replace("<service/>", serviceXML);

            result.txn_id = txn_id;
            result.status = OrderStatus.passthru;
            result.createOrderHandlerName = location;
            result.orderingIP = webAccessor.extractRequesterIPFromHttpRequest(HttpContext.Current);

            result.payload_hrnx = createOrderXML;

            // create the order in HRNX
            dbAccessor.writeToQProcessing(result, location);
            dbAccessor.writeInitialRequestToHRNXLogTable(result, location, initialRequest);
            dbAccessor.writeInfoToHRNXLogTable(result, location, logContent.ToString());
			String authFormLog = authFormLogContent.ToString();
			if(!String.IsNullOrEmpty(authFormLog))
				// Only log it to HRNX log if we have a value
				dbAccessor.writeInfoToHRNXLogTable(result, location, authFormLog);

			return result;
        }

        public override Order findExistingOrder(Order result, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor)
        {
			Order returnVal = dbAccessor.getOrderFromDB(original_txn_id, result, location);

            // pull current package and store it in the order's metadata
            StringBuilder getPackageLogContent = new StringBuilder();
            currentPackage = getPackage(result, jobURL, icimsHeader, timeout, webAccessor, dbAccessor, getPackageLogContent);
            result.additionalData.Add(OrderAdditionalDataKeys.package, currentPackage);

            // need to copy over data that exist in qProcessing, everything else should be re-get
            result.txn_id = returnVal.txn_id;
            result.payload_hrnx = returnVal.payload_hrnx;

			// set postbackURL
			postbackLocation = HRNXUtil.extractDataFromOrder(returnVal.payload_hrnx, "/order/workflowDetailsURL");

            // Log an entry to indicate that a duplicate request has come in
            dbAccessor.writeInitialRequestToHRNXLogTable(result, location, initialRequest);
            if (!String.IsNullOrEmpty(getPackageLogContent.ToString()))
                dbAccessor.writeInfoToHRNXLogTable(result, location, getPackageLogContent.ToString());

            return result;
        }

        public String getPackage(Order request, String jobURL, List<Tuple<String, String>> icimsHeader, int timeout, HRNXWebAccessor webAccessor, HRNXDBAccessor dbAccessor, StringBuilder logContent)
        {
            String phrase = parsedRequest.newStatus;

            HRNXPhase phraseData = dbAccessor.getPhraseData(request, location, phrase, associatedServiceProviderID);

            currentPhrase = (icimsPhrase)Enum.Parse(typeof(icimsPhrase), phraseData.enumValue);

            if (icimsPhrase.CREDIT_CHECK.Equals(currentPhrase) ||
                icimsPhrase.ADVERSE_ACTION.Equals(currentPhrase))
            {
                return phraseData.data;
            }
            else
            {
                // Try to pull the package from icims
                // now try to get package from icims
                jobURL = jobURL + "?fields=joblocation,bgcpackagetype";
                HRNXWebResponse rawResponse = httpSendToIcims(jobURL, "GET", "application/json", "", icimsHeader, timeout, logContent, webAccessor, dbAccessor, request, "getPackage");
				log.Debug(logContent.ToString());

				if(rawResponse == null)
				{
					log.Debug("GetPackage returned non-ok response, so exiting");
					return default(String);
				}
                // Create icimsSubjectProfile xml from the data
                try
                {
                    parsedJobProfile = JsonConvert.DeserializeObject<icimsJobProfile>(rawResponse.responseText);

                    if (parsedJobProfile == null)
                    {
                        HRNXUtil.logError(request, 3, location, string.Format(rm.GetString("ICIMS_RETURNED_EMPTY_RESPONSE"), "jobProfile"), "", logContent.ToString(), "", dbAccessor);
                        return default(String);
                    }

					if(parsedJobProfile.bgcpackagetype == null)
					{
						// Then we got no package to order, which we cannot accept at this time
						log.Debug("No services/pacakge has been obtained from icims. Erroring out");
						HRNXUtil.logError(request, 3, location,rm.GetString("NO_SERVICE_TO_ORDER"), "", logContent.ToString(), "", dbAccessor);
						return default(String);
					}

					StringBuilder returnStr = new StringBuilder();
                    for (int i = 0; i < parsedJobProfile.bgcpackagetype.Count; i++)
                    {
                        if (i > 0)
                            returnStr.Append(",");
                        returnStr.Append(parsedJobProfile.bgcpackagetype[i].value);
                    }

                    return returnStr.ToString();
                }
                catch (Newtonsoft.Json.JsonReaderException e)
                {
                    HRNXUtil.logException(request, 3, location, rm.GetString("INVALID_ICIMS_JOB_PROFILE_JSON"), logContent.ToString(), e, dbAccessor);
                    return default(String);
                }

            }
        }

        public override CreateOrderResponse sendAcknowledgement(Order result, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor)
        {
            CreateOrderResponse response = new CreateOrderResponse();

            if (OrderStatus.processing.Equals(result.status))
            {
                response.status = HttpStatusCode.OK;

                if ("true".Equals(result.additionalData[OrderAdditionalDataKeys.isNewOrder]))
                {
                    // Then, we store in ats_duplicate to indicate it had came in
                    String duplicateKey = client_group_id + "_" + candidateID + "_" + jobID + "_" + currentPhrase;
                    dbAccessor.writeToATSDuplicate(result, location, duplicateKey, result.getVendorOrderXML());
                }
            }
            else
            {
                response.status = HttpStatusCode.BadRequest;
                String errorDetail = "Current status = [" + result.status.ToString() + "]";

                if (!OrderStatus.rejected_w_errors.Equals(result.status))
                {
                    // so if it is not a rejected_w_errors, then we really don't recognize this HRNX status, so we throw.
                    String innerError = result.errorReason;
                    HRNXUtil.logError(result, 3, location, rm.GetString("UNRECOGNIZE_HRNX_STATUS"), "", errorDetail, innerError, dbAccessor);
                }
                StringBuilder acknowledgementLog = new StringBuilder();
                submitTaskStatusToIcimsBCG(result, dbAccessor, webAccessor, acknowledgementLog);
                dbAccessor.writeInfoToHRNXLogTable(result, location, acknowledgementLog.ToString());
            }

            StringBuilder acknowledgement = new StringBuilder();
            acknowledgement.Append("====================");
            acknowledgement.Append(Environment.NewLine);
            acknowledgement.Append("HTTP Status = [");
            acknowledgement.Append(response.status.ToString());
            acknowledgement.Append("]");

            // write to HRNX log
            dbAccessor.writeAcknowledgementToHRNXLogTable(result, location, acknowledgement.ToString());

            return response;
        }

        private void submitTaskStatusToIcimsBCG(Order result, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor, StringBuilder logContent)
        {
			log.Debug("Entering submitTaskStatusToIcimsBCG()");
			icimsBGResult bgResult = new icimsBGResult();
            bgResult.bgccompleteddate = HRNXUtil.getCurrentTimeInUTC();
            bgResult.bgcorderstatus = new icimsCompleteOrderStatus();

            //GENERATE PAYLOAD FOR RESULT
            String payload = JsonConvert.SerializeObject(bgResult, new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm tt" });

			if(postbackLocation != null)
			{
				HRNXWebResponse updateResponse = httpSendToIcims(postbackLocation, "PATCH", "application/json", payload, icimsHeader, timeout, logContent, webAccessor, dbAccessor, result, "submitTaskStatusToIcimsBCG");

				if(updateResponse == null)
				{
					log.Debug("Got a non-ok response from submitTaskStatusToIcimsBCG(), so exiting");
				}
				else
				{
					icimsErrorList potentialError = JsonConvert.DeserializeObject<icimsErrorList>(updateResponse.responseText);
					log.Debug(logContent.ToString());

					if (potentialError != null)
					{
						if (potentialError.errors != null)
						{
							if (potentialError.errors.Count > 0)
								HRNXUtil.logError(result, 3, location, rm.GetString("ICIMS_UPDATE_FAIL"), "", logContent.ToString(), "", dbAccessor);
						}
					}
				}
			}
			else
				log.Debug("Cannot send update to icims due to postbackLocation has not been set yet");

			log.Debug("Exiting submitTaskStatusToIcimsBCG()");
		}
	}

    public enum icimsPhrase
    {
        CREDIT_CHECK = 1,
        BACKGROUND = 2,
        ADVERSE_ACTION = 3
    }
}