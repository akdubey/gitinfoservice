﻿using getClientConfigurations.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Web;
using System.Web.Http;

namespace getClientConfigurations.ATSHandlers
{
    public abstract class ATSHandlerBase: IATSHandler
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger("MultiPhaseCreateOrderMainLogger");
		private ResourceManager rm = new ResourceManager("getClientConfigurations.Properties.HRNX", typeof(HRNXDBAccessor).Assembly);

		protected String location;
		protected String associatedServiceProviderID;

		public Order processRequest(OrderRequest rawRequest, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor)
        {
            Order result = new Order();
			result.additionalData.Add(OrderAdditionalDataKeys.atsID,getID());
            result.atsWorkflowStarts();
            result.status = OrderStatus.rejected_w_errors;

            result = analyzeRequest(result,rawRequest, dbAccessor, webAccessor);
            if (result.status.Equals(OrderStatus.rejected_w_errors))
            {
                log.Info("Analyze has returned an error, so terminating execution");
                return result;
            }

            // Try data that we know MUST exist in each other
            getServiceProviderID(result, webAccessor, dbAccessor);
			if(String.IsNullOrEmpty(associatedServiceProviderID))
			{
				HRNXUtil.logError(result, 3, location, rm.GetString("ORDER_SERVICE_PROVIDER_MISSING"), rm.GetString("CHECK_LOG_FOR_DETAILS"), "", "", dbAccessor);
				return result;
			}

			try
			{
				if (needToCreateNewOrder(result, dbAccessor, webAccessor))
				{
					log.Info("ATS handler indicate we need to create a new transaction");
					result.additionalData.Add(OrderAdditionalDataKeys.isNewOrder, "true");
					return createANewOrder(result, dbAccessor, webAccessor);
				}
				else
				{
					log.Info("ATS handler indicate we need to do an lookup");
					result.additionalData.Add(OrderAdditionalDataKeys.isNewOrder, "false");
					return findExistingOrder(result, dbAccessor, webAccessor);
				}
			}catch(HRNXException e)
			{
				HRNXUtil.logException(result, 3, location, rm.GetString("HRNX_EXCEPTION_THROWN"), rawRequest.ToString(), e,dbAccessor);
				return result;
			}
		}
		public CreateOrderResponse processResult(Order result, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor)
		{
			//re-start the ATS processing
			result.atsWorkflowStarts();

			return sendAcknowledgement(result, dbAccessor, webAccessor);
		}
		public virtual void getServiceProviderID(Order result, HRNXWebAccessor webAccessor, HRNXDBAccessor dbAccessor)
		{
			String[] listOfServiceProviderID = dbAccessor.isPassThruClient(result, location);
			log.Debug(string.Format("ServiceProviders returned has lenght of [{0}]", listOfServiceProviderID.Length));

			if (listOfServiceProviderID.Length > 0)
			{
				result.additionalData.Add(OrderAdditionalDataKeys.serviceProviderID, listOfServiceProviderID[0]);
				associatedServiceProviderID = listOfServiceProviderID[0];
				log.Debug(string.Format("associatedServiceProviderID has been set to [{0}]", associatedServiceProviderID));
			}
		}


		public abstract string getID();
		public abstract Order analyzeRequest(Order request, OrderRequest rawRequest, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor);
        public abstract Boolean needToCreateNewOrder(Order result,HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor);
        public abstract Order createANewOrder(Order result, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor);
        public abstract Order findExistingOrder(Order result, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor);

        public abstract CreateOrderResponse sendAcknowledgement(Order result, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor);
    }
}