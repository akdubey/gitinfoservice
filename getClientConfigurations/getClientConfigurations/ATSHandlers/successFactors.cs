﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using getClientConfigurations.Models;
using Newtonsoft.Json;
using System.Resources;
using System.Text;
using System.Net;
using System.Web.Http;
using Newtonsoft.Json.Converters;
using getClientConfigurations.Exceptions;

namespace getClientConfigurations.ATSHandlers
{
    public class successFactors : ATSHandlerBase
    {
        private ResourceManager rm = new ResourceManager("getClientConfigurations.Properties.HRNX", typeof(HRNXDBAccessor).Assembly);
		private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		/// <summary>
		/// Constants for successFactors
		/// </summary>
		public static readonly String SUCCESSFACTORS_LOCATION_NAME = "ATSHandler_successFactors";
		public static readonly String ATS_ID = "successFactors";
        private static readonly String clientUserID = "groupAuth";
        public static readonly int timeout = 10000; // Requested by Siegy. Because we need to call 2 calls to successFactors, we have to limit our timeout of each call to 10 seconds
        public static readonly int SUCCESSFACTORS_RETRY_MAX = 3;

        public successFactorsRequest parsedRequest;
        public successFactorsJobProfile parsedJobProfile;
        public successFactorsSubjectProfile parsedSubjectProfile;
        public successFactorsUser parsedUser;
        private String initialRequest;
        public String client_group_id;
        public String JobApplicationID;
        public String candidateID;
        private String system_id;
        private String profileURL;
        private String jobURL;
        public String workflowURL;
		public String postbackLocation;
		private String recruiter;
        private String recruiterEmail;
        public successFactorsPhrase currentPhrase;
        private String currentPackage;
        public String original_txn_id;
        private List<Tuple<String, String>> successFactorsHeader;

		public successFactors()
        {
            location = SUCCESSFACTORS_LOCATION_NAME;
        }
        public override string getID()
        {
            return ATS_ID;
        }

        private List<Tuple<String, String>> constructSuccessFactorsHeader(Order request, HRNXDBAccessor dbAccessor)
        {
            String username = dbAccessor.getATSCredentials(request, location, ATS_ID, "username");
            String password = dbAccessor.getATSCredentials(request, location, ATS_ID, "password");

            List<Tuple<String, String>> headers = new List<Tuple<string, string>>();
            headers.Add(HRNXUtil.generateBasicAuthHeader(username, password));
            return headers;
        }


        private void loadRecruiterInfo(Order result, List<Tuple<String, String>> successFactorsHeader, int timeout, HRNXWebAccessor webAccessor, HRNXDBAccessor dbAccessor, StringBuilder logContent)
        {
            if (!String.IsNullOrEmpty(parsedRequest.userId))
            {
                List<Tuple<string, string>> replacements = new List<Tuple<string, string>>();
                replacements.Add(new Tuple<string, string>("^^^CLIENT_GROUP_ID^^^", result.client_group_id));
                replacements.Add(new Tuple<string, string>("^^^USER_ID^^^", parsedRequest.userId));

                getFieldEndpoint endpointData = dbAccessor.getTemplateEndpoint(ATS_ID, ATS_ID + "_ep6", result, location, replacements);
                String recruiterURL = endpointData.url;

                recruiterURL = recruiterURL + "?fields=email";

                HRNXWebResponse rawResponse = httpSendToSuccessFactors(recruiterURL, "GET", "application/json", "", successFactorsHeader, timeout, logContent, webAccessor, dbAccessor, result, "user");

                if (rawResponse != null)
                {
                    // Create successFactorsUser from the data
                    try
                    {
                        parsedUser = JsonConvert.DeserializeObject<successFactorsUser>(rawResponse.responseText);
                        if (parsedUser != null)
                            recruiterEmail = parsedUser.email;
                        else
                            log.Debug("parsedUser appears to be empty");
                    }
                    catch (Newtonsoft.Json.JsonReaderException e)
                    {
                        // This field is optional, so not going to error out here, but we will log it
                        log.Fatal(e);
                    }
                }
                else
                    log.Debug("send to successFactors reported an error");
            }

        }

		public HRNXWebResponse httpSendToSuccessFactors(String targetURL, String httpMethod, String contentType, String payload, List<Tuple<String, String>> successFactorsHeader, int timeout, StringBuilder logContent, HRNXWebAccessor webAccessor, HRNXDBAccessor dbAccessor, Order result, String caller)
        {
            HRNXWebResponse rawResponse;
            int currentTry = 0;

            do
            {
                rawResponse = webAccessor.httpSend(targetURL, httpMethod, contentType, payload, successFactorsHeader, timeout, logContent);
                currentTry++;
            }
            while (HttpStatusCode.RequestTimeout.Equals(rawResponse.status) && currentTry < SUCCESSFACTORS_RETRY_MAX);

			// Validate the response, so we can throw errors out
			if (HttpStatusCode.OK.Equals(rawResponse.status) || HttpStatusCode.Created.Equals(rawResponse.status) || HttpStatusCode.NoContent.Equals(rawResponse.status))
				return rawResponse;
			else
			{
				HRNXUtil.logError(result, 3, location, string.Format(rm.GetString("SUCCESSFACTORS_RETURNED_NON_OK_RESPONSE"), caller), "", logContent.ToString(), "", dbAccessor);
				return null;
			}
		}
		private String getSubjectProfileData(Order result, String profileURL, List<Tuple<String, String>> successFactorsHeader, int timeout, HRNXWebAccessor webAccessor, HRNXDBAccessor dbAccessor, StringBuilder logContent)
        {
            profileURL = profileURL + "?fields=firstname,middlename,lastname,email,gender,birthdate";
            HRNXWebResponse rawResponse = httpSendToSuccessFactors(profileURL, "GET", "application/json", "", successFactorsHeader, timeout, logContent, webAccessor, dbAccessor, result, "subjectProfile");
			if(rawResponse == null)
			{
				// Then something went wrong, so we need to just return
				return default(string);
			}

			// Create successFactorSubjectProfile xml from the data
			try
            {
                parsedSubjectProfile = JsonConvert.DeserializeObject<successFactorsSubjectProfile>(rawResponse.responseText);
                if (parsedSubjectProfile == null)
                {
                    HRNXUtil.logError(result, 3, location, string.Format(rm.GetString("SuccessFactors_RETURNED_EMPTY_RESPONSE"), "subjectProfile"), "", logContent.ToString(), "", dbAccessor);
                    return default(String);
                }
				if (String.IsNullOrEmpty(parsedSubjectProfile.GivenName))
				{
					HRNXUtil.logError(result, 3, location, rm.GetString("MISSING_FIRSTNAME"), "", logContent.ToString(), "", dbAccessor);
					return default(String);
				}
				if (String.IsNullOrEmpty(parsedSubjectProfile.FamilyName))
				{
					HRNXUtil.logError(result, 3, location, rm.GetString("MISSING_LASTNAME"), "", logContent.ToString(), "", dbAccessor);
					return default(String);
				}
				if (String.IsNullOrEmpty(parsedSubjectProfile.Email.Address))
				{
					HRNXUtil.logError(result, 3, location, rm.GetString("MISSING_EMAIL"), "", logContent.ToString(), "", dbAccessor);
					return default(String);
				}

                if (String.IsNullOrEmpty(parsedSubjectProfile.Web.Address))
                {
                    HRNXUtil.logError(result, 3, location, rm.GetString("WEB_ADDRESS"), "", logContent.ToString(), "", dbAccessor);
                    return default(String);
                }
                if (String.IsNullOrEmpty(parsedSubjectProfile.Gender))
                {
                    HRNXUtil.logError(result, 3, location, rm.GetString("MISSING_GENDER"), "", logContent.ToString(), "", dbAccessor);
                    return default(String);
                }
                if (String.IsNullOrEmpty(parsedSubjectProfile.DateOfBirth))
                {
                    HRNXUtil.logError(result, 3, location, rm.GetString("MISSING_DOB"), "", logContent.ToString(), "", dbAccessor);
                    return default(String);
                }
                if (String.IsNullOrEmpty(parsedSubjectProfile.Phone.Number))
                {
                    HRNXUtil.logError(result, 3, location, rm.GetString("MISSING_PHONE_NUMBER"), "", logContent.ToString(), "", dbAccessor);
                    return default(String);
                }
                if (String.IsNullOrEmpty(parsedSubjectProfile.addresses.AddressLine))
                {
                    HRNXUtil.logError(result, 3, location, rm.GetString("MISSING_ADDRESS_ADDRESS_LINE"), "", logContent.ToString(), "", dbAccessor);
                    return default(String);
                }
                if (String.IsNullOrEmpty(parsedSubjectProfile.addresses.CityName))
                {
                    HRNXUtil.logError(result, 3, location, rm.GetString("MISSING_ADDRESS_CITY_NAME"), "", logContent.ToString(), "", dbAccessor);
                    return default(String);
                }
                if (String.IsNullOrEmpty(parsedSubjectProfile.addresses.CountryCode))
                {
                    HRNXUtil.logError(result, 3, location, rm.GetString("MISSING_COUNTRY_CODE"), "", logContent.ToString(), "", dbAccessor);
                    return default(String);
                }
                if (String.IsNullOrEmpty(parsedSubjectProfile.addresses.PostalCode))
                {
                    HRNXUtil.logError(result, 3, location, rm.GetString("POSTAL_CODE_MISSING"), "", logContent.ToString(), "", dbAccessor);
                    return default(String);
                }
                return parsedSubjectProfile.convertToHRNXSubjectProfile();
            }
            catch (Newtonsoft.Json.JsonReaderException e)
            {
                HRNXUtil.logException(result, 3, location, rm.GetString("INVALID_SUCCESSFACTORS_SUBJECT_PROFILE_JSON"), logContent.ToString(), e, dbAccessor);
                return default(String);
            }
        }

        private String getServiceData(Order result, String jobURL, List<Tuple<String, String>> successFactorsHeader, int timeout, HRNXWebAccessor webAccessor, HRNXDBAccessor dbAccessor, StringBuilder logContent)
        {

            currentPackage = getPackage(result, jobURL, successFactorsHeader, timeout, webAccessor, dbAccessor, logContent);

            if (String.IsNullOrEmpty(currentPackage))
                // If we fail to extract package, then we should bubble the error up above
                return default(string);

            // Update additionalData of Order
            result.additionalData.Add(OrderAdditionalDataKeys.package, currentPackage);

            StringBuilder serviceXML = new StringBuilder();
            serviceXML.Append("\t\t<service type=\"passthru\" tp=\"");
            serviceXML.Append(associatedServiceProviderID);
            serviceXML.Append("\">");
            serviceXML.Append(Environment.NewLine);

            serviceXML.Append("\t\t\t<field name=\"package\" type=\"value\">");
            serviceXML.Append("<![CDATA[");

            serviceXML.Append(currentPackage);

            serviceXML.Append("]]>");
            serviceXML.Append("</field>");
            serviceXML.Append(Environment.NewLine);

            serviceXML.Append("\t\t</service>");
            serviceXML.Append(Environment.NewLine);

            extractEducationData(serviceXML);
            extractEmploymentData(serviceXML);
            extractReferenceData(serviceXML);
			extractLicenseData(serviceXML);

			return serviceXML.ToString();
        }
		/// <summary>
		/// Extract License data from successFactors over to HRNX
		/// </summary>
		/// <param name="serviceXML"></param>
		private void extractLicenseData(StringBuilder serviceXML)
		{
			parsedSubjectProfile.licenseProcessing(serviceXML);
		}

		/// <summary>
		/// Extract education data from successFactors over to HRNX
		/// </summary>
		/// <param name="serviceXML"></param>
		private void extractEducationData(StringBuilder serviceXML)
        {
            parsedSubjectProfile.EducationProcessing(serviceXML);
        }

        /// <summary>
        /// Extract employment data from successFactors over to HRNX
        /// </summary>
        /// <param name="serviceXML"></param>
        private void extractEmploymentData(StringBuilder serviceXML)
        {
            parsedSubjectProfile.EmploymentProcessing(serviceXML);
        }
        /// <summary>
        /// Extract reference data from successFactors over to HRNX
        /// </summary>
        /// <param name="serviceXML"></param>
        private void extractReferenceData(StringBuilder serviceXML)
        {
			parsedSubjectProfile.referenceProcessing(serviceXML);
		}

        private String extractLocationFromSuccessFactors(Order result, String workflowURL, String payloadJson, List<Tuple<String, String>> successFactorsHeader, int timeout, HRNXWebAccessor webAccessor, HRNXDBAccessor dbAccessor, StringBuilder logContent)
        {
            HRNXWebResponse response = httpSendToSuccessFactors(workflowURL, "POST", "application/json", payloadJson, successFactorsHeader, timeout, logContent, webAccessor, dbAccessor, result, "extractLocationFromSuccessFactors");

			if(response == null)
			{
				log.Debug("Cannot extract location from successFactors because we got a non-ok response from successFactors");
				return default(String);
			}
			try
            {
                return response.headers["Location"];
            }
            catch (System.Collections.Generic.KeyNotFoundException e)
            {
                HRNXUtil.logException(result, 3, location, rm.GetString("UNABLE_TO_GET_POSTBACK_LOCATION"), logContent.ToString(), e, dbAccessor);
                return default(String);
            }
        }

        public override Order analyzeRequest(Order result, OrderRequest rawRequest, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor)
        {
            // Convert to successFactorsRequest
            initialRequest = rawRequest.rawPayload;

            try
            {
                parsedRequest = JsonConvert.DeserializeObject<successFactorsRequest>(initialRequest);
            }
            catch (Newtonsoft.Json.JsonReaderException e)
            {
                HRNXUtil.logException(result, 3, location, rm.GetString("INVALID_REQUEST_JSON"), initialRequest, e, dbAccessor);
                return result;
            }
            if (parsedRequest == null)
            {
                HRNXUtil.logError(result, 3, location, rm.GetString("INVALID_REQUEST_EMPTY"), "", initialRequest, "", dbAccessor);
                return result;
            }

            client_group_id = parsedRequest.customerId;
            if (String.IsNullOrEmpty(client_group_id))
            {
                HRNXUtil.logError(result, 3, location, rm.GetString("CLIENT_GROUP_ID_EMPTY"), "", initialRequest, "", dbAccessor);
                return result;
            }

            result.client_group_id = client_group_id;
            result.client_user_id = clientUserID;

            if (!dbAccessor.isClientGroupIDValid(result, location))
            {
                HRNXUtil.logError(result, 3, location, rm.GetString("CLIENT_GROUP_ID_INVALID"), "", initialRequest, "", dbAccessor);
                return result;
            }


            system_id = parsedRequest.systemId;
            if (String.IsNullOrEmpty(system_id))
            {
                HRNXUtil.logError(result, 3, location, rm.GetString("REQUIRED_DATA_MISSING"), rm.GetString("SYSTEM_ID"), initialRequest, "", dbAccessor);
                return result;
            }
            JobApplicationID = parsedRequest.JobApplicationID;

            candidateID = parsedRequest.personId;
            if (String.IsNullOrEmpty(candidateID))
            {
                HRNXUtil.logError(result, 3, location, rm.GetString("REQUIRED_DATA_MISSING"), rm.GetString("CANDIDATE_ID"), initialRequest, "", dbAccessor);
                return result;
            }
            result.candidate_id = candidateID;

            profileURL = parsedRequest.getProfileURL();
            if (String.IsNullOrEmpty(profileURL))
            {
                HRNXUtil.logError(result, 3, location, rm.GetString("REQUIRED_DATA_MISSING"), rm.GetString("PERSON_URL"), initialRequest, "", dbAccessor);
                return result;
            }

            workflowURL = parsedRequest.getWorkflowURL();
            if (String.IsNullOrEmpty(workflowURL))
            {
                HRNXUtil.logError(result, 3, location, rm.GetString("REQUIRED_DATA_MISSING"), rm.GetString("APPLICANT_WORKFLOW_URL"), initialRequest, "", dbAccessor);
                return result;
            }

			successFactorsHeader = constructSuccessFactorsHeader(result, dbAccessor);

			// Make it use TLS 1.2 because successFactors needs it
			ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

			result.status = OrderStatus.passthru;
            return result;
        }

        public override bool needToCreateNewOrder(Order request, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor)
        {
            String phrase = parsedRequest.newStatus;

            // Pull DB to figure out what type this status is
            HRNXPhase phraseData = dbAccessor.getPhraseData(request, location, phrase, associatedServiceProviderID);
			if (phraseData == null)
			{
				HRNXUtil.logError(request, 3, location, rm.GetString("UNKNOWN_STAGE"), phrase, initialRequest, "", dbAccessor);
				throw new CannotDetermineOrderStatusException();
			}

            successFactorsPhrase currentPhrase = (successFactorsPhrase)Enum.Parse(typeof(successFactorsPhrase), phraseData.enumValue);

            if (successFactorsPhrase.BACKGROUND.Equals(currentPhrase))
            {
                // If background, then we need to check duplicate table to see if we have any credit already ordered
                String creditCheckDuplicateKey = client_group_id + "_" + candidateID + "_" + JobApplicationID + "_" + successFactorsPhrase.CREDIT_CHECK;
                HRNXATSDuplicateEntry entry = dbAccessor.getATSDuplicateEntryData(request, creditCheckDuplicateKey, location);

                if (!(entry == null))
                {
                    // save off the original txn_id, so we don't need to repull
                    original_txn_id = entry.original_txn_id;
                    return false;
                }
            }

            // so if it gets to here because it didn't hit the other paths, then we will create new txn_id
            return true;
        }

        public override Order createANewOrder(Order result, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor)
        {
            StringBuilder logContent = new StringBuilder();

            // Extract data from successFactors request and build out qProcessing payload
            String subjecrProfileXML = getSubjectProfileData(result, profileURL, successFactorsHeader, timeout, webAccessor, dbAccessor, logContent);
            if (String.IsNullOrEmpty(subjecrProfileXML))
            {
                // then an error has occurred, and we had tracked the error previous, so just return
                return result;
            }

            // Load recruiter info from successFactors
            loadRecruiterInfo(result, successFactorsHeader, timeout, webAccessor, dbAccessor, logContent);

            // GENERATING SERVICE NODES
            String serviceXML = getServiceData(result, jobURL, successFactorsHeader, timeout, webAccessor, dbAccessor, logContent);
            if (String.IsNullOrEmpty(serviceXML))
            {
                // then an error has occurred, and we had tracked the error previous, so just return
                return result;
            }

            String createOrderXML = dbAccessor.getLoadWidgetXSLT(result, location, ATS_ID);

            String txn_id = dbAccessor.generateTxnID(result, location);

            //GENERATE PAYLOAD TO GET FINAL WORKFLOW URL(A URL ON WHICH WE WILL UPDATE THE RESULT USING PATCH)
            List<Tuple<String, String>> addressReplacement = new List<Tuple<string, string>>();
            addressReplacement.Add(new Tuple<string, string>("^^PERSON_URL^^", profileURL));
            getFieldEndpoint endpoint = dbAccessor.getTemplateEndpoint(ATS_ID, ATS_ID + "_ep3", result, location, addressReplacement);
            String fullWorkflowUrl = endpoint.url;
            String payloadJson = endpoint.xslt;

            postbackLocation = extractLocationFromSuccessFactors(result, fullWorkflowUrl, payloadJson, successFactorsHeader, timeout, webAccessor, dbAccessor, logContent);
            if (String.IsNullOrEmpty(postbackLocation))
            {
                // then an error has occurred, and we had tracked the error previous, so just return
                return result;
            }

			
			createOrderXML = createOrderXML.Replace("^^txn_id^^", txn_id);
            createOrderXML = createOrderXML.Replace("^^txn_requestdate^^", HRNXUtil.getCurrentTimeInUTCInStringForm());
            createOrderXML = createOrderXML.Replace("<client_group_id/>", "<client_group_id>" + client_group_id + "</client_group_id>");
            createOrderXML = createOrderXML.Replace("^^tp_id^^", ATS_ID);
            createOrderXML = createOrderXML.Replace("<tp_uqid/>", "<tp_uqid>" + candidateID + "</tp_uqid>");
            createOrderXML = createOrderXML.Replace("<workflowDetailsURL/>", "<workflowDetailsURL>" + postbackLocation + "</workflowDetailsURL>");
            createOrderXML = createOrderXML.Replace("<quoteback id=\"2\"/>", "<quoteback id=\"2\">" + system_id + "</quoteback>");
            createOrderXML = createOrderXML.Replace("<requisitionId/>", "<requisitionId>" + JobApplicationID + "</requisitionId>");
            createOrderXML = createOrderXML.Replace("^^requestor_email^^", recruiterEmail);
            createOrderXML = createOrderXML.Replace("^^requestor_name^^", recruiter);
            createOrderXML = createOrderXML.Replace("<subject/>", subjecrProfileXML);
            createOrderXML = createOrderXML.Replace("<service/>", serviceXML);

            result.txn_id = txn_id;
            result.status = OrderStatus.passthru;
            result.createOrderHandlerName = location;
            result.orderingIP = webAccessor.extractRequesterIPFromHttpRequest(HttpContext.Current);

            result.payload_hrnx = createOrderXML;

            // create the order in HRNX
            dbAccessor.writeToQProcessing(result, location);
            dbAccessor.writeInitialRequestToHRNXLogTable(result, location, initialRequest);
            dbAccessor.writeInfoToHRNXLogTable(result, location, logContent.ToString());

			return result;
        }

        public override Order findExistingOrder(Order result, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor)
        {
			Order returnVal = dbAccessor.getOrderFromDB(original_txn_id, result, location);

            // pull current package and store it in the order's metadata
            StringBuilder getPackageLogContent = new StringBuilder();
            currentPackage = getPackage(result, jobURL, successFactorsHeader, timeout, webAccessor, dbAccessor, getPackageLogContent);
            result.additionalData.Add(OrderAdditionalDataKeys.package, currentPackage);

            // need to copy over data that exist in qProcessing, everything else should be re-get
            result.txn_id = returnVal.txn_id;
            result.payload_hrnx = returnVal.payload_hrnx;

			// set postbackURL
			postbackLocation = HRNXUtil.extractDataFromOrder(returnVal.payload_hrnx, "/order/workflowDetailsURL");

            // Log an entry to indicate that a duplicate request has come in
            dbAccessor.writeInitialRequestToHRNXLogTable(result, location, initialRequest);
            if (!String.IsNullOrEmpty(getPackageLogContent.ToString()))
                dbAccessor.writeInfoToHRNXLogTable(result, location, getPackageLogContent.ToString());

            return result;
        }

        public String getPackage(Order request, String jobURL, List<Tuple<String, String>> successFactorsHeader, int timeout, HRNXWebAccessor webAccessor, HRNXDBAccessor dbAccessor, StringBuilder logContent)
        {
            String phrase = parsedRequest.newStatus;

            HRNXPhase phraseData = dbAccessor.getPhraseData(request, location, phrase, associatedServiceProviderID);

            currentPhrase = (successFactorsPhrase)Enum.Parse(typeof(successFactorsPhrase), phraseData.enumValue);

            if (successFactorsPhrase.CREDIT_CHECK.Equals(currentPhrase) ||
                successFactorsPhrase.ADVERSE_ACTION.Equals(currentPhrase))
            {
                return phraseData.data;
            }
            else
            {
                // Try to pull the package from successFactors
                // now try to get package from successFactors
                jobURL = jobURL + "?fields=joblocation,bgcpackagetype";
                HRNXWebResponse rawResponse = httpSendToSuccessFactors(jobURL, "GET", "application/json", "", successFactorsHeader, timeout, logContent, webAccessor, dbAccessor, request, "getPackage");
				log.Debug(logContent.ToString());

				if(rawResponse == null)
				{
					log.Debug("GetPackage returned non-ok response, so exiting");
					return default(String);
				}
                // Create SuccessFactorsSubjectProfile xml from the data
                try
                {
                    parsedJobProfile = JsonConvert.DeserializeObject<successFactorsJobProfile>(rawResponse.responseText);

                    if (parsedJobProfile == null)
                    {
                        HRNXUtil.logError(request, 3, location, string.Format(rm.GetString("successFactors_RETURNED_EMPTY_RESPONSE"), "jobProfile"), "", logContent.ToString(), "", dbAccessor);
                        return default(String);
                    }

					if(parsedJobProfile.bgcpackagetype == null)
					{
						// Then we got no package to order, which we cannot accept at this time
						log.Debug("No services/pacakge has been obtained from successFactors. Erroring out");
						HRNXUtil.logError(request, 3, location,rm.GetString("NO_SERVICE_TO_ORDER"), "", logContent.ToString(), "", dbAccessor);
						return default(String);
					}

					StringBuilder returnStr = new StringBuilder();
                    for (int i = 0; i < parsedJobProfile.bgcpackagetype.Count; i++)
                    {
                        if (i > 0)
                            returnStr.Append(",");
                        returnStr.Append(parsedJobProfile.bgcpackagetype[i].value);
                    }

                    return returnStr.ToString();
                }
                catch (Newtonsoft.Json.JsonReaderException e)
                {
                    HRNXUtil.logException(request, 3, location, rm.GetString("INVALID_successFactors_JOB_PROFILE_JSON"), logContent.ToString(), e, dbAccessor);
                    return default(String);
                }

            }
        }

        public override CreateOrderResponse sendAcknowledgement(Order result, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor)
        {
            CreateOrderResponse response = new CreateOrderResponse();

            if (OrderStatus.processing.Equals(result.status))
            {
                response.status = HttpStatusCode.OK;

                if ("true".Equals(result.additionalData[OrderAdditionalDataKeys.isNewOrder]))
                {
                    // Then, we store in ats_duplicate to indicate it had came in
                    String duplicateKey = client_group_id + "_" + candidateID + "_" + JobApplicationID + "_" + currentPhrase;
                    dbAccessor.writeToATSDuplicate(result, location, duplicateKey, result.getVendorOrderXML());
                }
            }
            else
            {
                response.status = HttpStatusCode.BadRequest;
                String errorDetail = "Current status = [" + result.status.ToString() + "]";

                if (!OrderStatus.rejected_w_errors.Equals(result.status))
                {
                    // so if it is not a rejected_w_errors, then we really don't recognize this HRNX status, so we throw.
                    String innerError = result.errorReason;
                    HRNXUtil.logError(result, 3, location, rm.GetString("UNRECOGNIZE_HRNX_STATUS"), "", errorDetail, innerError, dbAccessor);
                }
                StringBuilder acknowledgementLog = new StringBuilder();
                submitTaskStatusToSuccessFactorsBCG(result, dbAccessor, webAccessor, acknowledgementLog);
                dbAccessor.writeInfoToHRNXLogTable(result, location, acknowledgementLog.ToString());
            }

            StringBuilder acknowledgement = new StringBuilder();
            acknowledgement.Append("====================");
            acknowledgement.Append(Environment.NewLine);
            acknowledgement.Append("HTTP Status = [");
            acknowledgement.Append(response.status.ToString());
            acknowledgement.Append("]");

            // write to HRNX log
            dbAccessor.writeAcknowledgementToHRNXLogTable(result, location, acknowledgement.ToString());

            return response;
        }

        private void submitTaskStatusToSuccessFactorsBCG(Order result, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor, StringBuilder logContent)
        {
			log.Debug("Entering submitTaskStatusToSuccessFactorsBCG()");
            successFactorsBGResult bgResult = new successFactorsBGResult();
            bgResult.bgccompleteddate = HRNXUtil.getCurrentTimeInUTC();
            bgResult.bgcorderstatus = new successFactorsCompleteOrderStatus();

            //GENERATE PAYLOAD FOR RESULT
            String payload = JsonConvert.SerializeObject(bgResult, new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm tt" });

			if(postbackLocation != null)
			{
				HRNXWebResponse updateResponse = httpSendToSuccessFactors(postbackLocation, "PATCH", "application/json", payload, successFactorsHeader, timeout, logContent, webAccessor, dbAccessor, result, "submitTaskStatusToSuccessFactorsBCG");

				if(updateResponse == null)
				{
					log.Debug("Got a non-ok response from submitTaskStatusToSuccessFactorsBCG(), so exiting");
				}
				else
				{
                    successFactorsErrorList potentialError = JsonConvert.DeserializeObject<successFactorsErrorList>(updateResponse.responseText);
					log.Debug(logContent.ToString());

					if (potentialError != null)
					{
						if (potentialError.errors != null)
						{
							if (potentialError.errors.Count > 0)
								HRNXUtil.logError(result, 3, location, rm.GetString("SUCCESS_FACTORS_UPDATE_FAIL"), "", logContent.ToString(), "", dbAccessor);
						}
					}
				}
			}
			else
				log.Debug("Cannot send update to successFactors due to postbackLocation has not been set yet");

			log.Debug("Exiting submitTaskStatusToSuccessFactorsBCG()");
		}
	}

    public enum successFactorsPhrase
    {
        CREDIT_CHECK = 1,
        BACKGROUND = 2,
        ADVERSE_ACTION = 3
    }
}