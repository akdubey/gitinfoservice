﻿using getClientConfigurations.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Web;
using System.Web.Http;
using getClientConfigurations.ATSHandlers;
using System.Resources;
using getClientConfigurations.Exceptions;
using System.Configuration;
using getClientConfigurations.ServiceProviderHandlers;

namespace getClientConfigurations.Controllers
{
    public class SuccessFactorsCreateOrderController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger("SuccessFactorsCreateOrderMainLogger");
        private static readonly string DB_CONNECTION_NAME_DEFAULT = "HRNXDBConfiguration";
		private static readonly string FAILED_SQL_STORAGE_FOLDER_PATH = "hrnxSQLConnExecuteErrorLog";

		private Dictionary<string, IATSHandler> handlersMapping;
        private Dictionary<string, IServiceProviderHandler> sphandlersMapping;
        private ResourceManager rm = new ResourceManager("getClientConfigurations.Properties.HRNX", typeof(SuccessFactorsCreateOrderController).Assembly);
        private HRNXDBAccessor dbAccessor;
        private HRNXWebAccessor webAccessor;
        private String location = "SuccessFactorsCreateOrderController";

        public SuccessFactorsCreateOrderController()
        {
            String connectionStr = ConfigurationManager.AppSettings[DB_CONNECTION_NAME_DEFAULT];
            if (String.IsNullOrEmpty(connectionStr))
            {
                String detailMsg = string.Format("Cannot find Connection String with name [{0}]", DB_CONNECTION_NAME_DEFAULT);
                log.Error(detailMsg);
                throw new CannotFindDBConfigurationException(detailMsg);
            }

			// Load the path to store the log file from Configuration
			String failedSQLFolderPath = ConfigurationManager.AppSettings[FAILED_SQL_STORAGE_FOLDER_PATH];
			if (String.IsNullOrEmpty(failedSQLFolderPath))
			{
				String detailMsg = string.Format("Cannot find appSetting with name [{0}]", FAILED_SQL_STORAGE_FOLDER_PATH);
				log.Error(detailMsg);
				throw new CannotFindFailedSQLStorageConfigurationException(detailMsg);
			}
			else
			{
				log.Debug(string.Format("failedSQLFolderPath is [{0}]", failedSQLFolderPath));
			}

			dbAccessor = new HRNXDBAccessor(connectionStr, failedSQLFolderPath);
            webAccessor = new HRNXWebAccessor();
            setupHandlersMapping();
        }
        public SuccessFactorsCreateOrderController(HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor)
        {
            this.dbAccessor = dbAccessor;
            this.webAccessor = webAccessor;
            setupHandlersMapping();
        }
        public SuccessFactorsCreateOrderController(HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor, Dictionary<string, IATSHandler> handlersMapping)
        {
            this.dbAccessor = dbAccessor;
            this.webAccessor = webAccessor;
            this.handlersMapping = handlersMapping;
            spHanderSetup();
        }
        public SuccessFactorsCreateOrderController(HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor, Dictionary<string, IATSHandler> handlersMapping, Dictionary<string, IServiceProviderHandler> sphandlersMapping)
        {
            this.dbAccessor = dbAccessor;
            this.webAccessor = webAccessor;
            this.handlersMapping = handlersMapping;
            this.sphandlersMapping = sphandlersMapping;
        }
        private void setupHandlersMapping()
        {
            atsHandlerSetup();
            spHanderSetup();
        }
        private void atsHandlerSetup()
        {
            handlersMapping = new Dictionary<string, IATSHandler>();
            handlersMapping.Add("successFactors", new successFactors());
        }
        private void spHanderSetup()
        {
            sphandlersMapping = new Dictionary<string, IServiceProviderHandler>();
            //sphandlersMapping.Add("clearstar2~301", new clearstar());
        }
       
        [Route("API/CreateOrder/{atsID}/{serviceProviderID}")]
        [HttpPost]
        public IHttpActionResult acceptOrder(string atsID, string serviceProviderID)
        {
            log.Info(string.Format("atsID = [{0}]", atsID));
            log.Info(string.Format("serviceProviderID = [{0}]", serviceProviderID));

            OrderRequest request = new OrderRequest();
            // Extract out the Request URL client used to call here
            String requestURL = webAccessor.extractRequestURLFromHttpRequest(HttpContext.Current);
            log.Info(string.Format("requestURL = [{0}]", requestURL));
            request.requestURL = requestURL;

			// Extract out the Request body
			String payload = webAccessor.extractRequestPayloadFromHttpRequest(HttpContext.Current);
            log.Info(string.Format("payload = [{0}]", payload));
            request.rawPayload = payload;

            IATSHandler atsHandler;
            IServiceProviderHandler spHandler;
            try
            {
                // Check #1: Verify atsID is a valid vendor that we have a handler for
                atsHandler = handlersMapping[atsID];
            }
			catch (KeyNotFoundException)
			{
				String errorShort = string.Format(rm.GetString("INVALID_ATS_ID"), atsID);

				// write to errorStack
				dbAccessor.pushToErrorStack(1, location, false, null, errorShort, "", requestURL, payload);

				// return error to caller
				return BadRequest(errorShort);
			}
			catch (System.ArgumentNullException)
            {
                String errorShort = rm.GetString("MISSING_ATS_ID");

                // write to errorStack
                dbAccessor.pushToErrorStack(1, location, false, null, errorShort, "", requestURL, payload);

                // return error to caller
                return BadRequest(errorShort);
            }
            try
            {
                // Check #1: Verify serviceProviderID is a valid vendor that we have a handler for
                spHandler = sphandlersMapping[serviceProviderID];
            }
            catch (KeyNotFoundException)
            {
                String errorShort = string.Format(rm.GetString("INVALID_SERVICE_PROVIDER_ID"), serviceProviderID);

                // write to errorStack
                dbAccessor.pushToErrorStack(1, location, false, null, errorShort, "", requestURL, payload);

                // return error to caller
                return BadRequest(errorShort);
            }
			catch (System.ArgumentNullException)
			{
				String errorShort = rm.GetString("MISSING_SERVICE_PROVIDER_ID");

				// write to errorStack
				dbAccessor.pushToErrorStack(1, location, false, null, errorShort, "", requestURL, payload);

				// return error to caller
				return BadRequest(errorShort);
			}

			Order atsCreateOrderResult = atsHandler.processRequest(request, dbAccessor,webAccessor);

            // Check if we had gotten any errors from processsing in the ATS. 
            // If we don't, then we will let Service Provider process the order first
            // Afterward, we just ask acknowledgement to provide a response
            if (!OrderStatus.rejected_w_errors.Equals(atsCreateOrderResult.status))
                atsCreateOrderResult = spHandler.processRequest(atsCreateOrderResult, dbAccessor, webAccessor);

            CreateOrderResponse processedResponse = atsHandler.processResult(atsCreateOrderResult, dbAccessor, webAccessor);

			if (processedResponse != null)
			{
				if (HttpStatusCode.OK.Equals(processedResponse.status))
					return Ok(processedResponse.responseMsg);
				else if (HttpStatusCode.BadRequest.Equals(processedResponse.status))
					return BadRequest(processedResponse.responseMsg);
				else
					// If this is the case, we don't know what to throw, so we will just recognize it here
					return InternalServerError();
			}
			else
			{
				dbAccessor.pushToErrorStack(4, location, false, atsCreateOrderResult, UnknownAcknowledgementException.errorShort, UnknownAcknowledgementException.errorDetail, requestURL, payload);
				return InternalServerError(new UnknownAcknowledgementException());
			}
        }
    }
}
