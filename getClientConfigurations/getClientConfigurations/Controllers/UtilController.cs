﻿using getClientConfigurations.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Web.Http;

namespace getClientConfigurations.Controllers
{
    public class UtilController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /* Sample way to call this
         *
         * http://localhost:233/API/Util/ICIMS?targetURL=https%3A%2F%2Fapi.icims.com%2Fcustomers%2FClientID%2Fforms%2FDocument.pdf&username=SomeUserURLEncoded&password=SomePasswordURLEncoded
         */
        [Route("API/Util/ICIMS")]
        [HttpGet]
        public IHttpActionResult convertOnlineDocumentToBase64EncodedString(string targetURL, string username, string password)

        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(targetURL);

            //set settings
            request.Method = "GET";
            request.ContentType = "application/json";
            String encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password));
            request.Headers.Add("Authorization", "Basic "+ encoded);

            // Make it use TLS 1.2 because icims needs it
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            // send it out
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            log.Info("response status code is " + response.StatusCode);

            using (Stream stream = response.GetResponseStream())
            {
                /*
                *using solution in https://stackoverflow.com/questions/221925/creating-a-byte-array-from-a-stream to create the byte[] from stream
                */
                MemoryStream ms = new MemoryStream();
                stream.CopyTo(ms);
                byte[] data = ms.ToArray();

                //Then, convert the byte[] into base64 string using native C# converter
                String base64response = System.Convert.ToBase64String(data);

                return Ok(base64response);
            }
        }
    }
}
