﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using getClientConfigurations.Models;
using System.Configuration;
using System.Text;
using System.Web;

namespace getClientConfigurations.Controllers
{
    public class ServiceProviderController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private HRNXWebAccessor webAccessor = new HRNXWebAccessor();

        /// <summary>
        /// Pull DB for the list of service providers for this client
        /// </summary>
        /// <param name="client_group_id">Target client_group_id whose service provider need to be fetch</param>
        /// <param name="serviceProviders">List of service provider getting return/updated by this function</param>
        /// <param name="connection">SQL connection to the DB</param>
        private void getServiceProviders(string client_group_id, ref List<serviceProvider> serviceProviders, OdbcConnection connection)
        {
            log.Info(string.Format("Calling getServiceProviders() with [{0}]", client_group_id));

            String serviceProvidersSQLQueryTemplate = "SELECT partner_id FROM hrnx.partner_service_mappings where client_group_id = ?";
            OdbcCommand command = new OdbcCommand(serviceProvidersSQLQueryTemplate, connection);
            command.Parameters.AddWithValue("@client_group_id", client_group_id);

            OdbcDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                serviceProvider target = new serviceProvider();
                target.vendorid = reader.GetString(0);

                log.Info(string.Format("getServiceProviders() found an entry. Creating a new Service Provider with vendorID to be [{0}]", target.vendorid));

                serviceProviders.Add(target);
            }
            log.Debug(string.Format("Final count of getServiceProviders() is [{0}]", serviceProviders.Count));
            reader.Close();
        }

        /// <summary>
        /// Get more detail about the service provider
        /// </summary>
        /// <param name="partner_id">the service provider to search for</param>
        /// <param name="vendorName">One of the return value, the name of the service provider</param>
        /// <param name="vendorType">One of the return value, the type of the service provider</param>
        /// <param name="connection">SQL connection to the DB</param>
        private void getServiceProviderDetail(string partner_id, ref string vendorName, ref string vendorType, OdbcConnection connection)
        {
            log.Info(string.Format("Calling getServiceProviderDetail() with partner_id=[{0}]", partner_id));

            String serviceProvidersSQLQueryTemplate = "SELECT partner_name , partner_type FROM hrnx.trading_partners where partner_id = ?";
            OdbcCommand command = new OdbcCommand(serviceProvidersSQLQueryTemplate, connection);
            command.Parameters.AddWithValue("@partner_id", partner_id);

            OdbcDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                vendorName = reader.GetString(0);
                vendorType = reader.GetString(1);

                log.Debug(string.Format("getServiceProviderDetail() found an entry updating vendorName=[{0}]; vendorType=[{1}]", vendorName, vendorType));
            }
            reader.Close();
        }

        /// <summary>
        /// Return the list of packages associated with this partner_id and client_group_id
        /// </summary>
        /// <param name="partner_id">service provider ID to search for</param>
        /// <param name="client_group_id">client_group_id to search for</param>
        /// <param name="connection">SQL connection to the DB</param>
        /// <returns></returns>
        private List<Package> getListOfPackages(string partner_id, string client_group_id, ref int adhoc_setting, OdbcConnection connection)
        {
            List<Package> packages = new List<Package>();
            log.Info(string.Format("Calling getListOfPackages() with partner_id=[{0}], client_group_id = [{1}]", partner_id, client_group_id));

            String serviceProvidersSQLQueryTemplate = "SELECT vendor_pkg_id , pkg_name, adhoc_setting FROM hrnx.packages where service_tp_id = ? and client_group_id = ? group by vendor_pkg_id";
            OdbcCommand command = new OdbcCommand(serviceProvidersSQLQueryTemplate, connection);
            command.Parameters.AddWithValue("@service_tp_id", partner_id);
            command.Parameters.AddWithValue("@client_group_id", client_group_id);

            OdbcDataReader reader = command.ExecuteReader();
            Package current;
            while (reader.Read())
            {
                current = new Package();
                current.packageid = reader.GetString(0);
                current.packagename = reader.GetString(1);

                // check for adhoc_setting
                if (!reader.IsDBNull(2))
                {
                    adhoc_setting = reader.GetInt32(2);
                }
                else
                    adhoc_setting = -1;

                log.Debug(string.Format("getListOfPackages() found an entry. Creating a new Package with vendorPkgID = [{0}]; packageName = [{1}]", current.packageid, current.packagename));

                packages.Add(current);
            }
            reader.Close();
            log.Debug(string.Format("getListOfPackages() final size = [{0}]", packages.Count));

            return packages;
        }

        /// <summary>
        /// Get the list of serviceID inside target packageID
        /// </summary>
        /// <param name="partner_id"></param>
        /// <param name="client_group_id"></param>
        /// <param name="vendor_pkg_id">The vendor package ID of the target package</param>
        /// <param name="connection">SQL connection to the DB</param>
        /// <returns></returns>
        private List<Service> getListOfServicesInPackages(string partner_id, string client_group_id, string vendor_pkg_id, OdbcConnection connection)
        {
            log.Info(string.Format("Calling getListOfServicesInPackages() with partner_id=[{0}]; client_group_id = [{1}]; vendor_pkg_id = [{2}]", partner_id, client_group_id, vendor_pkg_id));
            List<Service> services = new List<Service>();

            String serviceProvidersSQLQueryTemplate = "SELECT service_id FROM hrnx.packages where service_tp_id = ? and client_group_id = ? and vendor_pkg_id = ?";
            OdbcCommand command = new OdbcCommand(serviceProvidersSQLQueryTemplate, connection);
            command.Parameters.AddWithValue("@service_tp_id", partner_id);
            command.Parameters.AddWithValue("@client_group_id", client_group_id);
            command.Parameters.AddWithValue("@vendor_pkg_id", vendor_pkg_id);

            OdbcDataReader reader = command.ExecuteReader();
            Service current;
            while (reader.Read())
            {
                current = new Service();
                current.serviceid = reader.GetString(0);

                log.Debug(string.Format("getListOfServicesInPackages() found an entry. Creating a new Service with serviceID = [{0}]", current.serviceid));

                services.Add(current);
            }
            reader.Close();
            log.Debug(string.Format("getListOfServicesInPackages() final size = [{0}]", services.Count));

            return services;
        }

        /// <summary>
        /// Update target service with mode details from DB
        /// </summary>
        /// <param name="target">Target service object</param>
        /// <param name="partner_id">partner_id this service belongs to</param>
        /// <param name="client_group_id">client_group_id this service belongs to</param>
        /// <param name="service_id">ID of the service</param>
        /// <param name="connection">SQL connection to the DB</param>
        private void getServiceDetail(ref Service target, string partner_id, string client_group_id, string service_id, OdbcConnection connection)
        {
            log.Info(string.Format("Calling getServiceDetail() with partner_id=[{0}]; client_group_id = [{1}]; service_id = [{2}]", partner_id, client_group_id, service_id));

            String serviceProvidersSQLQueryTemplate = "SELECT service_name FROM hrnx.services where service_tp_id = ? and service_id = ? and (client_group_id = ? or client_group_id is null) order by client_group_id is not null limit 1";
            OdbcCommand command = new OdbcCommand(serviceProvidersSQLQueryTemplate, connection);
            command.Parameters.AddWithValue("@service_tp_id", partner_id);
            command.Parameters.AddWithValue("@service_id", service_id);
            command.Parameters.AddWithValue("@client_group_id", client_group_id);

            OdbcDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                // Update target service
                target.servicename = reader.GetString(0);

                log.Debug(string.Format("getServiceDetail() found an entry. Updating current service Name = [{0}]", target.servicename));
            }
            reader.Close();
        }

        /// <summary>
        /// Get individual service associated with this partner_id and client_group_id
        /// </summary>
        /// <param name="partner_id">Target partner_id</param>
        /// <param name="client_group_id">Target client_group_id</param>
        /// <param name="connection">SQL connection to the DB</param>
        /// <returns></returns>
        private List<Service> getListOfServicesForClient(string partner_id, string client_group_id, OdbcConnection connection)
        {
            log.Info(string.Format("Calling getListOfServicesForClient() with partner_id=[{0}]; client_group_id = [{1}]", partner_id, client_group_id));
            List<Service> services = new List<Service>();

            String serviceProvidersSQLQueryTemplate = "SELECT distinct(service_id) FROM hrnx.services where service_tp_id = ? and (client_group_id = ? or client_group_id is null)";
            OdbcCommand command = new OdbcCommand(serviceProvidersSQLQueryTemplate, connection);
            command.Parameters.AddWithValue("@service_tp_id", partner_id);
            command.Parameters.AddWithValue("@client_group_id", client_group_id);

            OdbcDataReader reader = command.ExecuteReader();
            Service current;
            while (reader.Read())
            {
                current = new Service();
                current.serviceid = reader.GetString(0);

                if (!current.serviceid.Equals("_core"))
                {
                    log.Debug(string.Format("getListOfServicesInPackages() found an entry. Creating a new Service with serviceID = [{0}]", current.serviceid));
                    services.Add(current);
                }
                else
                    log.Debug(string.Format("getListOfServicesInPackages() found an entry. However it is the _core service of HRNX. So entry not added"));
            }
            reader.Close();
            log.Debug(string.Format("getListOfServicesInPackages() final size = [{0}]", services.Count));

            return services;
        }

        private IHttpActionResult returnErrorToCaller(HRNXError error,HttpStatusCode statusCode)
        {
            return new System.Web.Http.Results.ResponseMessageResult(Request.CreateResponse(statusCode, error));
        }

        private void UpdatePackagesAndServices(String partner_id, String client_group_id, OdbcConnection connection)
        {
            // 1. pull getService endpoint from DB given the client_group_id and service provider
            String getServiceEndpointID = "";
            String getServiceURL = "";
            String getServiceHandlerURL = "";
            String getServiceEndpointOptions = "";

            log.Info(string.Format("Calling UpdatePackagesAndServices() with partner_id=[{0}]; client_group_id = [{1}]", partner_id, client_group_id));            
            String serviceProvidersSQLQueryTemplate = "SELECT id, address, address_preprocessing, endpoint_options FROM settings_endpoints where endpoint_category = 'getServices' and partner_id=? and (client_group_id = ? or client_group_id is null) and enabled = '1' order by client_group_id is not null limit 1";
            OdbcCommand command = new OdbcCommand(serviceProvidersSQLQueryTemplate, connection);
            command.Parameters.AddWithValue("@service_tp_id", partner_id);
            command.Parameters.AddWithValue("@client_group_id", client_group_id);

            OdbcDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                getServiceEndpointID = reader.GetString(0);
                log.Debug(string.Format("UpdatePackagesAndServices() found a getService endpoint. endpoint_id = [{0}]", getServiceEndpointID));

                getServiceURL = reader.GetString(1);
                log.Debug(string.Format("getServiceURL  = [{0}]", getServiceURL));

                getServiceHandlerURL = reader.GetString(2);
                log.Debug(string.Format("getServiceHandlerURL  = [{0}]", getServiceHandlerURL));

                getServiceEndpointOptions = reader.GetString(3);
                log.Debug(string.Format("getServiceEndpointOptions  = [{0}]", getServiceEndpointOptions));
            }
            reader.Close();

            // 2. Exit if there isn't any getService handler defined
            if (String.Empty.Equals(getServiceHandlerURL))
                return;

            // 3. Construct the full getServiceHandlerURL
            String hrnxEnvironmentBase = ConfigurationManager.AppSettings["hrnxWebHome"];
            String hrnxInternalHttpPrefix = ConfigurationManager.AppSettings["hrnxInternalHttpPrefix"];

            getServiceHandlerURL = webAccessor.getFullHRNXURL(hrnxEnvironmentBase, hrnxInternalHttpPrefix, getServiceHandlerURL);

            // 4. Parse the endpoint options, and prepare the payload
            String payloadFinal = webAccessor.constructHRNXInternalPayloadUsingEndpointOptions(getServiceEndpointOptions, getServiceURL, client_group_id);

            // 5. Actually send the request out
            int timeout = 5 * 60 * 1000;  //5minute as timeout
            HRNXWebResponse hrnxResponse = webAccessor.httpSend(getServiceHandlerURL, "POST", "application/x-www-form-urlencoded", payloadFinal, timeout);
            log.Debug(string.Format("getService HTTP Status is [{0}]", hrnxResponse.status));
            log.Debug(string.Format("getService ResponseText is [{0}]", hrnxResponse.responseText));
        }

        [Route("API/getClientConfiguration/{client_group_id}")]
        [HttpGet]
        public IHttpActionResult getPackage(string client_group_id)
        {
            log.Info(string.Format("Calling getPackage() with [{0}]", client_group_id));

            String connectionStr = ConfigurationManager.AppSettings["HRNXDBConfigurationForERecruit"];
            if (String.IsNullOrEmpty(connectionStr))
            {
                HRNXError invalidClientGroupIDError = new HRNXError();
                invalidClientGroupIDError.code = 1;
                invalidClientGroupIDError.message = string.Format("Cannot find Connection String");
                log.Error(string.Format("Cannot find Connection String with name [{0}]", connectionStr));

                return returnErrorToCaller(invalidClientGroupIDError, HttpStatusCode.InternalServerError);
            }

            // Fetch the list of service providers from DB
            List<serviceProvider> serviceProviders = new List<serviceProvider>();

            using (OdbcConnection connection = new OdbcConnection(connectionStr))
            {

                try
                {
                    // Open the DB connection
                    connection.Open();

                    // get the list of service provider given the client
                    getServiceProviders(client_group_id, ref serviceProviders, connection);

                    if (serviceProviders.Count == 0)
                    {
                        // Then we should just error out with invalid client_group_id
                        //return BadRequest(string.Format("Cannot find configurations for client_group_id [{0}]", client_group_id));

                        HRNXError invalidClientGroupIDError = new HRNXError();
                        invalidClientGroupIDError.code = 2;
                        invalidClientGroupIDError.message = string.Format("Cannot find configurations for client_group_id [{0}]", client_group_id);

                        return returnErrorToCaller(invalidClientGroupIDError, HttpStatusCode.BadRequest);
                    }

                    // populate the list of service provider with detail data from DB
                    for (int serviceProviderIndex = 0; serviceProviderIndex < serviceProviders.Count; serviceProviderIndex++)
                    {
                        serviceProvider current = serviceProviders[serviceProviderIndex];
                        string currentVendorName = "";
                        string currentVendorType = "";

                        getServiceProviderDetail(current.vendorid, ref currentVendorName, ref currentVendorType, connection);
                        current.vendorname = currentVendorName;
                        current.vendortype = currentVendorType;

                        // make any getService() call to update the package and/or services
                        UpdatePackagesAndServices(current.vendorid, client_group_id, connection);

                        // get the list of packages
                        int adhoc_setting = -1;
                        List<Package> packages = getListOfPackages(current.vendorid, client_group_id, ref adhoc_setting, connection);
                        current.packages = packages;

                        // get the packages detail
                        Package target;
                        for (int packageIndex = 0; packageIndex < packages.Count; packageIndex++)
                        {
                            target = packages[packageIndex];

                            List<Service> services = getListOfServicesInPackages(current.vendorid, client_group_id, target.packageid, connection);
                            target.packageservices = services;

                            for (int serviceIndex = 0; serviceIndex < services.Count; serviceIndex++)
                            {
                                Service targetSerivce = services[serviceIndex];
                                getServiceDetail(ref targetSerivce, current.vendorid, client_group_id, targetSerivce.serviceid, connection);
                            }
                        }

                        //Determine if we need to get Service Details
                        if (adhoc_setting == 1 || adhoc_setting == 2)
                        {
                            // For these flags, we do not allow service to be order, so just a blank list
                            current.individualservices = new List<Service>();
                        }
                        else
                        {
                            // we do need to get the full list of services because clients are allowed to order each of them
                            List<Service> individualServices = getListOfServicesForClient(current.vendorid, client_group_id, connection);
                            current.individualservices = individualServices;

                            // then get the service detail
                            for (int serviceIndex = 0; serviceIndex < individualServices.Count; serviceIndex++)
                            {
                                Service targetSerivce = individualServices[serviceIndex];
                                getServiceDetail(ref targetSerivce, current.vendorid, client_group_id, targetSerivce.serviceid, connection);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Fatal(ex);
                    HRNXError odbcError = new HRNXError();
                    odbcError.code = 3;
                    odbcError.message = ex.Message;

                    return returnErrorToCaller(odbcError, HttpStatusCode.BadRequest);
                }
                finally
                {
                    // Close the connection because we are done
                    connection.Close();
                }
            }
            ClientConfiguration result = new ClientConfiguration();
            result.serviceproviders = serviceProviders;

            return Ok(result);
        }
    }
}
