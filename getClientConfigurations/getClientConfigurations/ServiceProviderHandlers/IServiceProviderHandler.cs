﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using getClientConfigurations.Models;

namespace getClientConfigurations.ServiceProviderHandlers
{
    public interface IServiceProviderHandler
    {
        Order processRequest(Order hrnxOrder, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor);
    }
}
