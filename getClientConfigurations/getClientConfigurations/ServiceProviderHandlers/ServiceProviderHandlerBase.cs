﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using getClientConfigurations.Models;
using Newtonsoft.Json;
using System.Resources;
using System.Text;
using System.Net;
using System.Xml;

namespace getClientConfigurations.ServiceProviderHandlers
{
    public abstract class ServiceProviderHandlerBase : IServiceProviderHandler
    {
        protected ResourceManager rm = new ResourceManager("getClientConfigurations.Properties.HRNX", typeof(HRNXDBAccessor).Assembly);
		private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		public Order processRequest(Order hrnxOrder, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor)
        {
            hrnxOrder.serviceProviderWorkflowStarts();
            return providerSpecificProcessing(hrnxOrder, dbAccessor, webAccessor);
        }
		public String constructReportXML(Order hrnxOrder)
        {
			HRNXResult target = new HRNXResult();
			target.txn_id = hrnxOrder.txn_id;
			target.client_user_id = hrnxOrder.client_user_id;
			target.client_group_id = hrnxOrder.client_group_id;
			target.tp_uqid = hrnxOrder.candidate_id;
			target.status = hrnxOrder.status;

			String atsID = "";
			try
			{
				atsID = hrnxOrder.additionalData[OrderAdditionalDataKeys.atsID];
			}
			catch (KeyNotFoundException)
			{ }
			target.tp_id = atsID;

			HRNXResultDetail detailReport = new HRNXResultDetail();
			String service_tp_id = "";
			try
			{
				service_tp_id = hrnxOrder.additionalData[OrderAdditionalDataKeys.serviceProviderID];
			}
			catch (KeyNotFoundException)
			{ }
			detailReport.tp = service_tp_id;
			String serviceName = "";
			try
			{
				serviceName = hrnxOrder.additionalData[OrderAdditionalDataKeys.packageName];
			}
			catch (KeyNotFoundException)
			{ }
			detailReport.name = serviceName;
			detailReport.VendorOrderId = hrnxOrder.vendorOrderID;

			HRNXResultDetailCollection deatilList = new HRNXResultDetailCollection();
			deatilList.service = detailReport;
			target.services = deatilList;

			String result = HRNXUtil.serializeObjToXML(target);

			return result;
        }

        public abstract Order providerSpecificProcessing(Order hrnxOrder, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor);


    }
}