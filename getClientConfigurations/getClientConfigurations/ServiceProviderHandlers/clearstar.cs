﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using getClientConfigurations.Models;
using Newtonsoft.Json;
using System.Resources;
using System.Text;
using System.Net;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace getClientConfigurations.ServiceProviderHandlers
{
    public class clearstar : ServiceProviderHandlerBase
    {
		private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
		public static readonly String LOCATION = "clearStar Passthru Request handler";
        public static readonly String SERVICE_TP_ID = "clearstar2";
        public static readonly int timeout = 30000;

        public static String extractBusinesUnitIDFromServiceProviderID(String serviceProviderID)
        {
            int separatorIndex = serviceProviderID.IndexOf('~');

            if(separatorIndex > 0)
                return serviceProviderID.Substring(separatorIndex + 1);
            return "";
        }
        public clearStarCountry getClearStarCountryCode(String hrnxCountryValue, Order hrnxOrder, clearStarCredential credentials, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor, StringBuilder logContent)
        {
            // pull templateEndpoint from DB
            getFieldEndpoint endpointData = dbAccessor.getTemplateEndpoint(SERVICE_TP_ID, "clearstar2_ep1",hrnxOrder,LOCATION,null);
            String countryCodeLookupURL = endpointData.url;
            String countryCodeLookupTemplate = endpointData.xslt;

            // Substitute the payload
            countryCodeLookupTemplate = countryCodeLookupTemplate.Replace("^^^USERNAME^^^", credentials.username);
            countryCodeLookupTemplate = countryCodeLookupTemplate.Replace("^^^PASSWORD^^^", credentials.password);
            countryCodeLookupTemplate = countryCodeLookupTemplate.Replace("^^^BO_ID^^^", credentials.businessUnitID);

            String soapAction = "http://clearstar.net/Gateway/LookUp/GetISOCountryList";
            List<Tuple<string, string>> headers = new List<Tuple<string, string>>();
            headers.Add(new Tuple<string, string>("SOAPAction", soapAction));

            HRNXWebResponse response = webAccessor.httpSend(countryCodeLookupURL,"POST", "text/xml", countryCodeLookupTemplate,headers,timeout,logContent);

			clearStarCountry extractedCountryCode = null;

            if(response.status == HttpStatusCode.OK)
            {
                // Use xpath to extract the country code
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(response.responseText);

                String xpath = "//*[local-name()='GetISOCountryList']/*[local-name()='Country'][./Alpha2Code='" + hrnxCountryValue + "']";

                XmlNode current = xmlDoc.SelectSingleNode(xpath);
                if(current != null)
				{

					XmlSerializer serializer = new XmlSerializer(typeof(clearStarCountry));
					StringReader rdr = new StringReader(current.OuterXml);
					extractedCountryCode = (clearStarCountry)serializer.Deserialize(rdr);
				}
            }

            return extractedCountryCode;
        }

		public clearStarResponse sendToClearStar(String targetURL, String payload, String soapAction, String caller, Order hrnxOrder, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor, StringBuilder logContent)
		{
			List<Tuple<string, string>> headers = new List<Tuple<string, string>>();
			headers.Add(new Tuple<string, string>("SOAPAction", soapAction));
			//errorStack may not be able to hold everything that has ever submitted, so I need to create 
			//a local version that I could append to before I exit
			StringBuilder localLogContent = new StringBuilder();

			HRNXWebResponse response = webAccessor.httpSend(targetURL, "POST", "text/xml", payload, headers, timeout, localLogContent);
			// Adding everything back to the generic one
			logContent.Append(localLogContent.ToString());

			if (response.status == HttpStatusCode.OK)
			{
				// Use xpath to extract the country code
				clearStarResponse parsedResponse = null;
				try
				{
					parsedResponse = clearStarResponse.loadClearStarResponse(response.responseText);

					if(!"0".Equals(parsedResponse.status))
					{
						if("1".Equals(parsedResponse.status))
						{
							// report a warning to errorStack, but with severity 0
							HRNXUtil.logError(hrnxOrder, 0, LOCATION, string.Format(rm.GetString("CLEARSTAR_REPORTED_A_WARNING"), caller), "", localLogContent.ToString(), "", dbAccessor);
						}
						else
						{
							HRNXUtil.logError(hrnxOrder, 3, LOCATION, string.Format(rm.GetString("CLEARSTAR_REPORTED_AN_ERROR"), caller), "", localLogContent.ToString(), "", dbAccessor);
							return null;
						}
					}

					return parsedResponse;
				}
				catch (XmlException ex)
				{
					HRNXUtil.logException(hrnxOrder, 3, LOCATION, string.Format(rm.GetString("FAILED_TO_PARSE_CLEARSTAR_RESPONSE"), caller), localLogContent.ToString(), ex, dbAccessor);
					return parsedResponse;
				}
			}
			else
			{
				HRNXUtil.logError(hrnxOrder,3,LOCATION, string.Format(rm.GetString("CLEARSTAR_RETURNED_NON_200"), caller), "", localLogContent.ToString(), "", dbAccessor);
				return null;
			}
		}

		public String clearStarCreateOrder(String clearStarCountryID, Order hrnxOrder, clearStarCredential credentials, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor, StringBuilder logContent)
		{
			// pull templateEndpoint from DB
			getFieldEndpoint endpointData = dbAccessor.getTemplateEndpoint(SERVICE_TP_ID, "clearstar2_ep2", hrnxOrder, LOCATION, null);
			String createOrderURL = endpointData.url;
			String createOrderXSLT = endpointData.xslt;

			String createOrderTemplate = HRNXUtil.applyXSLT(hrnxOrder.payload_hrnx, createOrderXSLT, dbAccessor, hrnxOrder, LOCATION);

			// Substitute the payload
			createOrderTemplate = createOrderTemplate.Replace("getValue(sUsername)", credentials.username);
			createOrderTemplate = createOrderTemplate.Replace("getValue(sPassword)", credentials.password);
			createOrderTemplate = createOrderTemplate.Replace("getValue(service_tp_id)", credentials.businessUnitID);
			createOrderTemplate = createOrderTemplate.Replace("getValue(cust_id)", credentials.customerID);
			createOrderTemplate = createOrderTemplate.Replace("getValue(countryID)", clearStarCountryID);

			String soapAction = "http://clearstar.net/Gateway/Profile/CreateProfileForCountry";
			clearStarResponse parsedResponse = sendToClearStar(createOrderURL, createOrderTemplate, soapAction, "CreateOrder", hrnxOrder, dbAccessor, webAccessor, logContent);

			String extractedProfileID = "";
			String xpath = "//*[local-name()='CreateProfileForCountryResult']/*[local-name()='CreateProfileForCountry']/*[local-name()='Profile']/*[local-name()='Prof_No']";

			if(parsedResponse != null)
			{
				XmlNode current = parsedResponse.xmlDoc.SelectSingleNode(xpath);
				if (current != null)
					extractedProfileID = current.InnerText;

				logContent.Append(HRNXUtil.LOG_CONTENT_SEPARATOR);
				logContent.Append("extractedProfileID = [" + extractedProfileID + "]");
				logContent.Append(Environment.NewLine);
			}

			return extractedProfileID;
		}
		public Boolean clearStarAddServiceToOrder(String serviceID, String ProfileID, Order hrnxOrder, clearStarCredential credentials, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor, StringBuilder logContent)
		{
			// pull templateEndpoint from DB
			getFieldEndpoint endpointData = dbAccessor.getTemplateEndpoint(SERVICE_TP_ID, "clearstar2_ep3", hrnxOrder, LOCATION, null);
			String AddServiceToOrderURL = endpointData.url;
			String AddServiceToOrderPayloadTemplate = endpointData.xslt;

			// Substitute the payload
			AddServiceToOrderPayloadTemplate = AddServiceToOrderPayloadTemplate.Replace("getValue(sUsername)", credentials.username);
			AddServiceToOrderPayloadTemplate = AddServiceToOrderPayloadTemplate.Replace("getValue(sPassword)", credentials.password);
			AddServiceToOrderPayloadTemplate = AddServiceToOrderPayloadTemplate.Replace("getValue(service_tp_id)", credentials.businessUnitID);
			AddServiceToOrderPayloadTemplate = AddServiceToOrderPayloadTemplate.Replace("getValue(cust_id)", credentials.customerID);
			AddServiceToOrderPayloadTemplate = AddServiceToOrderPayloadTemplate.Replace("getValue(profile_no)", ProfileID);
			AddServiceToOrderPayloadTemplate = AddServiceToOrderPayloadTemplate.Replace("getValue(service_no)", serviceID);

			String soapAction = "http://clearstar.net/Gateway/Profile/AddServiceToProfile";

			clearStarResponse parsedResponse = sendToClearStar(AddServiceToOrderURL, AddServiceToOrderPayloadTemplate, soapAction, "AddServiceToOrder", hrnxOrder, dbAccessor, webAccessor, logContent);

			return parsedResponse != null;
		}
		public Boolean clearStarTransmitProfile(String ProfileID, Order hrnxOrder, clearStarCredential credentials, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor, StringBuilder logContent)
		{
			// pull templateEndpoint from DB
			getFieldEndpoint endpointData = dbAccessor.getTemplateEndpoint(SERVICE_TP_ID, "clearstar2_ep4", hrnxOrder, LOCATION, null);
			String TransmitProfileURL = endpointData.url;
			String TransmitProfilePayloadTemplate = endpointData.xslt;

			// Substitute the payload
			TransmitProfilePayloadTemplate = TransmitProfilePayloadTemplate.Replace("getValue(sUsername)", credentials.username);
			TransmitProfilePayloadTemplate = TransmitProfilePayloadTemplate.Replace("getValue(sPassword)", credentials.password);
			TransmitProfilePayloadTemplate = TransmitProfilePayloadTemplate.Replace("getValue(service_tp_id)", credentials.businessUnitID);
			TransmitProfilePayloadTemplate = TransmitProfilePayloadTemplate.Replace("getValue(cust_id)", credentials.customerID);
			TransmitProfilePayloadTemplate = TransmitProfilePayloadTemplate.Replace("getValue(profile_no)", ProfileID);

			String soapAction = "http://clearstar.net/Gateway/Profile/TransmitProfile";

			clearStarResponse parsedResponse = sendToClearStar(TransmitProfileURL, TransmitProfilePayloadTemplate, soapAction, "TransmitProfile", hrnxOrder, dbAccessor, webAccessor, logContent);

			return parsedResponse != null;
		}
		public Boolean clearStarReOpenProfile(String ProfileID, Order hrnxOrder, clearStarCredential credentials, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor, StringBuilder logContent)
		{
			// pull templateEndpoint from DB
			getFieldEndpoint endpointData = dbAccessor.getTemplateEndpoint(SERVICE_TP_ID, "clearstar2_ep6", hrnxOrder, LOCATION, null);
			String ReOpenProfileURL = endpointData.url;
			String ReOpenProfilePayloadTemplate = endpointData.xslt;

			// Substitute the payload
			ReOpenProfilePayloadTemplate = ReOpenProfilePayloadTemplate.Replace("getValue(sUsername)", credentials.username);
			ReOpenProfilePayloadTemplate = ReOpenProfilePayloadTemplate.Replace("getValue(sPassword)", credentials.password);
			ReOpenProfilePayloadTemplate = ReOpenProfilePayloadTemplate.Replace("getValue(service_tp_id)", credentials.businessUnitID);
			ReOpenProfilePayloadTemplate = ReOpenProfilePayloadTemplate.Replace("getValue(cust_id)", credentials.customerID);
			ReOpenProfilePayloadTemplate = ReOpenProfilePayloadTemplate.Replace("getValue(profile_no)", ProfileID);

			String soapAction = "http://clearstar.net/Gateway/Profile/ReOpenProfile";

			clearStarResponse parsedResponse = sendToClearStar(ReOpenProfileURL, ReOpenProfilePayloadTemplate, soapAction, "ReOpenProfile", hrnxOrder, dbAccessor, webAccessor, logContent);

			return parsedResponse != null;
		}
		public static String findProfileIDFromTxn(Order hrnxOrder, HRNXDBAccessor dbAccessor, StringBuilder logContent)
		{
			String ProfileID = "";

			logContent.Append(HRNXUtil.LOG_CONTENT_SEPARATOR);
			logContent.Append("Trying to pull ProfileID from DB");
			logContent.Append(Environment.NewLine);

			// pull the vendor_order_id, which contains the profileID and customerID
			VendorOrderIDLookupEntry currentVendorOrderIDLookupEntry = dbAccessor.getVendorOrderIDLookupEntriesFromDBUsingTxnID(hrnxOrder, LOCATION);

			if(currentVendorOrderIDLookupEntry != null)
			{
				String vendor_order_id = currentVendorOrderIDLookupEntry.vendor_order_id;
				logContent.Append("vendor_order_id = [");
				logContent.Append(vendor_order_id);
				logContent.Append("]");
				logContent.Append(Environment.NewLine);

				// clearStar's vendor_order_id is just profileID + "_" + ccustomerID, so I can easily lookup "_" as separator, and get everything in front of it
				int separatorIndex = vendor_order_id.IndexOf("_");
				ProfileID = vendor_order_id.Substring(0, separatorIndex);
			}

			return ProfileID;
		}
		public Boolean clearStarAddOrderToProfile(String inputPayload, String endpointName, String clearStarServiceID, String ProfileID, Order hrnxOrder, clearStarCredential credentials, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor, StringBuilder logContent)
		{
			return clearStarAddOrderToProfile(inputPayload, endpointName, clearStarServiceID, ProfileID, hrnxOrder, credentials, null, dbAccessor, webAccessor, logContent);
		}
		public Boolean clearStarAddOrderToProfile(String inputPayload, String endpointName, String clearStarServiceID, String ProfileID, Order hrnxOrder, clearStarCredential credentials, List<Tuple<String,String>> additionalReplacement, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor, StringBuilder logContent)
		{
			log.Debug(string.Format("inputPayload is [{0}]", inputPayload));

			// Pull endpooint from DB
			getFieldEndpoint endpointData = dbAccessor.getTemplateEndpoint(SERVICE_TP_ID, endpointName, hrnxOrder, LOCATION, null);
			String AddOrderToProfileURL = endpointData.url;
			String AddOrderToXSLT = endpointData.xslt;

			log.Debug(string.Format("AddOrderToXSLT is [{0}]", AddOrderToXSLT));

			// Apply the XSLT transform 
			String payloadFInal = HRNXUtil.applyXSLT(inputPayload, AddOrderToXSLT, dbAccessor, hrnxOrder, LOCATION);

			// Substitute the payload
			payloadFInal = payloadFInal.Replace("getValue(sUsername)", credentials.username);
			payloadFInal = payloadFInal.Replace("getValue(sPassword)", credentials.password);
			payloadFInal = payloadFInal.Replace("getValue(service_tp_id)", credentials.businessUnitID);
			payloadFInal = payloadFInal.Replace("getValue(service_name)", clearStarServiceID);
			payloadFInal = payloadFInal.Replace("getValue(cust_id)", credentials.customerID);
			payloadFInal = payloadFInal.Replace("getValue(profile_no)", ProfileID);

			if(additionalReplacement != null)
			{
				String key;
				String value;
				for(int i=0;i<additionalReplacement.Count;i++)
				{
					key = additionalReplacement[i].Item1;
					value = additionalReplacement[i].Item2;
					payloadFInal = payloadFInal.Replace(key, value);
				}
			}

			String soapAction = "http://clearstar.net/Gateway/Profile/AddOrderToProfile";

			clearStarResponse parsedResponse = sendToClearStar(AddOrderToProfileURL, payloadFInal, soapAction, "AddOrderToProfile", hrnxOrder, dbAccessor, webAccessor, logContent);

			return parsedResponse != null;
		}
		public String getCounty(clearStarCountry currentAddressCountry, Order hrnxOrder, clearStarCredential credentials, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor, StringBuilder logContent)
		{
			log.Debug("Entering getCounty()");
			// Try to get the county from hrnxpayload first
			String currentAddressCounty = HRNXUtil.extractDataFromOrder(hrnxOrder.payload_hrnx, "/order/subject/field[@name='address_present_county']");
			String currentAddressZip = HRNXUtil.extractDataFromOrder(hrnxOrder.payload_hrnx, "/order/subject/field[@name='address_present_zip']");
			log.Debug(string.Format("currentAddressCountry is [{0}]", currentAddressCountry));
			log.Debug(string.Format("currentAddressCounty from order is [{0}]", currentAddressCounty));
			log.Debug(string.Format("currentAddressZip from order is [{0}]", currentAddressZip));

			if (String.IsNullOrEmpty(currentAddressCounty))
			{
				log.Debug("We did not get any currentADdressCounty, so going to lookup from clearstar");

				// pull templateEndpoint from DB
				getFieldEndpoint endpointData = dbAccessor.getTemplateEndpoint(SERVICE_TP_ID, "clearstar2_ep13", hrnxOrder, LOCATION, null);
				String countyCodeLookupURL = endpointData.url;
				String countyCodeLookupTemplate = endpointData.xslt;
				log.Debug(string.Format("countyCodeLookupURL pulled is [{0}]", countyCodeLookupURL));

				// Substitute the payload
				countyCodeLookupTemplate = countyCodeLookupTemplate.Replace("^^^USERNAME^^^", credentials.username);
				countyCodeLookupTemplate = countyCodeLookupTemplate.Replace("^^^PASSWORD^^^", credentials.password);
				countyCodeLookupTemplate = countyCodeLookupTemplate.Replace("^^^BO_ID^^^", credentials.businessUnitID);
				countyCodeLookupTemplate = countyCodeLookupTemplate.Replace("^^^COUNTRY^^^", currentAddressCountry.FullName);
				countyCodeLookupTemplate = countyCodeLookupTemplate.Replace("^^^ZIP^^^", currentAddressZip);
				log.Debug(string.Format("countyCodeLookupTemplate final is [{0}]", countyCodeLookupTemplate));

				String soapAction = "http://clearstar.net/Gateway/LookUp/GetLocationsForZipCode";
				List<Tuple<string, string>> headers = new List<Tuple<string, string>>();
				headers.Add(new Tuple<string, string>("SOAPAction", soapAction));

				HRNXWebResponse response = webAccessor.httpSend(countyCodeLookupURL, "POST", "text/xml", countyCodeLookupTemplate, headers, timeout, logContent);
				log.Debug(string.Format("countyLookup logContent is [{0}]", logContent));

				if (response.status == HttpStatusCode.OK)
				{
					// Use xpath to extract the country code
					XmlDocument xmlDoc = new XmlDocument();
					xmlDoc.LoadXml(response.responseText);

					String xpath = "//*[local-name()='GetLocationsForZipCode']/*[local-name()='County']";

					XmlNode current = xmlDoc.SelectSingleNode(xpath);
					if (current != null)
						currentAddressCounty = current.InnerText;

					logContent.Append(HRNXUtil.LOG_CONTENT_SEPARATOR);
					logContent.Append("Extracted currentAddressCounty = [" + currentAddressCounty + "]");
					logContent.Append(Environment.NewLine);
				}
			}

			log.Debug("Exiting getCounty()");
			return currentAddressCounty;
		}
		public Boolean clearStarUploadDocument(String ProfileID, String authFormData, Order hrnxOrder, clearStarCredential credentials, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor, StringBuilder logContent)
		{
			// pull templateEndpoint from DB
			getFieldEndpoint endpointData = dbAccessor.getTemplateEndpoint(SERVICE_TP_ID, "clearstar2_ep5", hrnxOrder, LOCATION, null);
			String UploadDocumentURL = endpointData.url;
			String UploadDocumentPayloadTemplate = endpointData.xslt;

			// Substitute the payload
			UploadDocumentPayloadTemplate = UploadDocumentPayloadTemplate.Replace("getValue(sUsername)", credentials.username);
			UploadDocumentPayloadTemplate = UploadDocumentPayloadTemplate.Replace("getValue(sPassword)", credentials.password);
			UploadDocumentPayloadTemplate = UploadDocumentPayloadTemplate.Replace("getValue(service_tp_id)", credentials.businessUnitID);
			UploadDocumentPayloadTemplate = UploadDocumentPayloadTemplate.Replace("getValue(cust_id)", credentials.customerID);
			UploadDocumentPayloadTemplate = UploadDocumentPayloadTemplate.Replace("getValue(profile_no)", ProfileID);
			UploadDocumentPayloadTemplate = UploadDocumentPayloadTemplate.Replace("^^^AUTH_FORM_DATA^^^", authFormData);

			String soapAction = "http://clearstar.net/Gateway/Document/UploadProfileDocument3";

			clearStarResponse parsedResponse = sendToClearStar(UploadDocumentURL, UploadDocumentPayloadTemplate, soapAction, "UploadDocument", hrnxOrder, dbAccessor, webAccessor, logContent);

			return parsedResponse != null;
		}
		public override Order providerSpecificProcessing(Order hrnxOrder, HRNXDBAccessor dbAccessor, HRNXWebAccessor webAccessor)
		{
			hrnxOrder.serviceProviderWorkflowStarts();

			// Ensure TLS 1.2, so we can make the connection to clearStar
			ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

			// Get clearStar credentails and validate them
			clearStarCredential credentials = dbAccessor.getClearStarCredential(hrnxOrder, LOCATION);

			// create logContent
			StringBuilder logContent = new StringBuilder();

			Boolean createNewOrder;

			try
			{
				String isNewOrderStr = hrnxOrder.additionalData[OrderAdditionalDataKeys.isNewOrder];
				if (!Boolean.TryParse(isNewOrderStr, out createNewOrder))
				{
					HRNXUtil.logError(hrnxOrder, 3, LOCATION, rm.GetString("CANNOT_DETERMINE_NEW_ORDER"), "", hrnxOrder.payload_hrnx, string.Format(rm.GetString("CANNOT_DETERMINE_NEW_ORDER_ERROR_DETAIL"), isNewOrderStr), dbAccessor);
					return hrnxOrder;
				}
			}
			catch(KeyNotFoundException)
			{
				HRNXUtil.logError(hrnxOrder, 3, LOCATION, rm.GetString("NEW_ORDER_KEY_NOT_FOUND"), "", hrnxOrder.payload_hrnx, HRNXUtil.serializeObjToJSON(hrnxOrder.additionalData), dbAccessor);
				return hrnxOrder;
			}

			String profileID = "";
			Boolean hasError = false;
			Boolean isSucess;

			// 1. lookup the country code from clearStar
			String currentAddressCountryXPath = "order/subject/field[@name='address_present_country']";
			String currentAddressCountry = HRNXUtil.extractDataFromOrder(hrnxOrder.payload_hrnx, currentAddressCountryXPath);
			// Error out if we cannot get the countryCode
			if (String.IsNullOrEmpty(currentAddressCountry))
			{
				HRNXUtil.logError(hrnxOrder, 3, LOCATION, rm.GetString("EMPTY_CURRENT_COUNTRY_CODE"), "", hrnxOrder.payload_hrnx, currentAddressCountryXPath, dbAccessor);
				return hrnxOrder;
			}

			clearStarCountry targetCountry = getClearStarCountryCode(currentAddressCountry, hrnxOrder, credentials, dbAccessor, webAccessor, logContent);
			// Error out if we cannot get the countryCode
			if (targetCountry == null)
			{
				HRNXUtil.logError(hrnxOrder, 3, LOCATION, rm.GetString("CLEARSTAR_COUNTRY_CODE_EMPTY"), "", logContent.ToString(), "", dbAccessor);
				return hrnxOrder;
			}
			String countryCode = targetCountry.ID;

			logContent.Append(HRNXUtil.LOG_CONTENT_SEPARATOR);
			logContent.Append("ExtractedCountryCode = [" + countryCode + "]");
			logContent.Append(Environment.NewLine);

			log.Debug(string.Format("createNewOrder is [{0}]", createNewOrder));
			if (createNewOrder)
			{
				// 2. Create Profile
				profileID = clearStarCreateOrder(countryCode, hrnxOrder, credentials, dbAccessor, webAccessor, logContent);
				// Error out if we cannot get the profileID
				if (String.IsNullOrEmpty(profileID))
				{
					HRNXUtil.logError(hrnxOrder, 3, LOCATION, rm.GetString("CLEARSTAR_PROFILE_ID_EMPTY"), "", logContent.ToString(), "", dbAccessor);
					return hrnxOrder;
				}
			}
			else
			{
				// 1. Find original profileID
				profileID = findProfileIDFromTxn(hrnxOrder, dbAccessor, logContent);
				// Error out if we cannot get the profileID
				if (String.IsNullOrEmpty(profileID))
				{
					HRNXUtil.logError(hrnxOrder, 3, LOCATION, rm.GetString("UNABLE_TO_FIND_CLEARSTAR_PROFILE_ID"), "", logContent.ToString(), "", dbAccessor);
					return hrnxOrder;
				}

				// 2. Re-open Profile
				isSucess = clearStarReOpenProfile(profileID, hrnxOrder, credentials, dbAccessor, webAccessor, logContent);
				if(!isSucess)
				{
					// Error out if we cannot re-open profile
					// The real error would have been capture previously, so just return here
					return hrnxOrder;
				}
			}
			hrnxOrder.vendorOrderID = profileID;

			// 3. Add all services to profile
			// 3.1 Extract services from the order
			String serviceToOrderXpath = "order/services/service[@tp='" + hrnxOrder.getCurrentServiceProviderID() + "']/field[@name='package']";
			String serviceToOrder = hrnxOrder.additionalData[OrderAdditionalDataKeys.package];

			// 3.2 AddServiceToOrder
			isSucess = clearStarAddServiceToOrder(serviceToOrder, profileID, hrnxOrder, credentials, dbAccessor, webAccessor, logContent);
			if (!hasError)
				// report an error, but let the code continues
				hasError = !isSucess;

			// 4. Add fields to the profile if we need to
			// 4.2 Check if the service being ordered is a package
			List<String> servicesFromPackageID = dbAccessor.getServiceIDAssociateWithPackage(hrnxOrder, LOCATION, serviceToOrder);

			List<String> servicesToCheck;
			String serviceName = "";

			if(servicesFromPackageID.Count == 0)
			{
				log.Debug("Current service being ordered is just a single service");
				// Then current service is NOT a package, so we need to check it
				servicesToCheck = new List<String>();
				servicesToCheck.Add(serviceToOrder);

				// Then try to get the serviceName based on the serviceID
				serviceName = dbAccessor.getServiceName(hrnxOrder, LOCATION, serviceToOrder);
				log.Debug(string.Format("serviceName returned is [{0}]",serviceName));
			}
			else
			{
				log.Debug("Current service being ordered is just a package");
				servicesToCheck = servicesFromPackageID;
				// Then try to get the serviceName based on the serviceID
				serviceName = dbAccessor.getPackageName(hrnxOrder, LOCATION, serviceToOrder);
				log.Debug(string.Format("serviceName returned is [{0}]", serviceName));
			}
			if (!String.IsNullOrEmpty(serviceName))
				hrnxOrder.additionalData.Add(OrderAdditionalDataKeys.packageName, serviceName);

			String county = null;
			for (int i=0;i< servicesToCheck.Count;i++)
			{
				// 4.1 Convert the service being ordered to HRNX service, which we can understand
				String hrnxServiceType = dbAccessor.convertToHRNXService(hrnxOrder, LOCATION, servicesToCheck[i]);
				switch (hrnxServiceType)
				{
					case "criminal_county":
					case "criminal_federal":
						{
							String endpoint;
							if ("criminal_county".Equals(hrnxServiceType))
								endpoint = "clearstar2_ep7";
							else
								endpoint = "clearstar2_ep8";

							if(String.IsNullOrEmpty(county))
								// this way, it will be saved after 1st attempt
								county = getCounty(targetCountry, hrnxOrder, credentials, dbAccessor, webAccessor, logContent);

							List<Tuple<String, String>> replacementList = new List<Tuple<string, string>>();
							replacementList.Add(new Tuple<string, string>("^^^COUNTY^^^", county));
							replacementList.Add(new Tuple<string, string>("^^^COUNTRY^^^", targetCountry.FullName));

							// Because this needs recruiterEmail, I have to pass the entire order over to it
							isSucess = clearStarAddOrderToProfile(hrnxOrder.payload_hrnx, endpoint, servicesToCheck[i], profileID, hrnxOrder, credentials, replacementList, dbAccessor, webAccessor, logContent);
							break;
						}
					case "adverse":
						{
							// Because this needs recruiterEmail, I have to pass the entire order over to it
							isSucess = clearStarAddOrderToProfile(hrnxOrder.payload_hrnx, "clearstar2_ep9", servicesToCheck[i], profileID, hrnxOrder, credentials, dbAccessor, webAccessor, logContent);
							break;
						}
					case "mvr":
						{
							// Because this needs recruiterEmail, I have to pass the entire order over to it
							isSucess = clearStarAddOrderToProfile(hrnxOrder.payload_hrnx, "clearstar2_ep10", servicesToCheck[i], profileID, hrnxOrder, credentials, dbAccessor, webAccessor, logContent);
							break;
						}
					case "education":
					case "educationlv3":
					case "employment":
					case "employmentlv3":
						{
							String endpoint;
							String targetXpath;
							int serviceMax = -1;
							int serviceMaxTemp;

							if ("education".Equals(hrnxServiceType))
							{
								log.Debug("This is an education service");
								endpoint = "clearstar2_ep11";
								targetXpath = "/order/services/service[@name='education']";

								// check if clientOption has defined a serviceMax
								if(serviceMaxClientOptionExist(dbAccessor, hrnxOrder, "clearstarEduMax", out serviceMaxTemp))
									serviceMax = serviceMaxTemp;
							}
							else if ("educationlv3".Equals(hrnxServiceType))
							{
								log.Debug("This is an education service lv 3");
								endpoint = "clearstar2_ep15";
								targetXpath = "/order/services/service[@name='education']";

								// check if clientOption has defined a serviceMax
								if (serviceMaxClientOptionExist(dbAccessor, hrnxOrder, "clearstarEduMax", out serviceMaxTemp))
									serviceMax = serviceMaxTemp;
							}
							else if ("employment".Equals(hrnxServiceType))
							{
								log.Debug("This is an employment service");
								endpoint = "clearstar2_ep12";
								targetXpath = "/order/services/service[@name='employment']";

								// check if clientOption has defined a serviceMax
								if (serviceMaxClientOptionExist(dbAccessor, hrnxOrder, "clearstarEmpMax", out serviceMaxTemp))
									serviceMax = serviceMaxTemp;
							}
							else
							{
								log.Debug("This is an employment lv 3 service");
								endpoint = "clearstar2_ep16";
								targetXpath = "/order/services/service[@name='employment']";

								// check if clientOption has defined a serviceMax
								if (serviceMaxClientOptionExist(dbAccessor, hrnxOrder, "clearstarEmpMax", out serviceMaxTemp))
									serviceMax = serviceMaxTemp;
							}
							log.Debug(string.Format("serviceMax is [{0}]", serviceMax));

							XmlNodeList list = HRNXUtil.extractXMLPartsFromOrder(hrnxOrder.payload_hrnx, targetXpath);
							log.Debug(string.Format("list.Count is [{0}]", list.Count));

							// If there isn't any defined, then we won't run the rest
							if (list.Count > 0)
							{
								if (serviceMax < 0)
								{
									serviceMax = list.Count;
									log.Debug(string.Format("serviceMax is < 0, so using list.Count instead, which is [{0}]", list.Count));
								}

								for (int j=0;j< serviceMax; j++)
								{
									String currentEducationPartXML = list[j].OuterXml;
									List<Tuple<String, String>> replacementList = new List<Tuple<string, string>>();
									replacementList.Add(new Tuple<string, string>("^^^BEST_PHONE^^^", HRNXUtil.extractDataFromOrder(hrnxOrder.payload_hrnx, "/order/subject/field[@name='bestphone']")));

									Boolean currentStatus = clearStarAddOrderToProfile(currentEducationPartXML, endpoint, servicesToCheck[i], profileID, hrnxOrder, credentials, replacementList, dbAccessor, webAccessor, logContent);
									isSucess = isSucess && currentStatus;
								}
							}
							break;
						}
					case "license":
						{
							String endpoint;
							String targetXpath;
							int serviceMax = -1;
							int serviceMaxTemp;

							log.Debug("This is an license service");
							endpoint = "clearstar2_ep14";
							targetXpath = "/order/services/service[@name='license']";

							// check if clientOption has defined a serviceMax
							String[] clientOptions = dbAccessor.getClientOptions(hrnxOrder, LOCATION);
							if (clientOptions != null)
							{
								String serviceMaxStr = HRNXUtil.extractClientFlagValue(clientOptions, "clearstarLicMax");
								if (Int32.TryParse(serviceMaxStr, out serviceMaxTemp))
									serviceMax = serviceMaxTemp;
							}
							log.Debug(string.Format("serviceMax is [{0}]", serviceMax));

							XmlNodeList list = HRNXUtil.extractXMLPartsFromOrder(hrnxOrder.payload_hrnx, targetXpath);
							log.Debug(string.Format("list.Count is [{0}]", list.Count));

							// If there isn't any defined, then we won't run the rest
							if (list.Count > 0)
							{
								if (serviceMax < 0)
								{
									serviceMax = list.Count;
									log.Debug(string.Format("serviceMax is < 0, so using list.Count instead, which is [{0}]", list.Count));
								}

								for (int j = 0; j < serviceMax; j++)
								{
									String currentLicensePartXML = list[j].OuterXml;

									Boolean currentStatus = clearStarAddOrderToProfile(currentLicensePartXML, endpoint, servicesToCheck[i], profileID, hrnxOrder, credentials, null, dbAccessor, webAccessor, logContent);
									isSucess = isSucess && currentStatus;
								}
							}
							break;
						}
					case "referencelv3":
						{
							String endpoint;
							String targetXpath;
							int serviceMax = -1;
							int serviceMaxTemp;

							log.Debug("This is an professional reference level 3 service");
							endpoint = "clearstar2_ep17";
							targetXpath = "/order/services/service[@name='reference']";

							// check if clientOption has defined a serviceMax
							String[] clientOptions = dbAccessor.getClientOptions(hrnxOrder, LOCATION);
							if (clientOptions != null)
							{
								String serviceMaxStr = HRNXUtil.extractClientFlagValue(clientOptions, "clearstarRefMax");
								if (Int32.TryParse(serviceMaxStr, out serviceMaxTemp))
									serviceMax = serviceMaxTemp;
							}
							log.Debug(string.Format("serviceMax is [{0}]", serviceMax));

							XmlNodeList list = HRNXUtil.extractXMLPartsFromOrder(hrnxOrder.payload_hrnx, targetXpath);
							log.Debug(string.Format("list.Count is [{0}]", list.Count));

							// If there isn't any defined, then we won't run the rest
							if (list.Count > 0)
							{
								if (serviceMax < 0)
								{
									serviceMax = list.Count;
									log.Debug(string.Format("serviceMax is < 0, so using list.Count instead, which is [{0}]", list.Count));
								}

								for (int j = 0; j < serviceMax; j++)
								{
									String currentReferencePartXML = list[j].OuterXml;
									List<Tuple<String, String>> replacementList = new List<Tuple<string, string>>();
									replacementList.Add(new Tuple<string, string>("^^^BEST_PHONE^^^", HRNXUtil.extractDataFromOrder(hrnxOrder.payload_hrnx, "/order/subject/field[@name='bestphone']")));

									Boolean currentStatus = clearStarAddOrderToProfile(currentReferencePartXML, endpoint, servicesToCheck[i], profileID, hrnxOrder, credentials, replacementList, dbAccessor, webAccessor, logContent);
									isSucess = isSucess && currentStatus;
								}
							}
							break;
						}
					default:
						{
							// by default, no error has occurred
							isSucess = true;
							break;
						}
				}
				if (!hasError)
					// report an error, but let the code continues
					hasError = !isSucess;
			}

			if (createNewOrder)
			{
				// 6. Transmit profile if a new order was created
				isSucess = clearStarTransmitProfile(profileID, hrnxOrder, credentials, dbAccessor, webAccessor, logContent);
				if (!isSucess)
					// report an error, but let the code continues
					hasError = true;
			}

			if (!String.IsNullOrEmpty(profileID) && !String.IsNullOrEmpty(credentials.customerID))
			{
				// Then we can log in vendorOrderIDLookup.
				// However, we can only do it if there are entries already exist (which can happen now that we have multiple phrases)
				// Hence for lookup scenario, we need to reset its status instead
				if(createNewOrder)
				{
					log.Debug("create entry in vendor_order_id because this is a new order");
					String vendor_order_id = profileID + "_" + credentials.customerID;
					dbAccessor.writeToVendorOrderID(hrnxOrder, LOCATION, vendor_order_id);
				}
				else
				{
					log.Debug("reset venodrOrderID lookup entry");
					dbAccessor.resetVendorOrderIDLookupEntries(hrnxOrder,LOCATION);
				}
			}

			if(!hasError)
				hrnxOrder.status = OrderStatus.processing;

            hrnxOrder.orderingIP = "localhost";

            // Log to qIncoming regardless of error or not
            if (dbAccessor.writeToQIncoming(hrnxOrder, LOCATION, constructReportXML(hrnxOrder), logContent.ToString()))
                // Update qProcessing status appropriately
				if(OrderStatus.processing.Equals(hrnxOrder.status))
	                dbAccessor.setQProcessStatus(hrnxOrder, LOCATION, OrderStatus.submitted_passthru);
				else
					dbAccessor.setQProcessStatus(hrnxOrder, LOCATION, OrderStatus.rejected_w_errors);
			else
				dbAccessor.setQProcessStatus(hrnxOrder, LOCATION, OrderStatus.rejected_w_errors);

			if (createNewOrder)
			{
				// 5. Try to send over AuthForm if it exist
				// Only do this once per txn				
				String AuthFormData = HRNXUtil.extractDataFromOrder(hrnxOrder.payload_hrnx, "order/eForm[@type='Authorization']");
				if (String.IsNullOrEmpty(AuthFormData))
					log.Debug("AuthFormData is null");
				else
				{
					//isSucess = dbAccessor.addEntryToPendingAction(hrnxOrder, LOCATION, PendingActionAction.uploadDocument, profileID, AuthFormData);
					//log.Debug(string.Format("Pending Action write is [{0}]", isSucess));

					//going to build a new logContent for AuthForm because they are so huge
					StringBuilder authFormLogContent = new StringBuilder();

					//going to temporarily change the status of the order, so we don't need to copy everything over
					OrderStatus savedStats = hrnxOrder.status;
					Boolean isSuccess = clearStarUploadDocument(profileID, AuthFormData, hrnxOrder, credentials, dbAccessor, webAccessor, authFormLogContent);
					log.Debug(string.Format("Upload Document logContent is [{0}]", authFormLogContent.ToString()));
					if (isSuccess)
						hrnxOrder.status = OrderStatus.submitted_passthru;

					// write to qIncoming
					dbAccessor.writeToQIncoming(hrnxOrder, LOCATION, constructReportXML(hrnxOrder), authFormLogContent.ToString());

					//after write, return the value of status
					hrnxOrder.status = savedStats;
				}
			}

            return hrnxOrder;
        }
		public Boolean serviceMaxClientOptionExist(HRNXDBAccessor dbAccessor,Order hrnxOrder, String clientOptionName, out int serviceMaxTemp)
		{
			String[] clientOptions = dbAccessor.getClientOptions(hrnxOrder, LOCATION);
			if (clientOptions != null)
			{
				String serviceMaxStr = HRNXUtil.extractClientFlagValue(clientOptions, clientOptionName);
				return Int32.TryParse(serviceMaxStr, out serviceMaxTemp);
			}
			else
			{
				serviceMaxTemp = -1;
				return false;
			}
		}
	}

    public class clearStarCredential
    {
        public String customerID { get; set; }
        public String username { get; set; }
        public String password { get; set; }
        public String businessUnitID { get; set; }
    }

	[XmlRootAttribute("Country")]
	public class clearStarCountry
	{
		public string ID { get; set; }
		public string FullName { get; set; }
		public string Alpha2Code { get; set; }
	}
}