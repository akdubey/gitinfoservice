﻿using getClientConfigurations.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Web;

namespace getClientConfigurations
{
    public abstract class HRNXException: Exception
    {
        protected static ResourceManager rm = new ResourceManager("getClientConfigurations.Properties.HRNX", typeof(HRNXException).Assembly);

        HRNXError innerError;

        public HRNXException(int ID, String detailMsg)
        {
            innerError = new HRNXError();
            innerError.message = detailMsg;
            innerError.code = ID;
        }
    }
}