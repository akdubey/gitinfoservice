﻿using getClientConfigurations.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace getClientConfigurations.Exceptions
{
    public class UnknownAcknowledgementException: HRNXException
    {
        public static String errorShort = rm.GetString("HRNX_FAILED_PROCESSING");
        public static String errorDetail = rm.GetString("UNKNOWN_ACKNOWLEDGEMENT");

        public UnknownAcknowledgementException() : base(2, errorShort) { }
    }
}