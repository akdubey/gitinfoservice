﻿using getClientConfigurations.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace getClientConfigurations.Exceptions
{
    public class CannotFindFailedSQLStorageConfigurationException : HRNXException
    {
        public CannotFindFailedSQLStorageConfigurationException(String detailMsg) : base(4, detailMsg) { }
    }
}