﻿using getClientConfigurations.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Resources;
using System.Text;
using System.Web;
using System.Web.Http.ExceptionHandling;

namespace getClientConfigurations.Exceptions.Logger
{
	public class HRNXErrorStackLogger : ExceptionLogger
	{
		private HRNXDBAccessor dbaccessor;
		private static readonly string DB_CONNECTION_NAME_DEFAULT = "HRNXDBConfiguration";
		private ResourceManager rm = new ResourceManager("getClientConfigurations.Properties.HRNX", typeof(HRNXErrorStackLogger).Assembly);

		public HRNXErrorStackLogger()
		{
			String connectionStr = ConfigurationManager.AppSettings[DB_CONNECTION_NAME_DEFAULT];
			dbaccessor = new HRNXDBAccessor(connectionStr);
		}

		public override void Log(ExceptionLoggerContext context)
		{
			StringBuilder detail = new StringBuilder();
			HRNXWebAccessor webAcessor = new HRNXWebAccessor();

			detail.Append(context.Request.ToString());
			detail.Append(Environment.NewLine);
			detail.Append(HRNXUtil.LOG_CONTENT_SEPARATOR);

			// Try to extract out the payload
			object value;
			HttpRequestMessage request = context.Request;
			if(request != null)
			{
				request.Properties.TryGetValue("MS_HttpContext", out value);
				HttpContextBase webContextBase = value as HttpContextBase;

				if(webContextBase != null)
				{
					String payload = webAcessor.extractRequestPayloadFromHttpRequest(webContextBase.ApplicationInstance.Context);
					if(!String.IsNullOrEmpty(payload))
					{
						detail.Append("payload");
						detail.Append(Environment.NewLine);
						detail.Append(HRNXUtil.LOG_CONTENT_SEPARATOR);
						detail.Append(payload);
					}
				}
			}

			dbaccessor.pushToErrorStack(4, "HRNX C# catch-all exceptions logger",true,null, rm.GetString("UNCAUGHT_EXCEPTION"),"",context.ExceptionContext.Exception.ToString(),detail.ToString());
		}
	}
}