﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.ExceptionHandling;

namespace getClientConfigurations.Exceptions.Logger
{
	public class HRNXLogExceptionLogger : ExceptionLogger
	{
		private static readonly log4net.ILog log = log4net.LogManager.GetLogger("MultiPhaseCreateOrderMainLogger");

		public override void Log(ExceptionLoggerContext context)
		{
			log.Fatal(context.Exception);
		}
	}
}