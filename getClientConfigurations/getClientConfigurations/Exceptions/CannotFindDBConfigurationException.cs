﻿using getClientConfigurations.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace getClientConfigurations.Exceptions
{
    public class CannotFindDBConfigurationException: HRNXException
    {
        public CannotFindDBConfigurationException(String detailMsg) : base(1, detailMsg) { }
    }
}