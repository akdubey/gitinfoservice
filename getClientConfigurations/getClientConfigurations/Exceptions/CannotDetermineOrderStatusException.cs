﻿using getClientConfigurations.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace getClientConfigurations.Exceptions
{
    public class CannotDetermineOrderStatusException : HRNXException
    {
        public static String errorShort = rm.GetString("CANNOT_DETERMINE_ORDER_STATUS");

        public CannotDetermineOrderStatusException() : base(3, errorShort) { }
    }
}