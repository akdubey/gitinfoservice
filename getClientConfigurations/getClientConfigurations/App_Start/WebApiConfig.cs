﻿using getClientConfigurations.Exceptions.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;

namespace getClientConfigurations
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API 
            config.MapHttpAttributeRoutes();

            // Remove the XML formatter, so we won't deal with it
            MediaTypeFormatter xmlformatter = config.Formatters.XmlFormatter;
            config.Formatters.Remove(xmlformatter);

            // Indent the JSON output, so we can easily read it
            config.Formatters.JsonFormatter.Indent = true;

			// Associate generic exception logger, so we will have multiple places to log
			// Log #1: Log4net main rolling log
			config.Services.Add(typeof(IExceptionLogger), new HRNXLogExceptionLogger());
			// Log #2: errorStack
			config.Services.Add(typeof(IExceptionLogger), new HRNXErrorStackLogger());
		}
	}
}
