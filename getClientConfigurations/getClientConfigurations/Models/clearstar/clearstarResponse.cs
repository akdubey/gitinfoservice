﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace getClientConfigurations.Models
{
    public class clearStarResponse
    {
        public string responseText { get; private set; }
		public string status { get; private set; }
		public string message { get; private set; }
		public XmlDocument xmlDoc { get; private set; }

		private clearStarResponse(String responseText)
		{
			this.responseText = responseText;
		}

		public static clearStarResponse loadClearStarResponse(String responseText)
		{
			clearStarResponse result = new clearStarResponse(responseText);

			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(responseText);
			result.xmlDoc = xmlDoc;

			// load the status
			String xpath = "//*[local-name()='ErrorStatus']/*[local-name()='Code']";
			XmlNode current = xmlDoc.SelectSingleNode(xpath);
			if (current != null)
				result.status = current.InnerText;

			// load the message
			xpath = "//*[local-name()='ErrorStatus']/*[local-name()='Message']";
			current = xmlDoc.SelectSingleNode(xpath);
			if (current != null)
				result.message = current.InnerText;

			return result;
		}
	}
}