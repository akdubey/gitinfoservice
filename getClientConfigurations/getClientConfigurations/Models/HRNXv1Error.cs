﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;

namespace getClientConfigurations.Models
{
    public class HRNXv1Error
    {
        public String error_id { get; set; }
        public String error_timedatestamp { get; set; }
        public int error_severity { get; set; }
        public String error_loc { get; set; }
        public Boolean create_alert { get; set; }
        public String client_group_id { get; set; }
        public String service_tp_id { get; set; }
        public String txn_id { get; set; }
        public String error_short { get; set; }
        public String error_long { get; set; }
        public String error_blob { get; set; }
        public String error_blob_2 { get; set; }

        public static HRNXv1Error deserialize(String[] queryResult)
        {
            HRNXv1Error returnError = new HRNXv1Error();
            returnError.error_id = queryResult[0];
            returnError.error_timedatestamp = queryResult[1];
            returnError.error_severity = Int32.Parse(queryResult[2]);
            returnError.error_loc = queryResult[3];
            returnError.create_alert = "1".Equals(queryResult[4]);
            returnError.client_group_id = queryResult[5];
            returnError.service_tp_id = queryResult[6];
            returnError.txn_id = queryResult[7];
            returnError.error_short = queryResult[8];
            returnError.error_long = queryResult[9];
            returnError.error_blob = queryResult[10];
            returnError.error_blob_2 = queryResult[11];

            return returnError;
        }        
    }
}