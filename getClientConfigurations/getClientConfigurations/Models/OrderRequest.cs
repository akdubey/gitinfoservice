﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace getClientConfigurations.Models
{
    public class OrderRequest
    {
        public String rawPayload { get; set; }
        public String requestURL { get; set; }
        public List<Tuple<String,String>> headers { get; set; }

        public override String ToString()
        {
            StringBuilder logContent = new StringBuilder();
            logContent.Append("=====URL====");
            logContent.Append(Environment.NewLine);
            logContent.Append(requestURL);
            logContent.Append(Environment.NewLine);

            if (!(headers == null))
            {
                logContent.Append("=====Headers====");

                for (int i = 0; i < headers.Count; i++)
                {
                    logContent.Append("Key = [" + headers[i].Item1 + "] Value = [" + headers[i].Item2 + "]");
                    logContent.Append(Environment.NewLine);
                }
            }
            logContent.Append("=====Payload====");
            logContent.Append(Environment.NewLine);
            logContent.Append(rawPayload);
            logContent.Append(Environment.NewLine);

            return logContent.ToString();
        }
    }
}