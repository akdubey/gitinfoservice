﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;

namespace getClientConfigurations.Models
{
    public class HRNXLogEntry
    {
        public String txn_id { get; set; }
        public String content { get; set; }

        public enum HRNXLogEntryStatus
        {
            INITIAL_REQUEST = 1,
            INFO = 2,
            ACKNOWLEDGEMENT = 3
        }
    }
}