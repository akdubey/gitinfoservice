﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.IO;
using System.Configuration;
using System.Text;

namespace getClientConfigurations.Models
{
    public class HRNXWebAccessor
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public String getFullHRNXURL(String hrnxEnvironmentBase, String hrnxInternalHttpPrefix, String partialURL)
        {
            String returnURL;
            if (!partialURL.StartsWith("http"))
            {
                // then, append 
                //String hrnxEnvironmentBase = cgmr.getAppSettingFromConfig("hrnxWebHome");
                //String hrnxInternalHttpPrefix = cgmr.getAppSettingFromConfig("hrnxInternalHttpPrefix");

                returnURL = hrnxInternalHttpPrefix + hrnxEnvironmentBase + partialURL;
                log.Debug(string.Format("Updating getServiceHandlerURL to be  = [{0}]", partialURL));
            }
            else
                returnURL = partialURL;
                log.Debug("getServiceHandlerURL is already full");

            return returnURL;
        }

        public String constructHRNXInternalPayloadUsingEndpointOptions(String endpointOptions, String finalAddressURL, String client_group_id)
        {
            StringBuilder payloadBuilder = new StringBuilder("");

            if (!String.Empty.Equals(endpointOptions))
            {
                if (endpointOptions.Contains("address_final="))
                {
                    payloadBuilder.Append("address_final=");
                    payloadBuilder.Append(HttpUtility.UrlEncode(finalAddressURL));
                }
                if (endpointOptions.Contains("hrnxClientGroupId="))
                {
                    //Append the & if this is the 2nd paramater
                    if (payloadBuilder.Length > 0)
                        payloadBuilder.Append("&");

                    payloadBuilder.Append("hrnxClientGroupId=");
                    payloadBuilder.Append(HttpUtility.UrlEncode(client_group_id));
                }
            }
            String payloadFinal = payloadBuilder.ToString();
            log.Debug(string.Format("Final payload is [{0}]", payloadFinal));

            return payloadFinal;
        }

        public HRNXWebResponse httpSend(String url, String httpMethod, String contentType, String payload, int timeout)
        {
            StringBuilder logContent = new StringBuilder();
            HRNXWebResponse result = httpSend(url, httpMethod, contentType, payload, null, timeout, logContent);
            log.Debug(string.Format("LogContent is [{0}]", logContent.ToString()));

            return result;
        }
        public HRNXWebResponse httpSend(String url, String httpMethod, String contentType, String payload, List<Tuple<string, string>> headers, int timeout, StringBuilder logContent)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

            //set settings
            request.Method = httpMethod;
            request.ContentType = contentType;
            request.ContentLength = payload.Length;
            request.Timeout = timeout;
            if (!(headers==null))
            {
                for(int i=0;i<headers.Count;i++)
                    request.Headers.Add(headers[i].Item1,headers[i].Item2);
            }

            // Convert the payload into a UTF-8 byte array
                byte[] buf = System.Text.Encoding.UTF8.GetBytes(payload);

			logContent.Append(HRNXUtil.LOG_CONTENT_SEPARATOR);
            logContent.Append("URL");
            logContent.Append(Environment.NewLine);
			logContent.Append(HRNXUtil.LOG_CONTENT_SEPARATOR);
			logContent.Append(url);
            logContent.Append(Environment.NewLine);

            if (!(headers == null))
            {
				logContent.Append(HRNXUtil.LOG_CONTENT_SEPARATOR);
                logContent.Append("Headers");
				logContent.Append(Environment.NewLine);
				logContent.Append(HRNXUtil.LOG_CONTENT_SEPARATOR);

				for (int i = 0; i < headers.Count; i++)
                {
                    logContent.Append("Key = [" + headers[i].Item1 + "] Value = [" + headers[i].Item2 + "]");
                    logContent.Append(Environment.NewLine);
                }
            }
			logContent.Append(HRNXUtil.LOG_CONTENT_SEPARATOR);
			logContent.Append("Payload");
            logContent.Append(Environment.NewLine);
			logContent.Append(HRNXUtil.LOG_CONTENT_SEPARATOR);
			logContent.Append(payload);
            logContent.Append(Environment.NewLine);

            // set the payload
            if (payload.Length > 0)
                request.GetRequestStream().Write(buf, 0, payload.Length);

            // send it out
            try
            { 
                HttpWebResponse rawResponse = (HttpWebResponse)request.GetResponse();
                HRNXWebResponse response = convertToHRNXWebResponse(rawResponse);

				logContent.Append(HRNXUtil.LOG_CONTENT_SEPARATOR);
				logContent.Append("Response [" + response.status + "]");
                logContent.Append(Environment.NewLine);
				logContent.Append(HRNXUtil.LOG_CONTENT_SEPARATOR);
				logContent.Append(response.responseText);
                logContent.Append(Environment.NewLine);

                return response;

            }
            catch (WebException e)
            {
                if(e.Status.Equals(WebExceptionStatus.Timeout))
                {
                    // Then, e.Response will be null, so I am just going to construct the whole response here
                    HRNXWebResponse response = new HRNXWebResponse();
                    response.status = HttpStatusCode.RequestTimeout;
                    response.responseText = "";

					logContent.Append(HRNXUtil.LOG_CONTENT_SEPARATOR);
					logContent.Append("Response [TIMEOUT]");
                    logContent.Append(Environment.NewLine);
					logContent.Append(HRNXUtil.LOG_CONTENT_SEPARATOR);

					return response;
                }
                else
                { 
	                HRNXWebResponse response = convertToHRNXWebResponse((HttpWebResponse)e.Response);

					logContent.Append(HRNXUtil.LOG_CONTENT_SEPARATOR);
					logContent.Append("Response [" + response.status + "]");
		            logContent.Append(Environment.NewLine);
					logContent.Append(HRNXUtil.LOG_CONTENT_SEPARATOR);
					logContent.Append(response.responseText);
			        logContent.Append(Environment.NewLine);

                return response;
                }
            }
        }

        public virtual String extractRequestPayloadFromHttpRequest(HttpContext currentContext)
        {
            if (currentContext == null)
                // If this isn't from a HTTP request, then we just return empty string
                return "";

            Stream HttpBodyStream = currentContext.Request.InputStream;
            StreamReader bodyStream = new StreamReader(HttpBodyStream);
            bodyStream.BaseStream.Seek(0, SeekOrigin.Begin);
            String payload = bodyStream.ReadToEnd();

            return payload;
        }

        public virtual String extractRequestURLFromHttpRequest(HttpContext curentContext)
        {
            return curentContext.Request.Url.ToString();
        }

        public virtual String extractRequesterIPFromHttpRequest(HttpContext curentContext)
        {
            StringBuilder sb = new StringBuilder(curentContext.Request.ServerVariables["REMOTE_ADDR"]);
            sb.Append(" (");
            sb.Append(curentContext.Request.ServerVariables["REMOTE_HOST"]);
            sb.Append(")");

            return sb.ToString();
        }

        public HRNXWebResponse convertToHRNXWebResponse(HttpWebResponse response)
        {
            HRNXWebResponse hrnxResponse = new HRNXWebResponse();
            hrnxResponse.status = response.StatusCode;

            string[] keys = response.Headers.AllKeys;
            for(int i=0;i<keys.Length;i++)
            {
                hrnxResponse.headers.Add(keys[i],response.GetResponseHeader(keys[i]));
            }

            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                hrnxResponse.responseText = reader.ReadToEnd();
            }

            return hrnxResponse;
        }
    }
}