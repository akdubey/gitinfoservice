﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace getClientConfigurations.Models
{
	[XmlRootAttribute("Product")]
	public class VendorOrderXML
	{
		public string VendorOrderId { get; set; }
		public OrderStatus VendorOrderStatus { get; set; }
		public string VendorOrderURL { get; set; }
	}
}