﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace getClientConfigurations.Models
{
	[XmlRootAttribute("report")]
	public class HRNXResult
	{
		public string txn_id { get; set; }
		public string client_user_id { get; set; }
		public string client_group_id { get; set; }
		public string tp_id { get; set; }
		public string tp_uqid { get; set; }
		public OrderStatus status { get; set; }

		public HRNXResultDetailCollection services { get; set; }
	}

	public class HRNXResultDetailCollection
	{
		public HRNXResultDetail service { get; set; }
	}

	[XmlRootAttribute("service")]
	public class HRNXResultDetail
	{
		[XmlAttribute]
		public string tp { get; set; }
		[XmlAttribute]
		public string name { get; set; }

		public string VendorOrderId { get; set; }
	}
}