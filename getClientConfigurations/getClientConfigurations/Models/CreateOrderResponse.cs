﻿using System;
using System.Net;

namespace getClientConfigurations.Models
{
    public class CreateOrderResponse
    {
        public HttpStatusCode status { get; set; }
        public String responseMsg { get; set; }
    }
}