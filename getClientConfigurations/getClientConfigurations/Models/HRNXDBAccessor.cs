﻿using getClientConfigurations.ServiceProviderHandlers;
using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Resources;

namespace getClientConfigurations.Models
{

    public class HRNXDBAccessor
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private ResourceManager rm = new ResourceManager("getClientConfigurations.Properties.HRNX", typeof(HRNXDBAccessor).Assembly);

        private String connectionStr { get; set; }
		private String failedSQLStoragePath { get; set; }

		public HRNXDBAccessor(String connectionStr)
		{
			this.connectionStr = connectionStr;
		}

		public HRNXDBAccessor(String connectionStr, String failedSQLStoragePath)
        {
            this.connectionStr = connectionStr;
			this.failedSQLStoragePath = failedSQLStoragePath;
		}

        /// <summary>
        /// Execute a query. However, only the 1st row will be returned
        /// </summary>
        /// <param name="command">The query to use</param>
        /// <returns>result of the query in the form of a String[]</returns>
        private string executeQuerySingleField(OdbcCommand command, Order request, String origin)
        {
            log.Debug(string.Format("running against connectionStr [{0}]", connectionStr));
			String sqlStr = HRNXUtil.convertODBCToSQL(command);
			log.Debug(string.Format("queryString [{0}]", sqlStr));

            using (OdbcConnection connection = new OdbcConnection(connectionStr))
            {
                // Open the DB connection
                connection.Open();

                command.Connection = connection;
                try
                {
                    object result = command.ExecuteScalar();
                    if (result == null || typeof(System.DBNull).Equals(result.GetType()))
                        return default(string);
                    else
                        return (String)result;
                }
                catch(OdbcException e)
                {
					processODBCException(request, origin, e, sqlStr);
					return "";
                }
            }
        }        

        /// <summary>
        /// Execute a query that could fetch multiple rows from DB
        /// </summary>
        /// <param name="command">The query to use</param>
        /// <param name="fieldsToReadPerRowSize">The number of string fields to read per row</param>
        /// <returns>result of the query in the form of a List of String[]</returns>
        private List<String[]> executeQueryMultiRowResult(OdbcCommand command, int fieldsToReadPerRowSize, Order request, String origin)
        {
            List<String[]> result = new List<String[]>();

            log.Debug(string.Format("running against connectionStr [{0}]", connectionStr));
			String sqlStr = HRNXUtil.convertODBCToSQL(command);
			log.Debug(string.Format("queryString [{0}]", sqlStr));

            using (OdbcConnection connection = new OdbcConnection(connectionStr))
            {
                // Open the DB connection
                connection.Open();
                command.Connection = connection;
                try
                { 
                    OdbcDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        string[] rowContent = new string[fieldsToReadPerRowSize];
                        for (int i = 0; i < fieldsToReadPerRowSize; i++)
                        {
                            try
                            {
                                rowContent[i] = reader.GetString(i);
                            }
                            catch (InvalidCastException)
                            {
                                // then we will just set this to be default(string)
                                rowContent[i] = default(String);
                            }
                        }
                        result.Add(rowContent);
                    }
                }
                catch (OdbcException e)
                {
					processODBCException(request, origin, e, sqlStr);
					return null;
                }
            }
            return result;
        }

        private Boolean executeUpdate(OdbcCommand command, String origin, Order request)
        {

			log.Debug(string.Format("running against connectionStr [{0}]", connectionStr));
			String sqlStr = HRNXUtil.convertODBCToSQL(command);
			log.Debug(string.Format("queryString [{0}]", sqlStr));

			using (OdbcConnection connection = new OdbcConnection(connectionStr))
            {
                // Open the DB connection
                connection.Open();
                command.Connection = connection;
				try
				{
                    int affectedRow = command.ExecuteNonQuery();
					log.Debug(string.Format("affectedRow is [{0}]", affectedRow));

					return affectedRow > 0;
                }
                catch (OdbcException e)
                {
					processODBCException(request,origin,e, sqlStr);
					return false;
                }
            }
        }
		private void processODBCException(Order request, String origin, OdbcException e, String sqlStr)
		{
			//if an exception happens, log to disk
			log.Fatal(e);

			//Also, log the SQL to disk
			log.Debug(string.Format("Calling writeSQLStatementToDisk [{0}]", failedSQLStoragePath));
			String failedSQLfilename = HRNXUtil.writeSQLStatementToDisk(sqlStr, failedSQLStoragePath);
			log.Debug(string.Format("failedSQLfilename returned is [{0}]", failedSQLfilename));

			// Finally, log to errorStack
			// Because there was a SQL exception that cause this.
			// There is a high chance we will not be able to re-write the full content back to errorStack.
			// However, we will try to log the front ot it, so we can hopefully get some more information about it
			// I will also make sure the version written is the version WITHOUT any printable chracter, which can easily cause issue with our DB
			int MAX_SQL_LENGTH_WITHOUT_PAYLOAD = 5000;
			int errorDetailLength = sqlStr.Length;
			if (errorDetailLength > MAX_SQL_LENGTH_WITHOUT_PAYLOAD)
				errorDetailLength = MAX_SQL_LENGTH_WITHOUT_PAYLOAD;

			String partOfFailedSQL = HRNXUtil.removeNonPrintableCharacterBesideFormatting(sqlStr).Substring(0, errorDetailLength);
			pushToErrorStack(5, origin, false, request, rm.GetString("SQL_FAILED_ERROR_SHORT"), string.Format(rm.GetString("SQL_FAILED_ERROR_LONG"), failedSQLfilename), e.Message, partOfFailedSQL);
		}
		public virtual String generateTxnID(Order request, String origin)
        {
            string query = "select uuid()";
            OdbcCommand command = new OdbcCommand(query);
            return executeQuerySingleField(command, request, origin);
        }
        public virtual Boolean isClientGroupIDValid(Order request, String origin)
        {
            String client_group_id = request.client_group_id;
            String query = "SELECT client_group_id from clients where client_group_id = ? and enabled = '1'";
            OdbcCommand command = new OdbcCommand(query);
            command.Parameters.AddWithValue("@client_group_id", client_group_id);

            String result = executeQuerySingleField(command, request, origin);

            return client_group_id.Equals(result);
        }
        public virtual String getATSCredentials(Order request, String origin, String service_tp_id, String credential_name)
        {
            String client_group_id = request.client_group_id;
            String client_user_id = request.client_user_id;

            String query = "SELECT credential_value from ats_credentials where (client_group_id is null or client_group_id = ?) and tp_id = ? and credential_name  = ? and (client_user_id is null or client_user_id = ?) and enable = '1' order by client_group_id is not null desc, client_user_id is not null desc limit 1";
            OdbcCommand command = new OdbcCommand(query);
            command.Parameters.AddWithValue("@client_group_id", client_group_id);
            command.Parameters.AddWithValue("@service_tp_id", service_tp_id);
            command.Parameters.AddWithValue("@credential_name", credential_name);
            command.Parameters.AddWithValue("@client_user_id", HRNXUtil.sendNullIfEmpty(client_user_id));

            String result = executeQuerySingleField(command, request, origin);

            return result;
        }
        public virtual String getLoadWidgetXSLT(Order request, String origin, String service_tp_id)
        {
            String client_group_id = request.client_group_id;
            String query = "select xslt from settings_endpoints where `partner_id` = ? and enabled = '1' and endpoint_category = 'loadWidget' and (client_group_id is null or client_group_id = ?) order by client_group_id is not null desc limit 1";
            OdbcCommand command = new OdbcCommand(query);
            command.Parameters.AddWithValue("@service_tp_id", service_tp_id);
            command.Parameters.AddWithValue("@client_group_id", client_group_id);

            String result = executeQuerySingleField(command, request, origin);

            return result;
        }
        public virtual Boolean writeToQProcessing(Order requestToLog, String origin)
        {
            String insertStatement = "insert into q_processing (" + Order.getFieldStrForm() + ") values (?,?,?,?,?,?,?,?,?,?)";
            OdbcCommand command = new OdbcCommand(insertStatement);
            command.Parameters.AddWithValue("@txn_id", HRNXUtil.sendNullIfEmpty(requestToLog.txn_id));
            command.Parameters.AddWithValue("@req_timedatestamp", HRNXUtil.getCurrentTimeInUTC());
            command.Parameters.AddWithValue("@hrnx_status", HRNXUtil.sendNullIfEmpty(requestToLog.status.ToString()));
            command.Parameters.AddWithValue("@client_group_id", HRNXUtil.sendNullIfEmpty(requestToLog.client_group_id));
            command.Parameters.AddWithValue("@user_id", HRNXUtil.sendNullIfEmpty(requestToLog.client_user_id));
            command.Parameters.AddWithValue("@tp_uqid", HRNXUtil.sendNullIfEmpty(requestToLog.candidate_id));
            command.Parameters.AddWithValue("@source", HRNXUtil.sendNullIfEmpty(requestToLog.createOrderHandlerName));
            command.Parameters.AddWithValue("@ip_address", HRNXUtil.sendNullIfEmpty(requestToLog.orderingIP));
            command.Parameters.AddWithValue("@version", "2");
            command.Parameters.AddWithValue("@payload_hrnx", HRNXUtil.sendNullIfEmpty(HRNXUtil.removeNonPrintableCharacterBesideFormatting(requestToLog.payload_hrnx)));

            return executeUpdate(command, origin, requestToLog);
        }
        private Boolean writeToHRNXLogTable(String scriptLocation, Order request, String message, HRNXLogEntry.HRNXLogEntryStatus status)
        {
            String updateStatement = "INSERT INTO `hrnx`.`hrnx_logs` (`txn_id`, `timedatestamp`, `content`, `type`) VALUES (?, now(), ? , ?)";
            OdbcCommand command = new OdbcCommand(updateStatement);
            command.Parameters.AddWithValue("@txn_id", request.txn_id);
            command.Parameters.AddWithValue("@content", message);
            command.Parameters.AddWithValue("@status", (int)status);

            return executeUpdate(command, scriptLocation, request);
        }
        public virtual Boolean writeInitialRequestToHRNXLogTable(Order requestToLog, String origin, String message)
        {
            return writeToHRNXLogTable(origin, requestToLog, message, HRNXLogEntry.HRNXLogEntryStatus.INITIAL_REQUEST);
        }
        public virtual Boolean writeInfoToHRNXLogTable(Order requestToLog, String origin, String message)
        {
            return writeToHRNXLogTable(origin, requestToLog, message, HRNXLogEntry.HRNXLogEntryStatus.INFO);
        }
        public virtual Boolean writeAcknowledgementToHRNXLogTable(Order requestToLog, String origin, String message)
        {
            return writeToHRNXLogTable(origin, requestToLog, message, HRNXLogEntry.HRNXLogEntryStatus.ACKNOWLEDGEMENT);
        }
        /// <summary>
        /// Get the list of service_tp_id that this client is associated with
        /// </summary>
        /// <param name="client_group_id">The client</param>
        /// <returns>A list of service_tp_id, separated by comma, associated with this client</returns>
        public virtual string[] isPassThruClient(Order request, String origin)
        {
            String client_group_id = request.client_group_id;

            string query = "select group_concat(distinct partner_id) from settings_endpoints where partner_id in (select a.partner_id from partner_service_mappings a where a.client_group_id = ? and (a.enabled is null or a.enabled = 1)) and endpoint_category = 'passThru' and client_group_id = ?";
            OdbcCommand command = new OdbcCommand(query);
            command.Parameters.AddWithValue("@client_group_id", client_group_id);
            command.Parameters.AddWithValue("@client_group_id", client_group_id);

            string result = executeQuerySingleField(command, request, origin);

            if (String.IsNullOrEmpty(result))
            {
                return new String[0];
            }
            else
                return result.Split(',');
        }
        public virtual getFieldEndpoint getTemplateEndpoint(String service_tp_id, String endpointName, Order request, String origin, List<Tuple<String, String>> addressReplacement)
        {
            String client_group_id = request.client_group_id;

            string query = "select address, xslt from settings_endpoints where partner_id = ? and endpoint_category = \"getField\" and name = ? and (client_group_id = ? or client_group_id is null) and enabled=1 order by client_group_id is not null desc LIMIT 1";
            OdbcCommand command = new OdbcCommand(query);
            command.Parameters.AddWithValue("@service_tp_id", service_tp_id);
            command.Parameters.AddWithValue("@endpoint_name", endpointName);
            command.Parameters.AddWithValue("@client_group_id", client_group_id);

            List<String[]> result = executeQueryMultiRowResult(command, 2, request,origin);

            getFieldEndpoint entry = new getFieldEndpoint();
            if (result.Count > 0)
            {
                String[] firstRow = result[0];
                String finalURL = firstRow[0];

                //do string replacement if needed
                if (addressReplacement != null)
                {
                    for (int i = 0; i < addressReplacement.Count; i++)
                        finalURL = finalURL.Replace(addressReplacement[i].Item1, addressReplacement[i].Item2);
                }

                entry.url = finalURL;
                entry.xslt = firstRow[1];
            }

            return entry;
        }
        public virtual HRNXPhase getPhraseData(Order request, String origin, String phrase, String service_tp_id)
        {
            String client_group_id = request.client_group_id;

            string query = "select PhraseID, PhraseData from lookup_phrase where client_group_id = ? and service_tp_id = ? and Phrase = ? and enable = 1";
            OdbcCommand command = new OdbcCommand(query);
            command.Parameters.AddWithValue("@client_group_id", client_group_id);
            command.Parameters.AddWithValue("@service_tp_id", service_tp_id);
            command.Parameters.AddWithValue("@phrase", phrase);

            List<String[]> queryResults = executeQueryMultiRowResult(command, 2, request, origin);

            if(queryResults.Count > 0)
            {
                HRNXPhase result = new HRNXPhase();
                result.enumValue = queryResults[0][0];
                result.data = queryResults[0][1];

                return result;
            }
            return null;
        }
        public virtual Boolean pushToErrorStack(int severity, String scriptLocation, Boolean createAlert, Order request, String error_short, String error_long, String error_blob, String error_blob2)
        {
            String createAlertStr;
            if (createAlert)
                createAlertStr = "1";
            else
                createAlertStr = "0";

            String client_group_id;
            String service_tp_id;
            String txn_id;
            if (request == null)
            {
                client_group_id = default(string);
                txn_id = default(string);
                service_tp_id = default(string);
            }
            else
            {
                client_group_id = request.client_group_id;
                txn_id = request.txn_id;
                service_tp_id = request.getCurrentServiceProviderID();
            }

            String updateStatement = "insert into hrnx_error_stack (error_timedatestamp, error_severity, error_loc, create_alert, client_group_id, service_tp_id, txn_id, error_short, error_long, error_blob, error_blob_2) values (?,?,?,?,?,?,?,?,?,?,?)";
            OdbcCommand command = new OdbcCommand(updateStatement);
            command.Parameters.AddWithValue("@error_timedatestamp", HRNXUtil.getCurrentTimeInUTC());
            command.Parameters.AddWithValue("@error_severity", severity);
            command.Parameters.AddWithValue("@error_loc", HRNXUtil.sendNullIfEmpty(scriptLocation));
            command.Parameters.AddWithValue("@create_alert", createAlertStr);
            command.Parameters.AddWithValue("@client_group_id", HRNXUtil.sendNullIfEmpty(client_group_id));
            command.Parameters.AddWithValue("@service_tp_id", HRNXUtil.sendNullIfEmpty(service_tp_id));
            command.Parameters.AddWithValue("@txn_id", HRNXUtil.sendNullIfEmpty(txn_id));
            command.Parameters.AddWithValue("@error_short", HRNXUtil.sendNullIfEmpty(error_short));
            command.Parameters.AddWithValue("@error_long", HRNXUtil.sendNullIfEmpty(error_long));
            command.Parameters.AddWithValue("@error_blob", HRNXUtil.sendNullIfEmpty(error_blob));
            command.Parameters.AddWithValue("@error_blob2", HRNXUtil.sendNullIfEmpty(error_blob2));

            return executeUpdate(command, scriptLocation, request);
        }
        public virtual List<HRNXv1Error> getErrorFromErrorStackSince(String errorID, Order request, String origin)
        {
            List<HRNXv1Error> result = new List<HRNXv1Error>();

            String query = "SELECT error_id , error_timedatestamp , error_severity , error_loc , create_alert , client_group_id , service_tp_id , txn_id , error_short , error_long , error_blob , error_blob_2 FROM hrnx.hrnx_error_stack where error_id > ?";
            OdbcCommand command = new OdbcCommand(query);
            command.Parameters.AddWithValue("@error_id", errorID);

            List<String[]> queryResults = executeQueryMultiRowResult(command, 12, request, origin);

            for (int i = 0; i < queryResults.Count; i++)
            {
                result.Add(HRNXv1Error.deserialize(queryResults[i]));
            }

            return result;
        }

        /// <summary>
        /// Write the duplicateEntry to our table
        /// </summary>
        /// <param name="request">Order that trigger this</param>
        /// <param name="scriptLocation">caller of this function</param>
        /// <param name="dupliateKey">duplicate key used to uniquely identify this entry</param>
        /// <param name="hrnx_vendorXML">The vendorXML of hrnx processing</param>
        /// <returns></returns>
        public virtual Boolean writeToATSDuplicate(Order request,String scriptLocation, String dupliateKey, String hrnx_vendorXML)
        {
            String updateStatement = "INSERT INTO `hrnx`.`ats_duplicate` (`req_datetimestamp`, `txn_id`, `tp_id`, `duplicate_key`, `hrnx_vendorXML`) VALUES (now(),?,?,?,?)";
            OdbcCommand command = new OdbcCommand(updateStatement);
            command.Parameters.AddWithValue("@txn_id", HRNXUtil.sendNullIfEmpty(request.txn_id));
            command.Parameters.AddWithValue("@service_tp_id", HRNXUtil.sendNullIfEmpty(request.getCurrentServiceProviderID()));
            command.Parameters.AddWithValue("@dupliateKey", HRNXUtil.sendNullIfEmpty(dupliateKey));
            command.Parameters.AddWithValue("@hrnx_vendorXML", HRNXUtil.sendNullIfEmpty(hrnx_vendorXML));

            return executeUpdate(command, scriptLocation, request);
        }

        public virtual HRNXATSDuplicateEntry getATSDuplicateEntryData(Order request, String duplicateKey, String origin)
        {
            String query = "SELECT txn_id, hrnx_vendorXML FROM ats_duplicate where duplicate_key = ?";
            OdbcCommand command = new OdbcCommand(query);
            command.Parameters.AddWithValue("@duplicateKey", duplicateKey);

            List<String[]> queryResults = executeQueryMultiRowResult(command, 2, request, origin);

            if(queryResults.Count > 0)
            {
                HRNXATSDuplicateEntry result = new HRNXATSDuplicateEntry();
                result.original_txn_id = queryResults[0][0];
                result.hrnx_processed_payload = queryResults[0][1];

                return result;
            }

            return null;
        }

        public virtual Order getOrderFromDB(String txn_id, Order request, String origin)
        {
            Order result = new Order();
            result.txn_id = txn_id;

            String query = "SELECT " + Order.getFieldStrForm() + " FROM hrnx.q_processing where txn_id = ?";
            OdbcCommand command = new OdbcCommand(query);
            command.Parameters.AddWithValue("@txn_id",txn_id);

            List<String[]> queryResults = executeQueryMultiRowResult(command, 10, request, origin);

            return Order.deserialize(queryResults[0]);
        }
        public virtual String getLatestErrorID(Order request, String origin)
        {
            String query = "SELECT error_id FROM hrnx.hrnx_error_stack order by error_id desc limit 1";
            OdbcCommand command = new OdbcCommand(query);

            String errorID = executeQueryMultiRowResult(command, 1, request, origin)[0][0];

            return errorID;
        }
        public virtual Order getLatestOrder(Order request, String origin)
        {
            String query = "SELECT " + Order.getFieldStrForm() + " FROM hrnx.q_processing order by req_timedatestamp desc limit 1";
            OdbcCommand command = new OdbcCommand(query);

            List<String[]> queryResults = executeQueryMultiRowResult(command, 10, request, origin);

            return Order.deserialize(queryResults[0]);
        }

        public virtual String getLatestOrderTxnID(Order request, String origin)
        {
            return getLatestOrder(request, origin).txn_id;
        }

        public virtual clearStarCredential getClearStarCredential(Order request, String origin)
        {
            String service_tp_id = request.getCurrentServiceProviderID();
            clearStarCredential result = null;

            String query = "select settings_1 from partner_service_mappings where client_group_id = ? and partner_id = ? and (enabled is null or enabled = 1)";
            OdbcCommand command = new OdbcCommand(query);
            command.Parameters.AddWithValue("@client_group_id", request.client_group_id);
            command.Parameters.AddWithValue("@service_tp_id", service_tp_id);

            String settings_1 = executeQuerySingleField(command, request, origin);

            if(!String.IsNullOrEmpty(settings_1))
            {
                String[] settings = settings_1.Split(',');

                if(settings.Length == 3)
                {
                    result = new clearStarCredential();
                    result.customerID = settings[0];
                    result.username = settings[1];
                    result.password = settings[2];
                    result.businessUnitID = clearstar.extractBusinesUnitIDFromServiceProviderID(service_tp_id);
                }
            }

            return result;
        }

        public virtual Boolean writeToQIncoming(Order requestToLog, String origin, String payload_hrnx, String payload_original)
        {
            String insertStatement = "insert into q_incoming ("+ HRNXQIncomingEntry.getFieldStrForm() + ") values (?,?,?,?,?,?,?,?)";
            OdbcCommand command = new OdbcCommand(insertStatement);
            command.Parameters.AddWithValue("@txn_id", HRNXUtil.sendNullIfEmpty(requestToLog.txn_id));
            command.Parameters.AddWithValue("@service_tp_id", HRNXUtil.sendNullIfEmpty(requestToLog.getCurrentServiceProviderID()));
            command.Parameters.AddWithValue("@req_timedatestamp", HRNXUtil.getCurrentTimeInUTC());
            command.Parameters.AddWithValue("@hrnx_status", HRNXUtil.sendNullIfEmpty(requestToLog.status.ToString()));
            command.Parameters.AddWithValue("@version", "2");
            command.Parameters.AddWithValue("@ip_address", HRNXUtil.sendNullIfEmpty(requestToLog.orderingIP));
            command.Parameters.AddWithValue("@payload_original", HRNXUtil.sendNullIfEmpty(HRNXUtil.removeNonPrintableCharacterBesideFormatting(payload_original)));
            command.Parameters.AddWithValue("@payload_hrnx", HRNXUtil.sendNullIfEmpty(HRNXUtil.removeNonPrintableCharacterBesideFormatting(payload_hrnx)));

            return executeUpdate(command, origin, requestToLog);
        }

        public virtual List<HRNXQIncomingEntry> getQIncomingEntriesFromDB(Order request, String origin)
        {
            String query = "SELECT " + HRNXQIncomingEntry.getFieldStrForm() + " FROM hrnx.q_incoming where txn_id = ? order by receipt_timedatestamp";
            OdbcCommand command = new OdbcCommand(query);
            command.Parameters.AddWithValue("@txn_id", request.txn_id);

            List<String[]> queryResults = executeQueryMultiRowResult(command, 8, request, origin);
            List<HRNXQIncomingEntry> results = new List<HRNXQIncomingEntry>();

            for(int i=0;i<queryResults.Count;i++)
            {
                results.Add(HRNXQIncomingEntry.deserialize(queryResults[i]));
            }
            return results;
        }

        public virtual Boolean setQProcessStatus(Order request, String origin, OrderStatus newStatus)
        {
            String insertStatement = "UPDATE `hrnx`.`q_processing` SET `hrnx_status`= ? WHERE `txn_id`=?";
            OdbcCommand command = new OdbcCommand(insertStatement);
            command.Parameters.AddWithValue("@hrnx_status", HRNXUtil.sendNullIfEmpty(newStatus.ToString()));
            command.Parameters.AddWithValue("@txn_id", HRNXUtil.sendNullIfEmpty(request.txn_id));

            return executeUpdate(command, origin, request);
        }

		public virtual Boolean writeToVendorOrderID(Order requestToLog, String origin, String vendor_order_id)
		{
			String insertStatement = "INSERT INTO vendor_order_id_lookup (" + VendorOrderIDLookupEntry.getFieldStrForm() + ") values (?,?,?,?,?)";
			OdbcCommand command = new OdbcCommand(insertStatement);
			command.Parameters.AddWithValue("@service_tp_id", HRNXUtil.sendNullIfEmpty(requestToLog.getCurrentServiceProviderID()));
			command.Parameters.AddWithValue("@vendor_order_id", HRNXUtil.sendNullIfEmpty(vendor_order_id));
			command.Parameters.AddWithValue("@txn_id", HRNXUtil.sendNullIfEmpty(requestToLog.txn_id));
			command.Parameters.AddWithValue("@req_timedatestamp", HRNXUtil.getCurrentTimeInUTC());
			command.Parameters.AddWithValue("@status", "processing");

			return executeUpdate(command, origin, requestToLog);
		}

		public virtual List<VendorOrderIDLookupEntry> getVendorOrderIDLookupEntriesFromDB(Order request, String origin, String vendor_order_id)
		{
			String query = "SELECT " + VendorOrderIDLookupEntry.getFieldStrForm() + " FROM hrnx.vendor_order_id_lookup where service_tp_id = ? and vendor_order_id = ? and status='processing'";
			OdbcCommand command = new OdbcCommand(query);
			command.Parameters.AddWithValue("@service_tp_id", HRNXUtil.sendNullIfEmpty(request.getCurrentServiceProviderID()));
			command.Parameters.AddWithValue("@vendor_order_id", HRNXUtil.sendNullIfEmpty(vendor_order_id));

			List<String[]> queryResults = executeQueryMultiRowResult(command, 5, request, origin);
			List<VendorOrderIDLookupEntry> results = new List<VendorOrderIDLookupEntry>();

			for (int i = 0; i < queryResults.Count; i++)
			{
				results.Add(VendorOrderIDLookupEntry.deserialize(queryResults[i]));
			}
			return results;
		}
		public virtual List<VendorOrderIDLookupEntry> getVendorOrderIDLookupEntriesFromDBRegardlessOfStatus(Order request, String origin, String vendor_order_id)
		{
			String query = "SELECT " + VendorOrderIDLookupEntry.getFieldStrForm() + " FROM hrnx.vendor_order_id_lookup where service_tp_id = ? and vendor_order_id = ?";
			OdbcCommand command = new OdbcCommand(query);
			command.Parameters.AddWithValue("@service_tp_id", HRNXUtil.sendNullIfEmpty(request.getCurrentServiceProviderID()));
			command.Parameters.AddWithValue("@vendor_order_id", HRNXUtil.sendNullIfEmpty(vendor_order_id));

			List<String[]> queryResults = executeQueryMultiRowResult(command, 5, request, origin);
			List<VendorOrderIDLookupEntry> results = new List<VendorOrderIDLookupEntry>();

			for (int i = 0; i < queryResults.Count; i++)
			{
				results.Add(VendorOrderIDLookupEntry.deserialize(queryResults[i]));
			}
			return results;
		}
		public virtual List<VendorOrderIDLookupEntry> getVendorOrderIDLookupEntriesFromDB(Order request, String origin)
		{
			String query = "SELECT " + VendorOrderIDLookupEntry.getFieldStrForm() + " FROM hrnx.vendor_order_id_lookup where txn_id = ? and status='processing'";
			OdbcCommand command = new OdbcCommand(query);
			command.Parameters.AddWithValue("@txn_id", HRNXUtil.sendNullIfEmpty(request.txn_id));

			List<String[]> queryResults = executeQueryMultiRowResult(command, 5, request, origin);
			List<VendorOrderIDLookupEntry> results = new List<VendorOrderIDLookupEntry>();

			for (int i = 0; i < queryResults.Count; i++)
			{
				results.Add(VendorOrderIDLookupEntry.deserialize(queryResults[i]));
			}
			return results;
		}
		public virtual VendorOrderIDLookupEntry getVendorOrderIDLookupEntriesFromDBUsingTxnID(Order request, String origin)
		{
			String query = "SELECT " + VendorOrderIDLookupEntry.getFieldStrForm() + " FROM hrnx.vendor_order_id_lookup where txn_id = ? limit 1";
			OdbcCommand command = new OdbcCommand(query);
			command.Parameters.AddWithValue("@txn_id", HRNXUtil.sendNullIfEmpty(request.txn_id));

			List<String[]> queryResults = executeQueryMultiRowResult(command, 5, request, origin);
			VendorOrderIDLookupEntry result = null;

			if(queryResults.Count > 0)
			{
				result = VendorOrderIDLookupEntry.deserialize(queryResults[0]);
			}
			return result;
		}
		public virtual Boolean resetVendorOrderIDLookupEntries(Order request, String origin)
		{
			String insertStatement = "UPDATE `hrnx`.`vendor_order_id_lookup` SET `status`='processing' WHERE `txn_id`= ?";
			OdbcCommand command = new OdbcCommand(insertStatement);
			command.Parameters.AddWithValue("@txn_id", HRNXUtil.sendNullIfEmpty(request.txn_id));

			return executeUpdate(command, origin, request);
		}
		public virtual Boolean markVendorOrderIDasCompleted(Order request, String origin)
		{
			String insertStatement = "UPDATE `hrnx`.`vendor_order_id_lookup` SET `status`='sent' WHERE `txn_id`= ?";
			OdbcCommand command = new OdbcCommand(insertStatement);
			command.Parameters.AddWithValue("@txn_id", HRNXUtil.sendNullIfEmpty(request.txn_id));

			return executeUpdate(command, origin, request);
		}
		public virtual String convertToHRNXService(Order request, String origin, String vendorServiceID)
		{
			string query = "SELECT hrnx_service_type FROM `hrnx`.`hrnx_service_type_mapping` where client_group_id = ? and service_tp_id = ? and service_id = ?";
			OdbcCommand command = new OdbcCommand(query);
			command.Parameters.AddWithValue("@client_group_id", HRNXUtil.sendNullIfEmpty(request.client_group_id));
			command.Parameters.AddWithValue("@service_tp_id", HRNXUtil.sendNullIfEmpty(request.additionalData[OrderAdditionalDataKeys.serviceProviderID]));
			command.Parameters.AddWithValue("@service_id", HRNXUtil.sendNullIfEmpty(vendorServiceID));
			return executeQuerySingleField(command, request, origin);
		}
		public virtual List<String> getServiceIDAssociateWithPackage(Order request, String origin, String packageID)
		{
			String query = "SELECT service_id FROM hrnx.packages where vendor_pkg_id = ? and  client_group_id = ?";
			OdbcCommand command = new OdbcCommand(query);
			command.Parameters.AddWithValue("@vendor_pkg_id", HRNXUtil.sendNullIfEmpty(packageID));
			command.Parameters.AddWithValue("@client_group_id", HRNXUtil.sendNullIfEmpty(request.client_group_id));

			List<String[]> queryResults = executeQueryMultiRowResult(command, 1, request, origin);

			List<String> result = new List<string>();
			for(int i=0;i<queryResults.Count;i++)
				result.Add(queryResults[i][0]);

			return result;
		}
		public virtual Boolean addEntryToPendingAction(Order requestToLog, String origin, PendingActionAction pending_action, String reference_id, String reference_data)
		{
			String insertStatement = "INSERT INTO hrnx_pending_actions (" + PendingActionEntry.getFieldStrForm() + ") values (?,?,?,?,?,?,?)";
			OdbcCommand command = new OdbcCommand(insertStatement);
			command.Parameters.AddWithValue("@txn_id", HRNXUtil.sendNullIfEmpty(requestToLog.txn_id));
			command.Parameters.AddWithValue("@pending_action", pending_action.ToString());
			command.Parameters.AddWithValue("@client_group_id", HRNXUtil.sendNullIfEmpty(requestToLog.client_group_id));
			command.Parameters.AddWithValue("@service_tp_id", HRNXUtil.sendNullIfEmpty(requestToLog.getCurrentServiceProviderID()));
			command.Parameters.AddWithValue("@status", PendingActionStatus.pending.ToString());
			command.Parameters.AddWithValue("@reference_id", HRNXUtil.sendNullIfEmpty(reference_id));
			command.Parameters.AddWithValue("@reference_data", HRNXUtil.sendNullIfEmpty(reference_data));

			return executeUpdate(command, origin, requestToLog);
		}
		public virtual PendingActionEntry getPendingActionEntryFromDBUsingTxnID(Order request, String origin)
		{
			String query = "SELECT " + PendingActionEntry.getFieldStrForm() + " FROM hrnx.hrnx_pending_actions where txn_id = ? limit 1";
			OdbcCommand command = new OdbcCommand(query);
			command.Parameters.AddWithValue("@txn_id", HRNXUtil.sendNullIfEmpty(request.txn_id));

			List<String[]> queryResults = executeQueryMultiRowResult(command, 7, request, origin);
			PendingActionEntry result = null;

			if (queryResults.Count > 0)
			{
				result = PendingActionEntry.deserialize(queryResults[0]);
			}
			return result;
		}
		public virtual String getPackageName(Order request, String origin, String serviceID)
		{
			string query = "SELECT pkg_name FROM `hrnx`.`packages` where client_group_id = ? and service_tp_id = ? and pkg_id = ?";
			OdbcCommand command = new OdbcCommand(query);
			command.Parameters.AddWithValue("@client_group_id", HRNXUtil.sendNullIfEmpty(request.client_group_id));
			command.Parameters.AddWithValue("@service_tp_id", HRNXUtil.sendNullIfEmpty(request.additionalData[OrderAdditionalDataKeys.serviceProviderID]));
			command.Parameters.AddWithValue("@service_id", HRNXUtil.sendNullIfEmpty(serviceID));
			return executeQuerySingleField(command, request, origin);
		}
		public virtual String getServiceName(Order request, String origin, String serviceID)
		{
			string query = "SELECT service_name FROM `hrnx`.`services` where (client_group_id = ? or client_group_id is null) and service_tp_id = ? and service_id = ?";
			OdbcCommand command = new OdbcCommand(query);
			command.Parameters.AddWithValue("@client_group_id", HRNXUtil.sendNullIfEmpty(request.client_group_id));
			command.Parameters.AddWithValue("@service_tp_id", HRNXUtil.sendNullIfEmpty(request.additionalData[OrderAdditionalDataKeys.serviceProviderID]));
			command.Parameters.AddWithValue("@service_id", HRNXUtil.sendNullIfEmpty(serviceID));
			return executeQuerySingleField(command, request, origin);
		}

		/// <summary>
		/// Returns the client option associated with the order's client
		/// </summary>
		/// <param name="request">The order</param>
		/// <returns>A list of client options pulled from DB or null</returns>
		public virtual String[] getClientOptions(Order request, String origin)
        {

			string query = "select client_options FROM clients where client_group_id = ?";
			OdbcCommand command = new OdbcCommand(query);
			command.Parameters.AddWithValue("@client_group_id", HRNXUtil.sendNullIfEmpty(request.client_group_id));
			String clientOptionsStr = executeQuerySingleField(command, request, origin);

			if (clientOptionsStr != null)
			{
				String[] result = clientOptionsStr.Split(',');
				return result;
			}
			else
				return null;
		}
	}
}