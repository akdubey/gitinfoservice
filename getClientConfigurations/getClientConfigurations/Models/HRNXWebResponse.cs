﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;

namespace getClientConfigurations.Models
{
    public class HRNXWebResponse
    {
        public HttpStatusCode status { get; set; }
        public String responseText { get; set; }
        public Dictionary<String, String> headers { get; set; }

        public HRNXWebResponse()
        {
            headers = new Dictionary<string, string>();
        }
    }
}