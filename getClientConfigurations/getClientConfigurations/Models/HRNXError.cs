﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace getClientConfigurations.Models
{
    public class HRNXError
    {
        public int code { get; set; }
        public string message { get; set; }
    }
}