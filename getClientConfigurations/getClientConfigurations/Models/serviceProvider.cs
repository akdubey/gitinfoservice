﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace getClientConfigurations.Models
{
    public class serviceProvider
    {
        public string vendorid { get; set; }
        public string vendorname { get; set; }
        public string vendortype { get; set; }
        public List<Package> packages { get; set; }
        public List<Service> individualservices { get; set; }
    }
}