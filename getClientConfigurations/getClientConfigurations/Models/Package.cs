﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace getClientConfigurations.Models
{
    public class Package
    {
        public string packageid { get; set; }
        public string packagename { get; set; }
        public List<Service> packageservices { get; set; }
    }
}