﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace getClientConfigurations.Models
{
    public class icimsJobProfile
    {
        public List<icimsPackageEntry> bgcpackagetype { get; set; }
    }
    public class icimsPackageEntry
    {
        public string id { get; set; }
        public string formattedvalue { get; set; }
        public string value { get; set; }
    }
}