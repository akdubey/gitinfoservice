﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace getClientConfigurations.Models
{
    public class icimsErrorList
    {
        public List<icimsError> errors { get; set; }
	}
	public class icimsError
	{
		public String errorCode { get; set; }
		public String errorMessage { get; set; }
	}

}