﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace getClientConfigurations.Models
{
    public class icimsBGResult
    {
        public DateTime bgccompleteddate { get; set; }
		public icimsOrderStatus bgcorderstatus { get; set; }
	}

	public class icimsOrderStatus
	{
		public string id { get; set; }
		public string value { get; set; }
	}

	public class icimsCompleteOrderStatus: icimsOrderStatus
	{
		public icimsCompleteOrderStatus()
		{
			this.id = "D37002026001";
			this.value = "Complete";
		}
	}
}