﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace getClientConfigurations.Models
{
    public class icimsSubjectProfile
    {
		private static readonly log4net.ILog log = log4net.LogManager.GetLogger("icimsSubjectProfile");

		public string firstname { get; set; }
		public string lastname { get; set; }
		public string middlename { get; set; }
        public string email { get; set; }
		public string usssn { get; set; }
		public string gender { get; set; }
		public string birthdate { get; set; }
		public List<icimsAddress> addresses { get; set; }
		public List<icimsPhone> phones { get; set; }
        public List<Education> education { get; set; }
        public List<Workexperience> workexperience { get; set; }
		public string drivinglicensenumber { get; set; }
		public icimsState drivinglicensestate { get; set; }
		public List<icimsLicense> licensecertification { get; set; }
		public List<icimsReference> references { get; set; }

		public String convertToHRNXSubjectProifle()
        {
            StringBuilder resultXML = new StringBuilder("<subject>");
            resultXML.Append(Environment.NewLine);

            addValue("firstname", firstname, resultXML);
            addValue("middlename", middlename, resultXML);
			addValue("lastname", lastname, resultXML);
			addValue("email", email, resultXML);
			addValue("ssn", usssn, resultXML);

			genderProcessing(resultXML);
			birthdateProcessing(resultXML);
			addressProcessing(resultXML);
			phoneProcessing(resultXML);

			resultXML.Append("</subject>");

            return resultXML.ToString();
        }
		/// <summary>
		/// Load in gender data from icims into HRNX
		/// </summary>
		/// <param name="resultXML">qProcessing subjectProfile XML</param>
		private void genderProcessing(StringBuilder resultXML)
		{
			String genderHRNXVal;
			if ("Male".Equals(gender) || "Female".Equals(gender))
				genderHRNXVal = gender.ToLower();
			else
				genderHRNXVal = "unspecified";
			addValue("gender2", genderHRNXVal, resultXML);
		}
		/// <summary>
		/// Load in birthdate data from icims into HRNX
		/// </summary>
		/// <param name="resultXML">qProcessing subjectProfile XML</param>
		private void birthdateProcessing(StringBuilder resultXML)
		{
			if(birthdate != null)
				addValue("dob", convertToHRNXDate(birthdate), resultXML);
		}

		/// <summary>
		/// Load in address data from icims into HRNX
		/// </summary>
		/// <param name="resultXML">qProcessing subjectProfile XML</param>
		private void addressProcessing(StringBuilder resultXML)
        {
			if(addresses != null)
			{
				for (int i = 0; i < addresses.Count; i++)
				{
					if ("D84002".Equals(addresses[i].addresstype.id))
					{
						// Then, this is the address we want to save as current address
						addValue("address_present_line1", addresses[i].addressstreet1, resultXML);
						addValue("address_present_line2", addresses[i].addressstreet2, resultXML);
						addValue("address_present_city", addresses[i].addresscity, resultXML);
						addValue("address_present_zip", addresses[i].addresszip, resultXML);
						addValue("address_present_county", addresses[i].addresscounty, resultXML);

						if (addresses[i].addresscountry != null)
							addValue("address_present_country", addresses[i].addresscountry.abbrev, resultXML);
						if (addresses[i].addressstate != null)
							addValue("address_present_state", convertToHRNXState(addresses[i].addressstate.id), resultXML);
					}
				}
			}
		}
		public string convertToHRNXDate(String icimsDate)
		{
			if (String.IsNullOrEmpty(icimsDate))
				return "";

			StringBuilder result = new StringBuilder();
			String year = icimsDate.Substring(0, 4);
			int temp;
			Boolean yearIsNumber = Int32.TryParse(year, out temp);

			if (icimsDate.Length >= 10 && yearIsNumber)
			{
				result.Append(icimsDate.Substring(5, 2));
				result.Append("/");
				result.Append(icimsDate.Substring(8, 2));
				result.Append("/");
				result.Append(year);
			}
			else
				result.Append(icimsDate);

			return result.ToString();
		}
		public string convertToHRNXState(String icimsStateID)
		{
			String result = "";
			switch (icimsStateID)
			{
				// US States
				case "D41001001": result = "AL"; break;
				case "D41001002": result = "AK"; break;
				case "D41001003": result = "AZ"; break;
				case "D41001004": result = "AR"; break;
				case "D41001005": result = "AA"; break;
				case "D41001006": result = "AE"; break;
				case "D41001007": result = "AP"; break;
				case "D41001008": result = "CA"; break;
				case "D41001009": result = "CO"; break;
				case "D41001010": result = "CT"; break;
				case "D41001011": result = "DE"; break;
				case "D41001012": result = "FL"; break;
				case "D41001013": result = "GA"; break;
				case "D41001014": result = "HI"; break;
				case "D41001015": result = "ID"; break;
				case "D41001016": result = "IL"; break;
				case "D41001017": result = "IN"; break;
				case "D41001018": result = "IA"; break;
				case "D41001019": result = "KS"; break;
				case "D41001020": result = "KY"; break;
				case "D41001021": result = "LA"; break;
				case "D41001022": result = "ME"; break;
				case "D41001023": result = "MD"; break;
				case "D41001024": result = "MA"; break;
				case "D41001025": result = "MI"; break;
				case "D41001026": result = "MN"; break;
				case "D41001027": result = "MS"; break;
				case "D41001028": result = "MO"; break;
				case "D41001029": result = "MT"; break;
				case "D41001030": result = "NE"; break;
				case "D41001031": result = "NV"; break;
				case "D41001032": result = "NH"; break;
				case "D41001033": result = "NJ"; break;
				case "D41001034": result = "NM"; break;
				case "D41001035": result = "NY"; break;
				case "D41001036": result = "NC"; break;
				case "D41001037": result = "ND"; break;
				case "D41001038": result = "OH"; break;
				case "D41001039": result = "OK"; break;
				case "D41001040": result = "OR"; break;
				case "D41001041": result = "PA"; break;
				case "D41001042": result = "RI"; break;
				case "D41001043": result = "SC"; break;
				case "D41001044": result = "SD"; break;
				case "D41001045": result = "TN"; break;
				case "D41001046": result = "TX"; break;
				case "D41001047": result = "UT"; break;
				case "D41001048": result = "VT"; break;
				case "D41001049": result = "VA"; break;
				case "D41001050": result = "WA"; break;
				case "D41001051": result = "DC"; break;
				case "D41001052": result = "WV"; break;
				case "D41001053": result = "WI"; break;
				case "D41001054": result = "WY"; break;
				
				// Canada State
				case "D41040001": result = "AB"; break;
				case "D41040002": result = "BC"; break;
				case "D41040003": result = "MB"; break;
				case "D41040004": result = "NB"; break;
				case "D41040005": result = "NL"; break;
				case "D41040006": result = "NT"; break;
				case "D41040007": result = "NS"; break;
				case "D41040008": result = "NU"; break;
				case "D41040009": result = "ON"; break;
				case "D41040010": result = "PE"; break;
				case "D41040011": result = "QC"; break;
				case "D41040012": result = "SK"; break;
				case "D41040013": result = "YT"; break;
			}
			return result;
		}

		/// <summary>
		/// Load in Phone data from icims into HRNX
		/// </summary>
		/// <param name="resultXML">qProcessing subjectProfile XML</param>
		private void phoneProcessing(StringBuilder resultXML)
		{
			if(phones !=null )
			{
				for (int i = 0; i < phones.Count; i++)
				{
					if ("D83004".Equals(phones[i].phonetype.id))
						addValue("bestphone2", convertToHRNXPhone(phones[i].phonenumber), resultXML);
					else if ("D83002".Equals(phones[i].phonetype.id))
						addValue("bestphone", convertToHRNXPhone(phones[i].phonenumber), resultXML);
				}
			}
		}

		public string convertToHRNXPhone(String icimsPhoneNumber)
		{
			if(!String.IsNullOrEmpty(icimsPhoneNumber))
			{
				if(icimsPhoneNumber.Length < 12)
				{
					if(icimsPhoneNumber.Length == 10)
						return icimsPhoneNumber.Substring(0, 3) + "-" + icimsPhoneNumber.Substring(3, 3) + "-" + icimsPhoneNumber.Substring(6, 4);
					else if (icimsPhoneNumber.Length == 11 && icimsPhoneNumber.StartsWith("1"))
						return icimsPhoneNumber.Substring(1, 3) + "-" + icimsPhoneNumber.Substring(4, 3) + "-" + icimsPhoneNumber.Substring(7, 4);
				}
			}
			return icimsPhoneNumber;
		}
		public string convertToHRNXBoolean(Workmaywecontact bolVal)
		{
			if (bolVal != null)
			{
				return "Yes".Equals(bolVal.value).ToString().ToLower();
			}

			return "false";
		}
		/// <summary>
		/// Load in MVR data from icims into HRNX
		/// </summary>
		/// <param name="resultXML">qProcessing service XML</param>
		public void MVRProcessing(StringBuilder resultXML)
		{
			if(!String.IsNullOrEmpty(drivinglicensenumber))
			{
				resultXML.Append("\t\t<service name = \"mvr\" type = \"screening\" id = \"0\">");
				resultXML.Append(Environment.NewLine);

				addValue("dlnumber", drivinglicensenumber, resultXML, 3);
				if(drivinglicensestate != null)
					addValue("dlstate", convertToHRNXState(drivinglicensestate.id), resultXML, 3);

				resultXML.Append("\t\t</service>");
				resultXML.Append(Environment.NewLine);
			}
			else
			{
				log.Debug("No MVR data has been detected, so skipping creating the service node.");
			}
		}
		/// <summary>
		/// Load in Employment data from icims into HRNX
		/// </summary>
		/// <param name="resultXML">qProcessing service XML</param>
		public void EmploymentProcessing(StringBuilder resultXML)
		{
			if(workexperience != null)
			{
				for (int i = 0; i < workexperience.Count; i++)
				{
					if (!String.IsNullOrEmpty(workexperience[i].worktitle))
					{
						resultXML.Append("\t\t<service name = \"employment\" type = \"screening\" id = \"");
						resultXML.Append(i);
						resultXML.Append("\">");
						resultXML.Append(Environment.NewLine);

						addValue("companyName", workexperience[i].workemployer, resultXML, 3);
						addValue("company_address1", workexperience[i].workaddress, resultXML, 3);
						addValue("company_city", workexperience[i].workcity, resultXML, 3);
						addValue("company_zip", workexperience[i].workzip, resultXML, 3);
						addValue("startDate", convertToHRNXDate(workexperience[i].workstartdate), resultXML, 3);
						addValue("endDate", convertToHRNXDate(workexperience[i].workenddate), resultXML, 3);
						addValue("companyPhone", convertToHRNXPhone(workexperience[i].workemployerphonenumber), resultXML, 3);
						addValue("position", workexperience[i].worktitle, resultXML, 3);
						addValue("supervisorName", workexperience[i].worksupervisorname, resultXML, 3);
						addValue("supervisorTitle", workexperience[i].worksupervisortitle, resultXML, 3);
						addValue("reasonForLeaving", workexperience[i].workreasonforleaving, resultXML, 3);

						if (workexperience[i].workstate != null)
							addValue("company_state", convertToHRNXState(workexperience[i].workstate.id), resultXML, 3);
						if (workexperience[i].workmaywecontact != null)
							addValue("permissionToContact", "Yes".Equals(workexperience[i].workmaywecontact.value).ToString().ToLower(), resultXML, 3);
						if(workexperience[i].workendingsalary != null)
							addValue("payEnd", workexperience[i].workendingsalary.amountstring, resultXML, 3);
						if (workexperience[i].workcountry != null)
							addValue("company_country", workexperience[i].workcountry.abbrev, resultXML, 3);

						resultXML.Append("\t\t</service>");
						resultXML.Append(Environment.NewLine);
					}
				}
			}
			else
			{
				log.Debug("No Employment data has been detected, so skipping creating the service node.");
			}
		}

		/// <summary>
		/// Load in Education data from icims into HRNX
		/// </summary>
		/// <param name="resultXML">qProcessing service XML</param>
		public void EducationProcessing(StringBuilder resultXML)
		{
			if(education !=null)
			{
				for (int i = 0; i < education.Count; i++)
				{
					if (!String.IsNullOrEmpty(education[i].school.value))
					{
						resultXML.Append("\t\t<service name = \"education\" type = \"screening\" id = \"");
						resultXML.Append(i);
						resultXML.Append("\">");
						resultXML.Append(Environment.NewLine);

						addValue("schoolName", education[i].school.value, resultXML, 3);
						addValue("schoolCity", education[i].educationcity, resultXML, 3);
						addValue("schoolStart_mmddyyyy", convertToHRNXDate(education[i].educationstartdate), resultXML, 3);
						addValue("graduationDate_mmddyyyy", convertToHRNXDate(education[i].graduationdate), resultXML, 3);

						if (education[i].educationcountry != null)
							addValue("schoolCountry", education[i].educationcountry.abbrev, resultXML, 3);
						if (education[i].major != null)
							addValue("major", education[i].major.value, resultXML, 3);
						if (education[i].degree != null)
							addValue("degreeName", education[i].degree.value, resultXML, 3);
						if (education[i].educationstate != null)
							addValue("schoolState", convertToHRNXState(education[i].educationstate.id), resultXML, 3);

						resultXML.Append("\t\t</service>");
						resultXML.Append(Environment.NewLine);
					}
				}
			}
			else
			{
				log.Debug("No Education data has been detected, so skipping creating the service node.");
			}
		}
		public void licenseProcessing(StringBuilder resultXML)
		{
			if(licensecertification != null)
			{
				for(int i=0;i<licensecertification.Count;i++)
				{
					if (!String.IsNullOrEmpty(licensecertification[i].licensenumber))
					{
						resultXML.Append("\t\t<service name = \"license\" type = \"screening\" id = \"");
						resultXML.Append(i);
						resultXML.Append("\">");
						resultXML.Append(Environment.NewLine);

						addValue("licenseNumber", licensecertification[i].licensenumber, resultXML, 3);
						addValue("licenseName", licensecertification[i].licensetype, resultXML, 3);

						if (licensecertification[i].stateissued != null)
							addValue("licenseState", convertToHRNXState(licensecertification[i].stateissued.id), resultXML, 3);

						resultXML.Append("\t\t</service>");
						resultXML.Append(Environment.NewLine);
					}
				}
			}
		}
		public void referenceProcessing(StringBuilder resultXML)
		{
			if (references != null)
			{
				for (int i = 0; i < references.Count; i++)
				{
					if (!String.IsNullOrEmpty(references[i].referencesfirstname))
					{
						resultXML.Append("\t\t<service name = \"reference\" type = \"screening\" id = \"");
						resultXML.Append(i);
						resultXML.Append("\">");
						resultXML.Append(Environment.NewLine);

						addValue("reference_city", references[i].referencescity, resultXML, 3);						
						addValue("reference_fname", references[i].referencesfirstname, resultXML, 3);
						addValue("reference_zip", references[i].referenceszip, resultXML, 3);
						addValue("reference_lname", references[i].referenceslastname, resultXML, 3);
						addValue("reference_county", references[i].referencescounty, resultXML, 3);
						addValue("reference_company", references[i].referencescompany, resultXML, 3);
						addValue("reference_addressLine1", references[i].referencesstreet1, resultXML, 3);
						addValue("reference_addressLine2", references[i].referencesstreet2, resultXML, 3);
						addValue("reference_email", references[i].referencesemail, resultXML, 3);
						addValue("reference_phone", convertToHRNXPhone(references[i].referencesphone), resultXML, 3);

						if (references[i].referencesstate != null)
							addValue("reference_state", convertToHRNXState(references[i].referencesstate.id), resultXML, 3);
						if (references[i].referencescountry != null)
							addValue("reference_country", references[i].referencescountry.abbrev, resultXML, 3);

						resultXML.Append("\t\t</service>");
						resultXML.Append(Environment.NewLine);
					}
				}
			}
		}

		/// <summary>
		/// Add a field to the resultXML
		/// 
		/// This default version will put 1 indent (used by subjectProfile)
		/// </summary>
		/// <param name="fieldKey">Key of the field</param>
		/// <param name="fieldValue">Value of the field</param>
		/// <param name="resultXML">String buffer to write result to</param>
		private void addValue(String fieldKey, String fieldValue, StringBuilder resultXML)
		{
			addValue(fieldKey, fieldValue, resultXML, 1);
		}
		/// <summary>
		/// Add a field to the resultXML
		/// 
		/// </summary>
		/// <param name="fieldKey">Key of the field</param>
		/// <param name="fieldValue">Value of the field</param>
		/// <param name="resultXML">String buffer to write result to</param>
		/// <param name="indent">Amount of indent to use</param>
		private void addValue(String fieldKey, String fieldValue, StringBuilder resultXML, int indent)
        {
			for (int i = 0; i < indent; i++)
				resultXML.Append("\t");
			resultXML.Append("<field name=\"" + fieldKey + "\" ><![CDATA[" + fieldValue+ "]]></field>");
            resultXML.Append(Environment.NewLine);
        }
    }

	#region Address Region
	public class icimsAddress
	{
		public icimsAddressType addresstype { get; set; }
		public string addressstreet1 { get; set; }
		public string addressstreet2 { get; set; }
		public string addresscity { get; set; }
		public icimsState addressstate { get; set; }
		public string addresszip { get; set; }
		public string addresscounty { get; set; }
		public icimsCountry addresscountry { get; set; }
	}

	public class icimsAddressType
	{
		public string id { get; set; }
	}

	public class icimsState
	{
		public string id { get; set; }
	}

	public class icimsCountry
	{
		public string abbrev { get; set; }
	}
	#endregion

	#region Phone Region
	public class icimsPhone
	{
		public icimsPhoneType phonetype { get; set; }
		public string phonenumber { get; set; }
	}

	public class icimsPhoneType
	{
		public string id { get; set; }
	}
	#endregion

	#region Education Region
	public class Education
    {
        public icimsCountry educationcountry { get; set; }
        public Major major { get; set; }
        public School school { get; set; }
        public Degree degree { get; set; }
        public icimsState educationstate { get; set; }
        public string educationcity { get; set; }
		public string educationstartdate { get; set; }
		public string graduationdate { get; set; }
	}
	public class Major
    {
        public string value { get; set; }
    }
    public class School
    {
        public string value { get; set; }
    }

    public class Degree
    {
        public string value { get; set; }
    }
    #endregion

    #region Employement data
    public class Workexperience
    {
		public string workaddress { get; set; }
		public string workcity { get; set; }
		public icimsCountry workcountry { get; set; }
		public string workdescription { get; set; }
		public string workemployer { get; set; }
		public string workemployerphonenumber { get; set; }
		public string workenddate { get; set; }
		public Workmaywecontact workmaywecontact { get; set; }
		public string workreasonforleaving { get; set; }
		public string workstartdate { get; set; }
		public icimsState workstate { get; set; }
		public string worktitle { get; set; }
		public string worksupervisorname { get; set; }
		public string worksupervisortitle { get; set; }
		public string workzip { get; set; }
		public WorkSalary workendingsalary { get; set; }
	}
	public class Workmaywecontact
    {
        public string value { get; set; }
    }
	public class WorkSalary
	{
		public string amountstring { get; set; }
	}
	#endregion

	#region License data
	public class icimsLicense
	{
		public string licensenumber { get; set; }
		public string licensetype { get; set; }
		public icimsState stateissued { get; set; }
	}
	#endregion

	#region Reference data
	public class icimsReference
	{
		public String referencescity { get; set; }
		public icimsState referencesstate { get; set; }
		public String referencesfirstname { get; set; }
		public String referenceszip { get; set; }
		public String referenceslastname { get; set; }
		public String referencescounty { get; set; }
		public String referencescompany { get; set; }
		public String referencesstreet1 { get; set; }
		public String referencesstreet2 { get; set; }
		public String referencesemail { get; set; }
		public icimsCountry referencescountry { get; set; }
		public String referencesphone { get; set; }
	}
	#endregion
}