﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace getClientConfigurations.Models
{
    public class icimsAuthForm
    {
        public List<SearchResult> searchResults { get; set; }
    }
    public class SearchResult
    {
        public string self { get; set; }
        public string id { get; set; }
    }
}