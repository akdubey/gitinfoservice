﻿using Newtonsoft.Json;
using Saxon.Api;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.IO;
using System.Linq;
using System.Resources;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Xsl;

namespace getClientConfigurations.Models
{
    public class HRNXUtil
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger("MultiPhaseCreateOrderMainLogger");
        private static readonly ResourceManager rm = new ResourceManager("getClientConfigurations.Properties.HRNX", typeof(HRNXDBAccessor).Assembly);
		private static readonly int PRINTABLECHARACTERSTART = 32;    // space
		private static readonly int PRINTABLECHARACTEREND = 126;    // ~
		public static readonly String LOG_CONTENT_SEPARATOR = "===============" + Environment.NewLine;

		public static Object sendNullIfEmpty(String val)
        {
            if (String.IsNullOrEmpty(val))
                return DBNull.Value;
            else
                return val;
        }

		/// <summary>
		/// Pretty print an object.
		/// 
		/// Many objects such as a List are always hard to print out nicely unless you overwrite its toString(). 
		/// However, as suggested by https://stackoverflow.com/questions/6309254/a-way-to-pretty-print-a-c-sharp-object,
		/// A simplier way to do this is to convert to JSON then print because we have 1 line JSON converter
		/// </summary>
		/// <param name="target">object to be pretty print</param>
		/// <returns>JSON representation of the object</returns>
		public static String serializeObjToJSON(object target)
		{
			return JsonConvert.SerializeObject(target).ToString();
		}

        public static Tuple<String,String> generateBasicAuthHeader(String username, String password)
        {
            String encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password));
            return new Tuple<string, string>("Authorization", "Basic " + encoded);
        }

        public static DateTime getCurrentTimeInUTC()
        {
            return DateTime.UtcNow;
        }

        public static String getCurrentTimeInUTCInStringForm()
        {
            return DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");
        }

        public static String applyXSLT(String strInput, String strXSLT,HRNXDBAccessor dbAccessor, Order request, String origin)
        {
            // List<StaticError> errList = new List<StaticError>();
                // Create a Processor instance.
                Processor processor = new Processor();

                // Load the source document
                DocumentBuilder builder = processor.NewDocumentBuilder();
                builder.DtdValidation = false;
                builder.BaseUri = new Uri("http://localhost/");
                using (MemoryStream ms = new MemoryStream())
                using (StreamWriter tw = new StreamWriter(ms))
                {
                    tw.Write(strInput);
                    tw.Flush();
                    using (Stream instr = new MemoryStream(ms.GetBuffer(), 0, (int)ms.Length))
                    {
                        XdmNode input = builder.Build(instr);

                    // strXSLT
                    XsltCompiler compiler;
                    compiler = processor.NewXsltCompiler();
                    compiler.BaseUri = new Uri("http://localhost/");
                    try
                    {

                        using (StringReader sr = new StringReader(strXSLT))
                        {
                            XsltExecutable exec = compiler.Compile(sr);

                            //Create a transformer for the stylesheet
                            XsltTransformer transformer = exec.Load();

                            // Set the root node of the source document to be the initial context node
                            transformer.InitialContextNode = input;

                            using (StringWriter strWriter = new StringWriter())
                            {
                                // Create a serializer
                                Serializer serializer = new Serializer();
                                serializer.SetOutputWriter(strWriter);

                                transformer.Run(serializer);

                                return strWriter.ToString();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // log the error to both disk and ErrorStack
                        log.Fatal(ex);

                        List<StaticError> errList = (List<StaticError>)compiler.ErrorList;
                        StringBuilder sb = new StringBuilder();
                        for (int i = 0; i < errList.Count; i++)
                        {
                            log.Debug(errList[i].StackTrace);

                            sb.Append("Error [");
                            sb.Append(i);
                            sb.Append("] = ");
                            if(errList[i].Message.Equals("Error reported by XML parser"))
                                   // Then, we want to grab the specific XML error
                                sb.Append(errList[i].InnerMessage);
                            else
                                sb.Append(errList[i].Message);

                            sb.Append(Environment.NewLine);
                        }
                        String errorDetail = sb.ToString();
                        if (errorDetail.Length > 255)
                            // Then, we will replace with a generic message that user must check themselves
                            errorDetail = errorDetail.Substring(0,255);

                        dbAccessor.pushToErrorStack(3, origin, false, request, rm.GetString("XSLT_TRANSFORM_ERROR"), errorDetail, strInput, strXSLT);

                        // return an empty string
                        return "";
                    }

                }
            }
        }
        /// <summary>
        /// TODO: Copy over logic from HRNX
        /// 
        /// Remove out of range character from inputStr
        /// </summary>
        /// <param name="inputStr">string that needs to remove characters</param>
        /// <param name="minASCIIVal">smallest ASCII character we accept</param>
        /// <param name="maxASCIIVal">highest ASCII character we accept</param>
        /// <param name="exceptionList">exception of ASCII character that we accept outside of the range specified above</param>
        /// <returns></returns>
        private static String removeOutOfRangeCharacter(String inputStr, int minASCIIVal, int maxASCIIVal, int[] exceptionList)
        {
			if (String.IsNullOrEmpty(inputStr))
				// If it is null or empty, then we will just return whatever value was passed in
				return inputStr;

			char[] input = inputStr.ToCharArray();
			StringBuilder result = new StringBuilder();
			int currentCharASCII;

			for (int i=0;i< input.Length;i++)
			{
				currentCharASCII = System.Convert.ToInt32(input[i]);

				//Console.WriteLine(string.Format("currentChar[{0}]", currentCharASCII));

				if((currentCharASCII >= minASCIIVal && currentCharASCII <= maxASCIIVal) || exceptionList.Contains(currentCharASCII))
				{
					// add to result if it is within range OR it is in our exception list
					result.Append(System.Convert.ToChar(currentCharASCII));
				}
			}

			return result.ToString();
       }
		public static String removeNonPrintableCharacterBesideFormatting(String inputStr)
		{
			int[] exceptions = {
				9,   //Tab
				10,	 //Line Feed
				13   //Carriage Return
			};

			return removeOutOfRangeCharacter(inputStr, PRINTABLECHARACTERSTART, PRINTABLECHARACTEREND, exceptions);
		}

		public static void logException(Order result, int severity, String location, String errorReason, String error_blob, Exception e, HRNXDBAccessor dbAccessor)
		{
			// convert an exception to how we would track this without one
			logError(result, severity, location, errorReason, e.Message, error_blob, e.StackTrace, dbAccessor);
		}
		public static void logError(Order result, int severity, String location, String errorReason, String errorLong, String error_blob, String error_blob_2, HRNXDBAccessor dbAccessor)
		{
			// log to errorStack
			dbAccessor.pushToErrorStack(severity, location, false, result, errorReason, errorLong, error_blob, error_blob_2);

			// report an error
			result.status = OrderStatus.rejected_w_errors;
			result.errorReason = errorReason;
		}
		public static String extractDataFromOrder(String xmlPayload, String xpath)
		{
			log.Debug(string.Format("xmlPayload = [{0}]", xmlPayload));
			log.Debug(string.Format("xpath = [{0}]", xpath));

			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(xmlPayload);

			XmlNode target = xmlDoc.SelectSingleNode(xpath);
			if (target != null)
				return target.InnerText;
			else
			{
				log.Debug("target XMLNode is null");
				return "";
			}
		}
		public static XmlNodeList extractXMLPartsFromOrder(String xmlPayload, String xpath)
		{
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(xmlPayload);

			XmlNodeList target = xmlDoc.SelectNodes(xpath);
			return target;
		}
		public static string serializeObjToXML(Object target)
		{
			//Create our own namespaces for the output that is blank
			XmlSerializerNamespaces ns = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });

			XmlSerializer xmlSerializer = new XmlSerializer(target.GetType());

			XmlWriterSettings settings = new XmlWriterSettings();
			settings.Indent = true;
			settings.OmitXmlDeclaration = true;

			using (var stream = new StringWriter())
			using (var writer = XmlWriter.Create(stream, settings))
			{
				xmlSerializer.Serialize(writer, target, ns);
				return stream.ToString();
			}
		}
		public static string extractClientFlagValue(String[] clientFlags, String clientFlagName)
		{
			for(int i=0;i<clientFlags.Length;i++)
			{
				if(clientFlags[i].StartsWith(clientFlagName))
				{
					// then we found the client option we want to deal with
					// 1. look for = in the flag
					int separatorCharIndex = clientFlags[i].IndexOf('=');

					// 2. grab everything behind the separator Index
					return clientFlags[i].Substring(separatorCharIndex + 1);
				}
			}

			// if we got to here, then we didn't find any client flag that works
			return default(String);
		}

		public static string convertODBCToSQL(OdbcCommand command)
		{
			StringBuilder builder = new StringBuilder();

			// Split the command by ?, so we know where to insert the parameter
			string[] splitted = command.CommandText.Split(new[] { '?' }, StringSplitOptions.RemoveEmptyEntries);

			for(int i=0;i< command.Parameters.Count;i++)
			{
				builder.Append(splitted[i]);

				OdbcParameter param = command.Parameters[i];
				if(param.Value is DBNull)
				{
					builder.Append("NULL");
				}
				else
				{
					if(param.Value is DateTime)
					{
						DateTime current = (DateTime)param.Value;
						builder.Append(string.Format("'{0}'", current.ToString("yyyy-MM-dd HH:mm:ss")));
					}
					else
					{
						// If the input is NULL, but it is not a DB null, we want to differenciate beacuse it could be a bug
						if(param.Value == null)
							builder.AppendFormat("''");
						else
						{
							// quote the string
							builder.AppendFormat("'{0}'", SQLescape(param.Value.ToString()));
						}
					}
				}
			}
			if(splitted.Length > command.Parameters.Count)
			{
				// There is a chance we got more string after the last parameter, 
				//so we may need to add it in this way, but it cannot be more than 1
				builder.Append(splitted[splitted.Length-1]);
			}

			return builder.ToString();
		}
		public static string SQLescape(String sqlString)
		{
			//sqlString = sqlString.Replace(@"\", @"\\");
			String retrunSqlString = sqlString.Replace("'", "\\'");

			return retrunSqlString;
		}
		public static string writeSQLStatementToDisk(String sqlToSaved, String failedSQLFolderPath)
		{
			// If I don't have the path to write this to, then I can't write it
			if (failedSQLFolderPath == null)
				return "";

			StringBuilder sb = new StringBuilder();
			sb.Append(DateTime.Now.ToString("yyyyMMdd_HHmmss_t"));
			sb.Append(new Random().Next());
			sb.Append(".log");
			String sqlFilename = sb.ToString();

			log.Debug(string.Format("sqlFilename is [{0}]", sqlFilename));
			string sqlFileFullPath = System.IO.Path.Combine(failedSQLFolderPath, sqlFilename);
			using (StreamWriter sw = File.CreateText(sqlFileFullPath))
			{
				sw.WriteLine(sqlToSaved);
			}

			return sqlFileFullPath;
		}
	}
}