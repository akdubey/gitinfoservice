﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Linq;
using System.Text;
using System.Web;

namespace getClientConfigurations.Models
{
    public class Order
    {
        // q_processing fields
        public string txn_id { get; set; }
        public string requestTimeInUTC { get; set; }
        public OrderStatus status { get; set; }
        public string client_group_id { get; set; }
        public string client_user_id { get; set; }
        public string candidate_id { get; set; }
        public string createOrderHandlerName { get; set; }
        public string orderingIP { get; set; }
        public string version { get; set; }
        public string payload_hrnx { get; set; }

        // additional data
        public Dictionary<OrderAdditionalDataKeys, String> additionalData { get; set; }

        /// <summary>
        /// Indicate whether the current order is in the ATS side or the service provider side
        /// </summary>
        private Boolean flowInATS;
        
        // Order results
        public string vendorOrderID { get; set; }
        public string vendorURL { get; set; }
        public string errorReason { get; set; }

        public Order()
        {
            additionalData = new Dictionary<OrderAdditionalDataKeys, string>();
        }

        public static String getFieldStrForm()
        {
            return "txn_id, req_timedatestamp, hrnx_status, client_group_id, user_id, tp_uqid, source, ip_address, version, payload_hrnx";
        }

        public static Order deserialize(String[] queryResult)
        {
            Order returnVal = new Order();
            returnVal.txn_id = queryResult[0];
            returnVal.requestTimeInUTC = queryResult[1];
            returnVal.status =  (OrderStatus)Enum.Parse(typeof(OrderStatus),queryResult[2]);
            returnVal.client_group_id = queryResult[3];
            returnVal.client_user_id = queryResult[4];
            returnVal.candidate_id = queryResult[5];
            returnVal.createOrderHandlerName = queryResult[6];
            returnVal.orderingIP = queryResult[7];
            returnVal.version = queryResult[8];
            returnVal.payload_hrnx = queryResult[9];

            return returnVal;
        }

        public void atsWorkflowStarts()
        {
            flowInATS = true;
        }

        public void serviceProviderWorkflowStarts()
        {
            flowInATS = false;
        }
        public string getCurrentServiceProviderID()
        {
            try
            {
                if (flowInATS)
                    return additionalData[OrderAdditionalDataKeys.atsID];
                else
                    return additionalData[OrderAdditionalDataKeys.serviceProviderID];
            }catch(System.Collections.Generic.KeyNotFoundException)
            {
                // If the one we want to return is not set then we want to return empty string
                return String.Empty;
            }
        }

		public String getVendorOrderXML()
		{
			VendorOrderXML response = new VendorOrderXML();

			response.VendorOrderId = vendorOrderID;
			response.VendorOrderStatus = status;
			response.VendorOrderURL = vendorURL;

			return HRNXUtil.serializeObjToXML(response);
		}
    }

    public enum OrderStatus
    {
        passthru = 2,
        submitted_passthru = 21,
        rejected_w_errors = 35,
        processing = 40,
		completed = 50
    }

    public enum OrderAdditionalDataKeys
    {
        atsID,
        serviceProviderID,
        package,
        isNewOrder,
		packageName
    }

}