﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace getClientConfigurations.Models
{
    public class VendorOrderIDLookupEntry
	{
        public String service_tp_id { get; set; }
		public String vendor_order_id { get; set; }
		public String txn_id { get; set; }
		public String req_datetimestamp { get; set; }
		public String status { get; set; }

		private VendorOrderIDLookupEntry() { }

		public static String getFieldStrForm()
		{
			return "service_tp_id, vendor_order_id, txn_id, req_datetimestamp, status";
		}

		public static VendorOrderIDLookupEntry deserialize(String[] data)
		{
			VendorOrderIDLookupEntry current = new VendorOrderIDLookupEntry();
			current.service_tp_id = data[0];
			current.vendor_order_id = data[1];
			current.txn_id = data[2];
			current.req_datetimestamp = data[3];
			current.status = data[4];

			return current;
		}
	}
}