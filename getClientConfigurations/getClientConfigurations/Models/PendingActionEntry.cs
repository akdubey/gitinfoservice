﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace getClientConfigurations.Models
{
    public class PendingActionEntry
	{
		public String txn_id { get; set; }
		public PendingActionAction pending_action { get; set; }
		public String client_group_id { get; set; }
		public String service_tp_id { get; set; }
		public PendingActionStatus status { get; set; }
		public String reference_id { get; set; }
		public String reference_data { get; set; }

		private PendingActionEntry() { }

		public static String getFieldStrForm()
		{
			return "txn_id , pending_action , client_group_id , service_tp_id , status , reference_id , reference_data ";
		}

		public static PendingActionEntry deserialize(String[] data)
		{
			PendingActionEntry current = new PendingActionEntry();
			current.txn_id = data[0];
			current.pending_action = (PendingActionAction)Enum.Parse(typeof(PendingActionAction), data[1]);
			current.client_group_id = data[2];
			current.service_tp_id = data[3];
			current.status = (PendingActionStatus)Enum.Parse(typeof(PendingActionStatus), data[4]);
			current.reference_id = data[5];
			current.reference_data = data[6];

			return current;
		}
	}

	public enum PendingActionAction
	{
		uploadDocument,
		uploadlicense
	}

	public enum PendingActionStatus
	{
		pending,
		done
	}
}