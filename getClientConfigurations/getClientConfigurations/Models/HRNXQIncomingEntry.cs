﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Linq;
using System.Web;

namespace getClientConfigurations.Models
{
    public class HRNXQIncomingEntry
    {
        // q_incoming fields
        public string txn_id { get; set; }
        public string service_tp_id { get; set; }
        public string receiptTimedatestampInUTC { get; set; }
        public string ip_address { get; set; }
        public OrderStatus status { get; set; }
        public string version { get; set; }
        public string payload_original { get; set; }
        public string payload_hrnx { get; set; }

        public static String getFieldStrForm()
        {
            return "txn_id, tp_id, receipt_timedatestamp, hrnx_status, version, ip_address, payload_original, payload_hrnx";
        }

        public static HRNXQIncomingEntry deserialize(String[] queryResult)
        {
            HRNXQIncomingEntry returnVal = new HRNXQIncomingEntry();
            returnVal.txn_id = queryResult[0];
            returnVal.service_tp_id = queryResult[1];
            returnVal.receiptTimedatestampInUTC = queryResult[2];
            returnVal.status = (OrderStatus)Enum.Parse(typeof(OrderStatus), queryResult[3]);
            returnVal.version = queryResult[4];
            returnVal.ip_address = queryResult[5];
            returnVal.payload_original = queryResult[6];
            returnVal.payload_hrnx = queryResult[7];

            return returnVal;
        }
    }
}