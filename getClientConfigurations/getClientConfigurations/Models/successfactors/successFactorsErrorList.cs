﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace getClientConfigurations.Models
{
  
    public class successFactorsErrorList
    {
        public List<successFactorsError> errors { get; set; }
    }
    public class successFactorsError
    {
        public String errorCode { get; set; }
        public String errorMessage { get; set; }
    }


}