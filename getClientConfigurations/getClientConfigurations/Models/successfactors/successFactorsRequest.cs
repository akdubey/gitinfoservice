﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace getClientConfigurations.Models
{
    public class successFactorsRequest
    {
        public string customerId { get; set; }
        public string eventType { get; set; }
        public string systemId { get; set; }
        public string personId { get; set; }
        public string JobApplicationID { get; set; }
        public string userId { get; set; }
        public List<successFactorLinkes> links { get; set; }
        public string newStatus { get; set; }
		
		private string profileURL = "";
        private string workflowURL = "";
        private string jobURL = "";

        public void initialize()
        {
            List<successFactorLinkes> linksFromRequest = links;
            for (int i = 0; i < linksFromRequest.Count; i++)
            {
                switch (linksFromRequest[i].rel.ToLower())
                {
                    case "person":
                        profileURL = linksFromRequest[i].url; break;
                    case "applicantworkflow":
                        workflowURL = linksFromRequest[i].url; break;
                    case "job":
                        jobURL = linksFromRequest[i].url; break;
                }
            }
        }

        public string getProfileURL()
        {
            if (String.IsNullOrEmpty(profileURL))
                initialize();
            return profileURL;
        }
        public string getWorkflowURL()
        {
            if (String.IsNullOrEmpty(workflowURL))
                initialize();
            return workflowURL;
        }
        public string getJobURL()
        {
            if (String.IsNullOrEmpty(jobURL))
                initialize();
            return jobURL;
        }

    }
    public class successFactorLinkes
    {
        public string rel { get; set; }
        public string title { get; set; }
        public string url { get; set; }
    }

}