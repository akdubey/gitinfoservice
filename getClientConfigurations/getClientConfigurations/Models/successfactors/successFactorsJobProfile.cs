﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace getClientConfigurations.Models
{
    public class successFactorsJobProfile
    {
        public List<successFactorsPackageEntry> bgcpackagetype { get; set; }
    }
    public class successFactorsPackageEntry
    {
        public string id { get; set; }
        public string formattedvalue { get; set; }
        public string value { get; set; }
    }
}