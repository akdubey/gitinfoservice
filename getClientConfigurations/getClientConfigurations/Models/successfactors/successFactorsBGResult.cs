﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace getClientConfigurations.Models
{
    public class successFactorsBGResult
    {
        public DateTime bgccompleteddate { get; set; }
		public successFactorsOrderStatus bgcorderstatus { get; set; }
	}

    public class successFactorsOrderStatus
	{
		public string value { get; set; }
	}

    public class successFactorsCompleteOrderStatus : successFactorsOrderStatus
	{
        public successFactorsCompleteOrderStatus()
		{
			this.value = "Complete";
		}
	}
}