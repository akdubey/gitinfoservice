﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace getClientConfigurations.Models
{
    public class successFactorsSubjectProfile
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger("successFactorsSubjectProfile");

        public string ClientID { get; set; }
        public int JobApplicationID { get; set; }
        public string BGAccount { get; set; }
        public string BGPackage { get; set; }
        public BackgroundCheckRequest backgroundCheckRequest { get; set; }
        public CandidateId candidateId { get; set; }
        public int CandidateURI { get; set; }
        public string GivenName { get; set; }
        public string FamilyName { get; set; }
        public string FormattedName { get; set; }
        public string DateOfBirth { get; set; }
        public string Gender { get; set; }
        public string DisabilityIndicator { get; set; }
        public string DisabilitySummary { get; set; }
        public Address addresses { get; set; }
        public Phone Phone { get; set; }
        public Email Email { get; set; }
        public Web Web { get; set; }
        public PersonCompetency PersonCompetency { get; set; }
        public WorkEligibility WorkEligibility { get; set; }
        public List<EducationOrganizationAttendance> EducationOrganizationAttendance { get; set; }
        public List<PositionHistory> PositionHistory { get; set; }
        public List<Certification> Certification { get; set; }
        public License License { get; set; }
        public EmploymentReferences EmploymentReferences { get; set; }
        public SpecialCommitment SpecialCommitment { get; set; }
        public Attachment Attachment { get; set; }
        public string UserArea { get; set; }
        public JobApplicationId jobApplicationId { get; set; }

        public String convertToHRNXSubjectProfile()
        {
            StringBuilder resultXML = new StringBuilder("<subject>");
            resultXML.Append(Environment.NewLine);

            addValue("firstname", GivenName, resultXML);
            addValue("middlename", FamilyName, resultXML);
            addValue("email", Email.Address, resultXML);

            genderProcessing(resultXML);
            birthdateProcessing(resultXML);
            addressProcessing(resultXML);
            phoneProcessing(resultXML);

            resultXML.Append("</subject>");
            return resultXML.ToString();
        }

        private void genderProcessing(StringBuilder resultXML)
        {
            String genderHRNXVal;
            if ("male".Equals(Gender) || "female".Equals(Gender))
                genderHRNXVal = Gender.ToLower();
            else
                genderHRNXVal = "unspecified";
            addValue("gender2", genderHRNXVal, resultXML);
        }

        private void birthdateProcessing(StringBuilder resultXML)
        {
            if (DateOfBirth != null)
                addValue("dob", convertToHRNXDate(DateOfBirth), resultXML);
        }

        private void addressProcessing(StringBuilder resultXML)
        {
            if (addresses != null)
            {
                addValue("address_present_line1", addresses.AddressLine, resultXML);
                addValue("address_present_city", addresses.CityName, resultXML);
                addValue("address_present_zip", addresses.CountryCode, resultXML);
                addValue("address_present_country", addresses.PostalCode, resultXML);
            }
        }

        public string convertToHRNXDate(String successFactorsDate)
        {
            if (String.IsNullOrEmpty(successFactorsDate))
                if (String.IsNullOrEmpty(successFactorsDate))
                    return "";
            StringBuilder result = new StringBuilder();
            String year = successFactorsDate.Substring(0, 4);
            int temp;
            Boolean yearIsNumber = Int32.TryParse(year, out temp);

            if (successFactorsDate.Length >= 10 && yearIsNumber)
            {
                result.Append(successFactorsDate.Substring(5, 2));
                result.Append("/");
                result.Append(successFactorsDate.Substring(8, 2));
                result.Append("/");
                result.Append(year);
            }
            else
                result.Append(successFactorsDate);

            return result.ToString();
        }

        private void phoneProcessing(StringBuilder resultXML)
        {
            if (Phone != null)
            {
                if (!String.IsNullOrEmpty(Phone.Number))
                    addValue("bestphone", convertToHRNXPhone(Phone.Number), resultXML);
            }
        }

        private string convertToHRNXPhone(string phonenumber)
        {
            if (!String.IsNullOrEmpty(phonenumber))
            {
                if (phonenumber.Length < 12)
                {
                    if (phonenumber.Length == 10)
                        return phonenumber.Substring(0, 3) + "-" + phonenumber.Substring(3, 3) + "-" + phonenumber.Substring(6, 4);
                    else if (phonenumber.Length == 11 && phonenumber.StartsWith("1"))
                        return phonenumber.Substring(1, 3) + "-" + phonenumber.Substring(4, 3) + "-" + phonenumber.Substring(7, 4);

                }
            }
            return phonenumber;
        }

        //public string convertToHRNXBoolean(WorkEmployerContactName bolVal)
        //{
        //    if(bolVal != null)
        //    {
        //        return "Yes".Equals(bolVal.value).ToString().ToLower();
        //    }

        //    return "false";
        //}

        public void EmploymentProcessing(StringBuilder resultXML)
        {
            if (PositionHistory != null)
            {
                for (int i = 0; i < PositionHistory.Count; i++)
                {
                    if (!String.IsNullOrEmpty(PositionHistory[i].Employer))
                    {
                        resultXML.Append("\t\t<service name = \"employment\" type = \"screening\" id = \"");
                        resultXML.Append(i);
                        resultXML.Append("\">");
                        resultXML.Append(Environment.NewLine);

                        addValue("companyName", PositionHistory[i].OrganizationUnitName, resultXML, 3);
                        addValue("employerName", PositionHistory[i].Employer, resultXML, 3);
                        addValue("startDate", convertToHRNXDate(PositionHistory[i].StartDate), resultXML, 3);
                        addValue("endDate", convertToHRNXDate(PositionHistory[i].EndDate), resultXML, 3);
                        addValue("position", PositionHistory[i].PositionTitle, resultXML, 3);
                      
                        if (PositionHistory[i].ReferenceLocation != null)
                        {
                            addValue("company_address1", PositionHistory[i].ReferenceLocation.Address, resultXML, 3);
                            addValue("city", PositionHistory[i].ReferenceLocation.CityName, resultXML, 3);
                            addValue("state", PositionHistory[i].ReferenceLocation.State, resultXML, 3);
                            addValue("zip_code", PositionHistory[i].ReferenceLocation.ZipCode, resultXML, 3);
                            addValue("countrycode", PositionHistory[i].ReferenceLocation.CountryCode, resultXML, 3);
                            addValue("companyContact", PositionHistory[i].ReferenceLocation.ContactName, resultXML, 3);
                            addValue("company_reference_email", PositionHistory[i].ReferenceLocation.ContactEmail, resultXML, 3);
                            addValue("company_reference_phone", convertToHRNXPhone(PositionHistory[i].ReferenceLocation.ContactPhone), resultXML, 3);
                        }
                        resultXML.Append("\t\t</service>");
                        resultXML.Append(Environment.NewLine);
                    }
                }
            }
            else
            {
                log.Debug("No Employment data has been detected, so skipping creating the service node.");
            }
        }

        public void EducationProcessing(StringBuilder resultXML)
        {
            if (EducationOrganizationAttendance != null)
            {
                for (int i = 0; i < EducationOrganizationAttendance.Count; i++)
                {
                    if (!String.IsNullOrEmpty(EducationOrganizationAttendance[i].School))
                    {
                        resultXML.Append("\t\t<service name = \"education\" type = \"screening\" id = \"");
                        resultXML.Append(i);
                        resultXML.Append("\">");
                        resultXML.Append(Environment.NewLine);

                        addValue("schoolName", EducationOrganizationAttendance[i].School, resultXML, 3);
                        addValue("degreeType", EducationOrganizationAttendance[i].DegreeType.Name, resultXML, 3);
                        addValue("educationScore", EducationOrganizationAttendance[i].EducationScore, resultXML, 3);
                        addValue("schoolStart", convertToHRNXDate(EducationOrganizationAttendance[i].AttendanceStartDate.DayOfYear.ToString()), resultXML, 3);
                        addValue("schoolEnd", convertToHRNXDate(EducationOrganizationAttendance[i].AttendanceEndDate.DayOfYear.ToString()), resultXML, 3);
                        addValue("graduationDate_mmddyyyy", convertToHRNXDate(EducationOrganizationAttendance[i].DegreeDate.DayOfYear.ToString()), resultXML, 3);

                        if (EducationOrganizationAttendance[i].ReferenceLocation != null)
                        {
                            addValue("schoolCity", EducationOrganizationAttendance[i].ReferenceLocation.CityName, resultXML, 3);
                            addValue("address", EducationOrganizationAttendance[i].ReferenceLocation.Address, resultXML, 3);
                            addValue("schoolState", EducationOrganizationAttendance[i].ReferenceLocation.State, resultXML, 3);
                            addValue("zipCode", EducationOrganizationAttendance[i].ReferenceLocation.ZipCode, resultXML, 3);
                            addValue("countryCode", EducationOrganizationAttendance[i].ReferenceLocation.CountryCode, resultXML, 3);
                            addValue("phone", convertToHRNXPhone(EducationOrganizationAttendance[i].ReferenceLocation.Phone), resultXML, 3);
                        }
                        if (EducationOrganizationAttendance[i].DegreeType.Code != null)
                            addValue("degreeType", EducationOrganizationAttendance[i].DegreeType.Name, resultXML, 3);
                        if (EducationOrganizationAttendance[i].MajorProgramName != null)
                            addValue("major", EducationOrganizationAttendance[i].MajorProgramName, resultXML, 3);
                        if (EducationOrganizationAttendance[i].MinorProgramName != null)
                            addValue("minor", EducationOrganizationAttendance[i].MinorProgramName, resultXML, 3);
                        if (EducationOrganizationAttendance[i].Comment != null)
                            addValue("comment", EducationOrganizationAttendance[i].Comment, resultXML, 3);

                        resultXML.Append("\t\t</service>");
                        resultXML.Append(Environment.NewLine);
                    }
                }
            }
            else
            {
                log.Debug("No Education data has been detected, so skipping creating the service node.");
            }
        }

        public void licenseProcessing(StringBuilder resultXML)
        {
            if (License != null)
            {
                if (!String.IsNullOrEmpty(License.LicenseName))
                {
                    resultXML.Append("\t\t<service name = \"license\" type = \"screening\" id = \"");
                    resultXML.Append("\">");
                    resultXML.Append(Environment.NewLine);

                    addValue("license_name", License.LicenseName, resultXML, 3);
                    addValue("startDate", convertToHRNXDate(License.FirstIssuedDate), resultXML, 3);
                    addValue("endDate", convertToHRNXDate(License.EndDate), resultXML, 3);

                    if (License.IssuingAuthorityName != null)
                        addValue("issuingAuthorityName", License.IssuingAuthorityName, resultXML, 3);

                    resultXML.Append("\t\t</service>");
                    resultXML.Append(Environment.NewLine);
                }
            }
        }

        public void referenceProcessing(StringBuilder resultXML)
        {
            if (EmploymentReferences != null)
            {
                if (!String.IsNullOrEmpty(EmploymentReferences.OrganizationName))
                {
                    resultXML.Append("\t\t<service name = \"reference\" type = \"screening\" id = \"");
                    resultXML.Append("\">");
                    resultXML.Append(Environment.NewLine);

                    addValue("reference_OrganizationName", EmploymentReferences.OrganizationName, resultXML, 3);
                    addValue("reference_positionTitle", EmploymentReferences.PositionTitle, resultXML, 3);
                    addValue("reference_name", EmploymentReferences.FormattedName, resultXML, 3);
                    addValue("reference_phone", convertToHRNXPhone(EmploymentReferences.PreferredPhone), resultXML, 3);
                    addValue("reference_email", EmploymentReferences.PreferredEmail, resultXML, 3);

                    resultXML.Append("\t\t</service>");
                    resultXML.Append(Environment.NewLine);
                }
            }
        }

        private void addValue(String fieldKey, String fieldValue, StringBuilder resultXML)
        {
            addValue(fieldKey, fieldValue, resultXML, 1);
        }
        private void addValue(String fieldKey, String fieldValue, StringBuilder resultXML, int indent)
        {
            for (int i = 0; i < indent; i++)
                resultXML.Append("\t");
            resultXML.Append("<field name=\"" + fieldKey + "\" ><![CDATA[" + fieldValue + "]]></field>");
            resultXML.Append(Environment.NewLine);
        }
    }

}
    public class BackgroundCheckRequest
    {
        public int Id { get; set; }
        public string OrderStatus { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string VendorCode { get; set; }
        public string VendorOrderNumber { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseDetail { get; set; }
    }

    public class CandidateId
    {
        public string primaryEmail { get; set; }
    }

    public class Address
    {
        public string CountryCode { get; set; }
        public string CountrySubDivisionCode { get; set; }
        public string CityName { get; set; }
        public string PostalCode { get; set; }
        public string AddressLine { get; set; }
        public string Label { get; set; }
    }

    public class Phone
    {
        public string Number { get; set; }
        public string Label { get; set; }
        public string Preferred { get; set; }
    }

    public class Email
    {
        public string Address { get; set; }
        public string Label { get; set; }
        public string Preferred { get; set; }
    }

    public class Web
    {
        public string Address { get; set; }
        public string Label { get; set; }
        public string LabelDescription { get; set; }
    }

    public class PersonCompetency
    {
        public string CompetencyID { get; set; }
        public string CompetencyName { get; set; }
        public string CompetencyLevel { get; set; }
    }

    public class WorkEligibility
    {
        public string CountryCode { get; set; }
        public string Permanent { get; set; }
    }

    public class ReferenceLocation
    {
        public string Address { get; set; }
        public string CityName { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string CountryCode { get; set; }
        public string Phone { get; set; }
        public string Type { get; set; }
        public string CountrySubDivisionCode { get; set; }
    }

    public class EducationLevel
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }

    public class DegreeType
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }

    public class EducationOrganizationAttendance
    {
        public string School { get; set; }
        public string SubSchool { get; set; }
        public ReferenceLocation ReferenceLocation { get; set; }
        public EducationLevel EducationLevel { get; set; }
        public string AttendanceStatusCode { get; set; }
        public DateTime AttendanceStartDate { get; set; }
        public DateTime AttendanceEndDate { get; set; }
        public string EducationScore { get; set; }
        public DegreeType DegreeType { get; set; }
        public DateTime DegreeDate { get; set; }
        public string MajorProgramName { get; set; }
        public string MinorProgramName { get; set; }
        public string AcademicHonors { get; set; }
        public string Comment { get; set; }
        public string UserArea { get; set; }
    }

    public class ReferenceLocation2
    {
        public string Address { get; set; }
        public string CityName { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string CountryCode { get; set; }
        public string CountrySubDivisionCode { get; set; }
        public string ContactName { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }
    }

    public class Industry
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }

    public class JobCategory
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }

    public class JobLevel
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }

    public class PositionHistory
    {
        public string Employer { get; set; }
        public string OrganizationUnitName { get; set; }
        public string PositionTitle { get; set; }
        public ReferenceLocation2 ReferenceLocation { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string CurrentIndicator { get; set; }
        public Industry Industry { get; set; }
        public JobCategory JobCategory { get; set; }
        public JobLevel JobLevel { get; set; }
        public string Description { get; set; }
        public string UserArea { get; set; }
    }

    public class Certification
    {
        public string CertificationTypeCode { get; set; }
        public string CertificationName { get; set; }
        public string Description { get; set; }
        public string IssuingAuthorityName { get; set; }
        public DateTime FirstIssuedDate { get; set; }
        public DateTime EndDate { get; set; }
        public string CertificationNumber { get; set; }
    }

    public class License
    {
        public string LicenseTypeCode { get; set; }
        public string LicenseName { get; set; }
        public string Description { get; set; }
        public string IssuingAuthorityName { get; set; }
        public string FirstIssuedDate { get; set; }
        public string EndDate { get; set; }
    }

    public class EmploymentReferences
    {
        public string RefereeTypeCode { get; set; }
        public string FormattedName { get; set; }
        public string PositionTitle { get; set; }
        public string OrganizationName { get; set; }
        public string PreferredPhone { get; set; }
        public string PreferredEmail { get; set; }
        public string YearsKnownNumber { get; set; }
        public string RelationshipTypeCode { get; set; }
        public string Comment { get; set; }
    }

    public class SpecialCommitment
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }

    public class AccessCredentials
    {
        public string UserID { get; set; }
        public string TypeCode { get; set; }
        public string Value { get; set; }
    }

    public class Attachment
    {
        public string EmbeddedData { get; set; }
        public string EmbeddedText { get; set; }
        public string URI { get; set; }
        public string FileName { get; set; }
        public string Description { get; set; }
        public string FileType { get; set; }
        public string DocumentTitle { get; set; }
        public AccessCredentials AccessCredentials { get; set; }
        public string UserArea { get; set; }
    }

    public class JobApplicationId
    {
        public string applicantFirstName { get; set; }
        public string applicantLastName { get; set; }
        public string applicantDateOfBirth { get; set; }
    }